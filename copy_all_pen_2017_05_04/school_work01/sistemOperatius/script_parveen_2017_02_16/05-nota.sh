#! /bin/bash
# nom parveen
# curs hisix
# gener 2017
# descripcion:- valida rep 2 arguments
# synopsis:- $ prog.sh arg1
#------------------------------------------------------------
ERROR_N_ARG=1
ERROR_ARG_VAL=2
OK=0
# 0) validar argument
if [ $# -ne 1 ]
then
	echo "error: wrong no. of argument"
	echo "usage: prog arg"
	exit $ERROE_N_ARG
fi
if [ "$1" -lt 0 -o "$1" -gt 10 ]
then
	echo "error: valor de argument no valida"
	echo "usage: valor de argument [0-10]"
	exit $ERROR_ARG_VAL
fi
# 1) main program
if [ "$1" -lt 5 ]
then 
	msg="suspes"
elif [ "$1" -lt 7 ]
then
	msg="aprovat"
else
	msg="notable"
fi
echo $msg
exit $OK
