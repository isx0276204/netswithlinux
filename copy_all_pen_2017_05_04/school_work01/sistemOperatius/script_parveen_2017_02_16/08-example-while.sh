#! /bin/bash
# nom parveen
# curs hisix
# gener 2017
# descripcion:- exampla bucle while
# synopsis:- $ prog.sh [arg...]
#-------------------------------------------------------------
#) 03 exampla
num=1
while [ $# -ne 0 ]
do 
	echo "$num:$1"
	num=$((num+1))
	shift
done
exit 0

# 02) amb exampla while
num=1
MAX=$#
while [ $num -le $MAX ]
do
	echo $0,$1,$#,$*
	num=$((num+1))
	shift
done
exit 0

#-----------------------------------------------------------
while [ $count -le 10 ]
do
	echo "$count"
	count=$((count+1))
done
exit 0
