#! /bin/bash
# @edt Adri isx48102233
# gener 2017
# Descripcio: Nota d'un alumne
# synopsis: $ prog.sh arg
# ---------------------------------
# 0)
OK=0
ERR_NARGS=1
ERR_ARGVALUE=2
#1) Validar Que n'hi ha una quantitat valida d'argv
if [ $# -ne 1 ]
then
  echo "Error, numero d'arguments no valid"
  echo "Usage: prog.sh arg"
  exit $ERR_NARGS
fi
#2) Validar que l'argument te un valor valid
if [ $1 -lt 0 -o $1 -gt 10 ]
then
  echo "Error, valor de l'argument no valid"
  echo "Usage: Valor de la nota [0-10]"
  exit $ERR_ARGVALUE
fi
#3) Calcular valoracio
if [ $1 -lt 5 ]
then
  msg="Suspes"
elif [ $1 -lt 7 ]
then
  msg="Aprovat"
else
  msg="Notable"
fi
echo "L'alumne amb nota $1 esta $msg"
exit $OK
