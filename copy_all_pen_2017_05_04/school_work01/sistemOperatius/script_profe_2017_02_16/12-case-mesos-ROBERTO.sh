#! /bin/bash
# @edt roberto isx47262285
# gener 2017
# descripcion: que dice los dias que tiene el mes
# synopsis: $ prog.sh arg
#------------------------------------------------------------
ERR=1
OK=0

#validar si existe un solo argument
if [ $# -ne 1 ]
then
 echo "error: numero de arguments incorrecte"
 echo "usage: prog.sh arg"
 exit $ERR
fi

# si el argument es el help mostramos el help y plegamos
if [ "$1" = "-h" ] 
then
  echo "help: roberto@edt isx47262285"
  echo "description: program says days of the month"
  echo "usage: prog.sh arg"
  echo "arg: is a number of month"
  exit $OK
fi

#si el argumento es diferente de entre 1 - 12 
if [ $1 -lt 1 -o $1 -gt 12 ] 
then
  echo "error: argument no valid"
  echo "usage: arg 1-12"
  exit $ERR
fi

# añadimos una lista de meses
case $1 in 
"1"|"3"|"5"|"7"|"8"|"10"|"12")
    echo "$1 tiene 31 dias";;
"2")
    echo "$1 tiene 28 dias";;
*)
    echo "$1 tiene 30 dias"
esac

exit $OK

















