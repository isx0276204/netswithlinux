#! /bin/bash
# @edt M01
# gener 2017
# Descripcio: exemple ordre case
# ---------------------------------
llista="pere marta anna jordi pere annaramon mireia ailaramon mireia ailass"
nens=0
nenes=0
altres=0
for nom in $llista
do
  case $nom in
  "pere"|"jordi"|"pau") 
     nens=$((nens+1))
     echo "$nom es un nen";;
  "marta"|"anna"|"julia")
     nenes=$((nenes+1))
     echo "$nom es una nena";;
  *)
     altres=$((altres+1))
     echo "$nom unknown";;
  esac
done
echo "nens: $nens, nenes: $nenes, altres: $altres"
exit 0
