#! /bin/bash
# @edt M01
# gener 2017
# Descripcio: prog [...]
#     indicar: #arguments, nom programa, 
#     llista arguments, últim argument
# -----------------------------------------

# a) usant bucle for
ultim=""
for arg in $*
do
  echo $arg
done
ultim=$arg
echo "$#, $0, $*, $ultim"
exit 0



# c) while amb $1 
while [ -n "$1" ]
do
  echo "$1, $#, $*"
  shift
done
exit 0

#b)  iterar while amb comptador
numargs=$#
while [ $numargs -gt 0 ]
do
  echo "$1, $numargs, $#, $*"
  shift
  numargs=$((numargs-1))
done
exit 0

# a) usant bucle for
for arg in $*
do
  echo $arg
done
exit 0

