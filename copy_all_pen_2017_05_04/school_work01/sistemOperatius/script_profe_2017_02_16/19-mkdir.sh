#! /bin/bash
# @edt M01-ISO
# febrer 2017
# Descripció: crear directoris
# Synopsis: prog.sh noudir[...]
# ---------------------------------------
ERR_ARGS=1
errors=0
status=0
if [ $# -eq 0 ]
then
  echo "error numero arguments incorrecte"
  echo "usage: prog noudir[...]"
  exit $ERR_ARGS
fi
llistaDir=$*
for nou in $llistaDir
do
  mkdir $nou &> /dev/null
  if [ $? -ne 0 ]
  then
    echo "error en crear: $nou" >> /dev/stderr
    errors=$((errors+1))
  fi
done
echo "errors: $errors"
if [ $errors -ne 0 ]
then
  status=2
fi
exit $status




