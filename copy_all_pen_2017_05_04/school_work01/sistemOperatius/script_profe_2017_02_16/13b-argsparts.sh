#! /bin/bash
# @edt M01
# gener 2017
# Descripcio: prog [...]
#     indicar: #arguments, nom programa, 
#     llista arguments, últim argument
# -----------------------------------------

#b)  iterar while amb comptador
ultim=""
preList=""
nargs=$#
fullList=$*
while [ $# -gt 1 ]
do
  preList="$preList $1"
  shift
done
ultim=$1
echo "$#, $0, $fullList, $ultim, $preList"
exit 0


# a) usant bucle for
ultim=""
preList=""
anterior=""
for arg in $*
do
  preList="$preList $anterior"
  anterior=$arg
done
ultim=$arg
echo "$#, $0, $*, $ultim, $preList"
exit 0


# c) while amb $2 
ultim=""
preList=""
nargs=$#
fullList=$*
while [ -n "$2" ]
do
  preList="$preList $1"
  shift
done
ultim=$1
echo "$#, $0, $fullList, $ultim, $preList"
exit 0

