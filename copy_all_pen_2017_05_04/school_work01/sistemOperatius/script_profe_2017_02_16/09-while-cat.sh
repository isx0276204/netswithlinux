#! /bin/bash
# @edt M01
# gener 2017
# Descripcio: while que fa echo
# ---------------------------------

# prgrama passa a majuscules
while read -r line
do
  echo $line  | tr '[a-z]' '[A-Z]' 
done
exit 0

# numerar per stdout l'entrada stdin
nlin=1
while read -r line
do
  echo "$nlin: $line"
  nlin=$((nlin+1))
done
exit 0
