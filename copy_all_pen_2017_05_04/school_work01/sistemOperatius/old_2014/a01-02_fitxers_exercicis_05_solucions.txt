ASIX MP1_ISO
Curs 2010-211
A01-02 Gestió de fitxers i directoris

Exercici_5
==========
-------------------------------------------------------------------------------
part1:
-------------------------------------------------------------------------------
00) situar-se al directori actiu /tmp i realizat TOTES les ordres des d'aquest directori.
	$ cd /tmp

01) Crear dins de tmp un directori anomenat mp1/fonts.
	$ mkdir -o mp1/fonts

03) Posar dins de fonts els següents fitxers:
    informe.txt carta01.txt carta02.txt carta3.txt carta4.txt carta05.txt carta06.txt
    .oculta .ocultba .ocultbc .ocultbe
    ls (és el fitxer binari de la ordre ls)
    pwd (és el fitxer binari de la ordre pwd)
    manls (és el fitxer tar.gz que conté la primera pàgina de manual de l'ordre ls)
    manpwd (és el fitxer tar.gz que conté la primera pàgina de manual de l'ordre pwd)
	$ ls > mp1/fonts/informe.txt
	$ ll > mp1/fonts/carta01.txt
	$ ls -la > mp1/fonts/carta02.txt
	$ ls -a > mp1/fonts/carta3.txt
	$ finger > mp1/fonts/carta4.txt
	$ stat mp1 > mp1/fonts/carta05.txt
	$ stat carta01.txt > mp1/fonts/carta06.txt
	$ quota > mp1/fonts/.oculta
	$ free > mp1/fonts/.ocultba
	$ df > mp1/fonts/.ocultbc
	$ fdisk -l > mp1/fonts/.ocultbe
	$ whereis ls pwd
	ls: /bin/ls /usr/share/man/man1/ls.1.gz /usr/share/man/man1p/ls.1p.gz
	pwd: /bin/pwd /usr/include/pwd.h /usr/share/man/man1/pwd.1.gz /usr/share/man/man1p/pwd.1p.gz
	$ cp /bin/ls mp1/fonts/.
	$ cp /bin/pwd mp1/fonts/.
	$ cp /usr/share/man/man1p/ls.1p.gz mp1/fonts/manls
	$ cp /usr/share/man/man1/pwd.1.gz mp1/fonts/manpwd

	$ tree mp1
	mp1
	├── dades
	└── fonts
	    ├── carta01.txt
	    ├── carta02.txt
	    ├── carta05.txt
	    ├── carta06.txt
	    ├── carta3.txt
	    ├── carta4.txt
	    ├── informe.txt
	    ├── ls
	    ├── manls
	    ├── manpwd
	    └── pwd

04) crear un directori de nom dades dins de mp1.
	$ mkdir mp1/dades

-------------------------------------------------------------------------------
part2:
-------------------------------------------------------------------------------
0x) situar-se al directori dades i fer TOTES les ordres des d'aquest directori.
	$ cd mp1/dades

05) crear al directori dades un ellaç al fitxer informe.txt.
	$ ll ../fonts/informe.txt
	-rw-rw-r-- 1 ecanet ecanet 156 12 oct 12:03 ../fonts/informe.txt
	$ ln ../fonts/informe.txt .
	$ ll informe.txt 
	-rw-rw-r-- 2 ecanet ecanet 156 12 oct 12:03 informe.txt
	$ ll ../fonts/informe.txt
	-rw-rw-r-- 2 ecanet ecanet 156 12 oct 12:03 ../fonts/informe.txt

06) crear al directori dades un enllaç al fitxer informe.txt anomenat informe-bis.txt.
	$ ln ../fonts/informe.txt informe-bis.txt
	$ ll 
	-rw-rw-r-- 3 ecanet ecanet 156 12 oct 12:03 informe-bis.txt
	-rw-rw-r-- 3 ecanet ecanet 156 12 oct 12:03 informe.txt
	$ ll ../fonts/informe.txt
	-rw-rw-r-- 3 ecanet ecanet 156 12 oct 12:03 ../fonts/informe.txt

07) crear al directori dades un ellaç per a cada un dels fitxers de nom carta (en una sola ordre).
	$ ln ../fonts/carta* .
	$ ll
	total 32
	-rw-rw-r-- 2 ecanet ecanet  694 12 oct 12:03 carta01.txt
	-rw-rw-r-- 2 ecanet ecanet 1219 12 oct 12:03 carta02.txt
	-rw-rw-r-- 2 ecanet ecanet  333 12 oct 12:04 carta05.txt
	-rw-rw-r-- 2 ecanet ecanet  347 12 oct 12:04 carta06.txt
	-rw-rw-r-- 2 ecanet ecanet  261 12 oct 12:04 carta3.txt
	-rw-rw-r-- 2 ecanet ecanet  239 12 oct 12:04 carta4.txt
	-rw-rw-r-- 3 ecanet ecanet  156 12 oct 12:03 informe-bis.txt
	-rw-rw-r-- 3 ecanet ecanet  156 12 oct 12:03 informe.txt

08) crear al directori dades un enllaç el directori fonts.
	$ ln -s ../fonts .
	$ ll fonts
	lrwxrwxrwx 1 ecanet ecanet 8 12 oct 12:26 fonts -> ../fonts

	$ ln -s /tmp/mp1/fonts fonts-bis
	$ ll fonts*
	lrwxrwxrwx 1 ecanet ecanet  8 12 oct 12:26 fonts -> ../fonts
	lrwxrwxrwx 1 ecanet ecanet 14 12 oct 12:27 fonts-bis -> /tmp/mp1/fonts

09) modificar el nom de tots els fitxers que comencin per carta per tal de que comencin per treball (a dades).
	$ rename carta treball carta*
	$ ll 
	total 32
	lrwxrwxrwx 1 ecanet ecanet    8 12 oct 12:26 fonts -> ../fonts
	lrwxrwxrwx 1 ecanet ecanet   14 12 oct 12:27 fonts-bis -> /tmp/mp1/fonts
	-rw-rw-r-- 3 ecanet ecanet  156 12 oct 12:03 informe-bis.txt
	-rw-rw-r-- 3 ecanet ecanet  156 12 oct 12:03 informe.txt
	-rw-rw-r-- 2 ecanet ecanet  694 12 oct 12:03 treball01.txt
	-rw-rw-r-- 2 ecanet ecanet 1219 12 oct 12:03 treball02.txt
	-rw-rw-r-- 2 ecanet ecanet  333 12 oct 12:04 treball05.txt
	-rw-rw-r-- 2 ecanet ecanet  347 12 oct 12:04 treball06.txt
	-rw-rw-r-- 2 ecanet ecanet  261 12 oct 12:04 treball3.txt
	-rw-rw-r-- 2 ecanet ecanet  239 12 oct 12:04 treball4.txt

10) crear al directori dades un enllaç simbòlic al fitxer /etc/fstab.
	$ ln -s /etc/fstab .
	$ ll fstab
	lrwxrwxrwx 1 ecanet ecanet 10 12 oct 12:30 fstab -> /etc/fstab

11) crear al directori dades un enllaç simbòlic al directori /boot.
	$ ln -s /boot .
	$ ll boot
	lrwxrwxrwx 1 ecanet ecanet 5 12 oct 12:31 boot -> /boot

-------------------------------------------------------------------------------
part3:
-------------------------------------------------------------------------------
12) comparar el fitxer informe.txt i informe-bis.txt del directori dades.
	$ cmp informe.txt informe-bis.txt 
	$ diff informe.txt informe-bis.txt 

13) comparar el fitxer carta01.txt i carta02.txt del directori fonts.
	$ cmp /tmp/mp1/fonts/carta01.txt /tmp/mp1/fonts/carta02.txt 
	/tmp/mp1/fonts/carta01.txt /tmp/mp1/fonts/carta02.txt differ: byte 7, line 1
	
	$ diff /tmp/mp1/fonts/carta01.txt /tmp/mp1/fonts/carta02.txt 
	1,12c1,21
	< total 32
	< -rw-rw-r-- 1 ecanet  ecanet     0 12 oct 12:03 carta01.txt
	< srwxrwxr-x 1 ecanet  ecanet     0 12 oct 11:59 gedit.ecanet.2427975925
	< srwxr-xr-x 1 root    root       0 20 mai 19:31 gedit.root.2921474226
	< -rw-rw-r-- 1 ecanet  ecanet   156 12 oct 12:03 informe.txt
	< drwx------ 2 encarna encarna 4096 11 jul 13:34 keyring-IUei6B
	< drwxrwxr-x 3 ecanet  ecanet  4096 12 oct 12:03 mp1
	< drwx------ 2 ecanet  ecanet  4096  1 gen  1970 orbit-ecanet
	< drwx------ 2 gdm     gdm     4096 12 oct 11:51 orbit-gdm
	< drwx------ 2 gdm     gdm     4096 12 oct 11:51 pulse-PKdhtXMmr18n
	< drwx------ 2 ecanet  ecanet  4096 12 oct 12:00 pulse-zOBjVSkeZMbb
	< -rw------- 1 ecanet  ecanet  2064 12 oct 11:58 XMCCitIX.txt.part
	---
	> total 72
	> drwxrwxrwt 13 root    root    4096 12 oct 12:03 .
	> dr-xr-xr-x 31 root    root    4096 12 oct 11:50 ..
	> -rw-rw-r--  1 ecanet  ecanet   694 12 oct 12:03 carta01.txt
	> -rw-rw-r--  1 ecanet  ecanet     0 12 oct 12:03 carta02.txt
	> drwx------  2 ecanet  ecanet  4096 12 oct 11:51 .esd-500
	> srwxrwxr-x  1 ecanet  ecanet     0 12 oct 11:59 gedit.ecanet.2427975925
	> srwxr-xr-x  1 root    root       0 20 mai 19:31 gedit.root.2921474226
	> drwxrwxrwt  2 root    root    4096 12 oct 11:51 .ICE-unix
	> -rw-rw-r--  1 ecanet  ecanet   156 12 oct 12:03 informe.txt
	> drwx------  2 encarna encarna 4096 11 jul 13:34 keyring-IUei6B
	> drwxrwxr-x  3 ecanet  ecanet  4096 12 oct 12:03 mp1
	> drwx------  2 ecanet  ecanet  4096  1 gen  1970 orbit-ecanet
	> drwx------  2 gdm     gdm     4096 12 oct 11:51 orbit-gdm
	> drwx------  2 gdm     gdm     4096 12 oct 11:51 pulse-PKdhtXMmr18n
	> drwx------  2 ecanet  ecanet  4096 12 oct 12:00 pulse-zOBjVSkeZMbb
	> drwx------  2 ecanet  ecanet  4096  2 ago 13:16 .vbox-ecanet-ipc
	> drwx------  3 ecanet  ecanet  4096 11 oct 01:50 .wine-500
	> -r--r--r--  1 root    root      11 12 oct 11:51 .X0-lock
	> drwxrwxrwt  2 root    root    4096 12 oct 11:51 .X11-unix
	> -rw-------  1 ecanet  ecanet  2064 12 oct 11:58 XMCCitIX.txt.part	

14) copiar carta3.txt a dades, modificar una sola paraula en una sola línia i posar una línia en blanc de més. Comparar els dos fitxers.
	$ cp /tmp/mp1/fonts/carta3.txt .
	$ vi carta3.txt 
	$ cmp /tmp/mp1/fonts/carta3.txt carta3.txt 
	/tmp/mp1/fonts/carta3.txt carta3.txt differ: byte 37, line 5

	$ diff /tmp/mp1/fonts/carta3.txt carta3.txt 
	5c5,6
	< carta3.txt
	---
	> carta3.nova
	> 

15) comparar el fitxer ls del directori fonts amb l'executable de l'ordre ls.
$ cmp /bin/ls /tmp/mp1/fonts/ls 
$ diff /bin/ls /tmp/mp1/fonts/ls 

16) copiar el fitxer ls al directori dades i dividir-lo en tres torços.
	$ cp /tmp/mp1/fonts/ls  .
	$ ll ls
	-rwxr-xr-x 1 ecanet ecanet 116688 12 oct 12:37 ls

	$ split -b 40000 ls
	$ ll x*
	-rw-rw-r-- 1 ecanet ecanet 40000 12 oct 12:43 xaa
	-rw-rw-r-- 1 ecanet ecanet 40000 12 oct 12:43 xab
	-rw-rw-r-- 1 ecanet ecanet 36688 12 oct 12:43 xac

17) ajuntar els tres troços en un nou fitxer anomemenat ajuntat.bin
	$ echo xa* 
	xaa xab xac
	$ cat xa* > ajuntat.bin

18) comparar el fitxer ls i el ajuntat.bin.
	$ cmp ls ajuntat.bin

19) dividir el fitxer ls en troços de 50k. Ajuntar-lo de nou amb el nom trocets.bin
	$ split -b 50k ls 
	$ ll x*
	-rw-rw-r-- 1 ecanet ecanet 51200 12 oct 12:39 xaa
	-rw-rw-r-- 1 ecanet ecanet 51200 12 oct 12:39 xab
	-rw-rw-r-- 1 ecanet ecanet 14288 12 oct 12:39 xac

	$ cat xa* > trocets.bin

20) fer l'ordre stat del fitxer ls i del fitxer trocets.bin.
	$ stat ls
	  File: «ls»
	  Size: 116688    	Blocks: 240        IO Block: 4096   fitxer ordinari
	Device: 807h/2055d	Inode: 17970       Links: 1
	Access: (0755/-rwxr-xr-x)  Uid: (  500/  ecanet)   Gid: (  500/  ecanet)
	Access: 2010-10-12 12:39:53.000000000 +0200
	Modify: 2010-10-12 12:37:29.000000000 +0200
	Change: 2010-10-12 12:37:29.000000000 +0200

	$ stat trocets.bin 
	  File: «trocets.bin»
	  Size: 116688    	Blocks: 240        IO Block: 4096   fitxer ordinari
	Device: 807h/2055d	Inode: 17973       Links: 1
	Access: (0664/-rw-rw-r--)  Uid: (  500/  ecanet)   Gid: (  500/  ecanet)
	Access: 2010-10-12 12:43:00.000000000 +0200
	Modify: 2010-10-12 12:42:51.000000000 +0200
	Change: 2010-10-12 12:42:51.000000000 +0200

-------------------------------------------------------------------------------
part4:
-------------------------------------------------------------------------------
21) realitza l'ordre updatedb
	$ updatedb

22) localitza tots els fitxers de nom vmlinuz
	$ locate vmlinuz

23) localitza el fitxer de nom carta03
	$ locate carta03
	$ locate carta3

24) localitza tots els fitxers que comencin per carta
	$ locate carta

25) comptar les línies del fitxer de passwords.
	$ cat /etc/passwd | wc -l

	$ wc -l /etc/passwd

26) llistar la quota de l'usuari.
	$ quota
	
27) llistar l'ocupació de disc (du) del directori mp1 calculant-ne el total.
	$ du -s /tmp/mp1
	692	/tmp/mp1

	$ du -sh /tmp/mp1
	692K	/tmp/mp1

28) llistar l'espai ocupat de disc del directori mp1 mostrant els subtotals de cada subdirectori (només un nivell de profunditat).
	$ du --max-depth=1 -h /tmp/mp1
	512K	/tmp/mp1/dades
	176K	/tmp/mp1/fonts
	692K	/tmp/mp1

29) fer el mateix del directori de l'usuri i de l'arrel del sistema.
	$ du --max-depth=1 -h ~

	$ du --max-depth=1 -h /

39) llistar l'ocupació de disc del directori de l'usuari agrupant fins a dos nivells de profunditat.
	$ du --max-depth=2 -h ~


Ordres:
-------------------------------------------------------------------------------
enllaç dur i enllaç simbòlic
comparar: diff, cmp, comp, diff3, compare
fitxers: head, tail, sort, less, more, grep, wc, od, hexdump, zcat
parts: split, cat
disc: quota, df, du
buscar: grep, locate, updatedb, find (nom, propietari, mida, permisos)
renombrar: rename

