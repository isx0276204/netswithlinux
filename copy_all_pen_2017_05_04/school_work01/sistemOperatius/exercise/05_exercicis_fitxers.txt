﻿Exercicis de fitxers (05)
	Conceptes clau:
* Informació d’usuari
* Quotes i ocupació de disc.
* Buscar fitxers (nom, mida, propietari, tipus)
* Empaquetar contingut: tar
Ordres a treballar:
id, who, finger, chfn, chsh, .plan
Quota, du, du --max-depth, du -s, df
updatedb, locate
find, find --size, find --user, find --type
tar, tar -pP
Exercicis
________________
Primera part
1. Fer actiu el directori /var/tmp.
##
cd /var/tmp
2. Identifica el ID i UID de l’usuari actual, de root i de guest.
##
[isx0276204@j02 ~]$ id
uid=202172(isx0276204) gid=200005(hisx1) groups=200005(hisx1),976(wireshark) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
[isx0276204@j02 ~]$ id root
uid=0(root) gid=0(root) groups=0(root)
[isx0276204@j02 ~]$ id guest
uid=1000(guest) gid=1000(guest) groups=1000(guest)
[isx0276204@j02 ~]$

3. Llista els usuaris que han fet login en el sistema.
##
[isx0276204@j02 ~]$ who
isx0276204 tty2         2016-10-14 09:08
isx0276204 tty3         2016-10-14 09:08
isx0276204 tty4         2016-10-14 09:08
[isx0276204@j02 ~]$

4. Identifica el funcionament de l’ordre finger i executa-la.
##
finger — user information lookup program
[isx0276204@j02 ~]$ finger
Login       Name         Tty      Idle  Login Time   Office     Office Phone   Host
isx0276204  isx0276204   tty2           Oct 14 09:08
isx0276204  isx0276204   tty3           Oct 14 09:08
isx0276204  isx0276204   tty4           Oct 14 09:08


5. Fes l’ordre finger per al teu isx, per a roor i per a guest.
##
[isx0276204@j02 ~]$ finger isx0276204
Login: isx0276204                       Name: isx0276204
Directory: /home/users/inf/hisx1/isx0276204     Shell: /bin/bash
On since Fri Oct 14 09:08 (CEST) on tty2   15 seconds idle
On since Fri Oct 14 09:08 (CEST) on tty3   36 seconds idle
On since Fri Oct 14 09:08 (CEST) on tty4   7 seconds idle
No mail.
No Plan.
[isx0276204@j02 ~]$ finger root
Login: root                             Name: root
Directory: /root                        Shell: /bin/bash
Last login Thu Oct 13 13:04 (CEST) on pts/1
No mail.
No Plan.
[isx0276204@j02 ~]$ finger guest
Login: guest                            Name:
Directory: /home/guest                  Shell: /bin/bash
Last login Thu Sep 15 10:42 (CEST) on tty2
No mail.
No Plan.
[isx0276204@j02 ~]$


6. Llistar els shells disponibles en el sistema: /etc/shells.
##
[isx0276204@j02 ~]$ cat /etc/shells
/bin/sh
/bin/bash
/sbin/nologin
/usr/bin/sh
/usr/bin/bash
/usr/sbin/nologin


7. Idetifica quin shell tens assignat.
##
[isx0276204@j02 ~]$ finger isx0276204
Login: isx0276204                       Name: isx0276204
Directory: /home/users/inf/hisx1/isx0276204     Shell: /bin/bash
On since Fri Oct 14 09:08 (CEST) on tty2   15 seconds idle
On since Fri Oct 14 09:08 (CEST) on tty3   36 seconds idle
On since Fri Oct 14 09:08 (CEST) on tty4   7 seconds idle
No mail.
No Plan.


8. Modificar les dades de l’usuari guest:
   1. Establir un nom, empresa i telèfon.
##
[root@j02 isx0276204]# chfn guest
Changing finger information for guest.
Name []: guest
Office []: treball
Office Phone []: 90000000
Home Phone []: 930000000


Finger information changed.
[root@j02 isx0276204]# finger guest
Login: guest                            Name: guest
Directory: /home/guest                  Shell: /bin/bash
Office: treball, 90000000               Home Phone: 930000000
Last login Thu Sep 15 10:42 (CEST) on tty2
No mail.
No Plan.
[root@j02 isx0276204]#

 
   2. Assignar-li el shell sh.
##
[root@j02 isx0276204]# chsh guest
Changing shell for guest.
New shell [/bin/bash]: /bin/sh
Shell changed.
[root@j02 isx0276204]# finger guest
Login: guest                            Name: guest
Directory: /home/guest                  Shell: /bin/sh
Office: treball, 90000000               Home Phone: 930000000
Last login Thu Sep 15 10:42 (CEST) on tty2
No mail.
No Plan.

   3. Per a què servei el fitxer .plan
##
to make future plan
Segona part
1. Fer actiu el directori home de l’usuari.
##
cd ~
2. Realitza l’ordre quaota i identifica la informació que mostra.
##
[isx0276204@j02 ~]$ quota
Disk quotas for user isx0276204 (uid 202172):
     Filesystem  blocks   quota   limit   grace   files   quota   limit   grace
gandhi.informatica.escoladeltreball.org://users/inf/hisx1/isx0276204
                     60   40000   50000              15       0       0

3. Calcula l’ocupació d’espai del teu home amb l’ordre du, du -s, du -c.
##
[isx0276204@j02 ~]$ du -s isx0276204/
68      isx0276204/
[isx0276204@j02 ~]$ du isx0276204/
4       isx0276204/.mozilla/plugins
4       isx0276204/.mozilla/extensions
28      isx0276204/.mozilla
12      isx0276204/.config/autostart
16      isx0276204/.config
68      isx0276204/
[isx0276204@j02 ~]$ du -c isx0276204/
4       isx0276204/.mozilla/plugins
4       isx0276204/.mozilla/extensions
28      isx0276204/.mozilla
12      isx0276204/.config/autostart
16      isx0276204/.config
68      isx0276204/
68      total

4. Llista l’ocupació del directori /var/tmp/esport. Calcula el total d’ocupació d’aquest directori.
##
[isx0276204@j02 ~]$ du /var/tmp/m01/
4       /var/tmp/m01/operatius
40      /var/tmp/m01/practiques
4       /var/tmp/m01/sistemes
4       /var/tmp/m01/apunts
76      /var/tmp/m01/
[isx0276204@j02 ~]$ du -s /var/tmp/m01/
76      /var/tmp/m01/

5. Ídem mostrant els subtotals de cada sots directori que conté (/var/tmp/esport). És a dir, mostrar un nivell de profunditat.
##
[parveen@localhost ~]$ du -h --max-depth=1 /var/tmp/
du: no se puede leer el directorio '/var/tmp/systemd-private-82fd3700e16a4217ab62ade3522edd3e-colord.service-oKIDg7': Permission denied
4,0K	/var/tmp/systemd-private-82fd3700e16a4217ab62ade3522edd3e-colord.service-oKIDg7
du: no se puede leer el directorio '/var/tmp/systemd-private-82fd3700e16a4217ab62ade3522edd3e-rtkit-daemon.service-7DSOBw': Permission denied
4,0K	/var/tmp/systemd-private-82fd3700e16a4217ab62ade3522edd3e-rtkit-daemon.service-7DSOBw
du: no se puede leer el directorio '/var/tmp/systemd-private-1cc471b28b854373b2819738cdc28b55-rtkit-daemon.service-NlMyBl': Permission denied
4,0K	/var/tmp/systemd-private-1cc471b28b854373b2819738cdc28b55-rtkit-daemon.service-NlMyBl
404K	/var/tmp/parts
4,0K	/var/tmp/abrt
du: no se puede leer el directorio '/var/tmp/systemd-private-1cc471b28b854373b2819738cdc28b55-colord.service-8cBHJ8': Permission denied
4,0K	/var/tmp/systemd-private-1cc471b28b854373b2819738cdc28b55-colord.service-8cBHJ8
60K	/var/tmp/dnf-parveen-hlr7x_ej
24K	/var/tmp/m01
512K	/var/tmp/
[parveen@localhost ~]$ 



6. Fer el mateix del directori home de l'usuari.
##
[parveen@localhost ~]$ du -h --max-depth=1 ~
156M	/home/parveen/.cache
4,0K	/home/parveen/Música
24K	/home/parveen/.gnupg
17M	/home/parveen/.local
4,0K	/home/parveen/Vídeos
1,9M	/home/parveen/.config
4,0K	/home/parveen/Descargas
5,0G	/home/parveen/Documentos
22M	/home/parveen/markdown-plus
8,0K	/home/parveen/.pki
9,0M	/home/parveen/netswithlinux
14M	/home/parveen/.mozilla
16K	/home/parveen/.ssh
4,0K	/home/parveen/Imágenes
5,8M	/home/parveen/Escritorio
5,2G	/home/parveen
[parveen@localhost ~]$ 

7. També fer-ho de l’arrel del sistema.
##
698M	/var
du: no se puede leer el directorio '/usr/share/polkit-1/rules.d': Permission denied
du: no se puede leer el directorio '/usr/libexec/initscripts/legacy-actions/auditd': Permission denied
4,0G	/usr
du: no se puede leer el directorio '/sys/fs/fuse/connections/44': Permission denied
du: no se puede leer el directorio '/sys/kernel/debug': Permission denied
0	/sys
du: no se puede leer el directorio '/boot/efi': Permission denied
100M	/boot
4,0K	/opt
du: no se puede leer el directorio '/home/guest': Permission denied
5,2G	/home
4,0K	/media
12G	/
[parveen@localhost ~]$ du -h --max-depth=1 /

8. Lllistar l'ocupació de disc del directori home l'usuari agrupant fins a dos nivells de profunditat.
##
[parveen@localhost ~]$ du -h --max-depth=2 ~
95M	/home/parveen/.cache/tracker
16K	/home/parveen/.cache/yelp
3,7M	/home/parveen/.cache/thumbnails
57M	/home/parveen/.cache/mozilla
12K	/home/parveen/.cache/libgweather
296K	/home/parveen/.cache/gnome-software
16K	/home/parveen/.cache/rhythmbox
8,0K	/home/parveen/.cache/folks
504K	/home/parveen/.cache/gstreamer-1.0
8,0K	/home/parveen/.cache/webkitgtk
52K	/home/parveen/.cache/evolution
8,0K	/home/parveen/.cache/abrt
4,0K	/home/parveen/.cache/media-art
16K	/home/parveen/.cache/simple-scan
156M	/home/parveen/.cache
4,0K	/home/parveen/Música
24K	/home/parveen/.gnupg
17M	/home/parveen/.local/share
17M	/home/parveen/.local
4,0K	/home/parveen/Vídeos
1,6M	/home/parveen/.config/libreoffice
12K	/home/parveen/.config/wireshark
4,0K	/home/parveen/.config/gconf
8,0K	/home/parveen/.config/yelp
4,0K	/home/parveen/.config/enchant
4,0K	/home/parveen/.config/nautilus
8,0K	/home/parveen/.config/gnome-session
36K	/home/parveen/.config/geany
8,0K	/home/parveen/.config/gtk-3.0
4,0K	/home/parveen/.config/eog
12K	/home/parveen/.config/evolution
8,0K	/home/parveen/.config/gnome-boxes
4,0K	/home/parveen/.config/abrt
8,0K	/home/parveen/.config/gtk-2.0
16K	/home/parveen/.config/dconf
72K	/home/parveen/.config/pulse
12K	/home/parveen/.config/ibus
4,0K	/home/parveen/.config/goa-1.0
8,0K	/home/parveen/.config/gedit
8,0K	/home/parveen/.config/terminator
1,9M	/home/parveen/.config
4,0K	/home/parveen/Descargas
5,0G	/home/parveen/Documentos/2016_10_10_all_data
5,6M	/home/parveen/Documentos/school_work
5,0G	/home/parveen/Documentos
15M	/home/parveen/markdown-plus/.git
6,8M	/home/parveen/markdown-plus/dist
22M	/home/parveen/markdown-plus
4,0K	/home/parveen/.pki/nssdb
8,0K	/home/parveen/.pki
16K	/home/parveen/netswithlinux/history
16K	/home/parveen/netswithlinux/01-ip_protocol
9,0M	/home/parveen/netswithlinux/.git
16K	/home/parveen/netswithlinux/exercises
9,0M	/home/parveen/netswithlinux
4,0K	/home/parveen/.mozilla/plugins
16K	/home/parveen/.mozilla/extensions
14M	/home/parveen/.mozilla/firefox
14M	/home/parveen/.mozilla
16K	/home/parveen/.ssh
4,0K	/home/parveen/Imágenes
5,8M	/home/parveen/Escritorio/school_work
5,8M	/home/parveen/Escritorio
5,2G	/home/parveen

9. Mostra l’ocupació d’espai del disc amb df -h.
##
[parveen@localhost ~]$ df -h
S.ficheros     Tamaño Usados  Disp Uso% Montado en
devtmpfs         1,9G      0  1,9G   0% /dev
tmpfs            1,9G   356K  1,9G   1% /dev/shm
tmpfs            1,9G   1,6M  1,9G   1% /run
tmpfs            1,9G      0  1,9G   0% /sys/fs/cgroup
/dev/sda1         99G    11G   84G  11% /
tmpfs            1,9G   156K  1,9G   1% /tmp
tmpfs            384M    20K  384M   1% /run/user/42
tmpfs            384M    32K  384M   1% /run/user/1001
/dev/sdb1        7,5G   1,4G  6,2G  18% /run/media/parveen/school_work

10. Mostra l’ocupació d’espai del disc de les particions ext4.
##
[parveen@localhost ~]$ df -h -t ext4
S.ficheros     Tamaño Usados  Disp Uso% Montado en
/dev/sda1         99G    11G   84G  11% /

#########################################################################################################
Tercera part


1. Fer actiu el directori /var/tmp.
##
[parveen@localhost ~]$ cd /var/tmp/
[parveen@localhost tmp]$ pwd
/var/tmp


2. Realitza l'ordre updatedb i esbrina què fa.
##
[parveen@localhost tmp]$ updatedb 
updatedb: no se puede abrir un archivo temporal para `/var/lib/mlocate/mlocate.db'
				or
[root@localhost tmp]# updatedb
[root@localhost tmp]# echo $?
0 
==> update data for command locate
3. Localitza tots els fitxers de nom vmlinuz.
##
[parveen@localhost tmp]$ locate vmlinuz
/boot/.vmlinuz-4.5.5-300.fc24.x86_64.hmac
/boot/vmlinuz-0-rescue-f7890e12881043bc95a4df7ac5531cf6
/boot/vmlinuz-4.5.5-300.fc24.x86_64
/usr/lib/modules/4.5.5-300.fc24.x86_64/.vmlinuz.hmac
/usr/lib/modules/4.5.5-300.fc24.x86_64/vmlinuz

4. Localitza el fitxer services.
##
[parveen@localhost tmp]$ locate services
/etc/services
/etc/avahi/services
/home/parveen/Documentos/2016_10_10_all_data/all_for_all/documentacio_ibm_linux/lp2-web-services.pdf
/usr/include/sepol/policydb/services.h
/usr/lib/firewalld/services
/usr/lib/firewalld/services/amanda-client.xml
/usr/lib/firewalld/services/amanda-k5-client.xml
/usr/lib/firewalld/services/bacula-client.xml
/usr/lib/firewalld/services/bacula.xml
/usr/lib/firewalld/services/ceph-mon.xml
/usr/lib/firewalld/services/ceph.xml
/usr/lib/firewalld/services/dhcp.xml
/usr/lib/firewalld/services/dhcpv6-client.xml
/usr/lib/firewalld/services/dhcpv6.xml
/usr/lib/firewalld/services/dns.xml
/usr/lib/firewalld/services/docker-registry.xml
/usr/lib/firewalld/services/dropbox-lansync.xml
/usr/lib/firewalld/services/freeipa-ldap.xml
/usr/lib/firewalld/services/freeipa-ldaps.xml
/usr/lib/firewalld/services/freeipa-replication.xml

5. Localitza els fitxers de nom ifcfg.
##
[parveen@localhost tmp]$ locate ifcfg
/etc/NetworkManager/dispatcher.d/10-ifcfg-rh-routes.sh
/etc/NetworkManager/dispatcher.d/no-wait.d/10-ifcfg-rh-routes.sh
/etc/NetworkManager/dispatcher.d/pre-up.d/10-ifcfg-rh-routes.sh
/etc/dbus-1/system.d/nm-ifcfg-rh.conf
/etc/sysconfig/network-scripts/ifcfg-MOVISTAR_8BA9
/etc/sysconfig/network-scripts/ifcfg-enp2s0
/etc/sysconfig/network-scripts/ifcfg-lo
/home/parveen/Documentos/2016_10_10_all_data/all_gruop/hisx1/MP7-Xarxes/ifcfg-p4p1_j22
/home/parveen/Documentos/2016_10_10_all_data/all_gruop/hisx1/MP7-Xarxes/ConfiguracionsXarxa_N2J/ifcfg-p4p1_J13

6. Localitza el fitxer carta03. Ho fa? Perquè?
##
[parveen@localhost tmp]$ locate carta
/home/parveen/Documentos/2016_10_10_all_data/all_for_all/M01-Operatius/A03-07_scripting/exemples-exercicis/a03-07_exemples_apunts/a09-validar_descartar_sinopsys.sh
[parveen@localhost tmp]$ echo $?
0
==> not only search file in /etc and manual
7. Identifica el funcionament de l’ordre find i troba tots els fitxers de més de 1M del directori /boot.
##
root@localhost tmp]# find /boot/ -size +3M
/boot/vmlinuz-0-rescue-f7890e12881043bc95a4df7ac5531cf6
/boot/vmlinuz-4.5.5-300.fc24.x86_64
/boot/grub2/themes/system/fireworks.png
/boot/grub2/themes/system/background.png
/boot/System.map-4.5.5-300.fc24.x86_64
/boot/initramfs-4.5.5-300.fc24.x86_64.img
/boot/initramfs-0-rescue-f7890e12881043bc95a4df7ac5531cf6.img

8. Amb l’ordre find i troba tots els fitxers de menys de 4M del directori /boot.
##
root@localhost tmp]# find /boot/ -size -4M
/boot/
/boot/efi
/boot/efi/System
/boot/efi/System/Library
/boot/efi/System/Library/CoreServices
/boot/efi/System/Library/CoreServices/SystemVersion.plist
/boot/efi/mach_kernel
/boot/efi/EFI
/boot/efi/EFI/BOOT
/boot/efi/EFI/BOOT/fallback.efi
/boot/efi/EFI/BOOT/BOOTX64.EFI
/boot/efi/EFI/fedora
/boot/efi/EFI/fedora/MokManager.efi
/boot/efi/EFI/fedora/fw
/boot/efi/EFI/fedora/grubx64.efi
/boot/efi/EFI/fedora/shim.efi
/boot/efi/EFI/fedora/BOOT.CSV

9. Amb l’ordre find i troba tots els fitxers de 1M a 4M del directori /boot.
##
[root@localhost tmp]# find /boot/ -size -4M -size +1M
/boot/efi/EFI/BOOT/BOOTX64.EFI
/boot/efi/EFI/fedora/MokManager.efi
/boot/efi/EFI/fedora/shim.efi
/boot/efi/EFI/fedora/shim-fedora.efi
/boot/efi/EFI/fedora/fonts/unicode.pf2
/boot/grub2/fonts/unicode.pf2

10. Localita de /var/tmp tots els fitxers de propietat teva.
##
root@localhost tmp]# find /var/tmp/ -user parveen
/var/tmp/dnf-parveen-hlr7x_ej
/var/tmp/dnf-parveen-hlr7x_ej/expired_repos.json
/var/tmp/dnf-parveen-hlr7x_ej/dnf.rpm.log
/var/tmp/dnf-parveen-hlr7x_ej/dnf.librepo.log
/var/tmp/dnf-parveen-hlr7x_ej/dnf.log

11. Localita de /var/tmp tots els fitxers de propietat de root.
##
[root@localhost tmp]# find /var/tmp/ -user root
/var/tmp/
/var/tmp/systemd-private-82fd3700e16a4217ab62ade3522edd3e-colord.service-oKIDg7
/var/tmp/systemd-private-82fd3700e16a4217ab62ade3522edd3e-colord.service-oKIDg7/tmp
/var/tmp/systemd-private-82fd3700e16a4217ab62ade3522edd3e-rtkit-daemon.service-7DSOBw
/var/tmp/systemd-private-82fd3700e16a4217ab62ade3522edd3e-rtkit-daemon.service-7DSOBw/tmp
/var/tmp/systemd-private-1cc471b28b854373b2819738cdc28b55-rtkit-daemon.service-NlMyBl
/var/tmp/systemd-private-1cc471b28b854373b2819738cdc28b55-rtkit-daemon.service-NlMyBl/tmp
/var/tmp/parts
/var/tmp/parts/xac
/var/tmp/parts/xab
/var/tmp/parts/newls
/var/tmp/parts/ls
/var/tmp/parts/xaa
/var/tmp/systemd-private-1cc471b28b854373b2819738cdc28b55-colord.service-8cBHJ8
/var/tmp/systemd-private-1cc471b28b854373b2819738cdc28b55-colord.service-8cBHJ8/tmp
/var/tmp/m01
/var/tmp/m01/letter06.pdf
/var/tmp/m01/envelop7.rtf
/var/tmp/m01/dossier.rtf
/var/tmp/m01/newdossier.rtf
/var/tmp/m01/operatius
/var/tmp/m01/operatius/prova
/var/tmp/m01/envelop02.rtf
/var/tmp/m01/tridossier.rtf
/var/tmp/m01/envelop8.rtf
/var/tmp/m01/envelop01.rtf
/var/tmp/m01/informe.txt
/var/tmp/m01/letter9.odt
/var/tmp/m01/letter05.pdf
/var/tmp/m01/envelop03.rtf
/var/tmp/m01/informe.md
/var/tmp/m01/bidossier.rtf
/var/tmp/m01/envelop04.rtf
/var/tmp/m01/newinforme.md

12. Identifica els fitxers de /boot de nom initrd, amb find.
##
[root@j02 isx0276204]# find /boot/ -name initramfs-4.7.3-200.fc24.x86_64.img
/boot/initramfs-4.7.3-200.fc24.x86_64.img

13. Identifica els fitxers de /boot de nom .cfg amb find.
##
[root@j02 isx0276204]# find /boot/ -name *.cfg
/boot/grub2/grub.cfg

14. Usant find llista els fitxers de tipus directori de /etc i de /tmp i de /boot.
##
[root@j02 isx0276204]# find /boot/ /tmp/ /etc/ -type f

15. Usant find llista els fitxers de tipus link de /etc/systemd/system i de /tmp
##
[root@j02 isx0276204]# find /boot/ /tmp/ /etc/ -type l

16. Usant find llista els fitxers de tipus file del teu home.
##
[root@j02 isx0276204]# find ~ -type f

17. Listar tots els fitxers del directori /var/tmp més nous que una data en concret.
##


18. Listar tots els fitxers del directori /var/tmp amb data de fa deu dies fins ara.
19. Listar tots els fitxers del directori /var/tmp amb data anterior a fa 72 hores.
Quarta part
1. Empaqueta el contingut de /var/tmp a un fitxer de /tmp anomenat paquet.tar.
2. Llistar el contingut de paquet. Són rutes absolutes o relatives?
3. Extreure el contingut de paquet dins de /tmp/newdata.
4. Extreure només els fitxers txt i desar-los a /tmp/prova.
5. Empaquetar el contingut de /tmp/newdata en un fitxer  anomenat paquet-absolute.tar al directori /tmp. Cal que el paquet es generi mantenint les rutes absolutes dels fitxers origens.
6. Llistar el contingut de paquet. Són rutes absolutes o relatives?
7. Esborrar el directori /tmp/newdata i tot el que conté.
8. Desempaquetar paquet-absolute.tar  indicant com a destí /tmp/prova, però observar on es generen els elements desempaquetats. Aquí dins o al seu path absolut?
9. Repatir l’exercici anterior provant les diferències de desenpaquetar amb l’opció -p o -P.
10. Empaquetar el contingut de /var/tmp generant un paquet comprimit. Llistar el contingut i extreure’n només els fitxers txt.
11. Empaquetar el contingut de /var/tmp generant un paquet comprimit amb bzip. Llistar el contingut i extreure’n només els fitxers txt.
