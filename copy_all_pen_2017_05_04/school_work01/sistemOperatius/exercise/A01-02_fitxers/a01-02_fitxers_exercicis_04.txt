ASIX MP1_ISO
Curs 2014-2015
A01-02 Gestió de fitxers i directoris
========================================================================
Exercici_4
========================================================================
cat, cat >, mv, mv -r, cp, cp -r, rm, rm -r

part1:
------------------------------------------------------------------------
00) situar-se al directori actiu /var/tmp i realizat TOTES les ordres 
    des d'aquest directori.
01) Crear dins de /var/tmp un directori anomenat m01/files.
02) crear un directori de nom dades dins de m01.
03) Posar dins de files els següents fitxers:
	informe.txt carta01.txt carta02.txt carta3.txt carta4.txt
	carta05.txt	carta06.txt
	.oculta		.ocultba	.ocultbc	.ocultbe
	- manls (és el fitxer tar.gz que conté la primera pàgina de manual 
	de l'ordre ls. Atenció, cal assignar-li aquest nom.
	- manpwd (és el fitxer tar.gz que conté la primera pàgina de manual
	de l'ordre pwd. Atenció, cal assignar-li aquest nom)

part2:
------------------------------------------------------------------------
0x) situar-se al directori files i fer TOTES les ordres des d'aquest directori.
05) mostra el contingut del fitxer carta01.txt
06) mostra el contingut dels fitxers carta01.txt i carta3.txt i informe.txt
07) genera un fitxer de nom concatenat.txt amb els tres fitxers anteriors
08) mou el fitxer carta01.tx i informe.txt al directori dades
09) mou els fitxers carta02.txt i carta3.txt al directori m01
10) modifica el nom del fitxer carta4 i anomena'l treball4
11) modifica el nom del directori files per el nom sources

part3:
------
12) copia el fitxer  carta05.txt i .oculta al directori dades
13) copia el fitxer carta06.txt al directori m01 i anomena'l treball6
14) copia tots els fitxers de sources a dades
15) copia el directori sources (tot sencer) a dins de m01/binaris
16) esborra els fitxers (tots) del directori sources però no el directori.
17) esborra el directori sources
18) esborra el directori binaris i tot el que conté.



