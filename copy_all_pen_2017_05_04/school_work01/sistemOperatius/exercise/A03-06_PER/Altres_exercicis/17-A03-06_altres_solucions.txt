******************************************************************************
  M01-ISO Sistemes Operatius
  UF2: Gestió de la informació i recursos de xarxa
******************************************************************************
  A03-06-PER i processament de text
  Exercici 17: exercicis d'altres filtres
					 4) split, diff, cmp, comp, sdiff, gzip, gunzip
					 5) file
******************************************************************************
------------------------------------------------------------------------
split, diff, cmp, comp, sdiff, gzip, gunzip
------------------------------------------------------------------------
01) Comptar el numero de linies del fitxer llistatc.txt.
	$ cat llistatc.txt | wc -l  --> 13 linies
       
02) Fer-ne tres parts de 5 linies cada part com a maxim.       
	$ split -l 5 llistatc.txt 
    genera els fitxers: xaa xab xac
       
03) Veure el contingut de xaa de xab i de xac
	$ cat xaa xab xac
       
04) Generar un fitxer de nom xtot amb el resultat d'ajuntar els 
    tres fitxers anteriors
    $ cat xaa xab xac > xtot
      
05) Comprobar que xtot i llistatc.txt son identics.
	$ diff llistatc.txt xtot
    $ cmp
    $ sdiff
       
06) Generar tres parts del fitxer llistatc.txt de noms NOMaa 
	NOMab i NOMac       
	$ split -l 5 llistatc.txt NOM

07) Comprimir el fitxer xtot.
	$ gzip xtot    --> genera el fitxer xtot.gz (123 bytes)
                       no existeix el xtot inicial
       
08) De quin tipus es aquest fitxer?
	$ file xtot.gz       
              
09) Fer-ne tres parts de 125 bytes cada part
	$ split -b 125 xtot.gz
       
10) Tornar a ajuntar les parts en un fitxer de nom: nou.gz
	$ cat xaa xab xac > nou.gz
       
11) Comprobar que nou.gz i xtot.gz son identics
	$ diff xtot.gz nou.gz
       
12) Descomprimir nou.gz i xtot.gz
	$ gunzip nou.gz  --> nou
    $ gunzip xtot.gz --> xtot
                     --> desaparix el .gz i es genera el fitxer original
	
13) Comprobar que nou i xtot son el mateix fitxer
	$ diff nou xtot

14) echo "Dividir un fitxer en parts segons una unica paraula clau"
	$ csplit -k llista2.txt '/capitol/' '{10}'

15) echo "Dividir un fitxer en parts segons diverses claus"
	$ csplit -k llista2.txt '/clau0/'  '/clau1/' '/clau2/'  '{10}'

------------------------------------------------------------------------
file
------------------------------------------------------------------------
16) Llistar tots els noms dels fitxers del directori actiu 
    que son del tipus ascii
	$ file `ls` | grep ASCII
	$ ls | xargs file | grep ASCII 
               
17) LListar el nom dels fitxers ASCII del directori x.

18) LListar el tipus de fitxers dels directoris x, y, z.

19) Indicar el numero total de fitxers del tipus a del directori x.

20) Indicar el numero total de fitxers del tipus a o del 
    tipus b del directori x.
   
21) Indicar el numero de fitxers del tipus (llistat_de_tipus) 
    del directori x.






