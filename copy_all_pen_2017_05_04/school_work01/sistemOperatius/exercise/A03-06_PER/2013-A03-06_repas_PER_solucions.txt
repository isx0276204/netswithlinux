******************************************************************************
  M01-ISO Sistemes Operatius
  UF2: Gestió de la informació i recursos de xarxa
******************************************************************************
  A03-06-PER i processament de text
  2013 REPAS PER
******************************************************************************
------------------------------------------------------------------------
PER i filtres
------------------------------------------------------------------------
01) Del resultat de fer un head de les 15 primeres línies del fitxer
	/etc/passwd mostrar les línies que contenen un 2 en algun lloc.
	$ head -15 /etc/passwd | grep 2
	daemon:x:2:2:daemon:/sbin:/sbin/nologin
	mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
	games:x:12:100:games:/usr/games:/sbin/nologin

02) Del resultat de fer un head de les 15 primeres línies del fitxer
	/etc/passwd mostrar només les línies que tenen uid 2.
	$ head -15 /etc/passwd | grep "^[^:]*:[^:]*:2:"
	daemon:x:2:2:daemon:/sbin:/sbin/nologin

03) Usant grep valida si un dni té el format apropiat.
	$ echo "12345678A" | grep -E "^[0-9]{8}[A-Za-z]$"
	12345678A

04) Usant grep valida si una data té el format dd-mm-aaaa.
	$ echo "31-01-2001" | grep -E "^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
	31-01-2001

05) Usant grep valida si una data té el format dd/mm/aaaa.
	$ echo "31/01/2001" | grep -E "^[0-9]{2}/[0-9]{2}/[0-9]{4}$"
	31-01-2001

	$ echo "31/01/2001" | grep "^[0-9]\{2\}/[0-9]\{2\}/[0-9]\{4\}$"
	31/01/2001

06) Usant grep validar si una data té un format vàlid. Els formats poden
	ser: dd-mm-aaaa o dd/mm/aa.
	$ echo "31/01/2001" | grep -E "^[0-9]{2}[/-][0-9]{2}[/-][0-9]{4}$"
	31/01/2001
	$ echo "31-01-2001" | grep -E "^[0-9]{2}[/-][0-9]{2}[/-][0-9]{4}$"
	31-01-2001

07) Buscar totes les línies del fitxer noms1.txt que tenen la cadena
	Anna o la cadena Jordi
	$ grep -E "Anna|Jordi" noms1.txt 
	Jordi	Puig	Barcelona	1
	Anna	Puig	Girona		2
	Anna	Mas	Barcelona	4
	Anna	Puig	Barcelona	5

08) Substituir del fitxer noms1.txt tots els noms Anna i Jordi per -nou-.
	$ sed -r 's/Anna|Jordi/-nou-/g' noms1.txt 
	nom	cognom	ciutat		codi
	-nou-	Puig	Barcelona	1
	-nou-	Puig	Girona		2
	Zeus	Mas	Hospitalet	3
	-nou-	Mas	Barcelona	4
	-nou-	Puig	Barcelona	5

09) Del resultat de fer un head (10 línies) del fitxer /etc/passwd
	substituir '/bin/nologin' per '-noshell'.
	*nota: atenció a com escapeu l'slash '/'
	$ head /etc/passwd | sed 's/\/sbin\/nologin/-noshell-/' 
	root:x:0:0:root:/root:/bin/bash
	bin:x:1:1:bin:/bin:-noshell-
	daemon:x:2:2:daemon:/sbin:-noshell-
	adm:x:3:4:adm:/var/adm:-noshell-
	lp:x:4:7:lp:/var/spool/lpd:-noshell-
	sync:x:5:0:sync:/sbin:/bin/sync
	shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
	halt:x:7:0:halt:/sbin:/sbin/halt
	mail:x:8:12:mail:/var/spool/mail:-noshell-
	uucp:x:10:14:uucp:/var/spool/uucp:-noshell-

10) Ídem que l'exercici anteior però fent la substitució només
	de les línies 4 a la 8.
	$ head /etc/passwd | sed '4,8 s/\/sbin\/nologin/-noshell-/' 
	root:x:0:0:root:/root:/bin/bash
	bin:x:1:1:bin:/bin:/sbin/nologin
	daemon:x:2:2:daemon:/sbin:/sbin/nologin
	adm:x:3:4:adm:/var/adm:-noshell-
	lp:x:4:7:lp:/var/spool/lpd:-noshell-
	sync:x:5:0:sync:/sbin:/bin/sync
	shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
	halt:x:7:0:halt:/sbin:/sbin/halt
	mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
	uucp:x:10:14:uucp:/var/spool/uucp:/sbin/nologin

11) Ídem exercici anterior però fent les substitucions des de la línia
	que conté admin fins la línia que conté halt.
	$ head /etc/passwd | sed '/adm/,/halt/ s/\/sbin\/nologin/-noshell-/' 
	root:x:0:0:root:/root:/bin/bash
	bin:x:1:1:bin:/bin:/sbin/nologin
	daemon:x:2:2:daemon:/sbin:/sbin/nologin
	adm:x:3:4:adm:/var/adm:-noshell-
	lp:x:4:7:lp:/var/spool/lpd:-noshell-
	sync:x:5:0:sync:/sbin:/bin/sync
	shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
	halt:x:7:0:halt:/sbin:/sbin/halt
	mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
	uucp:x:10:14:uucp:/var/spool/uucp:/sbin/nologin

------------------------------------------------------------------------
CUT
------------------------------------------------------------------------
12) Retallar del caracter 2 al 10 del d'un llistat llarg
	$ ls -la | cut -c 2-10

13) Retallar el nom d'usuari i el del fitxer. Primer cal normalitzar.
	b = espai en blanc
	$ ls -la | sed 's/bb*/b/g' | cut -f5,8 -d'b'
	         | tr -s 'b' 'b'   |
	         | tr -s [:blank:] |
	           normalitzar
	           
14) Retallar permisos, nom d'usuari i nom de fitxer.
	b = espai en blanc
	$ ls -la | sed 's/bb*/b/g' | cut -f1,5,8 -d'b' | cut -c2-
	           normalitzar                         només-permisos
	o bé  x | x | cut -c2-20 -f5,8
	
15) Retallar del fitxer llistatc.txt (o de un llistat llarg) 
    del primer al camp n, tot variant n de 1 al final.
    $ cut -f1-1 -d' ' llistatc.txt
    $ cut -f1-2 -d' ' llistatc.txt       
    $ cut -f1-3 -d' ' llistatc.txt   
    $ cut -f1-4 -d' ' llistatc.txt   
   
   Veure que si l'origen no esta normalitzat, els camps no es 
   corresponen entre línies a causa de l'espai com a separador
   de camp.

------------------------------------------------------------------------
SORT
------------------------------------------------------------------------
16) Ordenar per línia en ordre invers el llistat de /boot 
    $ sort -r llistatc.txt   
   
17) Ordenar número d'enllaços per magnitud.   
    $ sort +1n   <normalitzat>
           links
    
18) Ordenar per nom de fitxer.
    $ sort +8    <normalitzat>
           noms
    
19) Ordenar per mida de fitxer.
    $ sort +4n    <normalitzat>
           mida
    
20) Ordenar per nom de fitxer, usuari, grup i mida.
    $ sort +8   +2      +3     +4n    <normalitzat>
           nom  usuari  grup   mida
    

------------------------------------------------------------------------
filtres
------------------------------------------------------------------------
01) Compta quants usuaris hi ha connectats al sistema actualment.
	$ who | wc-l

02) Llista quins usuaris hi ha connectats al sistema i quantes connexions tenen establertes. Cal mostrar el login i el numero de connexions per a cada usuari ordenat de major a menor nombre de connexions.
	$ who | cut -f1 -d' ' | sort | uniq -c | sort -r -k1n,1 

03) Crea un directori de nom provaDirectori. Si no es pot crear cal mostrar un missatge d'error.
	$ mkdir provaDirectori || echo "tururut"

04) Normalitzar la taules de la base de dades training de nom clientes.dat usant ':' com a delimitador.
    (fer-ho tamné de les altres taules)
	$ sed 's/\t/:/g' clientes.dat > clientes.txt

05) Mostra el número d'oficina on treballa l'empleat 109.
	$ grep '^109:' repventas.txt | cut -f4 -d: 

06) Mostrar el número del director de l'oficina 11.
	$ grep '^11:' oficinas.txt | cut -f4 -d: 


