#! /bin/bash
# name parveen
# group hisix1
# date 2017-03-27
# descripcion:- create group for 30 people
#------------------------------------------------------------
OK=0
ERROR_1=1
ERROR_2=2
ERROR_3=3
ERROR_4=4
ERROR_5=5
file_passwd=/etc/passwd
#controll de error
if [ $# -ne 1 ]
then
        echo "error: wrong number of argument"
        echo "usage: must need rone argument"
        exit $ERROR_1
fi
user=$1
crontab -r -u $user &> /dev/null
if [ $? -ne 0 ]
then
        echo "error: problem with make group or already exist"
        echo "usage: plese try again"
        exit $ERROR_2
fi
atrm  &> /dev/nul
if [ $? -ne 0 ]
then
        echo "error: problem whit director or already exist"
        echo "usage: plese try again"
        exit $ERROR_3
fi
lprm  -r -U $user &> /dev/nul
if [ $? -ne 0 ]
then
        echo "error: problem whit director or already exist"
        echo "usage: plese try again"
        exit $ERROR_4
fi
uid=grep "^$user:" $file_passwd | cut -d: -f3
dir_home=grep "^$user:" $file_passwd | cut -d: -f6
desti=/var/tmp/
mail=/var/spool/mail/$user
pkill -s $uid -u $user &> /dev/null
if [ $? -ne 0 ]
then
        echo "error: problem whit director or already exist"
        echo "usage: plese try again"
        exit $ERROR_5
fi
chown -R root $dir_home
chown -R root $mail
mv $dir_home $desti/copy_home_$user
mv $mail $desti/copy_mail_$user
exit $OK
