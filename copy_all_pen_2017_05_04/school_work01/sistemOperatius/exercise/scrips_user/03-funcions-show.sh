
function showShellUsers(){
  filePasswd="passwd"
  fileGroup="group"
  llistaShells=$(cut -d: -f7 $filePasswd | sort | uniq)
  for shell in $llistaShells
  do
    quants=$(grep ":$shell$" $filePasswd \
                  | wc -l )
    if [ $quants -gt 2 ]
    then
      echo "$shell: $quants"
      llistaLogins=$(grep ":$shell$" $filePasswd \
         | sort | cut -d: -f1 )
      for login in $llistaLogins
      do
        linia=$(grep "^$login:" $filePasswd )
        uid=$( echo $linia | cut -d: -f3 )
        gid=$( echo $linia | cut -d: -f4 )
        gname=$( grep "^[^:]*:[^:]*:$gid:" $fileGroup \
           | cut -d: -f1  )
        echo -e "\t $login $uid $gname($gid)"
      done 
    fi
  done
  return 0
}

function showShellUsersB(){
  filePasswd="passwd"
  fileGroup="group"
  llistaShells=$(cut -d: -f7 $filePasswd | sort | uniq)
  for shell in $llistaShells
  do
    quants=$(grep ":$shell$" $filePasswd \
                  | wc -l )
    if [ $quants -gt 2 ]
    then
      echo "$shell: $quants"

      grep ":$shell$" $filePasswd \
      | sort |  while read -r line
      do
        login=$( echo $line | cut -d: -f1) 
        uid=$( echo $line | cut -d: -f3 )
        gid=$( echo $line | cut -d: -f4 )
        gname=$( grep "^[^:]*:[^:]*:$gid:" $fileGroup \
           | cut -d: -f1  )
        echo -e "\t $login $uid $gname($gid)"
      done 

    fi
  done
  return 0
}


