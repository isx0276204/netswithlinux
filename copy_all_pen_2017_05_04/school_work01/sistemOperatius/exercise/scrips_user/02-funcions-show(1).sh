#! /bin/bash

function showUserGecos(){
  #showUserGecos(login)
  fileIn="passwd"
  if [ $# -ne 1 ]
  then
    echo "error falta argument login"
    echo "usage: showuser login2"
    return 1
  fi
  login=$1
  linia=""
  linia=$(grep "^$login:" $fileIn)
  if [ -z "$linia" ]
  then
    echo "error login inexistent"
    echo "usage: showuser login2"
    return 2
  fi
  gecos=$(echo $linia | cut -d: -f5)
  name=$(echo $gecos | cut -d, -f1)
  office=$(echo $gecos | cut -d, -f1)
  phoneOffice=$(echo $gecos | cut -d, -f1)
  phoneHome=$(echo $gecos | cut -d, -f1)
  echo "office: $office"
  echo "phone-office: $phoneOffice"
  echo "phone-home: $phoneHome"
}

function showUserPasswd(){
  #showUserPasswd(login)
  fileIn="passwd"
  if [ $# -ne 1 ]
  then
    echo "error falta argument login"
    echo "usage: showuser login2"
    return 1
  fi
  login=$1
  linia=""
  linia=$(grep "^$login:" $fileIn)
  if [ -z "$linia" ]
  then
    echo "error login inexistent"
    echo "usage: showuser login2"
    return 2
  fi
  passwd=$(echo $linia | cut -d: -f2)
  uid=$(echo $linia | cut -d: -f3)
  gid=$(echo $linia | cut -d: -f4)
  gecos=$(echo $linia | cut -d: -f5)
  homedir=$(echo $linia | cut -d: -f6)
  shell=$(echo $linia | cut -d: -f7)
  echo "login: $login"
  echo "passwd: $passwd"
  echo "uid: $uid"
  echo "gid: $gid"
  echo "gecos: $gecos"
  echo "homedir: $homedir"
  echo "shell: $shell"
}

function showGroup(){
  #showGroup(gname)
 fileIn="group"
  if [ $# -ne 1 ]
  then
    echo "error falta argument login"
    echo "usage: showgroup login2"
    return 1
  fi
  gname=$1
  linia=""
  linia=$(grep "^$gname:" $fileIn)
  if [ -z "$linia" ]
  then
    echo "error login inexistent"
    echo "usage: showgroup login2"
    return 2
  fi
  passwd=$(echo $linia | cut -d: -f2)
  gid=$(echo $linia | cut -d: -f3)
  listUsers=$(echo $linia | cut -d: -f3)
  echo "gname: $gname"
  echo "passwd: $passwd"
  echo "gid: $gid"
  echo "listusers: $listUsers"
}

function showUser(){
 filePasswd="passwd"
 fileGroup="group"
  if [ $# -ne 1 ]
  then
    echo "error falta argument login"
    echo "usage: showuser login2"
    return 1
  fi
  login=$1
  linia=""
  linia=$(grep "^$login:" $filePasswd)
  if [ -z "$linia" ]
  then
    echo "error login inexistent"
    echo "usage: showuser login2"
    return 2
  fi
  uid=$(echo $linia | cut -d: -f3)
  gid=$(echo $linia | cut -d: -f4)
  shell=$(echo $linia | cut -d: -f7)
  gname=$(grep "^[^:]*:[^:]*:$gid:" $fileGroup | cut -d: -f1)
  echo "login: $login"
  echo "uid: $uid"
  echo "grup: $gname($gid)"
  echo "shell: $shell"
}

function showGroupMainMembers(){
  #showGroupMainMembers(gname)
  fileGroup="group"
  filePasswd="passwd"
  if [ $# -ne 1 ]
  then
    echo "error falta argument login"
    echo "usage: showgroup login2"
    return 1
  fi
  gname=$1
  gid=""
  gid=$(grep "^$gname:" $fileGroup | cut -d: -f3)
  if [ -z "$gid" ]
  then
    echo "error gname inexistent"
    echo "usage: show... gname"
    return 2
  fi
  grep "^[^:]*:[^:]*:[^:]*:$gid:" $filePasswd \
    | cut -d: -f1,3,4,7
}

function showGroupMembersAll(){
  #showGroupMembersAll(gname)
 fileGroup="group"
  filePasswd="passwd"
  if [ $# -ne 1 ]
  then
    echo "error falta argument login"
    echo "usage: showgroup login2"
    return 1
  fi
  gname=$1
  linia=""
  linia=$(grep "^$gname:" $fileGroup)
  if [ -z "$linia" ]
  then
    echo "error gname inexistent"
    echo "usage: show... gname"
    return 2
  fi
  gid=$( echo $linia | cut -d: -f3)
  llistaUsers=$( echo $linia | cut -d: -f4)
  grep "^[^:]*:[^:]*:[^:]*:$gid:" $filePasswd \
    | cut -d: -f1,3,4,7
  echo $llistaUsers
}


