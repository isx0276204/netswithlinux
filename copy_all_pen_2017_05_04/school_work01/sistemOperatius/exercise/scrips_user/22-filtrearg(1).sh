#! /bin/bash
# Raul Baena
# Filtrar args
# prog arg[..]
#Validar que exiteix almenys 1 arg
llista=$*
numok=0
for arg in $llista
do
 echo "$arg" | egrep "[a-z]{4}" &> /dev/null 
 if [ $? -eq 0 ]  
 then
  echo "$arg"
  numok=$((numok+1))
 fi
done
echo $numok
exit 0
