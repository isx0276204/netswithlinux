******************************************************************************
  M01-ISO Sistemes Operatius
  UF1: Linux Usuari: Instal·lació, configuració i explotació d'un sistema
                     informàtic
******************************************************************************
  A01-04-Redireccionaments,pipes, filtres, variables i shell bàsic
  Exercici 11: pipes, redireccionaments i variables
	- pipes
	- redireccionamnets
	- variables
******************************************************************************

==============================================================================
Ordre: pipes i redireccion
==============================================================================
00) situar-se al directori actiu /tmp/m01 i realizat TOTES les ordres des d'aquest directori (usuari hisx).
	$ cd /tmp/m01

01) llistar el número major i el número menor dels dispositius corresponents a la entrada estàndard, sortida estàndard i d'error. Seguir el lnk fins identificar el device real on esta lligat
	$ ls -l /dev/std*
	lrwxrwxrwx 1 root root 15  5 nov 07:56 /dev/stderr -> /proc/self/fd/2
	lrwxrwxrwx 1 root root 15  5 nov 07:56 /dev/stdin -> /proc/self/fd/0
	lrwxrwxrwx 1 root root 15  5 nov 07:56 /dev/stdout -> /proc/self/fd/1
	$ ls -l /proc/self/fd/2
	lrwx------. 1 user1 user1 64  5 nov 23:01 /proc/self/fd/2 -> /dev/pts/2
	$ ls -l /dev/pts/2 
	crw--w---- 1 ecanet tty 136, 2  5 nov 23:01 /dev/pts/2

02) Desar en un fitxer de nom http.txt tots els serveis que continguin la cadena http.
	$ cat /etc/services | grep http > http.txt

03) Desar en un fitxer de nom http.txt tots els serveis que continguin la cadena http però que al mateix temps es mostri per pantalla.
	$ cat /etc/services | grep http | tee  http.txt	

04) Desar en un fitxer de nom ftp.txt el llistat de tots els serveis que contenen la cadena ftp ordenats lexicogràficament. La sortida s'ha de mostrar simultàniament per pantalla
	$ cat /etc/services | grep ftp | sort | tee ftp.txt
	
05) Idem exercici anterior però mostrant per pantalla únicament quants serveis hi ha.
	$ cat /etc/services | grep ftp | sort | tee ftp.txt | wc -l
	54

06) Idem anterior però comptant únicament quants contenen la descrició TLS/SSL
	$ cat /etc/services | grep ftp | sort | tee ftp.txtq | grep "TLS/SSL"
	ftps            990/tcp            # ftp protocol, control, over TLS/SSL
	ftps            990/udp            # ftp protocol, control, over TLS/SSL
	ftps-data       989/tcp            # ftp protocol, data, over TLS/SSL
	ftps-data       989/udp            # ftp protocol, data, over TLS/SSL
	odette-ftps     6619/tcp           # ODETTE-FTP over TLS/SSL
	odette-ftps     6619/udp           # ODETTE-FTP over TLS/SSL
	
    $ cat /etc/services | grep ftp | sort | tee ftp.txt | \ 
      grep "TLS/SSL" | wc -l
	6

07) Llista l'ocupació d'espai del directori tmp fent que els missatges d'error s'ignorin.
	$ du /tmp/ 2> /dev/null

08) Idem anterior desant el resultat a disc.txt i ignorant els errors.
	$ du /tmp/ > disc.txt 2> /dev/null

09) Idem enviant tota la sortida (errors i dades) al fitxer disc.txt
	$ du /tmp/ > disc.txt 2>&1

10) Afegir al fitxer disc.txt el sumari de l'ocupació de disc dels directoris /boot i /mnt. Els errors cal ignorar-los
	$ du -s /boot /mnt >> disc.txt 2> /dev/null

11) Anomana per a què serveixen les variables:
	HOME, PWD, UID, EUID, HISTFILE; HISTFILESIZE, DISPLAY, SHELL, 
	HOSTNAME, HOSTYPE,LANG, PATH, PPID, PS1, PS2, TERM, USERS

12) Assigna a la variable NOM el teu nom complert (nom i cognoms) i assegura't que s'exporta als subprocessos.
	$ export NOM="pere pou prat"
	$ env | grep NOM
	NOM=pere pou prat
	$ bash
	$ echo $NOM
	pere pou prat

13)Assigna el prompt un format que mostri la hora, la ruta absoluta del directori actiu i el nom d'usuari.
	$ PS1='\t  \w  \h \$'

14) Assigna al prompt el format on mostra el nom d'usuari, de host, el número d'ordre i el número en l'històric d'ordres.
	$PS1='\u \# \! \$ '
	user1 15 14 $ 



