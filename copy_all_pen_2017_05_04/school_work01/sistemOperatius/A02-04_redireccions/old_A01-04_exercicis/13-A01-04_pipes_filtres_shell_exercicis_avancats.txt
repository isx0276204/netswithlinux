******************************************************************************
  M01-ISO Sistemes Operatius
  UF1: Linux Usuari: Instal·lació, configuració i explotació d'un sistema
                     informàtic
******************************************************************************
  A01-04-Redireccionaments,pipes, filtres, variables i shell bàsic
  Exercici 13: exercicis globals avançats
******************************************************************************

01) Compta quants usuaris hi ha connectats al sistema actualment.


02) Llista quins usuaris hi ha connectats al sistema i quantes connexions tenen establertes. Cal mostrar el login i el numero de connexions per a cada usuari ordenat de major a menor nombre de connexions.


03) Crea un directori de nom provaDirectori. Si no es pot crear cal mostrar un missatge d'error.


04) Normalitzar les taules de la base de dades training usant ':' com a delimitador.


05) Mostra el número d'oficina on treballa l'empleat 109.


06) Mostrar el número del director de l'oficina 11.


07) Mostrar el número d'empleat del director de l'oficina del treballador 109.


08) Mostrar el numero i nom de l'empleat 109, el numero i la ciutat de l'oficina on treballa i també el número i el nom del director d'aquesta oficina.
    num_empl+nombre(empleat)    oficina+ciudad(oficinas) \
    num_empl+nombre(director)


09) Quina oficina (número i nom) dirigueix l'empleat 106?


10) Per a cada empleat de repventas mostrar el número d'oficina on treballa i el número d'empleat del director de l'oficina on treballa.
    num_empl(empleat)  oficina(numero)  dir(director de l'oficina)


11) Idem mostrant el nom del director de l'oficina on treballa cada empleat.
    num_empl(empleat)  oficina(numero)  nombre(director de l'oficina) 


12) Idem mostrant també la ciutat de l'oficina.
    num_empl(empleat)  oficina(numero) ciudad(oficina)  
    nombre(director de l'oficina) 


13) Posar a l'empleat 109 una quota de 150000


14) Assignar a l'oficina 11 l'objectiu 300000.


15) A totes les oficines de 'Este' duplicar el seu objectiu.
    Solució incorrecte:



