******************************************************************************
  M01-ISO Sistemes Operatius
  UF1: Linux Usuari: Instal·lació, configuració i explotació d'un sistema
                     informàtic
******************************************************************************
  A02-05-processos
  Exercici 15: exercicis de processos i tasques automatitzades
                     -- prcessos i PID
                     -- tasques en segon pla: jobs
                     -- tasques automatitzades
******************************************************************************
------------------------------------------------------------------------
Processos
------------------------------------------------------------------------
01) Mostrar tots els processos del sistema.

##
ps ax


02) Mostrar tot l'arbre de processos incloent el pid i la ordre.

##
[isx0276204@j01 ~]$ pstree -lap 1783
bash,1783
  └—pstree,2176 -lap 1783
[isx0276204@j01 ~]$

    
03) Prova les ordres: ps, ps a, ps x, ps ax, ps -fu pere, ps -Fu pere.
##


04) Llistar els processos  fent un llistat llarg. el PID i el PPID.
##
ps axp | less

05) Entrar en un subshell i fer un llistat llarg dels processos. 
    Identificar el PID del procés pare del shell actual.

##
[isx0276204@j01 ~]$ pstree -spla 1783
systemd,1 --switched-root --system --deserialize 24
  └—login,1754
      └—bash,1783
          └—bash,2451
              └—pstree,2542 -spla 1783


06) Identifica el PID del procés init usant l'ordre pidof.
##
[isx0276204@j01 ~]$ pidof systemd
2272 1100 802 1


07) identifica el pid del servei d'impressió cupsd amb l'ordre pidof.
##
[isx0276204@j01 ~]$ pidof cupsd
1422


08) Usant l'ordre pgrep llista els processos de l'usuari root.
##
[isx0276204@j01 ~]$ pgrep -u root


09) usant l'ordre pgrep localitza el procés init.

##
[isx0276204@j01 ~]$ pgrep systemd
1
407
449
600
773
1075
[isx0276204@j01 ~]$



10) Utilitzant l'ordre fuser per saber quins processos utilitzen el 
    directori /tmp.
##
[isx0276204@j01 prova]$ fuser -mauv /tmp/
                     USER        PID ACCESS COMMAND
/tmp:                root     kernel mount (root)/tmp
                     isx0276204   1234 F.... (isx0276204)pulseaudio
[isx0276204@j01 prova]$


11) Llista tots els senyals generables amb l'ordre kill.

##
[isx0276204@j01 prova]$ kill -l
 1) SIGHUP       2) SIGINT       3) SIGQUIT      4) SIGILL       5) SIGTRAP
 6) SIGABRT      7) SIGBUS       8) SIGFPE       9) SIGKILL     10) SIGUSR1
11) SIGSEGV     12) SIGUSR2     13) SIGPIPE     14) SIGALRM     15) SIGTERM
16) SIGSTKFLT   17) SIGCHLD     18) SIGCONT     19) SIGSTOP     20) SIGTSTP
21) SIGTTIN     22) SIGTTOU     23) SIGURG      24) SIGXCPU     25) SIGXFSZ
26) SIGVTALRM   27) SIGPROF     28) SIGWINCH    29) SIGIO       30) SIGPWR
31) SIGSYS      34) SIGRTMIN    35) SIGRTMIN+1  36) SIGRTMIN+2  37) SIGRTMIN+3
38) SIGRTMIN+4  39) SIGRTMIN+5  40) SIGRTMIN+6  41) SIGRTMIN+7  42) SIGRTMIN+8
43) SIGRTMIN+9  44) SIGRTMIN+10 45) SIGRTMIN+11 46) SIGRTMIN+12 47) SIGRTMIN+13
48) SIGRTMIN+14 49) SIGRTMIN+15 50) SIGRTMAX-14 51) SIGRTMAX-13 52) SIGRTMAX-12
53) SIGRTMAX-11 54) SIGRTMAX-10 55) SIGRTMAX-9  56) SIGRTMAX-8  57) SIGRTMAX-7
58) SIGRTMAX-6  59) SIGRTMAX-5  60) SIGRTMAX-4  61) SIGRTMAX-3  62) SIGRTMAX-2
63) SIGRTMAX-1  64) SIGRTMAX


12) Genera un procés sleep 1000 i mata'l amb kill.

##
[isx0276204@j01 prova]$ sleep 1000 &
[1] 3295
[isx0276204@j01 prova]$ kill -15 3295
[isx0276204@j01 prova]$ ps
  PID TTY          TIME CMD
 2800 tty3     00:00:00 bash
 3297 tty3     00:00:00 ps
[1]+  Terminated              sleep 1000
[isx0276204@j01 prova]$

13) Mata el bash actual.

##
[isx0276204@j01 prova]$ bash
[isx0276204@j01 prova]$ ps
  PID TTY          TIME CMD
 2800 tty3     00:00:00 bash
 3311 tty3     00:00:00 bash
 3339 tty3     00:00:00 ps
[isx0276204@j01 prova]$ kill -15 2800
[isx0276204@j01 prova]$ ps
  PID TTY          TIME CMD
 2800 tty3     00:00:00 bash
 3311 tty3     00:00:00 bash
 3340 tty3     00:00:00 ps

Last login: Thu Dec 15 11:39:09 on tty4
[isx0276204@j01 ~]$ ps
  PID TTY          TIME CMD
 3383 tty3     00:00:00 bash
 3418 tty3     00:00:00 ps
[isx0276204@j01 ~]$ kill -9 2800


14) Llista tots els processos mingetty i mata'ls de cop tots usant u
    na sola ordre kill.

##


15) genera 3 processos sleeep 10000 i mata'ls tots de cop usant killall.

##

[isx0276204@j01 ~]$ sleep 1000 &
[1] 3609
[isx0276204@j01 ~]$ sleep 1000 &
[2] 3610
[isx0276204@j01 ~]$ sleep 1000 &
[3] 3611
[isx0276204@j01 ~]$ ps
  PID TTY          TIME CMD
 3383 tty3     00:00:00 bash
 3441 tty3     00:00:00 bash
 3609 tty3     00:00:00 sleep
 3610 tty3     00:00:00 sleep
 3611 tty3     00:00:00 sleep
 3612 tty3     00:00:00 ps
[isx0276204@j01 ~]$ killall sleep
[1]   Terminated              sleep 1000
[isx0276204@j01 ~]$ ps
  PID TTY          TIME CMD
 3383 tty3     00:00:00 bash
 3441 tty3     00:00:00 bash
 3621 tty3     00:00:00 ps
[2]-  Terminated              sleep 1000
[3]+  Terminated              sleep 1000
[isx0276204@j01 ~]$

------------------------------------------------------------------------
Jobs
------------------------------------------------------------------------
16) executa tres ordres sleep en segon pla i llista els treballs.

##
[isx0276204@j01 ~]$ sleep 10000 &
[1] 3474
[isx0276204@j01 ~]$ sleep 10000 &
[2] 3481
[isx0276204@j01 ~]$ sleep 10000 &
[3] 3488
[isx0276204@j01 ~]$ ps
  PID TTY          TIME CMD
 3356 pts/1    00:00:00 bash
 3474 pts/1    00:00:00 sleep
 3481 pts/1    00:00:00 sleep
 3488 pts/1    00:00:00 sleep
 3495 pts/1    00:00:00 ps
[isx0276204@j01 ~]$ 

17) Inicia l'edició d'un fitxer amb vi i deixa'l suspès d'execució en 
    segon pla. Mostrar els treballs.
##
[isx0276204@j01 prova]$ jobs
[1]   Running                 sleep 10000 &  (wd: ~)
[2]   Running                 sleep 10000 &  (wd: ~)
[3]   Running                 sleep 10000 &  (wd: ~)
[4]+  Stopped                 vim hello.txt
[5]-  Stopped                 sleep 100000


18) Mata el segon dels treballs (un sleep)

##
[isx0276204@j01 prova]$ jobs 
[1]   Running                 sleep 10000 &  (wd: ~)
[2]   Running                 sleep 10000 &  (wd: ~)
[3]   Running                 sleep 10000 &  (wd: ~)
[4]+  Stopped                 vim hello.txt
[5]-  Stopped                 sleep 100000
[isx0276204@j01 prova]$ kill %3
[3]   Terminated              sleep 10000  (wd: ~)
(wd now: /tmp/prova)
[isx0276204@j01 prova]$ jobs 
[1]   Running                 sleep 10000 &  (wd: ~)
[2]   Running                 sleep 10000 &  (wd: ~)
[4]+  Stopped                 vim hello.txt
[5]-  Stopped                 sleep 100000
[isx0276204@j01 prova]$ 


19) Passa a primer pla el primer dels treballs (un sleep), i mata'l amb 
    ctrl+c.
##
[isx0276204@j01 prova]$ jobs 
[1]   Running                 sleep 10000 &  (wd: ~)
[2]   Running                 sleep 10000 &  (wd: ~)
[4]+  Stopped                 vim hello.txt
[5]-  Stopped                 sleep 100000
[isx0276204@j01 prova]$ fg %1
sleep 10000	(wd: ~)
^C
[isx0276204@j01 prova]$ jobs 
[2]-  Running                 sleep 10000 &  (wd: ~)
[4]+  Stopped                 vim hello.txt
[5]   Stopped                 sleep 100000
[isx0276204@j01 prova]$ 


20) Passa a primer pla el treball més recent. Quin és. Acabar.
##
[isx0276204@j01 prova]$ fg +
vim hello.txt

    
21) Llistar tota l'estructura de directoris partint de l'arrel. Que no 
    es generin missatges d'error i enviar la sortida a null (no volem 
    desar res és només per fer-lo treballar!).
    Un cop iniciat aturar el proces. Llistar els treballs.
##
[isx0276204@j01 prova]$ tree / &> /dev/null
^Z
[6]+  Stopped                 tree / &> /dev/null
[isx0276204@j01 prova]$ bg +
[6]+ tree / &> /dev/null &
[isx0276204@j01 prova]$ ps -S
  PID TTY      STAT   TIME COMMAND
 1115 tty2     Ssl+   0:00 /usr/libexec/gdm-x-session --run-script gn
 1118 tty2     Sl+    0:52 /usr/libexec/Xorg vt2 -displayfd 3 -auth /
 1134 tty2     Sl+    0:00 /usr/libexec/gnome-session-binary
 1229 tty2     Sl+    0:50 /usr/bin/gnome-shell
 1257 tty2     Sl     0:00 ibus-daemon --xim --panel disable
 1262 tty2     Sl     0:00 /usr/libexec/ibus-dconf
 1264 tty2     Sl     0:00 /usr/libexec/ibus-x11 --kill-daemon
 1346 tty2     Sl+    0:00 /usr/libexec/gnome-settings-daemon
 1378 tty2     Sl     0:00 /usr/libexec/ibus-engine-simple
 1400 tty2     Sl+    0:00 /usr/libexec/evolution/evolution-alarm-not
 1405 tty2     SNl+   0:00 /usr/libexec/tracker-miner-fs
 1407 tty2     Sl+    0:03 /usr/bin/gnome-software --gapplication-ser
 1408 tty2     Sl+    0:00 /usr/libexec/gsd-printer
 1444 tty2     Sl+    0:00 /usr/bin/seapplet
 1456 tty2     SNl+   0:00 /usr/libexec/tracker-miner-apps
 1464 tty2     Sl+    0:00 abrt-applet
 1496 tty2     SNl+   0:00 /usr/libexec/tracker-miner-user-guides
 1512 tty2     SNl+   0:00 /usr/libexec/tracker-extract
 3302 pts/0    Ss     0:00 bash
 3356 pts/1    Ss     0:13 bash
 3470 pts/0    S+     0:00 vim /run/media/isx0276204/school_work/scho
 3481 pts/1    S      0:00 sleep 10000
 3588 pts/1    T      0:00 sleep 100000
 3887 pts/1    R      0:01 tree /
 3900 pts/1    R+     0:00 ps -S
[isx0276204@j01 prova]$ ps -S
  PID TTY      STAT   TIME COMMAND
 1115 tty2     Ssl+   0:00 /usr/libexec/gdm-x-session --run-script gn
 1118 tty2     Sl+    0:52 /usr/libexec/Xorg vt2 -displayfd 3 -auth /
 1134 tty2     Sl+    0:00 /usr/libexec/gnome-session-binary
 1229 tty2     Sl+    0:50 /usr/bin/gnome-shell
 1257 tty2     Sl     0:00 ibus-daemon --xim --panel disable
 1262 tty2     Sl     0:00 /usr/libexec/ibus-dconf
 1264 tty2     Sl     0:00 /usr/libexec/ibus-x11 --kill-daemon
 1346 tty2     Sl+    0:00 /usr/libexec/gnome-settings-daemon
 1378 tty2     Sl     0:00 /usr/libexec/ibus-engine-simple
 1400 tty2     Sl+    0:00 /usr/libexec/evolution/evolution-alarm-not
 1405 tty2     SNl+   0:00 /usr/libexec/tracker-miner-fs
 1407 tty2     Sl+    0:03 /usr/bin/gnome-software --gapplication-ser
 1408 tty2     Sl+    0:00 /usr/libexec/gsd-printer
 1444 tty2     Sl+    0:00 /usr/bin/seapplet
 1456 tty2     SNl+   0:00 /usr/libexec/tracker-miner-apps
 1464 tty2     Sl+    0:00 abrt-applet
 1496 tty2     SNl+   0:00 /usr/libexec/tracker-miner-user-guides
 1512 tty2     SNl+   0:00 /usr/libexec/tracker-extract
 3302 pts/0    Ss     0:00 bash
 3356 pts/1    Ss     0:13 bash
 3470 pts/0    S+     0:00 vim /run/media/isx0276204/school_work/scho
 3481 pts/1    S      0:00 sleep 10000
 3588 pts/1    T      0:00 sleep 100000
 3887 pts/1    D      0:02 tree /
 3907 pts/1    R+     0:00 ps -S
[isx0276204@j01 prova]$ ps -S
  PID TTY      STAT   TIME COMMAND
 1115 tty2     Ssl+   0:00 /usr/libexec/gdm-x-session --run-script gn
 1118 tty2     Sl+    0:52 /usr/libexec/Xorg vt2 -displayfd 3 -auth /
 1134 tty2     Sl+    0:00 /usr/libexec/gnome-session-binary
 1229 tty2     Sl+    0:50 /usr/bin/gnome-shell
 1257 tty2     Sl     0:00 ibus-daemon --xim --panel disable
 1262 tty2     Sl     0:00 /usr/libexec/ibus-dconf
 1264 tty2     Sl     0:00 /usr/libexec/ibus-x11 --kill-daemon
 1346 tty2     Sl+    0:00 /usr/libexec/gnome-settings-daemon
 1378 tty2     Sl     0:00 /usr/libexec/ibus-engine-simple
 1400 tty2     Sl+    0:00 /usr/libexec/evolution/evolution-alarm-not
 1405 tty2     SNl+   0:00 /usr/libexec/tracker-miner-fs
 1407 tty2     Sl+    0:03 /usr/bin/gnome-software --gapplication-ser
 1408 tty2     Sl+    0:00 /usr/libexec/gsd-printer
 1444 tty2     Sl+    0:00 /usr/bin/seapplet
 1456 tty2     SNl+   0:00 /usr/libexec/tracker-miner-apps
 1464 tty2     Sl+    0:00 abrt-applet
 1496 tty2     SNl+   0:00 /usr/libexec/tracker-miner-user-guides
 1512 tty2     SNl+   0:00 /usr/libexec/tracker-extract
 3302 pts/0    Ss     0:00 bash
 3356 pts/1    Ss     0:13 bash
 3470 pts/0    S+     0:00 vim /run/media/isx0276204/school_work/scho
 3481 pts/1    S      0:00 sleep 10000
 3588 pts/1    T      0:00 sleep 100000
 3887 pts/1    D      0:02 tree /
 3914 pts/1    R+     0:00 ps -S
[isx0276204@j01 prova]$ vim hello.txt 


22) Reanudar l'execució del tree anterior en segon pla.


------------------------------------------------------------------------
Examinar l'Estat 
------------------------------------------------------------------------
23) Executar l'ordre que monitoritza els processos. Llistar-los per 
    prioritat.

##

24) Executar l'ordre vmstat. Descriu almenys tres dels elements dels que 
    informa.


25) Executar l'ordre free i descriure la informació que mostra.


25) Digues quanta estopna fa que el sistema està engegat 
    ininterrumpudament.


26) disgues quina versió de kernel i de harware s'està utilitzant.

    
------------------------------------------------------------------------
tasques periòdiques
------------------------------------------------------------------------
27) Executa d'aquí cinc minuts l'odre date, w, i finger (totes de cop).


28) Executa a les xx en punt l'ordre who.


29) Executa quan la càrrega del sistema ho permerti un llistat de tots 
    els directoris de home. Envia el llistat a /dev/null (per no 
    ocupar). Acaba el llistat amb el missatge FI.


30) Executa cada 5 minuts l'ordre nmap localhost, també cada hora (en el
    minut 15) dels dilluns i els divendres l'ordre df -h.
##
# all crob
*/5 * * * * /usr/bin/nmap localhost
15 * * * 1,5  /usr/bin/du -h
~


