# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 13-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- mirar si es number es capicou or no

#ESPECIFICACIONS D'ENTRADA:-

########################################################################
import math
import modul_examen6
import sys
########################################################################
#control de error
if len(sys.argv) != 2 :
	print 'error_1'
	exit (1)
	
if not(sys.argv[1].isdigit()) :
	print 'error_2'
	exit (1)

number = int(sys.argv[1])

new_cadena = ''

# main programa
for i in range(0,number) :
	cadena_decimal = i
	cadena_base_3 = modul_examen6.de_decimal_a_base_b(i,3) 
	cadena_base_2 = modul_examen6.de_decimal_a_base_b(i,2) 


	if modul_examen6.es_palindrom(cadena_base_3) and modul_examen6.es_palindrom(cadena_base_2) :
		new_cadena = new_cadena + str(i) + ',' 
		
print new_cadena[:-1]
