# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 10-1-2017
#VERSION:- 1.1

#######################################################################

########################################################################
import math
import sys

########################################################################

#Mòdul de funcions per executar amb els programes de l'examen 6:
#Buscar capicues en base 2 i base 3 alhora

#FUNCIONS PER A L'EXERCICI 4

#ATENCIÓ: és més reusable és palíndrom que és capicua
def es_palindrom(cadena):
	'''
	funcio mirar si es palindron
	entrada:- cadeana
	return:- boolean
	'''
	new_cadena = ''
 	for c in cadena :
		new_cadena = c + new_cadena
	return cadena == new_cadena
	
#es pot modificar
def de_decimal_a_base_b(num, base):
	'''
	funcio para cambiar decimal en cual sabe base
	entrands:- dos int number
	return:- cadena
	'''
	cad = ''
	if num == 0:
		cad = '0'
	while num >= 1:
		cad = str(num % base) + cad 
		num = num / base
	return cad

def cadena_inver(cadena):
	'''
	funcio para inver cadena
	entrada:- cadean
	return:- cadena
	'''
	new_cadena = ''
	for c in cadena :
		new_cadena = c + new_cadena
	return new_cadena
	
def change_cual_base_3__in_decimal(cadena,base):
	'''
	funcion cambiar cadena in decimal number
	entrda:- cadena,int number
	return:- int number
	'''
	suma = 0
	count = 0
	cadena = cadena_inver(cadena)
	for c in cadena :
		suma = suma + (base**count * int(c))   
		count += 1
	return suma
	
#FUNCIONS PER A L'EXERCICI 5
#si feu aquesta l'exercici comptarà sobre mig punt
#def es_nombre_en_base_3


#si feu aquesta l'exercici comptarà sobre un punt
#def es_nombre_en_base_b


#FUNCIONS PER A L'EXERCICI 7  (QUE NO S'HA DE CODIFICAR) 
#def seguent_capicua_base_3(num):
	'''Funció que donat un nombre en base tres, troba el següent capicua
	Entrada: str en base 3 correcte
	Sortida: str en base 3 correcte
	'''
	#AQUESTA FUNCIÓ NO S'HA DE CODIFICAR EN AQUEST EXAMEN
