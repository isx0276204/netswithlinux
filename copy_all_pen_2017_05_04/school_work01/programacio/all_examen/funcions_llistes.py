# !/usr/bin/python
# -*-coding: utf-8-*-

#################################################################

#NOM AUTOR:		Adrián Dávila Montañez
#ISX: 			isx48102233
#DATA: 			14/03/2017
#VERSIÓ: 		1.0

#modul de llistes corregit
#################################################################


def maxim(sequencia):
	'''
	Funció que troba el màxim d'una seqüència
	Entrada: sequüencia d'elements., de len > 0
	Sortida: seqüència
	'''
	maxim = sequencia[0]
	for e in sequencia:
		if e > maxim:
			maxim = e
	return maxim
	
def cerca_sequencial(llista, element):
	'''
	Funció que donat un element, ens retorna la primera posició en la que es troba
	a la llista; -1 si no hi és
	Entrada: sequencia d'elements, element
	Sortida: enter (posició en la que es troba l'element). -1 si l'element 
	no està a la llista
	'''
	pos = 0
	while pos < len(llista) and llista[pos] != element:
		pos = pos + 1
	if pos == len([llista]):
		pos= -1		
	return pos

def comptarFrequencies(frasestr, llistaabuscar):
	'''
	funcio que calcula la frequencia de les repeticions
	entrada: sequencia on es busca, sequencia d'elements a buscar
	sortida: llista frecuencies
	'''
	frase=frasestr
	llistafrequencia = [0] * len(llistaabuscar)
	for lletra in frase:
		for i in range(0, len(llistaabuscar)):
			if lletra == llistaabuscar[i]:
				llistafrequencia[i] += 1
	return llistafrequencia

def histograma(elements, frequencies):
	'''
	entrada: llista strings, llista enters
	sortida: dibuix
	'''
	CAR = '*'
	tot_llista = len(elements)
	for i in range(0, tot_llista):
		print '%10s : %s' % (elements[i], frequencies[i] * CAR)
	return
	
def criteri_camp(d1, d2):
	'''
	Criteri de comparació pel camp que vulguem
	Entrada: 2 elements amb minim de camp com el valor de la variable que volem comparar ['maria','pi']
	Sortida: int -1, 0, 1
	'''
	camp=1
	valor=0
	if d1[camp] > d2[camp]:
		valor=1
	elif d1[camp] < d2[camp]:
		valor = -1
	else:
		if d1[camp-1] > d2[camp-1]:
			valor=1
		elif d1[camp-1] < d2[camp-1]:
			valor=-1
	return valor 
	

if __name__ == '__main__':
	
	if True: #Maxim
		print "Maxim de la llista [2,3,5,1,7,9,4]= ", maxim([2,3,5,1,7,9,4])
		print "Maxim de la llista [-2,-3,-5,-1,-7,-9,-4]= ", maxim([-2,-3,-5,-1,-7,-9,-4])
		
	if True: #histograma
		histograma('aeiou', [2,3,5,1,7])
		print "---------"
		histograma('aeu', [2,3,5])
		print
		histograma(['hola', 'adeu'], [5, 7])
		
	if True:
		print comptarFrequencies('hola que tal', ['a','e','i','o','u'])
		print comptarFrequencies('hola que tal', ['l','t','h'])
		
	if True:
		print cerca_sequencial(['0','1','2'], '0'), "es la posicio de '0'"
		print cerca_sequencial(['0','1','2'], '1'), "es la posicio de '1'"
		print cerca_sequencial(['0','1','2'], '2'), "es la posicio de '2'"
		print cerca_sequencial(['0','1','2'], '3'), "es la posicio de '3'"
		print cerca_sequencial(['dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte', 'diumenge'], 'dimarts'), "es la posicio de 'dimarts"
		print cerca_sequencial(['dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte', 'diumenge'], 'patxi'), "es la posicio de 'patxi"
		
	if 	True:
		llista=[['Maria','Pi'],['Carles','Romani'],['Josep Maria','Ribalta'],['Ramon','Guerra'],['Patxi','Guerra']]
		llista.sort(cmp=criteri_camp)
		print llista
