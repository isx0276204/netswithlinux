# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 30-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA PRINT PARA DEVIDO I MODEL DE DOS NUMBER
#IN CHAR SEPICAL # I $.

#ESPECIFICACIONS D'ENTRADA:- DOS INT NUMBER AND NOT POSIBLE ZERO .

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#	NUM1	NUM2				
#	7		5								# $ $
#	7      -5								# $ $
#	.
#	.	
#	.	
#	5		5								#
#	
########################################################################

#REFINAMENT

#CONSTANT SPECILA CHAR
CHAR_1 = '# '
CHAR_2 = '$ '

#LLEGIR DATA
number_1 = int(raw_input('number_1 please= '))
number_2 = int(raw_input('number_2 please= '))

#CALCULAR VALOR ABSLOT FOR BOTH NUMBER
if number_1 < 0 :
	number_1 = -(number_1)
	
if number_2 < 0 :
	 number_2 = -(number_2)
	 
#BUSCAR MAJOR I MENOR NUMBER ENTRE DOS NUMBER
major = number_1
menor = number_2

if major < number_2 :
	major = number_2
	menor = number_1
	
#CALCULAR DIVISION I MODUL
divison = major / menor
modul = major % menor

#MOSTRA DATA
print divison * CHAR_1,modul * CHAR_2
