# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 03-03-2017
#VERSION:- 1.1

########################################################################
#DESCRIPCION:-  

#ESPECIFICACIONS D'ENTRADA:-

########################################################################
#PROGRAMA PRINCIPAL

import sys
import examen4_i_5
import funcions_dates

#######################################################################


#Programa que llegeix un any i el dia de la setmana en què comença i ens
#mostra tots els mesos amb cinc caps de setmana

#ATENCIÓ: aquest programa incorpora el control d'errors, i és per això que hem
#modificat els arguments que reb la funció dia_de_la_setmana, definint la
#constant DIES_DE_LA_SETMANA al programa principal. Veus perquè?

########################################################################
	
def cerca_sequencial(llista, element):
	'''Funció que donat un element, ens retorna la posició en la que es troba
	a la llista; -1 si no hi és
	Entrada: llista de strings, string
	Sortida: enter (posició en la que es troba l'element). -1 si l'element 
	no està a la llista
	'''
	count = 0
	for elem in llista :
		if elem == element :
			return count
		count += 1
	return -1
	
	
#FIXA'T QUE HEM AFEGIT UN NOU ARGUMENT; ELS NOMS DELS DIES DE LA SETMANA ES
#PASSEN DES DEL PROGRAMA PRINCIPAL
def dia_de_la_setmana(nom_dia_inici, quantitat_de_dies, noms_dies):
	'''Funció que, sabent el dia de la setmana en que comencem, ens retorna 
	quin dia de la setmana serà si han passat quantitat_dies
	Entrada: cadena que correspon a un dia de la setmana vàlid, enter positiu, 
	llista de strings amb els noms dels dies de la setmana 
	amb els noms dels dies de la setmana
	Sortida: cadena que correspon a un dia de la setmana vàlid'''
	
	
	#busco quin nombre (0, 1, 2, 3, 4, 5 o 6) correspon a el dia de la setmana
	number_dia = cerca_sequencial(noms_dies,nom_dia_inici)
	
	#calculo quin nombre tindrà el dia de la setmana si han passat quantitat_de_dies
	total_dia = number_dia + quantitat_de_dies
	
	#busco el nom del dia de la setmana a SETMANA
	last_dia = total_dia % 7
	#retorno el nom
	return noms_dies[last_dia]
	

#PROGRAMA
MISSATGE = 'Usage: programa.py   dia_de_la_setmana_1_gener    any'

DIES_SETMANA = ['dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 
	'dissabte', 'diumenge']

#CONTROL D'ERRORS
if len(sys.argv) != 3 :
	print 'error: need two argument'
	exit (0)
	
if sys.argv[1] < 0 or sys.argv[2] not in DIES_SETMANA :
	print 'ERROR_2: wrong argument'
	exit (0)
	


# LLIGIR DATA
anyo = int(sys.argv[1])
primer_dia_anyo = sys.argv[2]

total = 0
mes = 1
for i in range(1,13) :
	print 'primer dia de mes',mes,' : ',dia_de_la_setmana(primer_dia_anyo,total,DIES_SETMANA)
	if funcions_dates.ultim_dia_mes(i,anyo) == 31 and dia_de_la_setmana(primer_dia_anyo,total,DIES_SETMANA) == 'divendres' :
		print  'mes',mes,'es 5 semana',anyo
	total += funcions_dates.ultim_dia_mes(i,anyo)
	mes += 1
	
