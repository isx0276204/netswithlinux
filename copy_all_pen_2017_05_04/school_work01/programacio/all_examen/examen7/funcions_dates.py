# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################
#AUTOR:	 	parveen
#ISX: 	 	isx0276204
#DATA: 	 	10/03/2017 
#VERSIÓ: 	1.0
########################################################################


########################################################################
#                    MÒDUL QUE TREBALLA AMB DATES                      #
########################################################################
import datetime
	
def es_data_format_ddmmyyyy (data):
	'''
	Funció per validar el format de la data. Diu si una cadena
	és o no una data correcta en format dd/mm/aaaa
	Entrada: str
	Sortida: boolean
	'''
	llistadata=data.split('/')
	if len(llistadata) != 3:
		return False
	
	dia=llistadata[0]
	mes=llistadata[1]
	anny=llistadata[2]
	
	if not (dia.isdigit() and len(dia) == 2):
		return False
	
	if not (mes.isdigit() and len(mes) == 2):
		return False
	
	if not (anny.isdigit() and len(anny) == 4):
		return False
	
	return True

def diumengePasqua(anyo):
	'''
	Def: funció que ens mostra la data (en format dd/mm/aaaa) en que cau el diumenge de pasqua de l'any en qüestió
	Entrada: int positiu
	Sortida: string (data en format correcte)
	'''
	a = anyo % 19
	b = anyo % 4
	c = anyo % 7
	a = (19 * a + 24) % 30
	b = (2 * b + 4 * c + 6 * a + 5) % 7
	dia = 22 + a + b
	
	
	if dia > 31:
		mes = 4
		dia = dia - 31
	return data_a_format_correcte(dia, mes, anyo)

def dies_any (anyo):
	'''
	Def: funció que ens calcula els dies de l'any 
	Entrada: int positiu
	Sortida: int posistiu
	'''
	if es_any_transpas(anyo):
		dies = (4 * 30) + (31 * 7)  + 29
		
	else:
		dies = (4 * 30) + (31 * 7)  + 28
	return  dies

def es_any_transpas (anny):
	'''
	Def: Retorna True si un any es de transpass
	Entrada: Int positiu
	Sortida: Boolean
	'''
	return (anny % 400 == 0) or (anny % 4 == 0 and anny % 100 != 0)

def data_a_format_correcte (dia, mes, anyo):
	'''
	Def: Ens retorna una data a format dd/mm/aaaa
	Entrada: Tres int's positius
	Sortida: String en format dd/mm/aaaa
	'''
	return '%02d/%02d/%04d' % (dia, mes, anyo) 

def es_data_valida (dia, mes, anyo):
	'''
	Def: Valida si una data es correcte
	Entrada: 3 Ints positius
	Sortida: Boolean
	'''
	if mes < 1 or mes > 12:
		return False
	
	if anyo < 0:
		return False
	 	
	#Comprovem que els dies son correctes mirant si esta entre el primer i l'ultim dia del mes. 
	#Mirem quin es el numero de dies del mes.... 
	diesmes = ultim_dia_mes(mes, anyo)

	#Comprovem que sigui correcte
	if dia > diesmes or dia < 1:
		return False
	
	return True

def es_data_ddmmyyyy_valida (data):
	'''
	Def: Retorna True si la data compleix amb el format dd/mm/yyyy i es valida
	Entrada: String
	Retorn: Boolean
	'''
	#Primer, validem el format
	if not es_data_format_ddmmyyyy(data):
		return False
	
	#Despres, validem que sigui correcte
	#Extreiem el dia mes i lany
	dia = extreu_dia(data)
	mes = extreu_mes(data)
	anny = extreu_any(data)
	
	#Comprovem que siguin correcte
	if not es_data_valida(dia, mes, anny):
		return False
	
	#Imprimim resultat	
	return True
	
def extreu_ddmmaaaa(data):
	'''
	Def: Retorna una llista com enters on.. [dia, mes, any]. Es a dir, ens extreu el dia el mes lany a partir d'un string de data.
	Entrada: Data valida amb format dd/mm/aaaa
	Sortida: 3 ints
	'''
	llistadata=data.split('/')
	dia = int(data[0:2])
	mes = int(data[3:5])
	anny = int(data[6:11])
	return [dia, mes, anny]
	
def extreu_dia(data):
	'''
	Def: Retorna el dia d'una data a format dd/mm/aaaa
	Entrada: String data format dd/mm/aaaa
	Sortida: Int
	'''
	llistadata=data.split('/')	
	return int(llistadata[0])
	
def extreu_mes(data):	
	'''
	Def: Retorna el mes d'una data a format dd/mm/aaaa
	Entrada: String data format dd/mm/aaaa
	Sortida: Int
	'''
	llistadata=data.split('/')	
	return int(llistadata[1])
	
def extreu_any(data):	
	'''
	Def: Retorna el any d'una data a format dd/mm/aaaa
	Entrada: String data format dd/mm/aaaa
	Sortida: Int
	'''
	llistadata=data.split('/')	
	return int(llistadata[2])
	
def ultim_dia_mes(mes, anny):
	'''
	Def: Ens retorna quin dia del mes es l'ultim
	Entrada: Dos enters
	Sortida: int
	'''
	DIES_DELS_MESOS=[0,31,28,31,30,31,30,31,31,30,31,30,31]
	
	if es_any_transpas(anny):
		DIES_DELS_MESOS[2] = 29
		
	return DIES_DELS_MESOS[mes]

def dia_seguent(data):
	'''
	Def: Funcio que ens retorna el seguent dia 
	Entrada:String (Data valida en format dd/mm/aaaa)
	Sortida:String (Data valida en format dd/mm/aaaa)
	'''
	diamesany=extreu_ddmmaaaa(data)
	dia = extreu_dia(data)
	mes = extreu_mes(data)
	anny = extreu_any (data) 
	
	ultimdiames=ultim_dia_mes(mes, anny)

	if dia < ultimdiames:
		dia = dia + 1
		
	else :
		if mes < 12:
			dia = 1
			mes = mes + 1
		else:
			dia = 1
			mes = 1
			anny = anny + 1
	
	return data_a_format_correcte(dia, mes, anny)

def dies_entre_dates(data1, data2):
	'''
	Def: Funció que ens retorna quants dies hi ha entre dos dates
	Entrada: 2 strings dates valides
	Sortida: Int
	'''
	
	#Si la data1 es posterior a data2, les intercambiem per compararles.
	if compara_dates(data1, data2) == 1:
		t=data1
		data1=data2
		data2=t
	
	dies = 0
	
	#Mentre que data 1 es diferent a data2, ves comptant dies.
	while data1 != data2:
		data1=dia_seguent(data1)
		dies = dies + 1
		
	return dies
		
def compara_dates(data1, data2):
	'''
	Def: Funcio que ens compara si la data 1 es posterior a la data 2.
		 Retorna 0 si son iguals,
		 Retorna 1 si data 1 es posterior a data 2
		 Retorna -1 si data 1 no es posterior a data 2.
	Entrada: Dates strings format dd/mm/aaaa
	Sortida: Boolean
	'''
	diadata1=extreu_dia(data1)
	mesdata1=extreu_mes(data1)
	anydata1=extreu_any(data1)
	
	diadata2=extreu_dia(data2)
	mesdata2=extreu_mes(data2)
	anydata2=extreu_any(data2)
	
	if anydata2 > anydata1:
		comparador = -1
	
	elif anydata2 == anydata1 and mesdata2 > mesdata1:
		comparador = -1
	
	elif anydata2 == anydata1 and mesdata2 == mesdata1 and diadata2 > diadata1:
		comparador = -1
		
	elif anydata2 == anydata1 and mesdata2 == mesdata1 and diadata2 == diadata1:
		comparador = 0
	
	else:
		comparador = 1
	
	return comparador

def dies_falten_aniversari(data_avui, diaaniversari, mesaniversari):
	'''
	Def: Funcio que ens mostra quants dies falten per l'aniversari
	Entrada: Data_avui -> String data format dd/mm/aaaa avui
			 mesaniversari -> int entre 1 i 12
			 anyaniversari -> int positiu
	Sortida: Int
	'''
	#Extreiem el dia el mes i l'any de la data d'avui
	diaavui=extreu_dia(data_avui)
	mesavui=extreu_mes(data_avui)
	annyavui=extreu_any(data_avui)
	
	#Afegim l'any d'avui a data aniversari de manera temporal
	data_aniversari=data_a_format_correcte(diaaniversari, mesaniversari, annyavui)
	
	#Comparem les dates. Si la data aniversari es posterior, afegirem 1 a l'any, sino no. 
	if  data1_posterior_data2(data_aniversari, data_avui) == -1:
		data_aniversari=data_a_format_correcte(diaaniversari, mesaniversari, int(annyavui)+1)
		
	dies = dies_entre_dates(data_avui, data_aniversari)
	
	return dies
	
def fast_dies_entre_dates(data1, data2):
	'''
	Def: Funció que ens retorna quants dies hi ha entre dos dates
	Entrada: 2 strings dates valides
	Sortida: Int
	'''
	
	#Si la data1 es posterior a data2, les intercambiem per compararles.
	if compara_dates(data1, data2) == 1:
		t=data1
		data1=data2
		data2=t
	
	dies = 0
	day = extreu_dia(data1)
	month = extreu_mes(data1)
	year = extreu_any(data1)
	if month < 3 and es_any_transpas(year) :
		ok = True
	else :
		ok = False
	year += 1
	data1 = data_a_format_correcte(day,month,year)
	while compara_dates(data1,data2) < 1 :
		if es_any_transpas(year) :
			dies += 366
		else :
			dies += 365
		year += 1
		data1 = data_a_format_correcte(day,month,year)
	if compara_dates(data1,data2) == 1 :
		year =year -1
		data1 = data_a_format_correcte(day,month,year)
	#Mentre que data 1 es diferent a data2, ves comptant dies.
	while data1 != data2:
		data1=dia_seguent(data1)
		dies = dies + 1
	if ok :
		dies = dies + 1
	return dies
	
def data_avui():
	'''
	Funció que em diu quin és el dia d'avui en format data correcte (‘dd/mm/aaaa’)
	Entrada: res (la funció busca les dades en el sistema)	
	Sortida: str en format data
	'''
	dia_actual = datetime.datetime.now().day
	mes_actual = datetime.datetime.now().month
	year_actual = datetime.datetime.now().year		
	return data_a_format_correcte(dia_actual,mes_actual,year_actual)
	
def dia_setmana_avui():
	'''
	Funció que em retorna el nom del dia de la setmana d’avui
	Entrada: res (la funció busca les dades en el sistema i utilitza la constant DIES_SETMANA definida
	en el propi fitxer
	Sortida: str que indica el nom del dia de la setmana
	'''
	avui=datetime.datetime.now()
	return avui.weekday()

def dia_setmana_avui_malti_language(lg):
	'''
	Funció que em retorna el nom del dia de la setmana d’avui
	Entrada: es,en,cat for example choose languge
	Sortida: str que indica el nom del dia de la setmana
	'''
	if lg == 'es' :
		number_add = 7
	elif lg == 'en' :
		number_add = 14
	else :
		number_add = 0
	SETMANA = ['dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 
	'dissabte', 'diumenge','lunes','martes','miercules','juves','viernes'\
	,'sabado','domigo','monday','tuesday','wednesday','thursday','friday','saturday','sunday']
	avui=datetime.datetime.now()
	return SETMANA[avui.weekday() + number_add ]
		
def cerca_sequencial(llista, element):
	'''Funció que donat un element, ens retorna la posició en la que es troba
	a la llista; -1 si no hi és
	Entrada: llista de strings, string
	Sortida: enter (posició en la que es troba l'element). -1 si l'element 
	no està a la llista
	'''
	count = 0
	position = -1
	ok = True
	while ok == True :
		if llista[count] == element :
			position = count
			ok = False
		count += 1
	return position
	
#exercici 5
def dia_de_la_setmana(nom_dia_inici, quantitat_de_dies):
	'''Funció que, sabent el dia de la setmana en que comencem, ens retorna 
	quin dia de la setmana serà si han passat quantitat_dies
	Entrada: cadena que correspon a un dia de la setmana vàlid, enter positiu, 
	llista de strings 	amb els noms dels dies de la setmana
	Sortida: cadena que correspon a un dia de la setmana vàlid'''

	SETMANA = ['dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 
	'dissabte', 'diumenge']
	
	#busco quin nombre (0, 1, 2, 3, 4, 5 o 6) correspon a el dia de la setmana
	number_dia = cerca_sequencial(SETMANA,nom_dia_inici)
	
	#calculo quin nombre tindrà el dia de la setmana si han passat quantitat_de_dies
	total_dia = number_dia + quantitat_de_dies
	
	#busco el nom del dia de la setmana a SETMANA
	last_dia = total_dia % 7
	#retorno el nom
	return SETMANA[last_dia]
	

def dia_de_la_setmana_back(quantitat_de_dies):
	'''Funció que, sabent el dia de la setmana en que comencem, ens retorna 
	quin dia de la setmana serà si han passat quantitat_dies
	Entrada: enter positiu 
	llista de strings 	amb els noms dels dies de la setmana
	Sortida: cadena que correspon a un dia de la setmana vàlid'''

	SETMANA = ['dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 
	'dissabte', 'diumenge']
	nom_dia_inici = SETMANA[dia_setmana_avui()]
	
	#busco quin nombre (0, 1, 2, 3, 4, 5 o 6) correspon a el dia de la setmana
	number_dia = cerca_sequencial(SETMANA,nom_dia_inici)
	
	#calculo quin nombre tindrà el dia de la setmana si han passat quantitat_de_dies
	total_dia = number_dia + quantitat_de_dies
	
	#busco el nom del dia de la setmana a SETMANA
	last_dia = total_dia % 7
	#retorno el nom
	return SETMANA[last_dia]	
	
	
#Test driver
if __name__ == "__main__" :
	#Joc proves valida format data
	if False:
		print 'Valida format data 12/12/1994', es_data_format_ddmmyyyy('12/12/1994')
		print 'Valida format data 122/12/1994', es_data_format_ddmmyyyy('122/12/1994')
		print 'Valida format data 12-12/1994', es_data_format_ddmmyyyy('12-12/1994')
		print 'Valida format data 15/555/1994', es_data_format_ddmmyyyy('15/555/1994')
		print 'Valida format data 32/10/1994', es_data_format_ddmmyyyy('32/10/1994')
		print 'Valida format data 12/00/1994', es_data_format_ddmmyyyy('12/00/1994')
		print 'Valida format data 00/01/2017', es_data_format_ddmmyyyy('00/01/2017')
		print 'Valida format data 30/02/2400', es_data_format_ddmmyyyy('30/02/2400')
		print 'Valida format data 29/02/2012', es_data_format_ddmmyyyy('29/02/2012')
		print 'Valida format data 29/02/2013', es_data_format_ddmmyyyy('29/02/2013')

	if False:
		print 'Valida data 12/12/1994', es_data_valida(12, 12, 1994)
		print 'Valida data 122/12/1994', es_data_valida(122, 12, 1994)
		print 'Valida data 12-12/1994', es_data_valida(12, 12, 1994)
		print 'Valida data 15/555/1994', es_data_valida(15, 555, 1994)
		print 'Valida data 32/10/1994', es_data_valida(32, 10, 1994)
		print 'Valida data 32/10/1994', es_data_valida(12, 00, 1994)
		print 'Valida data 00/01/2017', es_data_valida(00, 01, 2017)
		print 'Valida data 30/02/2400', es_data_valida(30, 02, 2400)
		print 'Valida data 29/02/2012', es_data_valida(29, 02, 2012)
		print 'Valida data 29/02/2013', es_data_valida(29, 02, 2012)

	if False:
		print '2010', diumengePasqua(2010)
		print '2017', diumengePasqua(2017)

	if False:
		print '2010', dies_any(2010)
		print '2017', dies_any(2017)
		print '2004', dies_any(2004)

	if False:
		print '2010', es_any_transpas(2010)
		print '2017', es_any_transpas(2017)
		print '2004', es_any_transpas(2004) 
		print '2013', es_any_transpas(2012) 
		print '2014', es_any_transpas(2013) 
		
	if False:
		print 'Valida data ddmmaaaa 12/12/1994',  es_data_ddmmyyyy_valida('12/12/1994')
		print 'Valida data ddmmaaaa 122/12/1994', es_data_ddmmyyyy_valida('122/12/1994')
		print 'Valida data ddmmaaaa 12-12/1994',  es_data_ddmmyyyy_valida('12-12/1994')
		print 'Valida data ddmmaaaa 15/555/1994', es_data_ddmmyyyy_valida('15/555/1994')
		print 'Valida data ddmmaaaa 32/10/1994',  es_data_ddmmyyyy_valida('32/10/1994')
		print 'Valida data ddmmaaaa 32/10/1994',  es_data_ddmmyyyy_valida('12/00/1994')
		print 'Valida data ddmmaaaa 00/01/2017',  es_data_ddmmyyyy_valida('00/01/2017')
		print 'Valida data ddmmaaaa 30/02/2400',  es_data_ddmmyyyy_valida('30/02/2400')
		print 'Valida data ddmmaaaa 29/02/2012',  es_data_ddmmyyyy_valida('29/02/2012')
		print 'Valida data ddmmaaaa 29/02/2013',  es_data_ddmmyyyy_valida('29/02/2013')		
		
	if False:
		print '2010', es_primer(2010)
		print '2017', es_primer(2017)
		print '2004', es_primer(2004)

	if False:
		print 'Extreu dia 12/12/1994', extreu_dia('12/12/1994')
		print 'Extreu dia 30/02/2400', extreu_dia('30/02/2400')
		print 'Extreu dia 29/02/2012', extreu_dia('29/02/2012')
		print 'Extreu dia 29/02/2013', extreu_dia('29/02/2013')
		
	if False:
		print 'Extreu mes 12/12/1994', extreu_mes('12/12/1994')
		print 'Extreu mes 30/02/2400', extreu_mes('30/02/2400')
		print 'Extreu mes 29/02/2012', extreu_mes('29/02/2012')
		print 'Extreu mes 29/02/2013', extreu_mes('29/02/2013')
		
	if False:
		print 'Extreu any 12/12/1994', extreu_any('12/12/1994')
		print 'Extreu any 30/02/2400', extreu_any('30/02/2400')
		print 'Extreu any 29/02/2012', extreu_any('29/02/2012')
		print 'Extreu any 29/02/2013', extreu_any('29/02/2013')

	if False:
		print '2, 2004', ultim_dia_mes(2,2004)
		print '2, 2005', ultim_dia_mes(2,2005)
		print '3, 2004', ultim_dia_mes(3,2004)
		print '3, 2005', ultim_dia_mes(3,2005)
		print '12, 2016', ultim_dia_mes(12,2016)
		print '1, 2016', ultim_dia_mes(1, 2016)
		print '4, 2016', ultim_dia_mes(4, 2016)

	if False:
		print 'dia seguent 12/12/1994', dia_seguent('12/12/1994')
		print 'dia seguent 28/02/2400', dia_seguent('28/02/2400')
		print 'dia seguent 29/02/2012', dia_seguent('29/02/2012')
		print 'dia seguent 05/08/2013', dia_seguent('05/08/2013')
		print 'dia seguent 31/12/2013', dia_seguent('31/12/2013')
		print 'dia seguent 01/01/2013', dia_seguent('01/01/2013')


	if False:
		print 'dies entre dates 26/12/2017 a 27/12/2017', dies_entre_dates('26/12/2017', '27/12/2017')
		print 'dies entre dates 20/12/2017 a 27/12/2017', dies_entre_dates('20/12/2017','27/12/2017')
		print 'dies entre dates 01/11/2017 a 27/12/2017', dies_entre_dates('01/11/2017','27/12/2017')
		print 'dies entre dates 28/12/2017 a 27/12/2018', dies_entre_dates('28/12/2017','27/12/2018')
		print 'dies entre dates 10/12/2017 a 05/12/2018', dies_entre_dates('10/12/2017','05/12/2017')
		
	if False:
		print 'dies falten aniversari 26/12/2017', dies_falten_aniversari('26/12/2017', 27, 12)
		print 'dies falten aniversari 20/12/2017', dies_falten_aniversari('20/12/2017', 27, 12)
		print 'dies falten aniversari 28/12/2017', dies_falten_aniversari('28/12/2017', 27, 12)
		print 'dies falten aniversari 26/12/2018', dies_falten_aniversari('26/12/2018', 27, 12)
		print 'dies falten aniversari 27/12/2017', dies_falten_aniversari('27/12/2017', 27, 12)
		print 'dies falten aniversari 31/12/2017', dies_falten_aniversari('31/12/2017', 27, 12)

	if False:
		print '11/10/2017 es despres de 10/10/2017?', data1_posterior_data2('11/10/2017','10/10/2017 ')
		print '09/10/2017 es despres de 10/10/2017?', data1_posterior_data2('09/10/2017','10/10/2017 ')
		print '10/08/2017 es despres de 10/10/2018?', data1_posterior_data2('10/08/2017','10/10/2018 ')
		print '10/12/2018 es despres de 10/10/2017?', data1_posterior_data2('10/12/2018','10/10/2017 ')
		print '10/10/2017 es despres de 10/10/2018?', data1_posterior_data2('10/10/2017','10/10/2018 ')
		print '10/10/2018 es despres de 10/10/2017?', data1_posterior_data2('10/10/2018','10/10/2017 ')
		print '10/10/2018 es despres de 10/10/2018?', data1_posterior_data2('10/10/2018','10/10/2018 ')

	if False:
		print 'dies entre dates 26/12/2017 a 27/12/2017', fast_dies_entre_dates('26/02/2016', '27/02/2017')
		print 'dies entre dates 20/12/2017 a 27/12/2017', fast_dies_entre_dates('20/12/2017','27/12/2017')
		print 'dies entre dates 01/11/2017 a 27/12/2017', fast_dies_entre_dates('01/11/2017','27/12/2017')
		print 'dies entre dates 28/12/2017 a 27/12/2018', fast_dies_entre_dates('28/12/2017','27/12/2018')
		print 'dies entre dates 10/12/2017 a 05/12/2017', fast_dies_entre_dates('10/12/2017','05/12/2017')
		print 'dies entre dates 25/03/2015 a 25/03/2017', fast_dies_entre_dates('25/03/2015','25/03/2017')
		print 'dies entre dates 25/03/2015 a 25/03/2017', fast_dies_entre_dates('25/03/1911','24/04/2013')
	if False :
		print data_avui() 
		print dia_setmana_avui()
	if False :
		print dia_de_la_setmana('diumenge’', 31)	#dia ‘01/02/2017’, des de ‘01/01/2017’
		print dia_de_la_setmana('diumenge', 31 + 28)	#dia ‘01/03/2017’, des de ‘01/01/2017’
		print dia_de_la_setmana('dimecres', 28)	#dia ‘01/03/2017’, des de ‘01/02/2017’
		print dia_de_la_setmana('dimecres', 5)	#dia ‘13/02/2017’, des de ‘08/02/2017’
	if False :
		print dia_de_la_setmana_back(-13)	
		print dia_de_la_setmana_back(-1)	
		print dia_de_la_setmana_back(-365)	
		print dia_de_la_setmana_back(-5)
	if True :
		print dia_setmana_avui_malti_language('es')
		print dia_setmana_avui_malti_language('en')
		print dia_setmana_avui_malti_language('cat')
