# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 03-03-2017
#VERSION:- 1.1

########################################################################
#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR PRIMER DIA DE MES Y MES QUE 
#TENES 5 SEMANA

#ESPECIFICACIONS D'ENTRADA:-UNA ANYO CORRECTO Y PRIMER DIA DE ANYO CORRECTA

########################################################################

import sys
import examen4_i_5
import funcions_dates

# LLIGIR DATA
anyo = int(sys.argv[1])
primer_dia_anyo = sys.argv[2]

total = 0
mes = 1
for i in range(1,13) :
	print 'primer dia de mes',mes,' : ',examen4_i_5.dia_de_la_setmana(primer_dia_anyo,total)
	if funcions_dates.ultim_dia_mes(i,anyo) == 31 and examen4_i_5.dia_de_la_setmana(primer_dia_anyo,total) == 'divendres' :
		print  'mes',mes,'es 5 semana',anyo
	total += funcions_dates.ultim_dia_mes(i,anyo)
	mes += 1
	
