# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 03-03-2017
#VERSION:- 1.1

########################################################################

#definicions de funcions que utilitzaré per a fer el programa de buscar
#mesos amb cinc caps de setmana complerts

#exercici 4	
def cerca_sequencial(llista, element):
	'''Funció que donat un element, ens retorna la posició en la que es troba
	a la llista; -1 si no hi és
	Entrada: llista de strings, string
	Sortida: enter (posició en la que es troba l'element). -1 si l'element 
	no està a la llista
	'''
	count = 0
	for elem in llista :
		if elem == element :
			return count
		count += 1
	return -1
	
#exercici 5
def dia_de_la_setmana(nom_dia_inici, quantitat_de_dies):
	'''Funció que, sabent el dia de la setmana en que comencem, ens retorna 
	quin dia de la setmana serà si han passat quantitat_dies
	Entrada: cadena que correspon a un dia de la setmana vàlid, enter positiu, 
	llista de strings 	amb els noms dels dies de la setmana
	Sortida: cadena que correspon a un dia de la setmana vàlid'''

	SETMANA = ['dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 
	'dissabte', 'diumenge']
	
	#busco quin nombre (0, 1, 2, 3, 4, 5 o 6) correspon a el dia de la setmana
	number_dia = cerca_sequencial(SETMANA,nom_dia_inici)
	
	#calculo quin nombre tindrà el dia de la setmana si han passat quantitat_de_dies
	total_dia = number_dia + quantitat_de_dies
	
	#busco el nom del dia de la setmana a SETMANA
	last_dia = total_dia % 7
	#retorno el nom
	return SETMANA[last_dia]

# joc de proves per a la funció cerca_sequencial  (exercici 4)
#(és important que busquis un joc de proves general, no només per a dies de la setmana)




#joc de proves per a la funció dia_de_la_setmana   (exercici 5)
if __name__ == '__main__' :
	print dia_de_la_setmana('diumenge’', 31)	#dia ‘01/02/2017’, des de ‘01/01/2017’
	print dia_de_la_setmana('diumenge', 31 + 28)	#dia ‘01/03/2017’, des de ‘01/01/2017’
	print dia_de_la_setmana('dimecres', 28)	#dia ‘01/03/2017’, des de ‘01/02/2017’
	print dia_de_la_setmana('dimecres', 5)	#dia ‘13/02/2017’, des de ‘08/02/2017’
	
