

#CAPÇALERA COMPLERTA

########################################################################
#                    MÒDUL QUE TREBALLA AMB RACIONALS                   #
# per a nosaltres un racional és un str amb el format 'n/m', on n és un enter 
# qualsevol (pot ser negatiu o no) i m un natural major que 0. Un str amb 
# el format 'n' on n és un enter també és un racional

########################################################################


#funció que necessitem d'un mòdul anterior  però la incorporem aquí
def es_enter(s):
	'''Funció que ens diu si una cadena és o no un enter
	Entrada: str 
	Sortida: Boolean'''
	if len(s) == 0:
		return False
	#traiem el signe, si cal
	if s[0] in '+-':
		s = s[1:]
	return s.isdigit()


#Funcions bàsiques  (exercici 1)	
def es_racional_valid(s):
	'''
	Funció per validar si una cadena és un racional vàlid. Accepta com a 
	racionals 3/1 i també 3
	Entrada: str
	Sortida: boolean
	'''


def racional_a_format_correcte(num, den):
	'''
	Def: Ens retorna un racional
	Entrada: dos enters SEMPRE: Si volem passar un enter a racional, 
	hem de posar que el denominador és 1. En aquest cas mostrarà nomès num
	Sortida: String en format de racional (n/d o n)
	'''
	
def extreu_numerador(racional):
	'''
	Funció que ens retorna el numerador per a una cadena amb nostre format de racional
	Entrada: String format racional
	Sortida: Int
	'''
	
def extreu_denominador(racional):	
	'''
	Funció que ens retorna el denominador per a una cadena amb nostre format de racional
	Entrada: String format racional
	Sortida: Int (serà 1 en el cas que el racional sigui un enter
	'''
	
#Funcions que defineixen operacions amb racionals (Exercic 2)
	
def suma(rac1, rac2):
	'''Funció que suma dos racionals. Atenció, també pot acceptar 'num' com
	a racional
	Entrada: 2 str en format racional
	Sortida: str en format racional correcte
	'''
	
	
def multiplicacio(rac1, rac2):
	'''
	Funció que multiplica dos racionals. Atenció, també pot acceptar 'num' com
	a racional
	Entrada: 2 str en format racional
	Sortida: str en format racional correcte
	'''
	
#Ordre en els racionals (Exercici 3)
def compara_racionals(rac1, rac2):
	'''
	Def: Funcio que ens diu si el rac1 és més gran que el rac2.
		 Retorna 0 si son iguals,
		 Retorna 1 si rac1 es major que rac2
		 Retorna -1 si rac1 menor que rac2.
	Entrada: 2 str en format racional
	Sortida: int
	'''
	
#Simplificar (Fracció canònica)  (Exercici 4)	
def fraccio_canonica(racional):
	'''
	Funció que simplifica, i ens troba la fracció canònica, que és a/b, on 
	a i b no tenen factors en comú, a és un enter (pot ser negatiu) i b és
	un natural major que 0 (no pot ser negatiu). Si es tracta d'un enter, 
	la fracció canònica és un str amb el propi enter
	Entrada: str racional
	Sortida: str racional'''
	#agafem el numerador i denominador
	
	# mirem el signe que quedarà
		
	# agafem els valors absoluts del numerador i del denominador
		
	#Busquem el màxim comú divisor 
	
	#dividim numerador i denominador pel mcd
	
	#corregim el signe (si cal)
		
	#retornem el racional
	
	


	
	
#Test driver
if __name__ == '__main__':
	
	#Joc proves 
	if False:
		print es_enter('-22')
		print es_enter('-22.2')
		print es_enter('+2')
		print es_enter('2')
		print es_enter('')
		print
		
	if False:
		print 'es un racional vàlid?'
		print '45/4', es_racional_valid('45/4')
		print '2.3/7', es_racional_valid('2.3/7')
		print '-45/9', es_racional_valid('-45/9')
		print '234/0', es_racional_valid('234/0')
		print '23', es_racional_valid('23')
		print '3/-33', es_racional_valid('3/-33')
		print '2/1/-2', es_racional_valid('2/1/-2')
		print '34m/23', es_racional_valid('34/23m')
		print '34m/23.', es_racional_valid('34m/23.')
		print '/', es_racional_valid('/')
		print '6/', es_racional_valid('6/')
		print '0/2', es_racional_valid('0/2')		
		print  '3/1', es_racional_valid('3/1')


	if True:
		print 'racional a format correcte'
		print racional_a_format_correcte(-4, -234)
		print racional_a_format_correcte(-2, 3)
		print racional_a_format_correcte(-23, 1)
		print racional_a_format_correcte(23, -1)
		print

	if True:
		print 'extreu numerador i denominador'
		print '23/-1'
		print 'numerador: ', extreu_numerador('23/-1')
		print 'denominador: ', extreu_denominador('23/-1')
		print '23'
		print 'numerador: ', extreu_numerador('23')
		print 'denominador: ', extreu_denominador('23')
		print

	if False:
		print 'la fracció canònica'
		print '2/4',fraccio_canonica('2/4')
		print '14/24', fraccio_canonica('14/24')
		print '1/4', fraccio_canonica('1/4')
		print '-30/-10', fraccio_canonica('-30/-10')
		print '6/-6', fraccio_canonica('6/-6')
		print

	if False:
		print 'suma'
		print '1/2  +  1/2 =', suma('1/2', '1/2')
		print '2/4  +  1/6 =', suma('2/4', '1/6')
		print '1/2  +  1/-2 =', suma('1/2', '1/-2')
		print '3 + 5/2 = ', suma('3', '5/2')
		print '3 + 5/1 = ', suma('3', '5/1')
		print '3 + 5 = ', suma('3', '5')
		
	if False:
		print 'multiplicació'
		print '1/2  *  1/2	=', multiplicacio('1/2', '1/2')
		print '-6/5  *  5/-2 =', multiplicacio('-6/5', '5/-2')
		print '-6/-1  *  1/-6  =', multiplicacio('-6/-1', '1/-6')
		print 
		
	if False:
		print 'comparació'
		print compara_racionals('1/2', '1/3')
		print compara_racionals('2/1000', '3/10')
		print compara_racionals('2/100', '1/50')
		print
	
	
