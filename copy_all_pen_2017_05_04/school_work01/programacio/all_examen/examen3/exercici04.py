# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 09-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR NOTA DE DOS EXAM SOBRE 10.

#ESPECIFICACIONS D'ENTRADA:- DOS NUMBER FLOAT POSITIVE UN ENTRE 0.0 I 
#7,5 OTRO 0.0 I 10 TAMBIEN ZERO ENTRA.

########################################################################

#REFINAMENT

#LLIGIM DATA
nota_exam02 = float(raw_input('nota_exam02= '))
nota_exam03 = float(raw_input('nota_exam03= '))

#CALCULAR NOTA DE EXAM 2 SOBRE 10
nota_exam02 = (nota_exam02 * 10) / 7.5

#BUSCAR MAJOR DE NOTA
major_nota = nota_exam02
if nota_exam02 < nota_exam03 :
	major_nota = nota_exam03
	
#MOSTRA NOTA FINAL
print major_nota
