# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 31-03-2017
#VERSION:- 1.1



########################################################################
#                    MÒDUL QUE TREBALLA AMB RACIONALS                   #
# per a nosaltres un racional és un str amb el format 'n/m', on n és un enter 
# qualsevol (pot ser negatiu o no) i m un natural major que 0. Un str amb 
# el format 'n' on n és un enter també és un racional

########################################################################


#funció que necessitem d'un mòdul anterior  però la incorporem aquí
def es_enter(s):
	'''Funció que ens diu si una cadena és o no un enter
	Entrada: str 
	Sortida: Boolean'''
	if len(s) == 0:
		return False
	#traiem el signe, si cal
	if s[0] in '+-':
		s = s[1:]
	return s.isdigit()


#Funcions bàsiques  (exercici 1)	
def es_racional_valid(s):
	'''
	Funció per validar si una cadena és un racional vàlid. Accepta com a 
	racionals 3/1 i també 3
	Entrada: str
	Sortida: boolean
	'''
	for i in  range(0,len(s)) :
		if s[i] == '/' :
			if es_enter(s[0:i]) and es_enter(s[i+1:]) and int(s[i +1:]) != 0 :
				return True
	if es_enter(s) :
		return True	
	return False


def racional_a_format_correcte(num, den):
	'''
	Def: Ens retorna un racional
	Entrada: dos enters SEMPRE: Si volem passar un enter a racional,not posible den = 0 
	hem de posar que el denominador és 1. En aquest cas mostrarà nomès num
	Sortida: String en format de racional (n/d o n)
	'''
	num_racional = str(num)
	if den != 1 :
		num_racional = num_racional + '/' + str(den)
	return num_racional
	
def extreu_numerador(racional):
	'''
	Funció que ens retorna el numerador per a una cadena amb nostre format de racional
	Entrada: String format racional
	Sortida: Int
	'''
	racional = racional.split('/')
	return int(racional[0])
	
def extreu_denominador(racional):	
	'''
	Funció que ens retorna el denominador per a una cadena amb nostre format de racional
	Entrada: String format racional
	Sortida: Int (serà 1 en el cas que el racional sigui un enter
	'''
	racional = racional.split('/')
	if len(racional) == 1 :
		racional.append('1')
	return int(racional[1])
	
#Funcions que defineixen operacions amb racionals (Exercic 2)
	
def suma(rac1, rac2):
	'''Funció que suma dos racionals. Atenció, també pot acceptar 'num' com
	a racional
	Entrada: 2 str en format racional
	Sortida: str en format racional correcte
	'''
	rac1_num = extreu_numerador(rac1)
	rac1_den = extreu_denominador(rac1)
	rac2_num = extreu_numerador(rac2)
	rac2_den = extreu_denominador(rac2)
	
	suma_num = (rac1_num * rac2_den) + (rac2_num * rac1_den) 
	suma_den = rac1_den * rac2_den
	return racional_a_format_correcte(suma_num,suma_den)
	
def multiplicacio(rac1, rac2):
	'''
	Funció que multiplica dos racionals. Atenció, també pot acceptar 'num' com
	a racional
	Entrada: 2 str en format racional
	Sortida: str en format racional correcte
	'''
	rac1_num = extreu_numerador(rac1)
	rac1_den = extreu_denominador(rac1)
	rac2_num = extreu_numerador(rac2)
	rac2_den = extreu_denominador(rac2)
	
	suma_num = rac1_num * rac2_num 
	suma_den = rac1_den * rac2_den
	return racional_a_format_correcte(suma_num,suma_den)
	
#Ordre en els racionals (Exercici 3)
def compara_racionals(rac1, rac2):
	'''
	Def: Funcio que ens diu si el rac1 és més gran que el rac2.
		 Retorna 0 si son iguals,
		 Retorna 1 si rac1 es major que rac2
		 Retorna -1 si rac1 menor que rac2.
	Entrada: 2 str en format racional
	Sortida: int
	'''
	rac1_num = extreu_numerador(rac1)
	rac1_den = extreu_denominador(rac1)
	rac2_num = extreu_numerador(rac2)
	rac2_den = extreu_denominador(rac2)
	rac1 = float(extreu_numerador(rac1)) / extreu_denominador(rac1)
	rac2 = float(extreu_numerador(rac2)) / extreu_denominador(rac2)

	valor = 0
	if rac1 > rac2 :
		valor = 1
	if rac1 < rac2 :
		valor = -1
	return valor
#Simplificar (Fracció canònica)  (Exercici 4)	
def fraccio_canonica(racional):
	'''
	Funció que simplifica, i ens troba la fracció canònica, que és a/b, on 
	a i b no tenen factors en comú, a és un enter (pot ser negatiu) i b és
	un natural major que 0 (no pot ser negatiu). Si es tracta d'un enter, 
	la fracció canònica és un str amb el propi enter
	Entrada: str racional
	Sortida: str racional'''
	#agafem el numerador i denominador
	racional_num = extreu_numerador(racional)
	racional_den = extreu_denominador(racional)
	
	# mirem el signe que quedarà
	siqne = False 
	if (racional_num < 0 and racional_den > 0) or (racional_num > 0 and racional_den < 0) :
		siqne = True
	
	# agafem els valors absoluts del numerador i del denominador
	racional_num = abs(racional_num)
	racional_den = abs(racional_den)
			
	#Busquem el màxim comú divisor 
	small_num = racional_num
	if small_num > racional_den :
		small = racional_den
	divisor = 1
	while small_num > 1 and divisor == 1 :
		if racional_num % small_num == 0 and racional_den % small_num == 0 :
			divisor = small_num
		small_num -= 1
	
	#dividim numerador i denominador pel mcd
	racional_num = racional_num / divisor
	racional_den = racional_den /divisor
	#corregim el signe (si cal)
	if siqne :
		racional_num = -(racional_num)
	#retornem el racional
	return racional_a_format_correcte(racional_num,racional_den)
	

def buscar_maximo_racional(lista) :
	'''
	funcion busar una maxi de lista de racional
	param:- llista correcto de racional
	entreda:- maxim de lista in racional
	'''
	maxim = lista[0]
	for elem in lista :
		if compara_racionals(elem,maxim) == 1 :
			maxim = elem
	return maxim	
	
#Test driver
if __name__ == '__main__':
	
	#Joc proves 
	if False:
		print es_enter('-22')
		print es_enter('-22.2')
		print es_enter('+2')
		print es_enter('2')
		print es_enter('')
		print
		
	if True:
		print 'es un racional vàlid?'
		print '45/4', es_racional_valid('45/4')
		print '2.3/7', es_racional_valid('2.3/7')
		print '-45/9', es_racional_valid('-45/9')
		print '234/0', es_racional_valid('234/0')
		print '23', es_racional_valid('23')
		print '3/-33', es_racional_valid('3/-33')
		print '2/1/-2', es_racional_valid('2/1/-2')
		print '34m/23', es_racional_valid('34/23m')
		print '34m/23.', es_racional_valid('34m/23.')
		print '/', es_racional_valid('/')
		print '6/', es_racional_valid('6/')
		print '0/2', es_racional_valid('0/2')		
		print  '3/1', es_racional_valid('3/1')


	if True:
		print 'racional a format correcte'
		print racional_a_format_correcte(-4, -234)
		print racional_a_format_correcte(-2, 3)
		print racional_a_format_correcte(-23, 1)
		print racional_a_format_correcte(23, -1)
		print racional_a_format_correcte(23, 1)

	if True:
		print 'extreu numerador i denominador'
		print '23/-1'
		print 'numerador: ', extreu_numerador('23/-1')
		print 'denominador: ', extreu_denominador('23/-1')
		print '23'
		print 'numerador: ', extreu_numerador('23')
		print 'denominador: ', extreu_denominador('23')
		print

	if True:
		print 'la fracció canònica'
		print '2/4',fraccio_canonica('2/4')
		print '14/24', fraccio_canonica('14/24')
		print '1/4', fraccio_canonica('1/4')
		print '-30/-10', fraccio_canonica('-30/-10')
		print '6/-6', fraccio_canonica('6/-6')
		print '100/150', fraccio_canonica('100/150')

	if True:
		print 'suma'
		print '1/2  +  1/2 =', suma('1/2', '1/2')
		print '2/4  +  1/6 =', suma('2/4', '1/6')
		print '1/2  +  1/-2 =', suma('1/2', '1/-2')
		print '3 + 5/2 = ', suma('3', '5/2')
		print '3 + 5/1 = ', suma('3', '5/1')
		print '3 + 5 = ', suma('3', '5')
		print '3 + 5 = ', suma('3', '5')
	if True:
		print 'multiplicació'
		print '1/2  *  1/2	=', multiplicacio('1/2', '1/2')
		print '-6/5  *  5/-2 =', multiplicacio('-6/5', '5/-2')
		print '-6/-1  *  1/-6  =', multiplicacio('-6/-1', '1/-6')
		print 
		
	if True:
		print 'comparació'
		print compara_racionals('1/2', '1/3')
		print compara_racionals('2/1000', '3/10')
		print compara_racionals('2/100', '1/50')
		print
	
	
