# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 31-03-2017
#VERSION:- 1.1

########################################################################
#entrada:- una cadena
#descripcion:- orderna un lista
########################################################################
import sys
import funcions_racionals
########################################################################

#lligim data
lista = sys.argv[1:]

#cheak all num 
for elem in lista :
	if not funcions_racionals.es_racional_valid(elem) :
		print 'error'
		exit (0)
#main
lista.sort(cmp=funcions_racionals.compara_racionals)

print lista




