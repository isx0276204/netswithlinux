# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 20-01-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA MIRAR SI ES NUMBER ES PREFECTO OR NO.

#ESPECIFICACIONS D'ENTRADA:- CUAL QUIRO COSA WITH CONTROL DE ERROR.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		1									6									
#		2									6,28
#		3									6,28,496
#		
#		4                                   6,28,496,8128
#		
#		
#		
#		        										
#											
#																		
#		
#		
#		
#	
#	
#	

########################################################################

#REFINAMENT

#IMPORT MODUL
import sys

########################################################################

#NOW FUNCTION FOR NUMBER IS PREFETO OR NO
def number_prefecto(number_ok) :
	'''
	UNA FUNCIO QUE MIRAR SI ES  PREFECTO OR NO
	ENTRADA:- UNA INT POSITIVE NUMBER GRAN DE ZERO
	RETURN:- BOLLEAN
	'''
	suma = 0
	for d in range(1,number_ok) :
	
		if number_ok %  d == 0 :
			suma += d

	return suma == number_ok
	
	#joc de prova
	#	print number_prefect(6) --> true
	#	print number_prefect(7) ---> false
########################################################################

#CONTROL DE ERROR
if len(sys.argv) != 2 :
	print 'only need one argument for this programa'
	exit(1)
	
number = sys.argv[1]


#BUCLE FOR SI ES UNA DIGITS
for c in number :
	if c < '0' or c > '9' :
		print 'must need int positive gran de zero number'
		exit(2)

########################################################################

#VARIABLE COUNT
count = 0
start = 1

#VARIABLE CADENA
cadena = ''

#CHANGE IN INT FORM
number = int(number)


#BUCLE for find all number
while count < number :
	if number_prefecto(start) :
		cadena = cadena + str(start) + ','
		count += 1
	start += 1
			
#MOSTRA DATA
sys.stdout.write(cadena[:-1]+'\n')
