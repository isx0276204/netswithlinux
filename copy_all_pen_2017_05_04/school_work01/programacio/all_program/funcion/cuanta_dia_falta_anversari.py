# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 13-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA xixa.

#ESPECIFICACIONS D'ENTRADA:- UN CADENA .

########################################################################
import math
import modul_data
import sys
import datetime
########################################################################

#control error
if len(sys.argv) != 2 :
	print "help"
	exit (0)

########################################################################
data_correcta = sys.argv[1]
dia = modul_data.find_dia_data(data_correcta)
mes = modul_data.find_mes_data(data_correcta)
anyo = modul_data.find_year_data(data_correcta)


dia_actual = datetime.datetime.now().day
mes_actual = datetime.datetime.now().month
year_actual = datetime.datetime.now().year

data_anv = modul_data.make_formato_correcta(dia,mes,anyo)
data_actual = modul_data.make_formato_correcta(dia_actual,mes_actual,year_actual)

print data_actual
print data_anv

if modul_data.compara_data(data_actual,data_anv) == -1 :
	anyo += 1
	data_anv = modul_data.make_formato_correcta(dia,mes,anyo)

print data_anv

count = 0
while modul_data.compara_data(data_actual,data_anv) != 0 :
	count += 1
	data_actual = modul_data.next_day(data_actual)
	print data_actual
	
print count
