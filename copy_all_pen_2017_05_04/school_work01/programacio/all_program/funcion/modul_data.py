# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 01-02-2017
#VERSION:- 1.1

########################################################################

#IMPORT MODUL
import math
import datetime

########################################################################

	
def si_valida_format_data(cadena) :
	'''
	FUNCIO MIRAR SI ES DATA FORMA CORRECTA OR NO
	ENTRADA:- UNA CADENA
	RETURN:- BOOLEAN
	'''
	if len(cadena) != 10 :
		return False
	if cadena[2] != '/' or cadena[5] != '/' :
		return False
	return cadena[0:2].isdigit() and cadena[3:5].isdigit() and cadena[6:].isdigit()


def find_dia_data(cadena) :
	'''
	FUNCIO FIND DIA IN UNA FECHA 
	ENTRADA:- UNA CADENA DATA CORRECTA
	RETURN:- UN INT
	'''
	return int(cadena[:2])
	
def find_mes_data(cadena) :
	'''
	FUNCIO FIND MES IN UNA FECHA
	ENTRADA:- UNA CADENA DATA COTRRECTA
	RETURN:- UNA INT
	'''
	return int(cadena[3:5])
	
def find_anyo_data(cadena) :
	'''
	FUNCIO FIND YEAR FORM DATA
	ENTRADA:- UNA CDAENA DATA CORRECTA
	RETURN:- UNA INT
	'''
	return int(cadena[6:])
	
def si_anyo_traspas(cadena) :
	'''
	FUNCION MIRAR SI ES ANYO TRSA
	ENTRADA:- UNA INT CORRECT ANYO
	RETURN:- BOOLEAN
	'''
	return (cadena % 4 == 0 and cadena % 100 != 0) or (cadena % 400 == 0)
	
def last_dia_mes(cadena) :
	'''
	FUNCIO FIND LAST DAY OF MONTH
	ENTRADA:- UNA CADENA DE DATA CORRECTA
	RETURN:- UNA INT	
	'''
	year = find_anyo_data(cadena)
	mes = find_mes_data(cadena)
	
	if si_anyo_traspas(year) and mes == 2 :
		dias = 29
	elif mes in (4,6,9,11) :
		dias = 30
	elif mes == 2 :
		dias = 28
	else :
		dias = 31
	return dias
	
def total_dia_mes(data) :
	'''
	FUNCIO FIND LAST DAY OF MONTH
	ENTRADA:- DATA CORRECTA
	RETURN:- UNA INT	
	'''
	
	mes = find_mes_data(data)
	anyo = find_anyo_data(data)
	
	dies_mes = [0,31,28,31,30,31,30,31,31,30,31,30,31]
	if si_anyo_traspas(anyo) :
		dies_mes[2] = 29
	return dies_mes[mes]
	

def si_data_correcta(cadena) :
	'''
	FUNCIO MIRAR SI DATA ES CORRECTA OR NO
	ENTRADA:- UNA CADENA DATA
	RETURN:- BOOLEAN
	'''
	if not si_valida_format_data(cadena) :
		return False
	dia = find_dia_data(cadena)
	mes = find_mes_data(cadena)
	year = find_anyo_data(cadena)
	
	if year <= 0 :
		return False
	if mes <= 0 or mes > 12 :
		return False
	last_dia = last_dia_mes(cadena)
	return dia > 0 and dia <= last_dia 
	
def make_formato_correcta(dia,mes,year) :
	'''
	FUNCIO MAKE DATA IN CORRECT FORM
	ENTRADA:- TRES INT POSITIVE CORRECT DATA FROM
	RETURN:- UNA CADENA
	'''
	return '%02d/%02d/%04d' % (dia,mes,year)
	
def compara_data(data2,data1) :
	'''
	FUNCION PARA MIARAR CUAL DATA ES GRAN
	ENTRADA:- DOS DATA IN FROM CADENQA CORRECTA
	RETURN:- 1,-1,0
	'''
	dia_data1 = find_dia_data(data1)
	mes_data1 = find_mes_data(data1)
	anyo_data1 =find_anyo_data(data1)
	
	dia_data2 = find_dia_data(data2)
	mes_data2 = find_mes_data(data2)
	anyo_data2 =find_anyo_data(data2)
	
	if anyo_data2 > anyo_data1 :
		return 1
	elif anyo_data2 < anyo_data1 :
		return -1
	if mes_data2 > mes_data1 :
		return 1
	elif mes_data2 < mes_data1  :
		return -1
	if dia_data2 > dia_data1 :
		return 1
	elif dia_data2 < dia_data1 :
		return -1
	return 0
				
def next_day(data) :
	'''
	FUNCIO TO FIND NEXT DAY
	ENTRADDA:- DATA CORRECTA
	RETRURN:- UNA CADENA
	'''
	dia = find_dia_data(data)
	mes = find_mes_data(data)
	anyo = find_anyo_data(data)
	
	dia += 1
	
	data = make_formato_correcta(dia,mes,anyo)
	if not (si_data_correcta(data)) :
		if dia > last_dia_mes(data) :
			dia = 1
			mes += 1
			if mes > 12 :
				mes = 1
				anyo += 1
		data = make_formato_correcta(dia,mes,anyo)
	return data
	
def data_actual() :
	'''
	find dat actutal de system
	entrada:- res
	return:- cadena data correcta
	'''
	dia_actual = datetime.datetime.now().day
	mes_actual = datetime.datetime.now().month
	year_actual = datetime.datetime.now().year		
	
	return make_formato_correcta(dia_actual,mes_actual,year_actual)
						
if __name__ == '__main__' :
	if True :
		print si_valida_format_data('01/01/2045')
		print si_valida_format_data('01-01-2015')
		print si_valida_format_data('aa/02/2015')
		print si_valida_format_data('01/01/201a')
		print si_valida_format_data('001/01/201a')
		
	if True :
		print find_dia_data('01/05/2015')
		print find_mes_data('01/05/0000')
		print find_anyo_data('01/01/5015')
	
	if True :
		print si_anyo_traspas(2000)
		print si_anyo_traspas(2100)
	
	if True :
		print last_dia_mes('01/02/2000')
		print last_dia_mes('01/02/2100')
		print last_dia_mes('01/04/2100')
		print last_dia_mes('01/01/2100')
	if True :
		print si_data_correcta('29/02/2000')
		print si_data_correcta('28/02/2100')
		print si_data_correcta('31/01/2000')
		print si_data_correcta('30/04/2000')
		print si_data_correcta('29/02/2100')
		print si_data_correcta('30/02/2000')
	if True :
		print make_formato_correcta(1,2,17)
		print make_formato_correcta(29,2,2000)
		print make_formato_correcta(31,12,2017)
	if True :
		print total_dia_mes('05/02/2016')
		print total_dia_mes('01/04/2017')
	if True :
		print compara_data('01/01/2015','01/01/2015')
		print compara_data('01/02/2015','02/02/2015')
		print compara_data('01/01/2015','01/02/2015')
		print compara_data('01/01/2015','01/01/2016')
		print compara_data('02/01/2015','01/01/2015')
	if True :
		print next_day('01/01/2015')
		print next_day('31/01/2015')
		print next_day('28/02/2015')
		print next_day('28/02/2016')
		print next_day('31/12/2015')
		print next_day('29/02/2016')
	if True :
		print data_actual()
