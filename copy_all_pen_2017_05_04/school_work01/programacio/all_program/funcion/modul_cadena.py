# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 07-02-2017
#VERSION:- 1.1

########################################################################

#IMPORT MODUL
import math

########################################################################

def compressio_cadena(cadena) :
	'''
	FUNCION COMPRESSIO CADENA
	ENTRADA:- UNA CADENA NOTE --> NOT POSIBLE TO '@' IN CADENA
	SORTIDA:- UNA CADENA
	'''
	cadena += ' '
	new_cadena = ''
	last_char = ''
	count = 1

	for i in range(0,len(cadena)) :
		if cadena[i] == last_char :
			count += 1
			char = cadena[i]
		else :
			if count > 3 :
				position =  - count
				new_cadena = new_cadena[:position] + '@' + str(count) + '@' + last_char
			count = 1

		new_cadena += cadena[i]
		last_char = cadena[i]
	return new_cadena

def descompressio_cadena(cadena) :
	'''
	FUNCION COMPRESSIO CADENA
	ENTRADA:- UNA CADENA NOTE --> NOT POSIBLE TO '@' IN CADENA
	SORTIDA:- UNA CADENA
	'''
	cadena += ' '
	new_cadena = ''
	last_last_char = ''
	count = 0

	for i in range(0,len(cadena)) :
		new_cadena += cadena[i]
		if count == 2 :
			new_cadena = new_cadena[:-4] + int(last_last_char) * cadena[i]
			count = 0
		if cadena[i] == '@' :
			count += 1
		
		last_last_char = cadena[i-1]
	return new_cadena
	
	
	
def encripdited_data(cadena,key) :
	'''
	FUNCION FOR CRIPED DATA
	ENTRADA:- DOS CADENA
	RETURN:- UNA CADENA 
	'''
	new_cadena = ''
	for c in cadena :
		new_cadena = new_cadena + c + key
	return new_cadena
	
def desencripdited_data(cadena,key) :
	'''
	FUNCION FOR CRIPED DATA
	ENTRADA:- DOS CADENA
	RETURN:- UNA CADENA 
	'''
	new_cadena = ''
	for i in range(0,len(cadena)) :
		position = - len(key)
		new_cadena = new_cadena + cadena[i]
		if new_cadena[position:] == key :
			new_cadena = new_cadena[:position]
	return new_cadena

if __name__ == '__main__' :
	if False :
		print compressio_cadena('abbbbbbbcccaaa')
		print compressio_cadena('aaaa')			
		print compressio_cadena('aaaabbbcccssss')	
		print compressio_cadena('aaabbbbccccsss')	
		print compressio_cadena('aaaaaadiiiiii')    
		print compressio_cadena('jkkiiiikiiiiii') 
           
	if False :
		print descompressio_cadena('a@7@bcccaaa')
		print descompressio_cadena('@4@a') 
		print descompressio_cadena('@4@abbbccc@4@s') 
		print descompressio_cadena('aa@4@b@4@csss') 
		print descompressio_cadena('@6@ad@6@i') 
		print descompressio_cadena('jkk@4@ik@6@i') 
        
	if False :
		print encripdited_data('hello','xx')
		print encripdited_data('hello','01')
		print encripdited_data('hello','ab')
		print encripdited_data('hello','#$')

	if True :
		print desencripdited_data('hxxexxlxxlxxoxx','xx')
		print desencripdited_data('h01e01l01l01o01','01')
		print desencripdited_data('habeablablaboab','ab')
		print desencripdited_data('h#$e#$l#$l#$o#$','#$')
