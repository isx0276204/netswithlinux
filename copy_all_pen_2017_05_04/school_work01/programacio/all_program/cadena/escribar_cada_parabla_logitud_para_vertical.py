# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-12-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA ESCRIBAR DESPRES DE CADA PALABRA I SU 
#LOGITUD.

#ESPECIFICACIONS D'ENTRADA:- UN CADENA.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		'abc'								ABC3
#											
#												
#		'abcda hello'						ABCDA5 
#											HELLO5	
#		            										
#										
#																			
#	
#	
#	
#	
#	
#	

########################################################################

#REFINAMENT

#LLEGIR DATA
cadena = raw_input('cadena= ')

#MAKE NEW CADENA
new_cadena = ''
new_cadena_2 = ''

#CONSTANT 
WORD = '\n'

#GURDAR LAST WORD
last_word = ' '

#COUNT
count = 0

#BUCLE FOR BUSCAR A IN CADENA
for c in cadena :
	if not (c == last_word and c == ' ') :
		new_cadena += c
	last_word = c

#CONTROL ON LAST WORD
if last_word != ' ' :
	new_cadena += ' '

#BUCLE FOR ADD NEW CADENA
for c in new_cadena :
	if c == ' ' :
		new_cadena_2 = new_cadena_2 + str(count) + WORD
		count = 0
	else :
		new_cadena_2 += c
		count += 1 

#MOSTRA DATA
print new_cadena_2
