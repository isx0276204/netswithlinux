# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-12-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA BUSCAR MEJOR ENTRE DOS CADENA I 
#COMPTELETRA ENTRE A-Z OR a-z.

#ESPECIFICACIONS D'ENTRADA:- DOS CADENA.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		A	 		B
#		'abc'    'ok hello'					'ok hello' es major-->7
#											
#												
#		' da h'  'hell'						' da h' es mejpr-->3
#
#		'11'     'a'						'11' es mejor --> 0			
#		
#		'* a  '	 'ok'						'* a  ' es mejor --> 1
#	
#	
#	
#	

########################################################################

#REFINAMENT

#LLEGIR DATA
cadena_1 = raw_input('cadena_1= ')
cadena_2 = raw_input('cadena_2= ')

#MAKE CADENA CONSTANT
CADENA_a_z = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

#BUSCAR MEJOR CADENA
mejor = len(cadena_1)
mejor_cadena = cadena_1
if mejor < len(cadena_2) :
	mejor = len(cadena_2)
	mejor_cadena = cadena_2
	
#COUNT
count = 0	

#BUCLE FOR BUSCAR LETRA a-Z
for c in mejor_cadena :
	if c in CADENA_a_z :
		count += 1
		
#MOSTRA DATA
print mejor_cadena,'-->',count
