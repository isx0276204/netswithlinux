# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 18-01-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA MIRAR SI UNA CADENA ES DENTRO DE OTRA 
#CADENA.

#ESPECIFICACIONS D'ENTRADA:- DOS CADENA.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		A				B
#		TODO MUY BIEN   MUY					CADENA 'MUY' SI ESTA DIN DE 
#											'TODO MUY BIEN'
#		
#		
#		TODO MUY BIEN   MU					CADENA 'MU' NO ESTA DIN DE
#											 'TODO MUY BIEN' 
#		
#		
#		
#		        										
#											
#																		
#		
#		
#		
#	
#	
#	

########################################################################

#REFINAMENT

#IMPORT MODUL
import sys
import math

#VARIABLE
es_real = False
es_int = True

#VARIABLE BOOLEAN
ok = False

#LLEGIR DATA
cadena_1 = sys.argv[1]
cadena_2 = sys.argv[2]

#len de cadena 2
len_total = len(cadena_2)

#BUCLE FOR CADENA GRAN
for i in range(0,len(cadena_1)) :
	if cadena_1[i] == cadena_2[0] :
		if cadena_1[i:i+len_total] == cadena_2 :
			ok = True

print 'cadena "',cadena_2,'" esta',ok,'din de  "',cadena_1,'"'
		
