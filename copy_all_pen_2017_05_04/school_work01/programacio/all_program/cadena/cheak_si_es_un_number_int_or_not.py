# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-12-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULA SI ES NUMBER ES INT OR NOT.

#ESPECIFICACIONS D'ENTRADA:- UN NUMBER.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		35										T
#		3.5										F	
#		3+5										F
#		-3										T	
#		.5										F            										
#		5A										F										
#		0										T																	
#		+5										T	
#		++5										F
#		.										F
#	
#	
#	

########################################################################

#REFINAMENT

#IMPORT MODUL
import sys

#VARIABLE
es_int = True

#LLEGIR DATA
cadena = sys.argv[1]

#si number have any sign
if cadena[0] == '+' or cadena[0] == '-' :
	cadena = cadena[1:]

#bucle for if es un digit
for c in cadena :
	if c < '0' or c > '9' :
		es_int = False 

print cadena,es_int
