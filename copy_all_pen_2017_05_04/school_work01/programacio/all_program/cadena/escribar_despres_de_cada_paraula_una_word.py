# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-12-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA ESCRIBAR DESPRES DE CADA PALABRA UNA 
#PALABRA.

#ESPECIFICACIONS D'ENTRADA:- UN CADENA.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		'abc'								ABC BONICA PARAULA
#											
#												
#		'abcda hello'						ABCDA BONICA PARAULA HELLO	
#		            						BONICA PARAULA				
#										
#																			
#	
#	
#	
#	
#	
#	

########################################################################

#REFINAMENT

#LLEGIR DATA
cadena = raw_input('cadena= ')

#
new_cadena = ''
new_cadena_2 = ''

#CONSTANT 
WORD = 'BONICA PARAULA '

#GURDAR LAST WORD
last_word = ' '

#COUNT
count = 0

#BUCLE FOR BUSCAR A IN CADENA
for c in cadena :
	if not (c == last_word and c == ' ') :
		new_cadena += c
	last_word = c

#CONTROL ON LAST WORD
if last_word != ' ' :
	new_cadena += ' '

#BUCLE FOR WRITE WORD
for c in new_cadena :
	new_cadena_2 += c
	if c == ' ' :
		new_cadena_2 += WORD

#MOSTRA DATA
print new_cadena_2
