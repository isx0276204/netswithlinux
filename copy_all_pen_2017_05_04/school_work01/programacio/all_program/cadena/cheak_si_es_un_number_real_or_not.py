# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-12-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULA SI ES NUMBER ES real OR NOT.

#ESPECIFICACIONS D'ENTRADA:- UN NUMBER.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		35										T
#		3.5										T	
#		3+5										F
#		-3										T	
#		.5										F
#		0.5										T
#		5.										F
#		5.0										T           										
#		5A										F										
#		0										T																	
#		+5										T	
#		++5										F
#		.										F
#	
#	
#	

########################################################################

#REFINAMENT

#IMPORT MODUL
import sys
import math

#VARIABLE
es_real = False
es_int = True

#VARIABLE CADENA
cadena_1 = ''
cadena_2 = ''
cadena_final = 'not number'

#LLEGIR DATA
cadena = sys.argv[1]

#si number have any sign
if cadena[0] == '+' or cadena[0] == '-' :
	cadena = cadena[1:]
	
#bucle for if es un digit
for c in cadena :
	if c < '0' or c > '9' :
		es_int = False 

if es_int :
	cadena_final = 'int number'

else :
	if '.' in cadena :
		#BUCLE TO PUNTO
		for i in range(0,len(cadena)) :
			if cadena[i] == '.' :
				cadena_1 = cadena[:i]
				cadena_2 = cadena[i+1:]
				print cadena_1,cadena_2
				#MIRA SI ES CADA CADENA ES UN DIGITS.
				if cadena_1.isdigit() and cadena_2.isdigit() :
						es_real = True
						cadena_final = 'real number'
	
		
		
print cadena_final
