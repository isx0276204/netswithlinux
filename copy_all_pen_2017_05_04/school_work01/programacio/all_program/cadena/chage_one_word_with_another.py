# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 18-01-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA MIRAR SI UNA CADENA ES DENTRO DE OTRA 
#CADENA.

#ESPECIFICACIONS D'ENTRADA:- DOS CADENA.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		A				B    c
#		TODO MUY BIEN   MUY	 poco			TODO poco BIEN
#		
#		
#		TODO MUY BIEN   MU	n				TODO MUY BIEN 
#		
#		
#		
#		        										
#											
#																		
#		
#		
#		
#	
#	
#	

########################################################################

#REFINAMENT

#IMPORT MODUL
import sys
import math

#VARIABLE
es_real = False
es_int = True

#VARIABLE BOOLEAN
ok = False

#LLEGIR DATA
cadena_1 = sys.argv[1]
cadena_2 = sys.argv[2]
cadena_3 = sys.argv[3]
cadena_new = ''
cadena_extra = ''

#len de cadena 2
len_total_1 = len(cadena_1)
len_total_2 = len(cadena_2)
count = 0
start = 0
start_2 = 0

#BUCLE FOR CADENA GRAN
while start < len_total_1 :
		#if cadena_1[start] == cadena_2[0] :
		if cadena_1[start:start + len_total_2] == cadena_2 :
			cadena_new = cadena_new + cadena_3
			start += len_total_2 - 1
		else :
			cadena_new += cadena_1[start]
		start += 1
		
print cadena_new		
