# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 04-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR UN BYTE IN DECIMAL.
#ESPECIFICACIONS D'ENTRADA:- OCHO NUMBER INTRE 0-1.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		00000000								0
#		00001001								9
#		11111111								255
#		10000000								128

########################################################################

#REFINAMENT

#LLIGIR DATA
b_1 = int(raw_input('escribar b_1= '))
b_2 = int(raw_input('escribar b_2= '))
b_3 = int(raw_input('escribar b_3 = '))
b_4 = int(raw_input('escribar b_4= '))
b_5 = int(raw_input('escribar b_5= '))
b_6 = int(raw_input('escribar b_6= '))
b_7 = int(raw_input('escribar b_7= '))
b_8 = int(raw_input('escribar b_8= '))

#CALCULAR EL NUM. DECIMAL
num_decimal = (b_1 * 2 ** 7) + (b_2 * 2 ** 6) + (b_3 * 2 ** 5) + (b_4 * \
2 ** 4) + (b_5 * 2 ** 3) + (b_6 * 2 ** 2) + (b_7 * 2 ** 1) + (b_8 * 2 **\
0)

#MOSTRA EN DECIMAL
print num_decimal
