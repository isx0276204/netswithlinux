# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 07-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR QUIN DIA DE SEMANA I QUIAN SEMANA 
#DE ANYO ES .
#INTRODUCES DATAS:- UNA VARIABLE INT POSITIVE,MES GRAN DE ZERO I SMALL 
#OR EQUAL DE 366.

########################################################################
#			joc de prova
#	entrada					
#Dia	dia_1_genr				Dia de la setmana 		Setmana de l'any
#91 		1 					1						1
#40			6					3						6
#7			5					4						2 					
#365		0					0						53
#366		3					4						53

########################################################################
#REFINAMENT

#LLIGIR DATA
dia_anyo = int(raw_input('escribar el dia de anyo= '))
dia_de_1_gen = int(raw_input('escribar el primer dia de anyo= '))

#CALCULAR DIFFERENCEA EN DIA
diff_dia = dia_de_1_gen - 1

#CALCULAR DIA DE SEMANA
dia_semana = (dia_anyo + diff_dia ) % 7

#NOW CALCULATE SEMANA DE ANYO BY ADD ONE
semana_anyo = (dia_anyo / 7) + 1 

#NOW LETS SHOW THE CALCULATE VALUE
print 'dia = ',dia_semana,'\nsemana = ',semana_anyo
