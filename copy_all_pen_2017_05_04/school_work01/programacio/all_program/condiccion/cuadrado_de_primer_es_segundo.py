# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 09-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR SI UNA PRIMER NUMBER CUADRADO 
# DE SEGUN NUMBER.

#ESPECIFICACIONS D'ENTRADA:- DOS INT NUMBER  EN INC ORDER.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		A	B			
#		2	4						SEGUNDO ES CUADRADO DE PRIMER
#		-2	4						SEGUNDO ES CUADRADO DE PRIMER
#		1	2						SEGUNDO ES MAYOR CUADRADO DE PRIMER
#		4	6						SEGUNDO ES MENOR CUADRADO DE PRIMER
########################################################################

#REFINAMENT

#LLIGIR DATA
primer_number = int(raw_input('escribar primer_number= '))
segun_number = int(raw_input('escribar segun_number= '))

#CALCULAR CUADRAD DE PRIMER
cuadrado_primer = primer_number * primer_number

#SI CUADRADO EQUAL DE SEGUN NUMBER
if cuadrado_primer == segun_number :
	print 'SEGUNDO ES CUADRADO DE PRIMER'

#CAS SI SEGUNDO ES MAYOR DE CUADRARO
elif cuadrado_primer < segun_number :
	print 'SEGUNDO ES MAYOR CUADRADO DE PRIMER'

#ULTIMA CAS SEGUNDO ES MENOR CUADRADO DE PRIMER
else :
	print 'SEGUNDO ES MENOR CUADRADO DE PRIMER'
