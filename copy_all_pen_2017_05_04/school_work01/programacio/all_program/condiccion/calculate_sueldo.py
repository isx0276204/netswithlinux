# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 27-09-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- THIS PROGRAM TO CALCULATE SUELDO DE TRES EMPLEDO DEPUES 
#DE APLICAR 10 ,12,15 % RESPECTIVAMENT .
#INTRODUCTION DATAS:- INSERT ALWAYS TRES POSITIVE NUMBER I ALWAYES MES 
#GRAN DE ZERO.

########################################################################

#JOC DE PROVA
#			ENTRADA					SORTIDA
#	A		B		C				A		B		C
#	1000.0	1000.0	1000.0			1100.0	1120.0	1150.0
#	2000.0	3000.0	500.0			2200.0	3360.0	575.0		

#REFINAMENT
#LLEGIR ENTRADA TRES FLOAT NUMBER POSTIVE
print 'escriber sueldo de frist empledo:- '
sueldo_frist = float(raw_input())
print 'escriber sueldo de second empledo:- '
sueldo_second = float(raw_input())
print 'escriber sueldo de tercer emplado:- '
sueldo_tercer = float(raw_input())

#CALCULAR ELS PRECENTAGA
percentaga_1 = sueldo_frist * 10 / 100
percentaga_2 = sueldo_second * 12 / 100
percentaga_3 = sueldo_tercer * 15 / 100

# CALCULAR EL SUELADO TOTAL
sueldo_frist = sueldo_frist + percentaga_1
sueldo_second = sueldo_second + percentaga_2
sueldo_tercer = sueldo_tercer + percentaga_3

#MOSTAR LES SUELDO DE LOS EMPLEDO
print 'frist_sueldo: %s \nsecond_sueldo: %s \ntercer_sueldo: %s ' %(sueldo_frist,sueldo_second,sueldo_tercer)
