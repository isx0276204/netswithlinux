# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 04-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR UN BYTE IN DECIMAL.
#ESPECIFICACIONS D'ENTRADA:- OCHO NUMBER INTRE 0-1.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		00000000								0
#		00001001								9
#		11111111								255
#		10000000								128

########################################################################

#REFINAMENT

#LLIGIR DATA
b_1 = (raw_input('escribar b_1= '))
b_2 = (raw_input('escribar b_2= '))
b_3 = (raw_input('escribar b_3 = '))
b_4 = (raw_input('escribar b_4= '))
b_5 = (raw_input('escribar b_5= '))
b_6 = (raw_input('escribar b_6= '))
b_7 = (raw_input('escribar b_7= '))
b_8 = (raw_input('escribar b_8= '))

#UNA VARIABLE  PARA SUMA
suma_decimal = 0

#CALCULAR EL NUM. DECIMAL
if b_1 == '1' :
	suma_decimal += 128 
if b_2 == '1' :
	suma_decimal += 64
if b_3 == '1' :
	suma_decimal += 32
if b_4 == '1' :
	suma_decimal += 16
if b_5 == '1' :
	suma_decimal += 8
if b_6 == '1' :
	suma_decimal += 4
if b_7 == '1' :
	suma_decimal += 2
if b_8 == '1' :
	suma_decimal += 1

#MOSTRA EN DECIMAL
print suma_decimal
