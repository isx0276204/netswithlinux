# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 04-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR SI PUEDE UNA TRIANGULO OR NO.

#ESPECIFICACIONS D'ENTRADA:- TRES INT NUMBER POSITIVE EN INC ORDER.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		A	B	C		
#		3	6	8							TRUE
#		15	6	8							FALSE
#		5	5	5							TRUE

########################################################################

#REFINAMENT

#LLIGIR DATA
t_1 = int(raw_input('escribar t_1= '))
t_2 = int(raw_input('escribar t_2= '))
t_3 = int(raw_input('escribar t_3 = '))

#SI ES TRIANGLO
triangulo_correct = False

#SI SIQUE CONDICION
if ((t_1 < (t_3 + t_2)) and (t_1 > (t_3 - t_2)) and  (t_2 < (t_3 + t_1)) and (t_2 > (t_3 - t_1)) and (t_3 < (t_2 + t_1)) and (t_3 > (t_2 - t_1))) :
	triangulo_correct = True
	
#MOSTRA DATA
print triangulo_correct
