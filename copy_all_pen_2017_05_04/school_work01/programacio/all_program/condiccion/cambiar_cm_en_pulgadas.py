# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 01-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR EL CM. EN PULGADS.
#ESPECIFICACIONS D'ENTRADA:-ONE VARIABLA NUMBER FLOAT POSITIVE I TAMBIEN
#POSIBLE ZERO

########################################################################

#JOC DE PROVA
#			ENTRADA							SORTIDA
#			1.0								0.39737
#			0.0								0.0
#			2.0								0.79474

#NUMBER COSTANT VALUE DE CM. EN PULGADAS
COSTAT_PULGADAS = 0.39737

#REFINAMENT

#LLEGIR DATAS
cm = float(raw_input('escribar en cm = '))

#CALCULAR EN PULGADAS
pulgadas = cm * COSTAT_PULGADAS

#MOSTAR EN PULGADAS
print pulgadas
