# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 21-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR CUANTO DIA TE EL MES DE UN ANYO.

#ESPECIFICACIONS D'ENTRADA:- DOS VARIABLE INT NUMBER POSITIVE 
#I MES GRAN DE ZERO .

########################################################################
#			joc de prova
#		ENTRADA						SORTIDA
#	MONTH		YEAR
#	1			1988				31								
#	2			2000				29	
#	4			2013				30	
#	2			2100				28	
#	
########################################################################

#REFINAMENT

#LLIGIR DATA
mes = int(raw_input('escribar mes= '))
anyo = int(raw_input('escribar anyo = '))

#MIRAR SI MES ES FEBRERO I ANYO ES TRASPAS
if mes == 2 :
	if ((anyo % 4 == 0) and (anyo % 100 != 0)) or (anyo % 400 == 0) :
		dia = 29
	else :
		dia = 28

#MIRAR SI MES DE 30 DIAS
elif (mes == 4) or (mes == 6) or (mes == 9) or (mes == 11) :
	dia = 30

#SINO ES 31 DIAS
else :
	dia = 31
	
#MOSTRA DATA
print dia


