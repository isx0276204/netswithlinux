# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 21-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR ANYO DE TRASPAS.
#ESPECIFICACIONS D'ENTRADA:- UNA VARIABLE INT NUMBER POSITIVE 
#I MES GRAN DE ZERO .

########################################################################
#			joc de prova
#		ENTRADA						SORTIDA
#		1988						TRUE			
#		2000						TRUE
#		2013						FALSE
#		2100						FALSE
#	
########################################################################

#REFINAMENT

#LLIGIR DATA
anyo = int(raw_input('escribar anyo= '))

#MIRAR SI ANYO ES TRASPAS
resultado = ((anyo % 4 == 0) and (anyo % 100 != 0)) or (anyo % 400 == 0)

#MOSTRA DATA
print resultado
