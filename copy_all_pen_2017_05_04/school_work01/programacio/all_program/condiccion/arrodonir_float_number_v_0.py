# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 14-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR ARRODONOR NUMBER EN UNA NUMBER DE 
#DEPUES DE DECIMAL .
#ESPECIFICACIONS D'ENTRADA:- UNA VARIABLE FLOAT NUMBER POSITIVE 
#I OTRA VARIABLE INT POSITIVE.

########################################################################
#			joc de prova
#		ENTRADA						SORTIDA
#		33.37						33		
#		33.71						34
#		33.5						34
#		33.50						34
#		

########################################################################

#REFINAMENT

#LLIGIR DATA
number = float(raw_input('escribar number= '))

#CALCULAT INT DE NUMBER
number_int = int(number)

#CALCULAR ONE DECIMAL DESPUES
number_deci = number * 10
number_deci = int(number_deci)

#CALCULAR ULTIMA DIGIT
ultima_digit = number_deci % 10


#CHECK LAST DIGIT
if ultima_digit >= 5 :
	number_int = number_int + 1

#MOSTRA DATA
print number_int
