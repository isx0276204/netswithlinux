# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 04-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM MIRA SI FECHA ES VALIDA OR NO.
#ESPECIFICACIONS D'ENTRADA:- TRES VARIABLE INT NUMBER . 
#I MES GRAN DE ZERO .

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#	DIA		MONTH		YEAR
#	31		1			1988				TRUE(31)								
#	30		2			2000				FALSE(29)	
#	31		4			2013				FALSE(30)	
#	29		2			2100				FALSE(28)	
#	-5		5			2016				FALSE
#	0		4			0					FALSE
#
########################################################################

#REFINAMENT

#LLIGIR DATA
dia = int(raw_input('escribar dia= '))
mes = int(raw_input('escribar mes= '))
anyo = int(raw_input('escribar anyo = '))

#CONSTANT FOR TRUE
DATE_CORRECT = True

#MIRAR SI ANYO MES GRAN DE ZERO
if anyo <= 0 :
	DATE_CORRECT = False
	
#MIRAR SI MES ES NOT CORRECTA
if DATE_CORRECT and (mes <= 0 or mes > 12) :
	DATE_CORRECT = False
	

#MIRAR SI MES ES FEBRERO I ANYO ES TRASPAS
if mes == 2 :
	if ((anyo % 4 == 0) and (anyo % 100 != 0)) or (anyo % 400 == 0) :
		dia_de_mes = 29
	else :
		dia_de_mes = 28

#MIRAR SI MES DE 30 DIAS
elif (mes == 4) or (mes == 6) or (mes == 9) or (mes == 11) :
	dia_de_mes = 30

#SINO ES 31 DIAS
else :
	dia_de_mes = 31

#MIRAR SI DIA ES NOT CORRECT
if DATE_CORRECT and (dia <= 0 or dia > dia_de_mes) :
	DATE_CORRECT = False

#MOSTRA
print DATE_CORRECT
