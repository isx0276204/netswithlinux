# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 14-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR DIVISOR ALTRE.
#INTRODUCES DATAS:- DOS VARIABLE FLOAT NUMBER I NADA DE NUMBER ZERO.

########################################################################
#			joc de prova
#		ENTRADA						SORTIDA
#		A		B
#		4.0		5.0					NO
#		-8.0	-2.0				SI					
#		-5		1					SI
#		5		5					SI
#		8		4					SI
########################################################################

#REFINAMENT

#LLIGIR DATA
number_1 = int(raw_input('escribar number_1= '))
number_2 = int(raw_input('escriber number_2= '))



#APART EL SIQNE (CALCULAR VALOR ABSOLUT)
if number_1 < 0 :
	number_1 = -number_1
if number_2 < 0 :
	number_2 = -number_2

#BUSCAR EL MAJOR
maxi = number_1
mini = number_2
if number_2 > maxi :
	maxi = number_2
	mini = number_1
	
#DIVISOR SI EL MAJOR I DIVISOR PER MAJOR
if maxi % mini == 0 :
	final = 'si'
else :
	final = 'no'
	
#MOSTRA SI I NO DIVISOR
print final
