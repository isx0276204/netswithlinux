# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 02-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- UN PROGRAMA QUE REALICE LA CONVERSION DE PIES A METROS  .
#ESPECIFICACIONS D'ENTRADA:-ONE VARIABLA NUMBER FLOAT POSITIVE I TAMBIEN
#POSIBLE ZERO

########################################################################

#JOC DE PROVA
#			ENTRADA							SORTIDA
#			3.2808							1.0
#			0.0								0.0
#			6.5616							2.0


#NUMBER COSTANT VALUE DE METROS EM PIES.
COSTAT_PIES = 3.2808
#REFINAMENT

#LLEGIR DATAS
pies = float(raw_input('escribar en pies = '))

#CALCULAR EN PIES.
metros = pies / COSTAT_PIES

#MOSTAR EN METROS
print metros
