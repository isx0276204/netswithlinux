# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 04-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR EL HORA,MINTO I SEGUNDO
#UNA EXPERSION CORRECTA.
#ESPECIFICACIONS D'ENTRADA:- VARIABLA NUMBER INT POSITIVE I TAMBIEN 
#POSIBLE ZERO 

########################################################################

#JOC DE PROVA
#			ENTRADA							SORTIDA
#		SEGUNDO					HORA		MINTO		SEGUNDO	
#		195						0			3			15
#		0						0			0			0
#		3671					1			2			11			

#REFINAMENT
#LLEGIR DATAS
segundo = int(raw_input('segundo= '))

#CALCULAR SEGUNDO EN CORRECTA
nuevo_segundo = segundo % 60

#CALCULAR MINTO EN CORRECTA
minto = segundo / 60 
nuevo_minto = minto % 60

#CALCULAR HORA  CORRECTA
nuevo_hora = minto / 60 

#MOSTAR TODO HORA,MINTO I SEGUNDO
print 'hora %s\nminto %s\nsegundo %s' % (nuevo_hora,nuevo_minto,nuevo_segundo)
