# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 30-09-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR EL HORA,MINTO I SEGUNDO
#UNA EXPERSION CORRECTA.
#ESPECIFICACIONS D'ENTRADA:-TRES VARIABLA NUMBER INT POSITIVE I TAMBIEN 
#POSIBLE ZERO 

########################################################################

#JOC DE PROVA
#			ENTRADA							SORTIDA
#	HORA		MINTO		SEGUNDO		HORA		MINTO		SEGUNDO	
#	3			118			195			5			1			15
#	0			0			0			0			0			0
#	0			65			0			1			5			0
#	0			0			3605		1			0			5

#REFINAMENT
#LLEGIR DATAS
hora = int(raw_input('hora= '))
minto = int(raw_input('minto= '))
segundo = int(raw_input('segundo= '))

#CALCULAR SEGUNDO EN CORRECTA
nuevo_segundo = segundo % 60

#CALCULAR MINTO EN CORRECTA
minto = minto + ( segundo / 60 )
nuevo_minto = minto % 60

#CALCULAR HORA  CORRECTA
nuevo_hora = hora + ( minto / 60 )

#MOSTAR TODO HORA,MINTO I SEGUNDO
print 'hora %s\nminto %s\nsegundo %s' % (nuevo_hora,nuevo_minto,nuevo_segundo)
