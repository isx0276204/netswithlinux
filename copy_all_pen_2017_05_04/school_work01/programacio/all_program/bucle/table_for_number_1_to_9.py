# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR TABLE ENTRE 1 I 9.

#ESPECIFICACIONS D'ENTRADA:- UN NUMBER INT POSITIVE ENTRE 1 I 9.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		2									2*0 = 0
#											2*1 = 2
#											2*2 = 4
#											.
#											.
#											2*10 = 20
########################################################################

#REFINAMENT

#CONSTANT FIN
FINISH = 10

#LLEGIM DATA
number = int(raw_input('escrieu number= '))

#VARIABLE  START
start = 0




#BUCLE FOR CALCULAR I MOSTRA
while start <= FINISH :
	print number,' * ',start,' = ',number * start
	start += 1
	
