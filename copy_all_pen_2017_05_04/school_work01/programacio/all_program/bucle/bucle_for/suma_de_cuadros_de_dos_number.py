# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 13-12-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR SUMA DE DOS NUMBER CUADRADOS 
#DE INT NUMBER.

#ESPECIFICACIONS D'ENTRADA:- DOS INT NUMBER .

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#	A		B	
#	3		4								25
#	2		-5								29
#	-5		-5								50
#		
########################################################################

#REFINAMENT

#LLEGIR DATA
number_1 = int(raw_input('number_1= '))
number_2 = int(raw_input('number_2= '))

lista = [number_1,number_2]

#SUMA LET IMAGEN
suma = 0

#BUCLE FOR MOSTRA DATA
for n in lista :
		suma += n**2

#PRINT DATA
print suma

