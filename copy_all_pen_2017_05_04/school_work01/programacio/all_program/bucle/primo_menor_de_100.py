# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA BUSAR TODO PRIMO MENOR DE 100.

#ESPECIFICACIONS D'ENTRADA:- .

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#								2,3,5,7,11.....89,97
#	
#	
#	
#	
########################################################################

#REFINAMENT

#CONSTANT FIN NUMBER
FINISH_NUMBER = 100

#INICIAL NUMBER
i = 2

#START BUCLUE 1-100 I MOSTRA
while i < FINISH_NUMBER :

	#START VARIABLE
	start = 2

	#GUARDAR UNA VARIABLE
	primo_correct = True


	#BUCLE FOR MIRAR SI ES PRIMO
	while  start < i and primo_correct:
		if i % start == 0 :
			primo_correct = False
			
		start += 1
	#MOSTRA ESTA PRIMO
	if primo_correct :
		print i
		
	i += 1
