# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 15-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA SUMAR POTENCIAS DE 2 MENOR DE 100.

#ESPECIFICACIONS D'ENTRADA:- 

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#											127
#		
#		
#		
#		
########################################################################

#REFINAMENT

#CONSTANT START
i = 0

#LIMIT
LIMIT = 100

#SUMAR
suma = 0

#POTENCIA
potencia = 0

#bucle
while potencia < LIMIT :
	suma += 2 ** i
	print 'suma',suma
	i += 1
	potencia = 2 ** i
	print 'potencia',potencia

#MOSTRA
print 'suma de potencias de 2 es',suma
