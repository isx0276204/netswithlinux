# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 15-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR MAJOR NUMBER.

#ESPECIFICACIONS D'ENTRADA:- TRES NUMBER POSITIVE .

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		A	B	C		
#		2.0	3.0	5.0							5.0
#		3	2	5							5.0
#		0	6	5							6.0
#		5	5	5							5.0
########################################################################

#REFINAMENT

#CONSTANT START
i = 3

#START WITH ZERO
major = 0

#bucle
while i > 0 :
	number = float(raw_input())
	if number > major :
		major = number
	i -= 1

#MOSTRA
print major
