# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 07-12-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR MIRAR BILLETES.

#ESPECIFICACIONS D'ENTRADA:- UN NUMBER INT POSITIVE MAS GRAN DE 0.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		5									billetes de 5 es 1
#		32									billetes de 20 es 1
#											billetes de 10 es  1
#											monedas de 2 es 1
#		
#		
########################################################################

#REFINAMENT

#LLEGIR DATAS

euro = int(raw_input('euro= '))

#LLISTA FOR BUCLES
llista_billetes = [500,200,100,50,20,10,5]
llista_monedas = [2,1]


#BUCLE FOR BILLETS I PRINT
for i in llista_billetes :
	count = euro / i
	euro = euro % i
	if count > 0 :
		print 'billetes de',i,'es',count

#BUCLE FOR MONEDAS I PRINT
for i in llista_monedas :
	count = euro / i
	euro = euro % i
	if count > 0 :
		print 'monedas de',i,'es',count
