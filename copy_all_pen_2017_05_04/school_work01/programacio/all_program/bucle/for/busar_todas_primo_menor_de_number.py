# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 13-12-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA MIRARSI ES NUMBER ES PRIMO.

#ESPECIFICACIONS D'ENTRADA:- DOS INT NUMBER .

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		2									
#		5									2,3
#		9									2,3,5,7
#		19									2,3,5,7,11,13,17
#		
########################################################################

#REFINAMENT

#LLEGIR DATA
number = int(raw_input('number= '))


#BUCLE FOR ALL NUMBER MENOR DE GIVEN NUMBER
for n in range(2,number) :
#LET GUESS NUMBER IS PRIMO
	es_primo = True
#BUCLE PARA MIRAR SI ES NUMBER PRIMO
	for i in range(2,n) :
		if n % i == 0 :
			es_primo = False

#MOSTRA DATA
	if es_primo :
		print n 
