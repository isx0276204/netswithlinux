# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA BUSAR TODO PRIMO MENOR DE 100.

#ESPECIFICACIONS D'ENTRADA:- UN INT POSITIVE TAMBIEN POSIBLE ZERO.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		1									2						
#		2									2,3
#		
#	
#	
########################################################################

#REFINAMENT



#INICIAL NUMBER
i = 2

#COUNT NUMBER OF PRIMO
count = 0

#LLEGIM DATA
n_primero_number = int(raw_input('n primer number= '))

#START BUCLUE 1-100 I MOSTRA
while  count < n_primero_number :

	#START VARIABLE
	start = 2

	#GUARDAR UNA VARIABLE
	primo_correct = True


	#BUCLE FOR MIRAR SI ES PRIMO
	while  start < i and primo_correct:
		if i % start == 0 :
			primo_correct = False
			
		start += 1
	#MOSTRA ESTA PRIMO
	if primo_correct :
		count += 1
		print count, " - ", i
		
	i += 1
