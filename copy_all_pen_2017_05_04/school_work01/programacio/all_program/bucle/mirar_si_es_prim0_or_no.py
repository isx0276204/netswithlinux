# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA BUSAR NUMBER PRIMO OR NO.

#ESPECIFICACIONS D'ENTRADA:- UNA NUMBER INT POSITIVE MAS GRAN 1.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#	2										TRUE							
#	3										TRUE
#	4										FALSE
#	13										TRUE
#	19										TRUE
########################################################################

#REFINAMENT

#CONSTANT
count = 0

#START CONSTANT
start = 1

#GURDAR UNA VARIABLE
primo_correct = True

#LLIGIM DATA
number = int(raw_input('escribar number= '))

while count <= 2 and start <= number :
	if number % start == 0 :
		count += 1
	start += 1
	
	
#MIRAR SI NO ES PRIMO
if count > 2 :
	primo_correct = False

#MOSTRA DATA
print primo_correct
