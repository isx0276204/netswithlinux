# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 25-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA BUSCAR FACTOR DE NUMBER DE MULTIPUL.

#ESPECIFICACIONS D'ENTRADA:- UN INT NUMBER POSITIVE.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		4									24(1*2*3*4)
#		3									6(1*2*3)
#		1									1
#		0									0
#	
#	
########################################################################

#REFINAMENT
number = int(raw_input('number= '))

#START COUNT
count = 1

#MULTIPULE
multipul = 1

#BUCLE FOR COUNT N_TIME I MULTIPUL
while count <= number :
	multipul = multipul * count
	count += 1
	
	
#MOSTRA DATA
print multipul
