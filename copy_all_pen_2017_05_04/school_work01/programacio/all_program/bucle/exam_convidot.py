# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 29-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA BUSCAR MAKE TABLE.

#ESPECIFICACIONS D'ENTRADA:- STANDARD INPUT OF FILE.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		JOC DE PROVA DEPENDE FILE INT
#		
#		
#		
#	
#	
########################################################################

#REFINAMENT

#CONSTANT
TOTAL_COVIDATS = 61
#PERSONA_QUE_SOBRAN = 5

#MINIM PERSON PER TABLE
minim_person_table = 4
#PERSON QUE SOBRAN EN ULTIMA TABLA
persona_sobren = 0

#BUCLE FOR BUSCAR MEJOR OPION
while persona_sobren < (minim_person_table / 2) :
	minim_person_table += 1
	persona_sobren = TOTAL_COVIDATS % minim_person_table
	
total_table = TOTAL_COVIDATS / minim_person_table

#PRINT OPION MEJOR
print '\n\t*********************************************************',\
'\n\ttotal_tablas= ',total_table,\
'\n\tpersones per taula',minim_person_table,\
'\n\tpersones a la taula diferent:',persona_sobren,\
'\n\t*****************************************************************'

#COUNT TOTAL CONVIDATS I TABLE NUMBER
count = 1
table_number = 1

#BUCLE FOR MOSTRA TODA TABLE I CONVIDATS
while count <= TOTAL_COVIDATS :
	lines = raw_input()
	if count % minim_person_table == 1 :
		print '\ntable number ==>',table_number,'\n'
		table_number += 1
	print lines
	count += 1
	
