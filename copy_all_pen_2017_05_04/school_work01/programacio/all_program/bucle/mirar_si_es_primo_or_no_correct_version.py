# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA BUSAR NUMBER PRIMO OR NO.

#ESPECIFICACIONS D'ENTRADA:- UNA NUMBER INT POSITIVE MAS GRAN 1.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#	2										TRUE							
#	3										TRUE
#	4										FALSE
#	13										TRUE
#	19										TRUE
########################################################################

#REFINAMENT

#START CONSTANT
start = 2

#GUARDAR UNA VARIABLE
primo_correct = True

#LLEGIM DATA
number = int(raw_input('escribar number= '))

#BUCLE FOR MIRAR SI ES PRIMO
while  start < number and primo_correct:
	if number % start == 0 :
		primo_correct = False
		
	start += 1
	
	

#MOSTRA DATA
print primo_correct
