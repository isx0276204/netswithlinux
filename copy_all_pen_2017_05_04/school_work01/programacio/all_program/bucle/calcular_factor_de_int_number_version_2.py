# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 18-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR TODO FACTORS PRIMOS DE UN 
#NUMBER.

#ESPECIFICACIONS D'ENTRADA:- UN NUMBER INT POSITIVE I MES  GRAN ZERO.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		12								2,2,3
#		5								5
#		9								3
#		17								17
#		100								2,2,5,5
########################################################################

#REFINAMENT

#COUNT CONSTANT
count = 2


#LLIGIM DATA
number = int(raw_input('escribar number= '))

#BUCLE FOR MIRAR I PRINT
while  count <= number :
	
	if number % count == 0 : #MIRAR CONDITON SI ES MULTIPUL
		
		print count
		number = number / count
		count = 1
		
	count += 1
	
