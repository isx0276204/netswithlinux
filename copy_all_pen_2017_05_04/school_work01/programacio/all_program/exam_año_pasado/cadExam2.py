# !/usr/bin/python
# -*-coding: utf-8-*-
#################################################################

#NOM AUTOR:		Javier Lopez Gonzalez
#ISX: 			isx40995578
#DATA: 			
#VERSIÓ: 		1.0

#################################################################

# Entrada									Salida	
#   IP				
# 11111111111111111111111111111111 		255.255.255.255
# 00000001000000000000000000001111		1.0.0.15
# 0011111111							numero mal introducido	
# 200A0001000000000000000000001111		numero mal introducido						
#####################################################################
import sys

# Entrada de datos
IP = sys.argv[1]

# inicializacion variables
suma = 1
CampoIP = 0
ConvIP = ''
CampCont = 1
Validacion = True
# El dato es una ip valida?
if len(IP) == 32:
	for e in range (0,len(IP)):
		if IP[e] != '1' and IP[e] != '0':
			Validacion = False
else:
	Validacion = False

# Si es valida entonces	
if Validacion == True:
	
	# Conversion de datos a decimal
	for i in range(len(IP)-1,-1,-1):
		if IP[i] == '1':
			CampoIP += suma
		suma *= 2
		CampCont += 1
		# Si iniciamos con un campo diferente o se finaliza la conversion	
		if CampCont == 9  or i == 0:
			suma = 1
			CampCont = 1
			# Primer campo convertido
			if ConvIP == '':
				ConvIP = str(CampoIP)
			# Resto de campos	
			else:
				ConvIP = str(CampoIP) + '.' + ConvIP	
			CampoIP = 0	
	print ConvIP			
else:
	print "numero mal introducido"


