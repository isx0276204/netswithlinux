# !/usr/bin/python
# -*-coding: utf-8-*-
#################################################################

#NOM AUTOR:		Javier Lopez Gonzalez
#ISX: 			isx40995578
#DATA: 			
#VERSIÓ: 		1.0

#################################################################

# Especificaciones de entrada
# Nombre entero positivo
# 
# Entrada					Salida
#   n				
#	236							236 es multiple de dos:
#								True
#								236 es multiple de tres:
#								False
#								236 es multiple de deu:
#								False
#
#	12							12 es multiple de dos:
#								True
#								12 es multiple de tres:
#								True
#								12 es multiple de deu:
#								False
#
#	40							40 es multiple de dos:
#								True
#								40 es multiple de tres:
#								False
#								40 es multiple de deu:
#								True
#
#	30							30 es multiple de dos:
#								True
#								30 es multiple de tres:
#								True
#								30 es multiple de deu:
#								True
#
#	9							9 es multiple de dos:
#								False
#								9 es multiple de tres:
#								False
#								9 es multiple de deu:
#								True
#####################################################################
import sys

# Entrada de datos
n = sys.argv[1]

multiple2 = False
multiple3 = False
multiple10 = False
suma = 0
sSuma = ''
#mirar si un nombre és múltiple de 2, 3 o 10
if n[len(n)-1] == '0' or n[len(n)-1] == '2' or n[len(n)-1] == '4' or n[len(n)-1] == '6' or n[len(n)-1] == '8':
	multiple2 = True
	if n[len(n)-1] == '0':
		multiple10 = True
		
sSuma = n 
while len(sSuma) != 1:		
	for i in range(0,len(sSuma)):
		suma += int(sSuma[i])	
	sSuma = str(suma)
	suma = 0
	#mostrem el resultat
if sSuma[len(sSuma)-1] == '3' or sSuma[len(sSuma)-1] == '6' or sSuma[len(sSuma)-1] == '9':
	multiple3 = True	

print '%s es multiple de dos:' % n
print multiple2

print '%s es multiple de tres:' % n
print multiple3

print '%s es multiple de deu:' % n
print multiple10




