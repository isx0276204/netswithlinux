# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 14-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- MODUL

#ESPECIFICACIONS D'ENTRADA:- 

########################################################################
import math
import sys
########################################################################

def change_lista_de_str_in_int(lista) :
	'''
	FUNCIO CHANGE IN LISTA DE STR IN LISTA DE INT
	ENTRADA:- LISTA DE STR CORRECTA DE INT
	RETURN:- LISTA DE INT
	'''
	lista_int = []
	for i in range(0,len(lista)) :
		lista_int = lista_int + [int(lista[i])]
	return lista_int
	
def find_maxi_number(lista) :
	'''
	FUNCIO PARA BUSCAR NUMBER MAXI IN LISTA IN INT NUMBER
	ENTRADA:- UNA LISTA DE ALL INT NUMBER/elem
	RETURN:- UNA INT NUMBER
	'''
	maxi = lista[0]
	for elem in lista :
		if elem > maxi :
			maxi = elem
	return maxi
	
def common_lista(lista_1,lista_2) :
	'''
	funcio para buscar common elem
	entrada:- dos lista
	retrun:- una lista
	'''
	lista_common = []
	for elem_1 in lista_1 :
		if elem_1 in lista_2 :
			lista_common += [elem_1]
	return lista_common
	
def left_join_lista(lista_1,lista_2) :
	'''
	funcio para buscar common elem
	entrada:- dos lista
	retrun:- una lista
	'''
	lista_left_join = []
	for elem_1 in lista_1 :
		if elem_1 not in lista_2 :
			lista_left_join += [elem_1]
	return lista_left_join
	
def right_join_lista(lista_1,lista_2) :
	'''
	funcio para buscar common elem
	entrada:- dos lista
	retrun:- una lista
	'''
	lista_right_join = []
	for elem_2 in lista_2 :
		if elem_2 not in lista_1 :
			lista_right_join += [elem_2]
	return lista_right_join

def union_all(lista_1,lista_2) :
	'''
	FUNCION TO ALL ELEM IN LIST UNIQ
	ENTRADA:- DOS LISTA SIN REPETIR ELEM
	RETURN:- UNA LISTA UNIQ
	'''
	for elem_1 in lista_1 :
		if elem_1 not in lista_2 :
			lista_2.append(elem_1)
	return lista_2
	
def es_primo_number_or_no(number) :
	'''
	FUNCIO ES CHEAK NUMBER IS PRIMO OR NOT
	ENTRADA:- UNA INT POSITIVE NUMBER
	RETURN:- BOOLEAN
	'''
	for i in range(2,number-1) :
		if number % i == 0 :
			return False
	return True
	
def order_by_one_lista(lista) :
	'''
	funcion for make lista oredr by one uuorder lista
	entrada:- lista(no ordendnada)
	return:- lista ordenada
	'''
	'''
	######################or also can use###############################
	lista_length = len(lista)
	count = 1
	while count < lista_length :
		for i in range(0,lista_length - 1) :
			if lista[i] > lista[i + 1] :
				change_valor = lista[i]
				lista[i] = lista[i + 1]
				lista[i + 1] = change_valor
		lista_length -= 1
	return lista
	####################################################################
	'''   
	
	for i in range(1, len(lista)):
		final = len(lista) - i
		for j in range(0, final):
			if (lista[j] > lista[j + 1]):
			#comenta que es fa en les ordres de sota
				t = lista[j]
				lista[j] = lista[j + 1]
				lista[j + 1] = t
	return 
	

def bobolla_data(lista) :
	'''
	funcion for make lista oredr by one uuorder lista
	entrada:- lista(no ordendnada)
	return:- lista ordenada
	'''
	for i in range(1, len(lista)):
		final = len(lista) - i
		for j in range(0, final):
			#using this expresion 
			if modul_data.comprara_data(lista[j] > lista[j + 1]) == -1 :
			#comenta que es fa en les ordres de sota
				t = lista[j]
				lista[j] = lista[j + 1]
				lista[j + 1] = t
	return 
	
def lista_freqency(find_lista,cadena) :
	'''
	funcion find lista de freqencies
	entrada:- un cadena y una lista
	return:- lista de freqencies
	'''
	lista_number_times = [0] * len(find_lista)
	for c in cadena :
		pos = cerca_sequencial(find_lista,c) 
		if pos != -1 :
			lista_number_times[pos] += 1
	return lista_number_times
	
	
def histrograma(lista_cadena,lista_freqency) :
	'''
	funcion printa una histrograma de programa
	entrada:- dos lista una de number enteros i de otra de cadena
	return: res (print histrograma)
	print histrograma(['a','e','i','o','u'],'hola hola hola')
	a --> ***
	e --> 
	i --> 
	o --> ***
	u --> 
	'''
	CHAR = '*'
	for i in range(0,len(lista_cadena)) :
		print '%s    : %s' %(lista_cadena[i],lista_freqency[i] * CHAR)
	return

def cerca_sequencial(llista, element):
	'''Funció que donat un element, ens retorna la posició en la que es troba
	a la llista; -1 si no hi és
	Entrada: llista de element/una cadena,element/character
	Sortida: enter (posició en la que es troba l'element solo primer elem que contra)
	 -1 si l'element no està a la llista
	'''
	count = 0
	while count < len(llista) and llista[count] != element  :
		count += 1
	if count == len(llista) :
		count = -1
	return count
	
def una_lista_ordenada_buscar_pos(lista,element) :
	'''
	funcion find postion de char in una lista/una cadena
	parametro:-una lista/una cadena Y una str.
	return:- res (modifica misima lista/cadena)
	'''
	position = 0
	while lista[position] < element :
		position += 1
	lista_a = lista[:position]
	lista_b = lista[position:]
	lista_a.append(element) 
	return lista_a + lista_b
	
def comprara_segon_camp(lista_a,lista_b) :
	####falta terminar####
	'''
	funcio compra lista for segun camp
	parametro:- dos lista
	return:- -1,0,1
	'''
	valor = 0
	if lista_a[1] > lista_b[1] :
		valor = 1
	elif lista_a[1] < lista_b[1] :
		valor = -1
	return valor
	
def crea_lista_de_lista_elem(text) :
	'''
	funcio for make lista inside one more lista
	entrada:- str
	return:- lista de elem
	tipus ['maria','pi',35]
	'''
	lista = text.split(':')
	lista_final = []
	for elem in lista :
		lista_final.append(elem.split())
	return lista_final
	
	
	
if __name__ == '__main__' :
	if False :
		print find_maxi_number([1,4,5,6])
		print find_maxi_number([-1,4,-3,-9])
		print find_maxi_number([-1,-4,-5,-6])
		print find_maxi_number([11,0,-5,-6])
	if False :
		print change_lista_de_str_in_int(['1','3','4','-1'])
	if False :	
		print common_lista(['hola','hello','ok','bye'],['parveen','david','hello','ok'])
		print common_lista(['hola','hello','ok','bye','ok'],['parveen','david','hello','ok'])

	if False :
		print left_join_lista(['hola','hello','ok','bye'],['parveen','david','hello','ok'])	
	if False :
		print right_join_lista(['hola','hello','ok','bye'],['parveen','david','hello','ok'])
	if False :
		print union_all(['hola','hello','ok','bye'],['parveen','david','hello','ok'])
	if False :
		print order_by_one_lista([1,2,3,4])
		print order_by_one_lista([3,2,7,1])
		print order_by_one_lista([9,7,5,3])
		print order_by_one_lista([0,4,-1,-10])
	if False :
		print lista_freqency(['a','e','i','o','u'],'hola hola hola')
		print lista_freqency(['a','e','i','o','u'],'hela hula hila')
		print lista_freqency(['a','e','i','o','u'],'holo holo holo')
		print lista_freqency(['a','e','i','o','u'],'hl hl hl')
	if True :
		histrograma(['a','ioioie','i','o','u'],[3, 0, 0, 3, 0])
		histrograma(['a','e','i','o','u'],[3, 1, 1, 0, 1])
		histrograma(['a','e','i','o','u'],[0, 0, 0, 6, 0])
		histrograma(['a','e','i','o','u'],[0, 0, 0, 0, 0])
	if False :
		print cerca_sequencial([1,2,3,4],4)
		print cerca_sequencial(['a','b','c','d'],'c')
		print cerca_sequencial(['a','b','c','d'],'c')
		print cerca_sequencial('abcd','d')
		print cerca_sequencial([1,2,3,4],3)
	if False :
		print bobolla_data('05/12/1992','10/11/1992') 
	if False :
		print una_lista_ordenada_buscar_pos([1,2,4,5],3) 
	if False :
		print comprara_segon_camp(['maria','pi'],['calres','romani'])
	if False :	
		print crea_lista_de_lista_elem('maria pi 85:carla romani 23')
