# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 14-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- MOSTRA MAXI NUMBER INT

#ESPECIFICACIONS D'ENTRADA:- 

########################################################################
import math
import sys
import modul_lista
########################################################################

#CONTROL DE ERROR
if len(sys.argv) <= 1 :
	print 'error: falta argument'
	print 'must need prog arg [arg]...'
	exit (1)
	
all_element = sys.argv[1:]
	
for i in range(0,len(all_element)) :
	if all_element[i][0] == '-' or all_element[i][0] == '+' :
		all_element[i]=all_element[i][1:]
	if not all_element[i].isdigit() :
		print 'error: value of argument is wrong'
		print 'must need prog int_number... '
		exit (2)
		
#REFINAMENT 
#lligim data
all_element_final = sys.argv[1:]

all_element_final = modul_lista.change_lista_de_str_in_int(all_element_final)

print modul_lista.find_maxi_number(all_element_final)
