# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 19-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- MODUL

#ESPECIFICACIONS D'ENTRADA:- 

########################################################################
import math
import sys
#import some_importe_funcion_diccionaris
########################################################################

#CAPÇALERA COMPLERTA O SERÂ UN 0

def crea_dic_eleccions(l_partits):
	'''Funció que crea un diccionari de partits
	Entrada: llista de cadenes del tipus 
		VET:Verds Escola del Treball          :4000:Maria Pi:2007
	Retorn: diccionari de tots els partits, de tipus
		{SIGLES: {diccionari de partit}}
	on diccionari de partit és el tipus
		{'nom complert':'Verds Escola del Treball', 'vots': 4000, 
		'cap de llista':'Maria Pi', 'data fundacio':'27/02/2007'}'''
	dicc = {}
	for elem in l_partits :
		dicc[elem.split(':')[0]] = {'nom complert':elem.split(':')[1],\
		'vots':elem.split(':')[2],'cap de llista':elem.split(':')[3],\
		'data fundacio':elem.split(':')[4]}
	return dicc
def add_new_camp_dicc(dic,camp) :
	'''
	funcion add new camp in dicc
	entrada:- dicc and name of camp
	retrurn: res (modifica dicc aad new camp with value zero)
	'''
	for k in dic :
		dic[k][camp] = 0
	return
def busrar_maxi_vots(dic) :
	'''
	funcio buscar maxi de valor de dicc
	entrda:- dicc tipo {'a':{'votas':5,'escons':100},...}
	return :- key
	'''
	maxi = 0 #due to vots camp
	key = ''
	for k in dic :
		 if dic[k]['vots'] > maxi :
			 maxi = dic[k]['vots']
			 key = k

	return key

def make_tupla(dicc) :
	'''
	funcion make tupla de dicc
	entrada:- dicc tipo {'a':5,'d':5}
	return:- tupla tipo [('a',5),('b',5)]
	'''
	tupla = []
	for k in dicc :
		tupla.append((k,dicc[k]['vots']))
	return tupla

def make_tupla_three(dicc,camp1,camp2) :
	'''
	funcion make tupla de dicc
	entrada:- dicc tipo {'a':5,'d':5}
	return:- tupla tipo [('a',5),('b',5)]
	'''
	tupla = []
	for k in dicc :
		tupla.append((k,dicc[k][camp1],dicc[k][camp2]))
	return tupla

def add_new_value_camp(dic,key,camp,valor) :
	'''
	funcion to change valor de un camp
	emtrada:- dicc,key,camp,valor
	return:- res(modifica dicc
	'''
	dic[key][camp] = valor
	return
	
def comprara_segon_camp(lista_a,lista_b) :
	'''
	funcio compra lista for segun camp
	parametro:- dos lista
	return:- -1,0,1
	'''
	valor = 0
	if lista_a[1] > lista_b[1] :
		valor = 1
	elif lista_a[1] < lista_b[1] :
		valor = -1
	return valor
	
def comprara_segon_camp_y_mes(lista_a,lista_b) :
	'''
	funcio compra lista for segun camp
	parametro:- dos lista
	return:- -1,0,1
	'''
	valor = 0
	if lista_a[1] > lista_b[1] :
		valor = 1
	elif lista_a[1] < lista_b[1] :
		valor = -1
	else :
		if lista_a[2] > lista_b[2] :
			valor = 1
		elif lista_a[2] < lista_b[2] :
			valor = -1
	return valor
	
def repartir_escons_EDT(escons, dic_tots):
	'''	Funció per a repartir els escons segons la llei electoral de l'Escola del Treball
	(un escó a cada partit, des del més votat fins al menys, fins que s'acabin).
	El diccionari es passa per referència i la funció afegeix un camp 'escons' amb
	la quantitat d'escons de cada partit
	Entrada: escons a repartir (int), diccionari amb tots els partits
	Sortida: res
	'''
	add_new_camp_dicc(dic_tots,'escons')
	tupla = make_tupla(dic_tots)
	tupla.sort(cmp=comprara_segon_camp)
	for i in range(1,escons + 1) :
		clau = tupla[i % len(tupla)][0]
		escons = dic_tots[clau]['escons'] + 1
		add_new_value_camp(dic_tots,clau,'escons',escons)
	return dic_tots

def repartir_escons_majoria(escons, dic_tots):
	'''Funció per a repartir els escons segons la llei electoral de la majoria
	(tots els escons al partit més votat).
	El diccionari es passa per referència i la funció afegeix un camp 'escons' amb
	la quantitat d'escons de cada partit
	Entrada: escons a repartir (int), diccionari amb tots els partits
	Sortida: res
	'''
	add_new_camp_dicc(dic_tots,'escons')
	maxi_vota = busrar_maxi_vots(dic_tots)
	add_new_value_camp(dic_tots,maxi_vota,'escons',escons)
	
	return dic_tots
	
def mostra_dicc_amb_tubpla_ordenada(dicc,tupla_order) :
	'''
	funcion fro print dicc keys and valor
	entrada:- dicc tip {'a':5,'b':10}
	reurn:- res(print de dicc)
	'''
	for k in tupla_order :
		clau = k[0]
		print 'Partit:', clau,dicc[clau]['nom complert'],'Data fundacio:'\
		,dicc[clau]['data fundacio'],'VOTS:',dicc[clau]['vots'],'ESCONS:'\
		,dicc[clau]['escons']
	return	
	

