# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 19-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- MODUL

#ESPECIFICACIONS D'ENTRADA:- 

########################################################################
import math
import sys
import some_importe_funcion_diccionaris
########################################################################
def find_year(data):
	'''
	funcion to find year in data
	param:- dat correcta
	return.- year
	'''
	return data.split('/')[2]

def make_dicc_year(data_lista) :
	'''
	funcion make dicc
	param:- lista de data corercta
	reurna:- dicc tip {2009:1,2010:5}
	'''
	dicc = {}
	for elem in data_lista :
		year = find_year(elem)
		if year in dicc :
			dicc[year] += 1
		else :
			dicc[year] = 1
	return dicc

aniversaris =  ['19/02/2000', '02/03/1984', '24/13/1987', '01/01/2013', '02/02/2000', '02/01/2000', '23/01/1984']

dicc = make_dicc_year(aniversaris)

tupla = dicc.items()

some_importe_funcion_diccionaris.histrograma_orderna_dicc_amb_tupla(dicc,tupla)

tupla.sort()

some_importe_funcion_diccionaris.histrograma_orderna_dicc_amb_tupla(dicc,tupla)

tupla.sort(cmp=some_importe_funcion_diccionaris.comprara_segon_camp)

some_importe_funcion_diccionaris.histrograma_orderna_dicc_amb_tupla(dicc,tupla)

