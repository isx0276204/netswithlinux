# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 19-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- MODUL

#ESPECIFICACIONS D'ENTRADA:- 

########################################################################
import math
import sys
########################################################################

def mitjana(l):
	'''Funció que calcula la mitjana
	Entrada: llista d'enters/ float
	Sortida: int'''
	suma = 0
	count = 0
	for elem in l :
			suma += elem
			count += 1
	return suma / count
	
def arreglar_lista(data) :
	'''
	funcion for calculate to avg and change lista format
	entrada:- lista 
	return:- res
	'''
	lista_avg = []
	for i in range(0,len(data)) :
		avg = mitjana(data[i][2])
		data[i][2] = avg
	return
	
def mostraAlumne(l_alumne,dni):
	'''
	Funció que ens mostra les dades d'un alumne d'aquesta manera:
	maria casas, dni: 72345678A,  mitjana: 5
	Entrada: llista d'alumne [dni, no cognom, [nota1, nota2, nota3]]
	Sortida: res
	'''
	print 'dni:',dni,' nom:',l_alumne['nom'],' cognom:',l_alumne['cognom'],' avg:',l_alumne['avg']
	return
	
def llistat_Alumnes(tots):
	'''
	Funció que em mostra tots els alumnes, un per línia, utilitzant la funció 
	mostraAlumne
	Entrada: llista de llistes d'alumnes
	sortida: res
	'''
	for elem in tots :
		mostraAlumne(tots[elem],elem )	
	return
	
def criteri_cognom(alumne1,alumne2):
	'''criteri d' ordenació segons el cognom del segon camp d'una llista
	Entrada: 2 llistes del tipus...
	Sortida: 1 si el cognaom de l1 va alfabèticament abans del de l2
	'''
	cognom_1 = alumne1[1].split()[1]
	cognom_2 = alumne2[1].split()[1]
	valor = 0
	if cognom_1 > cognom_2 :
		valor = 1
	if cognom_2 > cognom_1 :
		valor = -1
	return valor
	
def criteri_mitjana(cognom1,cognom2):
	'''criteri d' ordenació segons la mitjana de les notes d'una llista 
	amb les dades d'un alumne
	Entrada: 2 llistes del tipus...
	Sortida: 1 si la mitjana de l'alumne 1 és menor a la mitjana de l'alumne2
	'''
	valor = 0
	if cognom1 > cognom2 :
		valor = 1
	if cognom2 > cognom1 :
		valor = -1
	return valor
	
def create_dicc_form_lista(lista) :
	'''
	funcio creat dicc
	param:- una lista
	reeturn dicc
	'''
	dicc = {}
	for elem in lista :
		dicc[elem[0]] = {'nom':elem[1].split()[0],'cognom':elem[1].split()[0],'avg':elem[2]}
	return dicc
#programa principal

dadesAlumnes = [['12345678A', 'jordi perez', [5.2, 8, 9, 4.3]], ['52345678A', 
	'manel garcia', [2, 1.5, 3, 5]], ['62345678A', 'joan-ramon vila', [7,7,8,10]],
	 ['72345678A', 'maria casas', [7, 2.5, 9, 3]],  ['111111111D', 'josep-lluis marquez',
	 [3, 10, 5]]]
	 
arreglar_lista(dadesAlumnes)

dicc_ok = create_dicc_form_lista(dadesAlumnes)

print 'alumnes sense ordenar:'

print llistat_Alumnes(dicc_ok)

print 'alumnes ordenats alfabèticament segons cognom:'
dadesAlumnes.sort(cmp=criteri_cognom)
print llistat_Alumnes(dadesAlumnes)

print 'alumnes ordenats per mitjana:'
dadesAlumnes.sort(cmp=criteri_mitjana)
print llistat_Alumnes(dadesAlumnes)
