BRIDGES Y ROUTERS

##### 1. Conexionado, nombres de interfaces y direcciones MAC

Para esta práctica hay que interconectar los equipos de una fila con 2 latiguillos de red (sin usar las rosetas del aula) siguiendo el siguiente esquema:

```
FILA DE 3 EQUIPOS:
                              |usbB                          
   +--------+            +--------+            +--------+   
   |        |eth0    usbA|        |usbC    eth0|        |   
   | HOST A +------------+ HOST B +------------+ HOST C |   
   |        |            |        |            |        |   
   +--------+            +--------+            +--------+   
                             |eth0                          
```

El equipo del medio de la fila ha de tener conectadas 3 tarjetas usb-ethernet. Se han de renombrar estas tarjetas y cambiar las mac. Las direcciones mac de cada tarjeta han de ser distintas siguiendo la siguiente
codificación:

AA:AA:AA:00:00:YY



Siendo YY la numeración de la tarjeta USB que lleva en su pegatina.

Hay que renombrar las tarjetas de red para que se llamen: usbA, usbB, usbC y eth0(la de la placa base)

Al final la orden ip link show ha de mostrar algo como lo siguiente:







Para empezar 

	HOST B (Router)
	
	```
	systemctl stop NetworkManager
	```
	
Para eliminar todas las rutas "ip route flush"

	```
	ip r f dev enp2s0
	```
Connectamos los 3 usb 

	Connectamos los 3 usb y Ejucutamos el srcipt para cambiar nombre de interface y MAC.
	
    ```
	label_A=1
	label_B=2
	label_C=3
	
	```
	
Activar bit de forwarding para que trabaje como router:
	```
	echo 1 > /proc/sys/net/ipv4/ip_forward
	```





##### 2. Conectar 3 equipos haciendo routing y saliendo a internet:

A. Configurar las ips en cada equpo según el esquema y hacer pings desde hostA a hostB y desde hostC a hostA

```
ESQUEMA CAPA 3
                       +---------+
    172.17.A.0/24    .1| ROUTER  |.1   172.17.C.0/24
   +-------------------+ HOST B  +--------------------+
        |.2        usbA|         |usbC        |.2
        |eth0          +---------+            |eth0
     +------+                              +------+
     |HOST A|                              |HOST C|
     +------+                              +------+
                            
                            

```

B. Activar el bit de forwarding y poner como default gateway al host B que hará de router entre las dos redes, listar las rutas en host B y verificar que se pueden hacer pings entre HOST A y HOST C

C. Conectar eth0 del HOST B a la roseta del aula, pedir una ip dinámica con dhclient. Verificar que desde el HOST B se puede hacer ping al 8.8.8.8. 

D. Activar el enmascarmiento de ips para el tráfico saliente por eth0 y verificar que HOST A Y HOST C pueden hacer ping al 8.8.8.8

E. Poner como servidor dns a 8.8.8.8 en /etc/resolv.conf en hostA y host C y verificar que se puede navegar por internet con  haciendo un wget de http://www.netfilter.org


Como Host B ( Router) Añaidmos los IP 
```
ip a a 172.17.3.1/24 dev usbC
ip a a 172.17.1.1/24 dev usbA

```
como Host C hemos hecho siguiente comandos.


```

--------------------------------------------------------------------------------
         Host A : desde ordenador  A
------------------------------------------------------------------------------- 
  704  systemctl stop NetworkManager
  705  ip a
  706  ip a f dev enp2s0
  707  ip r f all
  708  ip a
  709  ip a a 172.17.3.2/24 dev enp2s0
  710  ip a
  711  ping 172.17.3.1
  712  ip a
  713  ping 172.17.3.1
  714  ping 172.17.3.2
  715  ping 172.17.1.1=======no se puede hacer ping porque defualt via no esta puesta.
  716  ping 172.17.1.2=======no se puede hacer ping porque defualt via no esta puesta.
  717  ping 172.17.1.1
  718  ip r a default via 172.17.3.1
  719  ping 172.17.1.2
  720  ping 8.8.8.8
  721  wget http://www.netfilter.org
  722  echo "nameserver 8.8.8.8" > /etc/resolv.conf
  
```



Como Host A hemos hecho el sguiente commandos

```
--------------------------------------------------------------------------------
         Host A : desde ordenador  A
-------------------------------------------------------------------------------  
  704  systemctl stop NetworkManager
  705  ip a
  706  ip a f dev enp2s0
  707  ip r f all
  708  ip a
  709  ip a a 172.17.1.2/24 dev enp2s0
  710  ip a
  711  ping 172.17.1.1
  712  ip a
  713  ping 172.17.1.1
  714  ping 172.17.1.2
  715  ping 172.17.3.1=======no se puede hacer ping porque defualt via no esta puesta.
  716  ping 172.17.3.2=======no se puede hacer ping porque defualt via no esta puesta.
  717  ping 172.17.1.1
  718  ip r a default via 172.17.1.1
  719  ping 172.17.3.2
  720  ping 8.8.8.8
  721  wget http://www.netfilter.org
  722  echo "nameserver 8.8.8.8" > /etc/resolv.conf

```



##### 3. Conectar 2 equipos utilizando un equipo intermedio que hace de bridge. 

En esta práctica queremos que HOST B trabaje como si fuera un switch, interconectando a host A y host C. 

A. Hacer flush de todas las ips y verificar que no queda ninguna ruta ni dirección ip asociada a ningún equipo.

B. Crear el bridge br0 y añadir usbA y usbC a ese bridge. Listar con detalle la configuración del bridge.

C. En el host A nos ponemos la ip 192.168.100.A/24 y en el equipo C la ip 192.168.100.B/24. Hacemos ping entre los dos equipos y debería de ir.

D. Listar en host B la tabla de relación de puertos y MACs en el bridge


##### 4. Conectar todos los equipos del aula haciendo switching.

Conseguir esta interconexión entre las filas:
```
\
                   |                     
                   +------+              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+              
 +------+ +------+        |              
                          |              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+
 +------+ +------+        |
                          |
                          +
```
Cada equipo ha de tener una ip 192.168.100.B/24

A. Lanzar fpings para verificar que todos los equipos A y C responden

B. Asignar una ip al br0 del hostB y lanzar fping para verificar que todos los equipos A,B y C responden

C. Listar la tabla de asignación de puertos y MACs después de hacer un fping 

##### 5. Conectar todos los equipos del aula haciendo routing.

```
\
                         +
                         |
                         |
                         |
                     +---+-----+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2            |         |          .2|
      |              +---------+            |
   +--+---+              |.1             +--+---+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |172.16.XX.0/24
                         |
                         |
                         |
                     usbB|.2
                     +---------+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2        usbA|         |usbC        |.2
      |eth0          +---------+            |eth0
   +------+          eth0|.1             +------+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |
                         +
```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden

##### 6. Conectar todos los equipos del aula haciendo switching y routing.

Seguir este esquema


```
                                       172.16.0.0/24
           +------------------------------------------------------------------------------+
                        |.XX                                                     |.XX
                        |                                                        |
                        |br0(usbB,eth0)                                          |br0(usbB,eth0)
                    +---------+                                              +---------+
 172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24        172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
+-------------------+ HOST B  +--------------------+     +-------------------+ HOST B  +--------------------+
     |.2        usbA|         |usbC        |.2                |.2        usbA|         |usbC        |.2
     |eth0          +---------+            |eth0              |eth0          +---------+            |eth0
  +------+                              +------+           +------+                              +------+
  |HOST A|                              |HOST C|           |HOST A|                              |HOST C|
  +------+                              +------+           +------+                              +------+

```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden
