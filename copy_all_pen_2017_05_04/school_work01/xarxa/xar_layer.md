#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"
	
	[root@j01 ~]# ip a f dev enp2s0
	
	[root@j01 ~]# ip a f all
	
	Device "all" does not exist.

	[root@j01 ~]# ip a f a

	Device "a" does not exist.

	[root@j01 ~]# ip r f all
	[root@j01 ~]# ip a

	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		valid_lft forever preferred_lft forever
    
		inet6 ::1/128 scope host 
			valid_lft forever preferred_lft forever

	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff

	[root@j01 ~]# ip r

	[root@j01 ~]# 

	
Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
		inet 2.2.2.2/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 3.3.3.3/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 4.4.4.4/25 scope global enp2s0
		   valid_lft forever preferred_lft forever

Consulta la tabla de rutas de tu equipo

	[root@j01 ~]# ip r
	2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4 


Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132

	2.2.2.2 ==> OK PING
	2.2.2.254 ==>   [root@j01 ~]# ping 2.2.2.54
					PING 2.2.2.54 (2.2.2.54) 56(84) bytes of data.
					From 2.2.2.2 icmp_seq=1 Destination Host Unreachable
					From 2.2.2.2 icmp_seq=2 Destination Host Unreachable
					From 2.2.2.2 icmp_seq=3 Destination Host Unreachable

	2.2.5.2 ==> 	[root@j01 ~]# ping 2.2.5.2
					connect: Network is unreachable
	
	3.3.3.35 ==>  		[root@j01 ~]# ping 3.3.3.35
						PING 3.3.3.35 (3.3.3.35) 56(84) bytes of data.
						^C
						--- 3.3.3.35 ping statistics ---
						3 packets transmitted, 0 received, 100% packet loss, time 1999ms

	3.3.200.45 ==> 	waiting may be posible any of host cannect

	4.4.4.8		==> wating inside mascara but host connect with this ip
	
	4.4.4.132 ==> network is unreachable due to mascara is not primitted 
	
#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	[root@j01 ~]# ip a f dev enp2s0
	[root@j01 ~]# ip r f all
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	[root@j01 ~]# 


Conecta una segunda interfaz de red por el puerto usb

==>ok

Cambiale el nombre a usb0

==> ok

Modifica la dirección MAC

==> 

Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.

Observa la tabla de rutas

#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

Lanzar iperf en modo servidor en cada ordenador

Comprueba con netstat en qué puerto escucha

Conectarse desde otro pc como cliente

Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

Encontrar los 3 paquetes del handshake de tcp

Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas

