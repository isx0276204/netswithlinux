C2950 Boot Loader (C2950-HBOOT-M) Version 12.1(11r)EA1, RELEASE SOFTWARE (fc1)
Compiled Mon 22-Jul-02 17:18 by antonino
WS-C2950-24 starting...
Base ethernet MAC Address: 00:0d:29:be:93:00
Xmodem file system is available.
Initializing Flash...
flashfs[0]: 2 files, 1 directories
flashfs[0]: 0 orphaned files, 0 orphaned directories
flashfs[0]: Total bytes: 7741440
flashfs[0]: Bytes used: 2890240
flashfs[0]: Bytes available: 4851200
flashfs[0]: flashfs fsck took 6 seconds.
...done initializing flash.
Boot Sector Filesystem (bs:) installed, fsid: 3
Parameter Block Filesystem (pb:) installed, fsid: 4
Loading "flash:/c2950-i6q4l2-mz.121-13.EA1.bin"...############################################################################################################

File "flash:/c2950-i6q4l2-mz.121-13.EA1.bin" uncompressed and installed, entry point: 0x80010000
executing...

              Restricted Rights Legend

Use, duplication, or disclosure by the Government is
subject to restrictions as set forth in subparagraph
(c) of the Commercial Computer Software - Restricted
Rights clause at FAR sec. 52.227-19 and subparagraph
(c) (1) (ii) of the Rights in Technical Data and Computer
Software clause at DFARS sec. 252.227-7013.

           cisco Systems, Inc.
           170 West Tasman Drive
           San Jose, California 95134-1706



Cisco Internetwork Operating System Software 
IOS (tm) C2950 Software (C2950-I6Q4L2-M), Version 12.1(13)EA1, RELEASE SOFTWARE (fc1)
Copyright (c) 1986-2003 by cisco Systems, Inc.
Compiled Tue 04-Mar-03 02:14 by yenanh
Image text-base: 0x80010000, data-base: 0x805A8000


Initializing flashfs...
flashfs[1]: 2 files, 1 directories
flashfs[1]: 0 orphaned files, 0 orphaned directories
flashfs[1]: Total bytes: 7741440
flashfs[1]: Bytes used: 2890240
flashfs[1]: Bytes available: 4851200
flashfs[1]: flashfs fsck took 6 seconds.
flashfs[1]: Initialization complete.
Done initializing flashfs.
POST: System Board Test : Passed
POST: Ethernet Controller Test : Passed
ASIC Initialization Passed

POST: FRONT-END LOOPBACK TEST : Passed
cisco WS-C2950-24 (RC32300) processor (revision J0) with 20839K bytes of memory.
Processor board ID FOC0726Z036
Last reset from system-reset
Running Standard Image
24 FastEthernet/IEEE 802.3 interface(s)

32K bytes of flash-simulated non-volatile configuration memory.
Base ethernet MAC Address: 00:0D:29:BE:93:00
Motherboard assembly number: 73-5781-11
Power supply part number: 34-0965-01
Motherboard serial number: FOC072604EM
Power supply serial number: DAB07226PM5
Model revision number: J0
Motherboard revision number: A0
Model number: WS-C2950-24
System serial number: FOC0726Z036

         --- System Configuration Dialog ---

Would you like to enter the initial configuration dialog? [yes/no]: 
00:00:13: %SPANTREE-5-EXTENDED_SYSID: Extended SysId enabled for type vlan
00:00:17: %SYS-5-RESTART: System restarted --
Cisco Internetwork Operating System Software 
IOS (tm) C2950 Software (C2950-I6Q4L2-M), Version 12.1(13)EA1, RELEASE SOFTWARE (fc1)
Copyright (c) 1986-2003 by cisco Systems, Inc.
Compiled Tue 04-Mar-03 02:14 by yenanh
