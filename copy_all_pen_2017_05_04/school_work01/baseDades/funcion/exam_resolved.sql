---- 3
training=# select extract('year' from fecha_pedido),extract('month' from fecha_pedido),count(distinct clie),count(num_pedido),sum(importe) from pedidos group by 1,2 order by 1,2;
 date_part | date_part | count | count |   sum    
-----------+-----------+-------+-------+----------
      1989 |         1 |     1 |     1 |  1896.00
      1989 |        10 |     2 |     2 | 18978.00
      1989 |        11 |     1 |     1 |   760.00
      1989 |        12 |     3 |     5 | 65078.00
      1990 |         1 |     9 |    10 | 49393.00
      1990 |         2 |     8 |    10 | 80236.00
      1990 |         3 |     1 |     1 | 31350.00
      2017 |         4 |     1 |     1 |   550.00
(8 rows)

-----4
training=# select id_fab,id_producto,descripcion,left(descripcion,1),right(descripcion,1),case when sum(cant) is null then 0 else sum(cant) end as sum_cant from productos left join pedidos on id_fab = fab and id_producto = producto where left(descripcion,1) = upper(right(descripcion,1)) group by id_fab,id_producto;
 id_fab | id_producto | descripcion | left | right | sum_cant 
--------+-------------+-------------+------+-------+----------
 qsa    | xk48a       | Reductor    | R    | r     |        0
 qsa    | xk47        | Reductor    | R    | r     |       28
 qsa    | xk48        | Reductor    | R    | r     |        0
(3 rows)

----1
training=# select to_char(fecha_pedido,'Day') || '(' || to_char(fecha_pedido,'D') || ')', sum(importe) from pedidos group by to_char(fecha_pedido,'Day'),to_char(fecha_pedido,'D') order by to_char(fecha_pedido,'D');
   ?column?   |    sum    
--------------+-----------
 Sunday   (1) |  63528.00
 Monday   (2) |   8863.00
 Tuesday  (3) |  26730.00
 Wednesday(4) |   4056.00
 Thursday (5) |  28498.00
 Friday   (6) | 100330.00
 Saturday (7) |  16236.00
(7 rows)

---2
 select num_clie,empresa,concat(upper(a.fab),'-',upper(a.producto)) as codi_producto,
 (select left(descripcion,10) from productos where id_fab = a.fab and id_producto = a.producto),
 round(sum(importe)/sum(cant),2),
 (select round(sum(importe)/sum(cant),2) from pedidos b where b.fab = a.fab and b.producto = a.producto) as mijitas_aproxi_todos,
 (select precio from productos where id_fab = a.fab and id_producto = a.producto ) 
 from pedidos a 
 join productos on a.fab = id_fab and a.producto = id_producto
  join clientes on a.clie = num_clie
   group by num_clie,id_fab,id_producto 
   order by 3 ;


select num_clie,empresa,concat(upper(a.fab),'-',upper(a.producto)) as codi_producto
 
 from pedidos
 join productos on a.fab = id_fab and a.producto = id_producto
  join clientes on clie = num_clie
   group by num_clie,a.fab,a.producto 
   order by 3 ;
-----2 right one------
training=# select num_clie,empresa,concat(upper(a.fab),'-',upper(a.producto)) as codi_producto,
 (select left(descripcion,10) from productos where id_fab = a.fab and id_producto = a.producto),
 round(sum(importe)/sum(cant),2),
 (select round(sum(importe)/sum(cant),2) from pedidos b where b.fab = a.fab and b.producto = a.producto) as mijitas_aproxi_todos,
 (select precio from productos where id_fab = a.fab and id_producto = a.producto ) 
 from pedidos a 
 join productos on a.fab = id_fab and a.producto = id_producto
  join clientes on a.clie = num_clie
   group by num_clie,a.fab,a.producto 
   order by 3 ;
 num_clie |      empresa      | codi_producto |    left    |  round  | mijitas_aproxi_todos | precio  
----------+-------------------+---------------+------------+---------+----------------------+---------
     2118 | Midwest Systems   | ACI-41002     | Articulo T |   76.00 |                76.00 |   76.00
     2103 | Acme Mfg.         | ACI-41002     | Articulo T |   76.00 |                76.00 |   76.00
     2111 | JCP Inc.          | ACI-41003     | Articulo T |  107.00 |               107.00 |  107.00
     2103 | Acme Mfg.         | ACI-41004     | Articulo T |  117.00 |               117.00 |  117.00
     2102 | First Corp.       | ACI-41004     | Articulo T |  117.00 |               117.00 |  117.00
     2111 | JCP Inc.          | ACI-4100X     | Ajustador  |   25.00 |                25.00 |   25.00
     2108 | Holm & Landis     | ACI-4100X     | Ajustador  |   25.00 |                25.00 |   25.00
     2103 | Acme Mfg.         | ACI-4100Y     | Extractor  | 2500.00 |              2500.00 | 2750.00
     2114 | Orion Corp        | ACI-4100Z     | Montador   | 2500.00 |              2500.00 | 2500.00
     2107 | Ace International | ACI-4100Z     | Montador   | 2500.00 |              2500.00 | 2500.00
     2118 | Midwest Systems   | BIC-41003     | Manivela   |  652.00 |               652.00 |  652.00
     2108 | Holm & Landis     | FEA-112       | Cubierta   |  148.00 |               148.00 |  148.00
     2101 | Jones Mfg.        | FEA-114       | Bancada Mo |  243.00 |               243.00 |  243.00
     2112 | Zetacorp          | IMM-773C      | Riostra 1/ |  975.00 |               975.00 |  975.00
     2109 | Chen Associates   | IMM-775C      | Riostra 1- | 1425.00 |              1425.00 | 1425.00
     2108 | Holm & Landis     | IMM-779C      | Riostra 2- | 1875.00 |              1875.00 | 1875.00
     2120 | Rico Enterprises  | IMM-779C      | Riostra 2- | 1875.00 |              1875.00 | 1875.00
     2106 | Fred Lewis Corp.  | QSA-XK47      | Reductor   |  355.00 |               357.36 |  355.00
     2114 | Orion Corp        | QSA-XK47      | Reductor   |  355.00 |               357.36 |  355.00
     2118 | Midwest Systems   | QSA-XK47      | Reductor   |  388.00 |               357.36 |  355.00
     2111 | JCP Inc.          | REI-2A44G     | Pasador Bi |  350.00 |               350.00 |  350.00
     2117 | J.P. Sinclair     | REI-2A44L     | Bisagra Iz | 4500.00 |              4500.00 | 4500.00
     2112 | Zetacorp          | REI-2A44R     | Bisagra Dc | 4500.00 |              4500.00 | 4500.00
     2106 | Fred Lewis Corp.  | REI-2A45C     | V Stago Tr |   79.00 |                79.00 |   79.00
     2107 | Ace International | REI-2A45C     | V Stago Tr |   79.00 |                79.00 |   79.00
(25 rows)
