--          Modificació
-------------------------------------------------------------------------------

-- 6.1-  Inserir un nou venedor amb nom "Enric Jimenez" amb identificador 1012, oficina 18, títol "Dir Ventas", contracte d'1 de febrer del 2012, director 101 i vendes 0.

training=# insert into repventas (num_empl,nombre,oficina_rep,titulo,contrato,director,ventas) values (1012,'enric jimenez',18,'Dir Ventas','2012-02-01',101,0);
INSERT 0 1
training=# insert into repventas (num_empl,nombre,oficina_rep,titulo,contrato,director,cuota,ventas) values (1012,'enric jimenez',18,'Dir Ventas','2012-02-01',101,null,0);
ERROR:  duplicate key value violates unique constraint "repventas_pkey"
DETAIL:  Key (num_empl)=(1012) already exists.
training=# insert into repventas (num_empl,nombre,oficina_rep,titulo,contrato,director,cuota,ventas) values (1012,'enric jimenez',18,'Dir Ventas','2013-02-01',101,null,0);
ERROR:  duplicate key value violates unique constraint "repventas_pkey"
DETAIL:  Key (num_empl)=(1012) already exists.
training=# insert into repventas (num_empl,nombre,oficina_rep,titulo,contrato,director,cuota,ventas) values (1013,'enric jimenez',18,'Dir Ventas','2013-02-01',101,null,0);
INSERT 0 1


-- 6.2- Inserir un nou client "C1" i una nova comanda pel venedor anterior.

training=# insert into clientes (num_clie,empresa,rep_clie,limite_credito) values (5001,'c1',103,50000.00);
INSERT 0 1
training=# select * from clientes ;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2102 | First Corp.       |      101 |       65000.00
     2103 | Acme Mfg.         |      105 |       50000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2107 | Ace International |      110 |       35000.00
     2115 | Smithson Corp.    |      101 |       20000.00
     2101 | Jones Mfg.        |      106 |       65000.00
     2112 | Zetacorp          |      108 |       50000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2114 | Orion Corp        |      102 |       20000.00
     2124 | Peter Brothers    |      107 |       40000.00
     2108 | Holm & Landis     |      109 |       55000.00
     2117 | J.P. Sinclair     |      106 |       35000.00
     2122 | Three-Way Lines   |      105 |       30000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2106 | Fred Lewis Corp.  |      102 |       65000.00
     2119 | Solomon Inc.      |      109 |       25000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2113 | Ian & Schmidt     |      104 |       20000.00
     2109 | Chen Associates   |      103 |       25000.00
     2105 | AAA Investments   |      101 |       45000.00
     5001 | c1                |      103 |       50000.00
(22 rows)

training=# insert into pedidos (num_pedido,fecha_pedido,clie,rep,fab,producto,cant,importe) values (500001,'2017-04-10',5001,103,'aci','2a44r',10,550)
training-# ;
INSERT 0 1
training=# select * from pedidos ;
 num_pedido | fecha_pedido | clie | rep | fab | producto | cant | importe  
------------+--------------+------+-----+-----+----------+------+----------
     112961 | 1989-12-17   | 2117 | 106 | rei | 2a44l    |    7 | 31500.00
     113012 | 1990-01-11   | 2111 | 105 | aci | 41003    |   35 |  3745.00
     112989 | 1990-01-03   | 2101 | 106 | fea | 114      |    6 |  1458.00
     113051 | 1990-02-10   | 2118 | 108 | qsa | k47      |    4 |  1420.00
     112968 | 1989-10-12   | 2102 | 101 | aci | 41004    |   34 |  3978.00
     110036 | 1990-01-30   | 2107 | 110 | aci | 4100z    |    9 | 22500.00
     113045 | 1990-02-02   | 2112 | 108 | rei | 2a44r    |   10 | 45000.00
     112963 | 1989-12-17   | 2103 | 105 | aci | 41004    |   28 |  3276.00
     113013 | 1990-01-14   | 2118 | 108 | bic | 41003    |    1 |   652.00
     113058 | 1990-02-23   | 2108 | 109 | fea | 112      |   10 |  1480.00
     112997 | 1990-01-08   | 2124 | 107 | bic | 41003    |    1 |   652.00
     112983 | 1989-12-27   | 2103 | 105 | aci | 41004    |    6 |   702.00
     113024 | 1990-01-20   | 2114 | 108 | qsa | xk47     |   20 |  7100.00
     113062 | 1990-02-24   | 2124 | 107 | fea | 114      |   10 |  2430.00
     112979 | 1989-10-12   | 2114 | 102 | aci | 4100z    |    6 | 15000.00
     113027 | 1990-01-22   | 2103 | 105 | aci | 41002    |   54 |  4104.00
     113007 | 1990-01-08   | 2112 | 108 | imm | 773c     |    3 |  2925.00
     113069 | 1990-03-02   | 2109 | 107 | imm | 775c     |   22 | 31350.00
     113034 | 1990-01-29   | 2107 | 110 | rei | 2a45c    |    8 |   632.00
     112992 | 1989-11-04   | 2118 | 108 | aci | 41002    |   10 |   760.00
     112975 | 1989-12-12   | 2111 | 103 | rei | 2a44g    |    6 |  2100.00
     113055 | 1990-02-15   | 2108 | 101 | aci | 4100x    |    6 |   150.00
     113048 | 1990-02-10   | 2120 | 102 | imm | 779c     |    2 |  3750.00
     112993 | 1989-01-04   | 2106 | 102 | rei | 2a45c    |   24 |  1896.00
     113065 | 1990-02-27   | 2106 | 102 | qsa | xk47     |    6 |  2130.00
     113003 | 1990-01-25   | 2108 | 109 | imm | 779c     |    3 |  5625.00
     113049 | 1990-02-10   | 2118 | 108 | qsa | xk47     |    2 |   776.00
     112987 | 1989-12-31   | 2103 | 105 | aci | 4100y    |   11 | 27500.00
     113057 | 1990-02-18   | 2111 | 103 | aci | 4100x    |   24 |   600.00
     113042 | 1990-02-02   | 2113 | 101 | rei | 2a44r    |    5 | 22500.00
     500001 | 2017-04-10   | 5001 | 103 | aci | 2a44r    |   10 |   550.00
(31 rows)


-- 6.3- Inserir un nou venedor amb nom "Pere Mendoza" amb identificador 1013, contracte del 15 de agost del 2011 i vendes 0. La resta de camps a null.



training=# insert into repventas (num_empl,nombre,contrato,ventas) values (1014,'Pere Mendoza','2011-08-11',0);
INSERT 0 1
training=# select * from repventas ;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00
      104 | Bob Smith     |   33 |          12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00
      101 | Dan Roberts   |   45 |          12 | Rep Ventas | 1986-10-20 |      104 | 300000.00 | 305673.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00
      103 | Paul Cruz     |   29 |          12 | Rep Ventas | 1987-03-01 |      104 | 275000.00 | 286775.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00
     1012 | enric jimenez |      |          18 | Dir Ventas | 2012-02-01 |      101 |           |      0.00
     1013 | enric jimenez |      |          18 | Dir Ventas | 2013-02-01 |      101 |           |      0.00
     1014 | Pere Mendoza  |      |             |            | 2011-08-11 |          |           |      0.00
(13 rows)

-- 6.4- Inserir un nou client "C2" omplint els mínims camps.

training=# insert into clientes (num_clie,empresa,rep_clie) values (5002,'c2',106);
INSERT 0 1
training=# select * from clientes ;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2102 | First Corp.       |      101 |       65000.00
     2103 | Acme Mfg.         |      105 |       50000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2107 | Ace International |      110 |       35000.00
     2115 | Smithson Corp.    |      101 |       20000.00
     2101 | Jones Mfg.        |      106 |       65000.00
     2112 | Zetacorp          |      108 |       50000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2114 | Orion Corp        |      102 |       20000.00
     2124 | Peter Brothers    |      107 |       40000.00
     2108 | Holm & Landis     |      109 |       55000.00
     2117 | J.P. Sinclair     |      106 |       35000.00
     2122 | Three-Way Lines   |      105 |       30000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2106 | Fred Lewis Corp.  |      102 |       65000.00
     2119 | Solomon Inc.      |      109 |       25000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2113 | Ian & Schmidt     |      104 |       20000.00
     2109 | Chen Associates   |      103 |       25000.00
     2105 | AAA Investments   |      101 |       45000.00
     5001 | c1                |      103 |       50000.00
     5002 | c2                |      106 |               
(23 rows)


-- 6.5- Inserir una nova comanda del client "C2" al venedor "Pere Mendoza" sense especificar la llista de camps pero si la de valors.

training=# insert into clientes (num_clie,empresa,rep_clie) values (5002,'c2',106);
INSERT 0 1
training=# select * from clientes ;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2102 | First Corp.       |      101 |       65000.00
     2103 | Acme Mfg.         |      105 |       50000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2107 | Ace International |      110 |       35000.00
     2115 | Smithson Corp.    |      101 |       20000.00
     2101 | Jones Mfg.        |      106 |       65000.00
     2112 | Zetacorp          |      108 |       50000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2114 | Orion Corp        |      102 |       20000.00
     2124 | Peter Brothers    |      107 |       40000.00
     2108 | Holm & Landis     |      109 |       55000.00
     2117 | J.P. Sinclair     |      106 |       35000.00
     2122 | Three-Way Lines   |      105 |       30000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2106 | Fred Lewis Corp.  |      102 |       65000.00
     2119 | Solomon Inc.      |      109 |       25000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2113 | Ian & Schmidt     |      104 |       20000.00
     2109 | Chen Associates   |      103 |       25000.00
     2105 | AAA Investments   |      101 |       45000.00
     5001 | c1                |      103 |       50000.00
     5002 | c2                |      106 |               
(23 rows)

training=# insert into pedidos (num_pedido,fecha_pedido,clie,rep,fab,producto,cant,importe) values (500002,'2017-04-10',5002,1014,'aci','2a44r',10,550)
;
INSERT 0 1
training=# select * from repventas ;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00
      104 | Bob Smith     |   33 |          12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00
      101 | Dan Roberts   |   45 |          12 | Rep Ventas | 1986-10-20 |      104 | 300000.00 | 305673.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00
      103 | Paul Cruz     |   29 |          12 | Rep Ventas | 1987-03-01 |      104 | 275000.00 | 286775.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00
     1012 | enric jimenez |      |          18 | Dir Ventas | 2012-02-01 |      101 |           |      0.00
     1013 | enric jimenez |      |          18 | Dir Ventas | 2013-02-01 |      101 |           |      0.00
     1014 | Pere Mendoza  |      |             |            | 2011-08-11 |          |           |      0.00
(13 rows)

training=# 


-- 6.6- Esborrar de la còpia de la base de dades el venedor afegit anteriorment anomenat "Enric Jimenez".

training=# delete from repventas where nombre = 'enric jimenez';
DELETE 2
training=# select * from repventas ;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00
      104 | Bob Smith     |   33 |          12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00
      101 | Dan Roberts   |   45 |          12 | Rep Ventas | 1986-10-20 |      104 | 300000.00 | 305673.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00
      103 | Paul Cruz     |   29 |          12 | Rep Ventas | 1987-03-01 |      104 | 275000.00 | 286775.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00
     1014 | Pere Mendoza  |      |             |            | 2011-08-11 |          |           |      0.00
(11 rows)


-- 6.7- Eliminar totes les comandes del client "C1" afegit anteriorment.
training=# delete from pedidos where clie = 5001;
DELETE 1
training=# select * from pedidos ;
 num_pedido | fecha_pedido | clie | rep  | fab | producto | cant | importe  
------------+--------------+------+------+-----+----------+------+----------
     112961 | 1989-12-17   | 2117 |  106 | rei | 2a44l    |    7 | 31500.00
     113012 | 1990-01-11   | 2111 |  105 | aci | 41003    |   35 |  3745.00
     112989 | 1990-01-03   | 2101 |  106 | fea | 114      |    6 |  1458.00
     113051 | 1990-02-10   | 2118 |  108 | qsa | k47      |    4 |  1420.00
     112968 | 1989-10-12   | 2102 |  101 | aci | 41004    |   34 |  3978.00
     110036 | 1990-01-30   | 2107 |  110 | aci | 4100z    |    9 | 22500.00
     113045 | 1990-02-02   | 2112 |  108 | rei | 2a44r    |   10 | 45000.00
     112963 | 1989-12-17   | 2103 |  105 | aci | 41004    |   28 |  3276.00
     113013 | 1990-01-14   | 2118 |  108 | bic | 41003    |    1 |   652.00
     113058 | 1990-02-23   | 2108 |  109 | fea | 112      |   10 |  1480.00
     112997 | 1990-01-08   | 2124 |  107 | bic | 41003    |    1 |   652.00
     112983 | 1989-12-27   | 2103 |  105 | aci | 41004    |    6 |   702.00
     113024 | 1990-01-20   | 2114 |  108 | qsa | xk47     |   20 |  7100.00
     113062 | 1990-02-24   | 2124 |  107 | fea | 114      |   10 |  2430.00
     112979 | 1989-10-12   | 2114 |  102 | aci | 4100z    |    6 | 15000.00
     113027 | 1990-01-22   | 2103 |  105 | aci | 41002    |   54 |  4104.00
     113007 | 1990-01-08   | 2112 |  108 | imm | 773c     |    3 |  2925.00
     113069 | 1990-03-02   | 2109 |  107 | imm | 775c     |   22 | 31350.00
     113034 | 1990-01-29   | 2107 |  110 | rei | 2a45c    |    8 |   632.00
     112992 | 1989-11-04   | 2118 |  108 | aci | 41002    |   10 |   760.00
     112975 | 1989-12-12   | 2111 |  103 | rei | 2a44g    |    6 |  2100.00
     113055 | 1990-02-15   | 2108 |  101 | aci | 4100x    |    6 |   150.00
     113048 | 1990-02-10   | 2120 |  102 | imm | 779c     |    2 |  3750.00
     112993 | 1989-01-04   | 2106 |  102 | rei | 2a45c    |   24 |  1896.00
     113065 | 1990-02-27   | 2106 |  102 | qsa | xk47     |    6 |  2130.00
     113003 | 1990-01-25   | 2108 |  109 | imm | 779c     |    3 |  5625.00
     113049 | 1990-02-10   | 2118 |  108 | qsa | xk47     |    2 |   776.00
     112987 | 1989-12-31   | 2103 |  105 | aci | 4100y    |   11 | 27500.00
     113057 | 1990-02-18   | 2111 |  103 | aci | 4100x    |   24 |   600.00
     113042 | 1990-02-02   | 2113 |  101 | rei | 2a44r    |    5 | 22500.00
     500002 | 2017-04-10   | 5002 | 1014 | aci | 2a44r    |   10 |   550.00
(31 rows)




-- 6.8- Esborrar totes les comandes d'abans del 15-11-1989.

training=# delete from copy_pedidos where fecha_pedido < '1989-11-15' ;
DELETE 4
training=# select * from copy_pedidos ;
 num_pedido | fecha_pedido | clie | rep  | fab | producto | cant | importe  
------------+--------------+------+------+-----+----------+------+----------
     112961 | 1989-12-17   | 2117 |  106 | rei | 2a44l    |    7 | 31500.00
     113012 | 1990-01-11   | 2111 |  105 | aci | 41003    |   35 |  3745.00
     112989 | 1990-01-03   | 2101 |  106 | fea | 114      |    6 |  1458.00
     113051 | 1990-02-10   | 2118 |  108 | qsa | k47      |    4 |  1420.00
     110036 | 1990-01-30   | 2107 |  110 | aci | 4100z    |    9 | 22500.00
     113045 | 1990-02-02   | 2112 |  108 | rei | 2a44r    |   10 | 45000.00
     112963 | 1989-12-17   | 2103 |  105 | aci | 41004    |   28 |  3276.00
     113013 | 1990-01-14   | 2118 |  108 | bic | 41003    |    1 |   652.00
     113058 | 1990-02-23   | 2108 |  109 | fea | 112      |   10 |  1480.00
     112997 | 1990-01-08   | 2124 |  107 | bic | 41003    |    1 |   652.00
     112983 | 1989-12-27   | 2103 |  105 | aci | 41004    |    6 |   702.00
     113024 | 1990-01-20   | 2114 |  108 | qsa | xk47     |   20 |  7100.00
     113062 | 1990-02-24   | 2124 |  107 | fea | 114      |   10 |  2430.00
     113027 | 1990-01-22   | 2103 |  105 | aci | 41002    |   54 |  4104.00
     113007 | 1990-01-08   | 2112 |  108 | imm | 773c     |    3 |  2925.00
     113069 | 1990-03-02   | 2109 |  107 | imm | 775c     |   22 | 31350.00
     113034 | 1990-01-29   | 2107 |  110 | rei | 2a45c    |    8 |   632.00
     112975 | 1989-12-12   | 2111 |  103 | rei | 2a44g    |    6 |  2100.00
     113055 | 1990-02-15   | 2108 |  101 | aci | 4100x    |    6 |   150.00
     113048 | 1990-02-10   | 2120 |  102 | imm | 779c     |    2 |  3750.00
     113065 | 1990-02-27   | 2106 |  102 | qsa | xk47     |    6 |  2130.00
     113003 | 1990-01-25   | 2108 |  109 | imm | 779c     |    3 |  5625.00
     113049 | 1990-02-10   | 2118 |  108 | qsa | xk47     |    2 |   776.00
     112987 | 1989-12-31   | 2103 |  105 | aci | 4100y    |   11 | 27500.00
     113057 | 1990-02-18   | 2111 |  103 | aci | 4100x    |   24 |   600.00
     113042 | 1990-02-02   | 2113 |  101 | rei | 2a44r    |    5 | 22500.00
     500001 | 2017-04-10   | 5001 |  103 | aci | 2a44r    |   10 |   550.00
     500002 | 2017-04-10   | 5002 | 1014 | aci | 2a44r    |   10 |   550.00
(28 rows)


-- 6.9- Esborrar tots els clients dels venedors: Adams, Jones i Roberts.

training=# delete from copy_clientes where rep_clie in ( select num_empl from copy_repventas where nombre in ('Bill Adams','Dan Roberts','Mary Jones'));
DELETE 7
training=# select * from copy_clientes ;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2107 | Ace International |      110 |       35000.00
     2101 | Jones Mfg.        |      106 |       65000.00
     2112 | Zetacorp          |      108 |       50000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2114 | Orion Corp        |      102 |       20000.00
     2124 | Peter Brothers    |      107 |       40000.00
     2117 | J.P. Sinclair     |      106 |       35000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2106 | Fred Lewis Corp.  |      102 |       65000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2113 | Ian & Schmidt     |      104 |       20000.00
     2109 | Chen Associates   |      103 |       25000.00
     5001 | c1                |      103 |       50000.00
     5002 | c2                |      106 |               
(16 rows)



-- 6.10- Esborrar tots els venedors contractats abans del juliol del 1988 que encara no se'ls ha assignat una quota.

training=# delete from copy_repventas where contrato < '1988-07-01' and cuota is null ;
DELETE 0


-- 6.11- Esborrar totes les comandes.

training=# delete from copy_pedidos ;
DELETE 28
training=# select * from copy_pedidos ;
 num_pedido | fecha_pedido | clie | rep | fab | producto | cant | importe 
------------+--------------+------+-----+-----+----------+------+---------
(0 rows)



-- 6.12- Esborrar totes les comandes acceptades per la Sue Smith (cal tornar a disposar  de la taula pedidos)

training=# insert into copy_pedidos (select * from pedidos );
INSERT 0 31


training=# delete from copy_pedidos where rep = (select num_empl from copy_repventas where nombre = 'Sue Smith');
DELETE 4
training=# select * from copy_pedidos ;
 num_pedido | fecha_pedido | clie | rep  | fab | producto | cant | importe  
------------+--------------+------+------+-----+----------+------+----------
     112961 | 1989-12-17   | 2117 |  106 | rei | 2a44l    |    7 | 31500.00
     113012 | 1990-01-11   | 2111 |  105 | aci | 41003    |   35 |  3745.00
     112989 | 1990-01-03   | 2101 |  106 | fea | 114      |    6 |  1458.00
     113051 | 1990-02-10   | 2118 |  108 | qsa | k47      |    4 |  1420.00
     112968 | 1989-10-12   | 2102 |  101 | aci | 41004    |   34 |  3978.00
     110036 | 1990-01-30   | 2107 |  110 | aci | 4100z    |    9 | 22500.00
     113045 | 1990-02-02   | 2112 |  108 | rei | 2a44r    |   10 | 45000.00
     112963 | 1989-12-17   | 2103 |  105 | aci | 41004    |   28 |  3276.00
     113013 | 1990-01-14   | 2118 |  108 | bic | 41003    |    1 |   652.00
     113058 | 1990-02-23   | 2108 |  109 | fea | 112      |   10 |  1480.00
     112997 | 1990-01-08   | 2124 |  107 | bic | 41003    |    1 |   652.00
     112983 | 1989-12-27   | 2103 |  105 | aci | 41004    |    6 |   702.00
     113024 | 1990-01-20   | 2114 |  108 | qsa | xk47     |   20 |  7100.00
     113062 | 1990-02-24   | 2124 |  107 | fea | 114      |   10 |  2430.00
     113027 | 1990-01-22   | 2103 |  105 | aci | 41002    |   54 |  4104.00
     113007 | 1990-01-08   | 2112 |  108 | imm | 773c     |    3 |  2925.00
     113069 | 1990-03-02   | 2109 |  107 | imm | 775c     |   22 | 31350.00
     113034 | 1990-01-29   | 2107 |  110 | rei | 2a45c    |    8 |   632.00
     112992 | 1989-11-04   | 2118 |  108 | aci | 41002    |   10 |   760.00
     112975 | 1989-12-12   | 2111 |  103 | rei | 2a44g    |    6 |  2100.00
     113055 | 1990-02-15   | 2108 |  101 | aci | 4100x    |    6 |   150.00
     113003 | 1990-01-25   | 2108 |  109 | imm | 779c     |    3 |  5625.00
     113049 | 1990-02-10   | 2118 |  108 | qsa | xk47     |    2 |   776.00
     112987 | 1989-12-31   | 2103 |  105 | aci | 4100y    |   11 | 27500.00
     113057 | 1990-02-18   | 2111 |  103 | aci | 4100x    |   24 |   600.00
     113042 | 1990-02-02   | 2113 |  101 | rei | 2a44r    |    5 | 22500.00
     500002 | 2017-04-10   | 5002 | 1014 | aci | 2a44r    |   10 |   550.00
(27 rows)

-- 6.13- Suprimeix els clients atesos per venedors les vendes dels quals són inferiors al 80% de la seva quota.

training=# select * from clientes ;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2111 | JCP Inc.          |      103 |       50000.00
     2102 | First Corp.       |      101 |       65000.00
     2103 | Acme Mfg.         |      105 |       50000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2107 | Ace International |      110 |       35000.00
     2115 | Smithson Corp.    |      101 |       20000.00
     2101 | Jones Mfg.        |      106 |       65000.00
     2112 | Zetacorp          |      108 |       50000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2114 | Orion Corp        |      102 |       20000.00
     2108 | Holm & Landis     |      109 |       55000.00
     2117 | J.P. Sinclair     |      106 |       35000.00
     2122 | Three-Way Lines   |      105 |       30000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2106 | Fred Lewis Corp.  |      102 |       65000.00
     2119 | Solomon Inc.      |      109 |       25000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2109 | Chen Associates   |      103 |       25000.00
     2105 | AAA Investments   |      101 |       45000.00
     5001 | c1                |      103 |       50000.00
     5002 | c2                |      106 |               
(21 rows)


-- 6.14- Suprimir els venedors els quals el seu total de comandes actual (imports) és menor que el 2% de la seva quota.
training=# delete from repventas where num_empl in ( select rep from pedidos group by rep having sum(importe) < cuota * 2/100);
DELETE 1
training=# select * from repventas ;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00
      104 | Bob Smith     |   33 |          12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00
      101 | Dan Roberts   |   45 |          12 | Rep Ventas | 1986-10-20 |      104 | 300000.00 | 305673.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00
     1014 | Pere Mendoza  |      |             |            | 2011-08-11 |          |           |      0.00
(10 rows)



-- 6.15- Suprimeix els clients que no han realitzat comandes des del 10-11-1989.

training=# delete from copy_clientes where num_clie not in (select clie from pedidos where fecha_pedido > '1989-11-10');
DELETE 3
training=# select * from copy_p
copy_pedidos    copy_productos  
training=# select * from copy_pedidos ;
 num_pedido | fecha_pedido | clie | rep  | fab | producto | cant | importe  
------------+--------------+------+------+-----+----------+------+----------
     112961 | 1989-12-17   | 2117 |  106 | rei | 2a44l    |    7 | 31500.00
     113012 | 1990-01-11   | 2111 |  105 | aci | 41003    |   35 |  3745.00
     112989 | 1990-01-03   | 2101 |  106 | fea | 114      |    6 |  1458.00
     113051 | 1990-02-10   | 2118 |  108 | qsa | k47      |    4 |  1420.00
     112968 | 1989-10-12   | 2102 |  101 | aci | 41004    |   34 |  3978.00
     110036 | 1990-01-30   | 2107 |  110 | aci | 4100z    |    9 | 22500.00
     113045 | 1990-02-02   | 2112 |  108 | rei | 2a44r    |   10 | 45000.00
     112963 | 1989-12-17   | 2103 |  105 | aci | 41004    |   28 |  3276.00
     113013 | 1990-01-14   | 2118 |  108 | bic | 41003    |    1 |   652.00
     113058 | 1990-02-23   | 2108 |  109 | fea | 112      |   10 |  1480.00
     112997 | 1990-01-08   | 2124 |  107 | bic | 41003    |    1 |   652.00
     112983 | 1989-12-27   | 2103 |  105 | aci | 41004    |    6 |   702.00
     113024 | 1990-01-20   | 2114 |  108 | qsa | xk47     |   20 |  7100.00
     113062 | 1990-02-24   | 2124 |  107 | fea | 114      |   10 |  2430.00
     113027 | 1990-01-22   | 2103 |  105 | aci | 41002    |   54 |  4104.00
     113007 | 1990-01-08   | 2112 |  108 | imm | 773c     |    3 |  2925.00
     113069 | 1990-03-02   | 2109 |  107 | imm | 775c     |   22 | 31350.00
     113034 | 1990-01-29   | 2107 |  110 | rei | 2a45c    |    8 |   632.00
     112992 | 1989-11-04   | 2118 |  108 | aci | 41002    |   10 |   760.00
     112975 | 1989-12-12   | 2111 |  103 | rei | 2a44g    |    6 |  2100.00
     113055 | 1990-02-15   | 2108 |  101 | aci | 4100x    |    6 |   150.00
     113003 | 1990-01-25   | 2108 |  109 | imm | 779c     |    3 |  5625.00
     113049 | 1990-02-10   | 2118 |  108 | qsa | xk47     |    2 |   776.00
     112987 | 1989-12-31   | 2103 |  105 | aci | 4100y    |   11 | 27500.00
     113057 | 1990-02-18   | 2111 |  103 | aci | 4100x    |   24 |   600.00
     113042 | 1990-02-02   | 2113 |  101 | rei | 2a44r    |    5 | 22500.00
     500002 | 2017-04-10   | 5002 | 1014 | aci | 2a44r    |   10 |   550.00
(27 rows)

	

-- 6.16 Eleva el límit de crèdit de l'empresa Acme Manufacturing a 60000 i la reassignes a Mary Jones.

training=# update copy_clientes set limite_credito = 60000,rep_clie=(select num_empl from repventas where nombre = 'Mary Jones') where empresa ilike 'Acme%';
UPDATE 0


-- 6.17- Transferir tots els venedors de l'oficina de Chicago (12) a la de Nova York (11), i rebaixa les seves quotes un 10%.
training=# update repventas set oficina_rep = (select oficina from oficinas where ciudad = 'New York'),cuota = cuota * 0.9 where oficina_rep = ( select oficina from oficinas where ciudad = 'Chicago');
UPDATE 2



-- 6.18- Reassigna tots els clients atesos pels empleats 105, 106, 107, a l'emleat 102.
training=# update clientes set rep_clie = 102 where rep_clie = 105 or rep_clie = 106 or rep_clie = 107;
UPDATE 5



-- 6.19- Assigna una quota de 100000 a tots aquells venedors que actualment no tenen quota.

training=# update repventas set cuota = 100000 where cuota is null;
UPDATE 2

-- 6.20- Eleva totes les quotes un 5%.

training=# update repventas set cuota = cuota * 1.05;
UPDATE 10


-- 6.21- Eleva en 5000 el límit de crèdit de qualsevol client que ha fet una comanda d'import superior a 25000.

training=# update clientes set limite_credito = limite_credito + 5000 where num_clie in (select clie from pedidos where importe > 25000);
UPDATE 4

   
-- 6.22- Reassigna tots els clients atesos pels venedors les vendes dels quals són menors al 80% de les seves quotes. Reassignar al venedor 105.


  
-- 6.23- Fer que tots els venedors que atenen a més de tres clients estiguin sota de les ordres de Sam Clark (106).


-- 6.24- Augmentar un 50% el límit de credit d'aquells clients que totes les seves comandes tenen un import major a 30000.

training=# select * from clientes where 30000 < all ( select importe from pedidos where clie = num_clie) and num_clie in ( select clie from pedidos);
 num_clie |     empresa     | rep_clie | limite_credito 
----------+-----------------+----------+----------------
     2109 | Chen Associates |      103 |       30000.00
     2117 | J.P. Sinclair   |      102 |       40000.00
(2 rows)

training=# select * from clientes where 30000 < all ( select importe from pedidos where clie = num_clie) and exists ( select clie from pedidos where clie = num_clie);
 num_clie |     empresa     | rep_clie | limite_credito 
----------+-----------------+----------+----------------
     2109 | Chen Associates |      103 |       30000.00
     2117 | J.P. Sinclair   |      102 |       40000.00
(2 rows)

training=# update copy_clientes set limite_credito = (limite_credito) + (limite_credito)*0.5 where num_clie in (select num_clie from copy_clientes where 30000 < all ( select importe from copy_pedidos where clie = num_clie) and exists ( select clie from copy_pedidos where clie = num_clie));
UPDATE 2


-- ALERTA sense l'EXISTS augmenta el limit de credit dels clients que no tenen comandes.

-- 6.25- Disminuir un 2% el preu d'aquells productes que tenen un estoc superior a 200 i no han tingut comandes.

training=# update copy_productos set precio = (precio - precio * 0.02) where existencias > 200 and not exists ( select * from pedidos where id_fab = fab and id_producto = producto);
UPDATE 3



-- 6.26- Establir un nou objectiu per aquelles oficines en que l'objectiu actual sigui inferior a les vendes. Aquest nou objectiu serà el doble de la suma de les vendes dels treballadors assignats a l'oficina.

training=# update copy_oficinas set objetivo = 2*(select sum(ventas) from repventas where oficina = oficina_rep) where oficina in ( select oficina from oficinas where objetivo < (select sum(ventas) from repventas where oficina_rep = oficina group by oficina ));
UPDATE 3


-- 6.27- Modificar la quota dels directors d'oficina que tinguin una quota superior a la quota d'algun empleat de la seva oficina. Aquests directors han de tenir la mateixa quota que l'empleat de la seva oficina que tingui una quota menor.
---algo asi
training=# update copy_repventas d set cuota=(select min(e.cuota) from repventas e where e.oficina_rep in (select oficina from oficinas where dir = d.num_empl)) where exists (select * from repventas d where num_empl in ( select dir from oficinas) and cuota > any (select cuota from repventas e where e.oficina_rep in(select oficina from oficinas where dir = d.num_empl)));
UPDATE 13


-- 6.28- Cal que els 5 clients amb un total de compres (cantidad) més alt siguin transferits a l'empleat Tom Snyder i que se'ls augmenti el límit de crèdit en un 50%.



-- 6.29- Es volen donar de baixa els productes dels que no tenen existències i alhora no se n'ha venut cap des de l'any 89.

training=# delete from copy_productos where existencias = 0 and not exists( select * from pedidos where id_fab = fab and producto = id_producto and fecha_pedido >= '1992-01-01')
training-# ;
DELETE 1


-- 6.30- Afegir una oficina de la ciutat de "San Francisco", regió oest, el director ha de ser "Larry Fitch", les vendes 0, l'objectiu ha de ser la mitja de l'objectiu de les oficines de l'oest i l'identificador de l'oficina ha de ser el següent valor després del valor més alt.

training=# insert into copy_oficinas values ( (select max(oficina) + 1 from oficinas ),'san francisco','Oeste',(select num_empl from repventas where nombre = 'Larry Fitch'),(select avg(objetivo) from oficinas where region = 'Oesta'),0);
INSERT 0 1
