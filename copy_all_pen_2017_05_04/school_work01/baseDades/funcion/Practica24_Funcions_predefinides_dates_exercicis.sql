--1. Donar data i hora actual del sistema en format dd/MM/aaaa-hh:mm:ss

--2. Donar hora actual en format hh:mm:ss

--3. Determinar la durada de cada contracte fins avui en dies. S'ha de mostrar el nom del treballador, la data del contracte i la durada.

--4. Determinar la suma total de les durades de tots els contractes dels treballadors en dies fins avui.

--5. Determinar la diferència de temps treballat entre cadascun dels treballadors amb el treballador més antic. S'ha de mostrar el nom del treballador, la data del contracte i la diferència.

training=# select nombre,contrato,contrato-(select min(contrato) from repventas ) as difference from repventas ;
    nombre     |  contrato  | difference 
---------------+------------+------------
 Bill Adams    | 1988-02-12 |        480
 Mary Jones    | 1989-10-12 |       1088
 Sue Smith     | 1986-12-10 |         51
 Sam Clark     | 1988-06-14 |        603
 Bob Smith     | 1987-05-19 |        211
 Dan Roberts   | 1986-10-20 |          0
 Tom Snyder    | 1990-01-13 |       1181
 Larry Fitch   | 1989-10-12 |       1088
 Paul Cruz     | 1987-03-01 |        132
 Nancy Angelli | 1988-11-14 |        756
(10 rows)


--6. Calcular el nombre de comandes fetes el desembre pels representants de vendes contractats el mes de febrer.

training=# select count(num_pedido) from pedidos where extract(month from fecha_pedido) = 12 and rep in (select num_empl from repventas where extract(month from contrato) = 2);
 count 
-------
     3
(1 row)



--7. Llistar el número de treballadors que s'han contractat per a cada mes de l'any. El llistat ha d'estar ordenat pel mes. Ha de tenir el següent format:
---algo asi--
select extract(month from contrato) as mes_de_contractacio,date_part('month'

 mes_de_contractació | numero_contractes 
---------------------+------------------
                   1 |                1
                   2 |                1
                   3 |                1
...

--8.- Mostrar el nom dels venedors i la data del seu contracte. La data s'ha de mostrar usant el següent format:
	dia_de_la_setmana  dia_numèric, mes_numeric del any_4digits
	per exemple:
	   Bill Adams    | Friday 12, 02 del 1988

training=# select nombre,to_char(contrato,'day dd,month "del" yyyy') from repventas ;
    nombre     |             to_char             
---------------+---------------------------------
 Bill Adams    | friday    12,february  del 1988
 Mary Jones    | thursday  12,october   del 1989
 Sue Smith     | wednesday 10,december  del 1986
 Sam Clark     | tuesday   14,june      del 1988
 Bob Smith     | tuesday   19,may       del 1987
 Dan Roberts   | monday    20,october   del 1986
 Tom Snyder    | saturday  13,january   del 1990
 Larry Fitch   | thursday  12,october   del 1989
 Paul Cruz     | sunday    01,march     del 1987
 Nancy Angelli | monday    14,november  del 1988
(10 rows)

--9.- LListar el nom dels treballadors i la data del seu contracte mostrant-la amb el següent format:
		Dia_de_la_setmana dia_mes_numeric, mes_text del any_4digits
	per exemple
                Bill Adams    | Friday    12, February del 1988
                
--10.-  Cal fer un llistat dels productes dels quals les existències  són inferiors al total d'unitats venudes d'aquell producte els darrers 60 dies, a comptar des de la data actual. 
Cal mostrar els codis de fabricant i de producte, les existències, i les unitats totals venudes dels darrers 60(9999) dies.

training=# select *,(select sum(cant) from pedidos where id_fab = fab and id_producto = producto and (current_date -fecha_pedido) < 9999 ) from productos where existencias < (select sum(cant) from pedidos where id_fab = fab and id_producto =producto and (current_date -fecha_pedido ) < 9999);
 id_fab | id_producto |  descripcion  | precio  | existencias | sum 
--------+-------------+---------------+---------+-------------+-----
 imm    | 775c        | Riostra 1-Tm  | 1425.00 |           5 |  22
 rei    | 2a44r       | Bisagra Dcha. | 4500.00 |          12 |  15
 fea    | 114         | Bancada Motor |  243.00 |          15 |  16
(3 rows)


--11- Com l'exercici 29 però en comptes de 60 dies ara es volen dos mesos naturals: productes venuts durant el mes 
--actual o durant l'anterior (si no som a fi de mes seràn menys de 60 dies).
----algo asi--
training=# select producto,fab,(select existencias from productos where id_fab = fab and id_producto = producto ),(select sum(cant) from pedidos b where aa.producto=b.producto and aa.fab = b.fab and aa.fecha_pedido >= (current_date -interval '366 month')) from pedidos aa where ( select sum(cant) from pedidos b where aa.producto = b.producto and aa.fab = b.fab and aa.fecha_pedido >= (current_date-interval '366 month'));
