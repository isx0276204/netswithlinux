PRÀCTICA 12

PROCEDIMENTS ENMAGATZEMATS / STORED PROCEDURES / FUNCIONS en PLPGSQL


CREATE FUNCTION ADIC_UNO (INTEGER) RETURNS INTEGER AS '
BEGIN
	RETURN $1 + 1;
END;
' LANGUAGE 'PLPGSQL'; 


CREATE FUNCTION MAGIA (TEXT) RETURNS TEXT AS '
BEGIN
	RETURN "HOLA " || $1;
END;
' LANGUAGE 'PLPGSQL'; 


CREATE OR REPLACE FUNCTION ejemplo_txt(integer, integer) RETURNS text AS $$

DECLARE

 numero1 ALIAS FOR $1;
 numero2 ALIAS FOR $2;

 constante CONSTANT integer := 100;
 resultado INTEGER;

 resultado_txt TEXT DEFAULT 'El resultado es 104'; 

BEGIN

 resultado := (numero1 * numero2) + constante;

 IF resultado <> 104 THEN
    resultado_txt :=  'El resultado NO es 104';
 END IF;

 RETURN resultado_txt;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION  pri_insCli(a integer, b text, c integer, d integer) returns text as

$BODY$
DECLARE
  sql text;

BEGIN 
  execute 'INSERT into clientes values ($1, $2, $3, $4)' using a, b, c, d;
  return '1'; 

EXCEPTION 
  WHEN unique_violation THEN return '5'; 
  WHEN not_null_violation THEN return '4'; 
  WHEN foreign_key_violation THEN return '3'; 
 

END
$BODY$
language PLPGsql;



CREATE OR REPLACE FUNCTION pri_sel(id_cli integer)
RETURNS text AS
$$
DECLARE
    	result text := '';
   	searchsql text := '';
   	var_match record;
BEGIN
  	searchsql := 'SELECT * FROM clientes WHERE num_clie = ' || $1;
              
    
   	FOR var_match IN EXECUTE(searchsql) LOOP
      		 IF result > '' THEN
           		result := result || ';' || var_match.num_clie || '= ' || var_match;
      		 ELSE
       			result := var_match.num_clie || '= ' ||var_match;
        		END IF;
   	 END LOOP;
    	IF result = '' THEN
		result := 'Dades inexistents';
    	END IF;

   	RETURN searchsql || ': ' || result;
EXCEPTION 
  	WHEN others THEN return '5';
END;
$$
LANGUAGE 'plpgsql' IMMUTABLE;


CREATE OR REPLACE FUNCTION dni_correct(dni varchar)
RETURNS text AS
$$
DECLARE
    ret varchar := '1';
    cadena varchar := 'TRWAGMYFPDXBNJZSQVHLCKE';
    partnum numeric(8);
    partlletra varchar;
    res integer; 
    lletra varchar;
BEGIN
    IF char_length(dni) != 9 THEN
	ret := 'LONGITUD INCORRECTA';
    ELSE
	partnum := substr (dni, 1, 8);
	partlletra := substr (dni, 9, 1);
	res:= cast(partnum as int) % 23 + 1;
	lletra := substr(cadena,res,1);
	IF lletra != partlletra THEN 
		ret := '0'; 
	END IF;
    END IF; 
    RETURN ret; 
END;
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION crida_dni_correct(dni varchar)
RETURNS text AS
$$
DECLARE
     ret varchar := '1';
 
BEGIN
     ret = dni_correct(dni);

     RETURN ret;

END;
$$
LANGUAGE 'plpgsql';

----------------------------------------------------------------
Crear i retornar nousTipus de dades

CREATE TYPE my_type (
  user1_id   int,
  user1_name varchar(32),
  user2_id   int,
  user2_name varchar(32)
);


CREATE OR REPLACE FUNCTION xxx(varchar)
RETURNS my_type AS
DECLARE
  result my_type;
  user_id integer;
  quantity numeric(5);
  url varchar;
  myrow tablename%ROWTYPE;
  myfield tablename.columnname%TYPE;
  arow RECORD;
BEGIN
 
  return result ;
END
$$ language plpgsql

----------------------------------------------------------------
Retornar record
CREATE OR REPLACE FUNCTION somefun_recordset(param_id int)
  RETURNS SETOF record AS
$$
DECLARE
    result text := '';
    searchsql text := '';
    var_match record;
BEGIN
    searchsql := 'SELECT * FROM clientes WHERE num_clie = '  || param_id;
                
    
    FOR var_match IN EXECUTE(searchsql) LOOP
        RETURN NEXT var_match;
    END LOOP;
END;
$$
LANGUAGE 'plpgsql' IMMUTABLE;

----------------------------------------------------------------
Retornar set of record

CREATE OR REPLACE FUNCTION somefun_recordset(param_nom text)
  RETURNS SETOF record AS
$$
DECLARE
    result text := '';
    searchsql text := '';
    var_match record;
BEGIN
    searchsql := 'SELECT * FROM clientes WHERE empresa LIKE '''  || nom || '''';
                
    
    FOR var_match IN EXECUTE(searchsql) LOOP
        RETURN NEXT var_match;
    END LOOP;
END;
$$
LANGUAGE 'plpgsql' IMMUTABLE;

----------------------------------------------------------------
Retornar table
CREATE OR REPLACE FUNCTION fn_plpgsqltestmulti(param_subject varchar) 
    RETURNS TABLE(test_id integer, test_stuff text)
   AS
$$
DECLARE 
    var_r record;
BEGIN
     FOR var_r IN (SELECT id, test 
                FROM test WHERE test LIKE param_subject)  LOOP
            test_id := var_r.id ; test_stuff := var_r.test;
            RETURN NEXT;
     END LOOP;
END;
$$
  LANGUAGE 'plpgsql' VOLATILE;

