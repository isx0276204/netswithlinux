-- --------------------------------------------
--    Funcions predefinides
-- --------------------------------------------

-- 10.1- Mostreu la longitud de la cadena "hola que tal"


-- 10.2- Mostreu la longitud dels valors del camp "id_producto".


-- 10.3- Mostrar la longitud dels valors del camp "descripcion".


-- 10.4- Mostreu els noms dels venedors en majúscules.


-- 10.5- Mostreu els noms dels venedors en minúscules.


-- 10.6- Trobeu on és la posició de l'espai en blanc de la cadena 'potser 7'.


-- 10.7- Volem mostrar el nom, només el nom dels venedors sense el cognom, en majúscules.


-- 10.8- Crear una vista que mostri l'identificador dels representants de vendes i en columnes separades el nom i el cognom.


-- 10.9- Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'B. Adams'.
 

-- 10.10- Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'Adams, Bill'.


-- 10.11- Volem mostrar el camp descripcion de la taula productos però que en comptes de sortir espais en blanc, volem subratllats ('_').


-- 10.12- Volem treure per pantalla una columna, que conté el nom i les vendes, amb els següent estil:
--   vendes dels empleats
-- ---------------------------
--  Bill Adams..... 367911,00
--  Mary Jones..... 392725,00
--  Sue Smith...... 474050,00
--  Sam Clark...... 299912,00
--  Bob Smith...... 142594,00
--  Dan Roberts.... 305673,00
--  Tom Snyder.....  75985,00
--  Larry Fitch.... 361865,00
--  Paul Cruz...... 286775,00
--  Nancy Angelli.. 186042,00
-- (10 rows)


-- 10.13- Treieu per pantalla el temps total que fa que estan contractats els treballadors, ordenat pels cognoms dels treballadors amb un estil semblant al següent:
--       nombre  |     tiempo_trabajando
--    -----------+-------------------------
--    Mary Jones | 13 years 4 months 6 days

training=# select nombre,age(contrato) from repventas ;
    nombre     |           age           
---------------+-------------------------
 Bill Adams    | 29 years 1 mon 12 days
 Mary Jones    | 27 years 5 mons 12 days
 Sue Smith     | 30 years 3 mons 14 days
 Sam Clark     | 28 years 9 mons 10 days
 Bob Smith     | 29 years 10 mons 5 days
 Dan Roberts   | 30 years 5 mons 4 days
 Tom Snyder    | 27 years 2 mons 11 days
 Larry Fitch   | 27 years 5 mons 12 days
 Paul Cruz     | 30 years 23 days
 Nancy Angelli | 28 years 4 mons 10 days
(10 rows)
 
-- 10.14- Cal fer un llistat dels productes dels quals les existències són inferiors al total d'unitats venudes d'aquell producte els darrers 60 dies, a comptar des de la data actual. Cal mostrar els codis de fabricant i de producte, les existències, i les unitats totals venudes dels darrers 60 dies.

-----algo asi---not correct
training=# select a.producto,a.fab,(current_date - a.fecha_pedido),(select sum(b.cant) from pedidos b where b.producto = a.producto and b.fab = a.fab),(select existencias from productos where id_fab = a.fab and id_producto = a.producto) from pedidos a where (select existencias from productos where id_fab = a.fab and id_producto = a.producto) < (select sum(b.cant) from pedidos b where b.producto = a.producto and b.fab = a.fab and fecha_pedido <= current_date - 9900 ) order by 1,2;
 producto | fab | ?column? | sum | existencias 
----------+-----+----------+-----+-------------
 2a44r    | rei |     9915 |  15 |          12
 2a44r    | rei |     9915 |  15 |          12
(2 rows)


-- 10.15- Com l'exercici anterior però en comptes de 60 dies ara es volen aquells productes venuts durant el mes actual o durant l'anterior.

---algo asi falta complet ----
training=# select num_pedido,producto,fab,(select existencias from productos where id_fab = fab and producto = id_producto) from pedidos where fecha_pedido in ( select fecha_pedido from pedidos  where extract(month from fecha_pedido) = extract (month from current_date -1 ) or extract(month from fecha_pedido) = extract (month from current_date -2 )) or extract(month from current_date) = 1 and exract( month from current_date = 12;


-- 10.16- Per fer un estudi previ de la campanya de Nadal es vol un llistat on, per cada any del qual hi hagi comandes a la base de dades, el nombre de clients diferents que hagin fet comandes en el mes de desembre d'aquell any. Cal mostrar l'any i el número de clients, ordenat ascendent per anys.

------algo asi----
training=# select distinct (extract(year from fecha_pedido)),extract(month from fecha_pedido),count(distinct clie) from pedidos group by 1,2;
 date_part | date_part | count 
-----------+-----------+-------
      1989 |        10 |     2
      1989 |        12 |     3
      1989 |        11 |     1
      1989 |         1 |     1
      1990 |         1 |     9
      1990 |         3 |     1
      1990 |         2 |     8
(7 rows)




-- 10.17- Llisteu codi(s) i descripció dels productes. La descripció ha d'aperèixer en majúscules. Ha d'estar ordenat per la longitud de les descripcions (les més curtes primer).

training=# select id_producto,upper(descripcion) from productos order by char_length(descripcion);
 id_producto |       upper       
-------------+-------------------
 41089       | RETN
 41672       | PLATE
 xk48a       | REDUCTOR
 xk47        | REDUCTOR
 41003       | MANIVELA
 xk48        | REDUCTOR
 112         | CUBIERTA
 4100z       | MONTADOR
 4100x       | AJUSTADOR
 4100y       | EXTRACTOR
 779c        | RIOSTRA 2-TM
 775c        | RIOSTRA 1-TM
 887p        | PERNO RIOSTRA
 114         | BANCADA MOTOR
 2a44r       | BISAGRA DCHA.
 773c        | RIOSTRA 1/2-TM
 2a44l       | BISAGRA IZQDA.
 2a44g       | PASADOR BISAGRA
 41003       | ARTICULO TIPO 3
 41004       | ARTICULO TIPO 4
 887h        | SOPORTE RIOSTRA
 41001       | ARTICULO TIPO 1
 41002       | ARTICULO TIPO 2
 887x        | RETENEDOR RIOSTRA
 2a45c       | V STAGO TRINQUETE
(25 rows)


-- 10.18- LListar el nom dels treballadors i la data del seu contracte mostrant-la amb el següent format:
-- Dia_de_la_setmana dia_mes_numeric, mes_text del any_4digits
-- per exemple:
-- Bill Adams    | Friday    12, February del 1988

----algo asi---
training=# select nombre,concat(to_char(contrato,'day'),' ',extract(day from contrato),' , ',extract(month from contrato),' del ',extract(year from contrato)) from repventas;
    nombre     |           concat           
---------------+----------------------------
 Bill Adams    | friday    12 , 2 del 1988
 Mary Jones    | thursday  12 , 10 del 1989
 Sue Smith     | wednesday 10 , 12 del 1986
 Sam Clark     | tuesday   14 , 6 del 1988
 Bob Smith     | tuesday   19 , 5 del 1987
 Dan Roberts   | monday    20 , 10 del 1986
 Tom Snyder    | saturday  13 , 1 del 1990
 Larry Fitch   | thursday  12 , 10 del 1989
 Paul Cruz     | sunday    1 , 3 del 1987
 Nancy Angelli | monday    14 , 11 del 1988
(10 rows)

-- 10.19- Modificar els imports de les comandes que s'han realitzat durant l'estiu augmentant-lo un 20% i arrodonint a l'alça el resultat.


-- 10.20- Mostar les dades de les oficines llistant en primera instància aquelles oficines que tenen una desviació entre vendes i objectius més gran.


-- 10.21- Llistar les dades d'aquells representants de vendes que tenen un identificador senar i són directors d'algun representants de vendes.


