-- --------------------------------------------
--    Funcions d'usuari
-- --------------------------------------------

-- 1.- Llistar l'identificador i el nom dels representants de vendes. Mostrar un camp anomenat "result" que mostri 0 si la quota és inferior a les vendes, en cas contrari ha de mostrar 1 a no ser que sigui director d'oficina, en aquest cas ha de mostrar 2.

training=# select nombre,director,case when num_empl in (select dir from oficinas) then 2 when cuota < ventas then 0 else 1 end from repventas ;
    nombre     | director | case 
---------------+----------+------
 Bill Adams    |      104 |    2
 Mary Jones    |      106 |    0
 Sue Smith     |      108 |    0
 Sam Clark     |          |    2
 Bob Smith     |      106 |    2
 Dan Roberts   |      104 |    0
 Tom Snyder    |      101 |    1
 Larry Fitch   |      106 |    2
 Paul Cruz     |      104 |    0
 Nancy Angelli |      108 |    1
(10 rows)



-- 2.- Llistar tots els productes amb totes les seves dades afegint un nou camp anomenat "div". El camp div ha de contenir el resultat de la divisió entre el preu i les existències. En cas de divisió per zero, es canviarà el resultat a 0.

training=# select id_fab,id_producto,case when existencias = 0 then 0 else (precio/existencias) end as div from productos ;
 id_fab | id_producto |          div           
--------+-------------+------------------------
 rei    | 2a45c       | 0.37619047619047619048
 aci    | 4100y       |   110.0000000000000000
 qsa    | xk47        |     9.3421052631578947
 bic    | 41672       |                      0
 imm    | 779c        |   208.3333333333333333
 aci    | 41003       | 0.51690821256038647343
 aci    | 41004       | 0.84172661870503597122
 bic    | 41003       |   217.3333333333333333
 imm    | 887p        |    10.4166666666666667
 qsa    | xk48        | 0.66009852216748768473
 rei    | 2a44l       |   375.0000000000000000
 fea    | 112         |     1.2869565217391304
 imm    | 887h        | 0.24215246636771300448
 bic    | 41089       |     2.8846153846153846
 aci    | 41001       | 0.19855595667870036101
 imm    | 775c        |   285.0000000000000000
 aci    | 4100z       |    89.2857142857142857
 qsa    | xk48a       |     3.1621621621621622
 aci    | 41002       | 0.45508982035928143713
 rei    | 2a44r       |   375.0000000000000000
 imm    | 773c        |    34.8214285714285714
 aci    | 4100x       | 0.67567567567567567568
 fea    | 114         |    16.2000000000000000
 imm    | 887x        |    14.8437500000000000
 rei    | 2a44g       |    25.0000000000000000
(25 rows)

training=# select id_fab,id_producto,descripcion,case when existencias = 0 then 0 else round((precio/existencias),2) end as div from productos ;
 id_fab | id_producto |    descripcion    |  div   
--------+-------------+-------------------+--------
 rei    | 2a45c       | V Stago Trinquete |   0.38
 aci    | 4100y       | Extractor         | 110.00
 qsa    | xk47        | Reductor          |   9.34
 bic    | 41672       | Plate             |      0
 imm    | 779c        | Riostra 2-Tm      | 208.33
 aci    | 41003       | Articulo Tipo 3   |   0.52
 aci    | 41004       | Articulo Tipo 4   |   0.84
 bic    | 41003       | Manivela          | 217.33
 imm    | 887p        | Perno Riostra     |  10.42
 qsa    | xk48        | Reductor          |   0.66
 rei    | 2a44l       | Bisagra Izqda.    | 375.00
 fea    | 112         | Cubierta          |   1.29
 imm    | 887h        | Soporte Riostra   |   0.24
 bic    | 41089       | Retn              |   2.88
 aci    | 41001       | Articulo Tipo 1   |   0.20
 imm    | 775c        | Riostra 1-Tm      | 285.00
 aci    | 4100z       | Montador          |  89.29
 qsa    | xk48a       | Reductor          |   3.16
 aci    | 41002       | Articulo Tipo 2   |   0.46
 rei    | 2a44r       | Bisagra Dcha.     | 375.00
 imm    | 773c        | Riostra 1/2-Tm    |  34.82
 aci    | 4100x       | Ajustador         |   0.68
 fea    | 114         | Bancada Motor     |  16.20
 imm    | 887x        | Retenedor Riostra |  14.84
 rei    | 2a44g       | Pasador Bisagra   |  25.00
(25 rows)

-- 3.- Afegir una condició a la sentència de l'exercici anterior per tal de nomès mostrar aquells productes que el valor del camp div és menor a 1.

training=# select id_fab,id_producto,descripcion,case when existencias = 0 then 0 else round((precio/existencias),2) end as div from productos where case when existencias = 0 then 0 else (precio/existencias) end < 1;
 id_fab | id_producto |    descripcion    | div  
--------+-------------+-------------------+------
 rei    | 2a45c       | V Stago Trinquete | 0.38
 bic    | 41672       | Plate             |    0
 aci    | 41003       | Articulo Tipo 3   | 0.52
 aci    | 41004       | Articulo Tipo 4   | 0.84
 qsa    | xk48        | Reductor          | 0.66
 imm    | 887h        | Soporte Riostra   | 0.24
 aci    | 41001       | Articulo Tipo 1   | 0.20
 aci    | 41002       | Articulo Tipo 2   | 0.46
 aci    | 4100x       | Ajustador         | 0.68
(9 rows)


-- 4.- Donat l'identificador d'un client que retorni la importància del client, és a dir, el percentatge dels imports de les comandes del client respecte al total dels imports de les comandes.

----algo asi---

select aa.clie round(((select sum(b.importe) from pedidos b where aa.clie=b.clie)*100/(select sum(importe)

-- 5.- Calcular el que s'ha deixat de cobrar per a un producte determinat.
-- És a dir, la diferència que hi ha entre el que val i el que li hem cobrat
-- al total de clients.
----algo asi----
training=# select b.id_fab,b.id_producto,(select a.precio * sum(cant) - sum(importe) from pedidos join productos a on fab=a.id_fab and producto = a.id_producto where fab = b.id_fab and producto = b.id_producto group by id_fab,id_producto) from productos b;
 id_fab | id_producto | ?column? 
--------+-------------+----------
 rei    | 2a45c       |     0.00
 aci    | 4100y       |  2750.00
 qsa    | xk47        |   -66.00
 bic    | 41672       |         
 imm    | 779c        |     0.00
 aci    | 41003       |     0.00
 aci    | 41004       |     0.00
 bic    | 41003       |     0.00
 imm    | 887p        |         
 qsa    | xk48        |         
 rei    | 2a44l       |     0.00
 fea    | 112         |     0.00
 imm    | 887h        |         
 bic    | 41089       |         
 aci    | 41001       |         
 imm    | 775c        |     0.00
 aci    | 4100z       |     0.00
 qsa    | xk48a       |         
 aci    | 41002       |     0.00
 rei    | 2a44r       |     0.00
 imm    | 773c        |     0.00
 aci    | 4100x       |     0.00
 fea    | 114         |     0.00
 imm    | 887x        |         
 rei    | 2a44g       |     0.00
(25 rows)


-- 6.- Crea una funció que si li passem dos columnes com les de "ventas" i "cuota". Ha de retornar una columna amb el valor de restar la quota a les ventes.


-- 7.- Fes una funció que donat un identificador de representant de vendes retorni l'identificador dels clients que té assignats amb el seu límit de crèdit.


-- 8.- Crear una funció promig_anual(venedor, anyp) que retorni el promig d'imports de comandes del venedor durant aquell any.


-- 9.- Crear una funció max_promig_anual(anyp) que retorni el màxim dels promitjos anuals de tots els venedors. Useu la funció de l'exercici anterior.


-- 10.- Creeu una funció promig_anual_tots(anyp) que retorni el promig anual de cada venedor durant l'any indicat. Useu funcions creades en els exercicis anteriors.


-- 11.- Feu una funció que retorni tots els codis dels clients que no hagin comprat res durant el mes introduït.


-- 12.- Funció anomenada MaximMes a la que se li passa un any i retorna el mes en el que hi ha hagut les màximes vendes (imports totals del mes).


-- 13.- a) Crear una funció baixa_rep que doni de baixa el venedor que se li passa per paràmetre i reassigni tots els seus clients al venedor que tingui menys clients assignats (si hi ha empat, a qualsevol d'ells).
--      b) Com usarieu la funció per donar de baixa els 3 venedors que fa més temps que no han fet cap venda?


-- 14.- Crear una funció anomenada "nclientes" que donat un identificador d'un representant de vendes ens retorni el nombre de clients que te assignats. Si l'entrada és nul·la s'ha de retornar un valor nul. 


-- 15.- Crear una funció anomenada "natendidos" que donat un identificador d'un representant de vendes ens retorni el nombre de clients diferents que ha atès. Si l'entrada és nul·la s'ha de retornar un valor nul.


-- 16.- Crear una funció anomenada "totalimportes" que donat un identificador d'un representant de vendes ens retorni la suma dels imports de les seves comandes. Si l'entrada és nul·la s'ha de retornar un valor nul.


-- 17.- Crear una funció anomenada "informe_rep" que ens retorni una taula amb l'identificador del representant de vendes,
-- el resultat de "nclientes", "natendidos" i "totalimportes". Si a la funció se li passa un identificador de representant
-- de vendes només ha de retornar la informació relativa a aquest representant de vendes. En cas de passar un valor nul a
-- la funció aquesta ha de donar la informació de tots els representants de vendes.   


-- 18.- Crear una funció i les funcions auxiliars convenients per obtenir la següent informació:
-- Donat l'identificador d'un producte obtenir una taula amb les següents dades:
--  * Identificador de producte i fabricant.
--  * Nombre de representants de vendes que han venut aquest producte.
--  * Nombre de clients que han comprat el producte.
--  * Mitjana de l'import de les comandes d'aquest producte.
--  * Quantitat mínima i quantitat màxima que s'ha demanat del producte en una sola comanda.


-- 19.- Crear una funció que donada una oficina ens retorni una taula identificant els productes i mostrant la quantitat d'aquest producte que s'ha venut a l'oficina.


-- 20.- Crear les funcions necessàries per aconseguir el següent resultat:
-- Cridant resumen_cliente() ha de retornar una taula amb la amb els identificadors dels clients, la suma de les seves compres, nombre de comandes realitzades i nombre de representants de vendes que l'han atès.
-- Cridant resumen_cliente(num_clie) ha de retornar una taula amb els identificador dels representants de vendes i el nombre de comandes que ha realitzat el client amb aquest representant de vendes.
-- Cridant resumen_cliente(num_clie, num_empl) ha de retornar una taula amb el nombre de productes diferents que ha demanat el client especificat al representant de vendes especificat i la mitja de l'import de les comandes que ha realitzat el client especificat al representant de vendes especificat. 


-- 21.- Crear una funció el_millor() que retorni totes les dades del venedor que ha venut més (major total d'imports) durant l'any en curs.


-- 22.- Calcular el descompte fet a un client concret, respecte totes les comandes del client.
-- Cal crear dos funcions auxiliars:
-- La primera obtindrà el total dels imports de les comandes d'un client determinat.
-- La segona serà una funció preu de "comanda abstracta" que necessitarà el producte en qüestió i la quantitat de productes demanats.


