
1- Crear una funció que torni la descripció d''un id_fab + id_producte
CREATE OR REPLACE FUNCTION desc_pro(id_fab text, id_pro text)
RETURNS text AS
$$
DECLARE
    result text := '';
   	searchsql text := '';
   	var_fila record;
BEGIN
  	searchsql := 'SELECT * FROM productos WHERE id_producto = ''' || id_pro || ''' AND id_fab = ''' || id_fab || '''';
              
    RAISE  WARNING 'i: %', searchsql;
   	FOR var_fila IN EXECUTE(searchsql) LOOP
       	result = var_fila.descripcion;
    END LOOP;
    IF result = '' THEN
		result := 'Dades inexistents';
    END IF;

   	RETURN searchsql || ': ' || result;

EXCEPTION 
 	WHEN others THEN return '5';
END;
$$
LANGUAGE 'plpgsql' IMMUTABLE;

2- Crear una funció que torni els noms de productes que cumpleixin un patró de l''estil A%, AB%E, %e%, etc.

3- Crear una funció que inserti un client nou

4- Crear una funció que passi a majúscules tots els noms dels clients

5- Crear una funció que augmenti un X% el preu dels productes. El núm. X el rebrà la funció com a paràmetre;
