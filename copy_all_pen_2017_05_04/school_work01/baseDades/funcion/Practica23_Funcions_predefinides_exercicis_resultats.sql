--1.- Mostreu la longitud de la cadena "hola que tal"

-------------
          12
(1 row)

--2.- Mostreu la longitud de les variables "id_producto"

-------------
           5
           5
           5
        (...)
(25 rows)

--3.- El mateix amb el camp "descripcion"

-------------
          17
           9
           8
           5
          12
          15
        (...)
(25 rows)
 

--4.- Mostreu el nom dels venedors en majúscules

---------------
 BILL ADAMS
 MARY JONES
 SUE SMITH
 SAM CLARK
 BOB SMITH
        (...)
(10 rows)


--5.- El mateix d'abans però en minúscules.

     lower
---------------
 bill adams
 mary jones
 sue smith
 sam clark
 bob smith
 dan roberts
 tom snyder
 larry fitch
 paul cruz
 nancy angelli
(10 rows)

--6.- Trobeu on és la posició del espai en blanc de la cadena 'potser 7'
select position( ' ' in 'potser 7') ;

--------
      7
(1 row)

training=# select strpos('potser 7',' ') ;
 strpos 
--------
      7
(1 row)


--7.- Mostreu el cognom, només el cognom del venedors:

-----------
 Adams
 Jones
 Smith
 Clark
 Smith
 Roberts
 Snyder
 Fitch
 Cruz
 Angelli
(10 rows)

training=# select upper(left(nombre,strpos(nombre,' '))) from repventas ;
 upper  
--------
 BILL 
 MARY 
 SUE 
 SAM 
 BOB 
 DAN 
 TOM 
 LARRY 
 PAUL 
 NANCY 
(10 rows)


---Observació: adonem-nos que és important el + 1, d'altra banda ens quedarà un
--espai en blanc al principi que no veurem. Es pot comprovar amb la següent 
--instrucció:

------------
 ** Adams
 ** Jones
 ** Smith
 ** Clark
 ** Smith
 ** Roberts
 ** Snyder
 ** Fitch
 ** Cruz
 ** Angelli
(10 rows)

--8.- Treure el nom, només el nom, de la variable nombre.



--------
 Bill
 Mary
 Sue
 Sam
 Bob
 Dan
 Tom
 Larry
 Paul
 Nancy
(10 rows)
--Obs: el mateix comentari d'abans respecte el '-1'.

training=# select num_empl,left(nombre,strpos(nombre,' ')),right(nombre, -strpos(nombre,' ')) from repventas ;
 num_empl |  left  |  right  
----------+--------+---------
      105 | Bill   | Adams
      109 | Mary   | Jones
      102 | Sue    | Smith
      106 | Sam    | Clark
      104 | Bob    | Smith
      101 | Dan    | Roberts
      110 | Tom    | Snyder
      108 | Larry  | Fitch
      103 | Paul   | Cruz
      107 | Nancy  | Angelli
(10 rows)

training=# select substring(nombre,strpos(nombre,' ')+1,length(nombre))
from repventas ;
 substring 
-----------
 Adams
 Jones
 Smith
 Clark
 Smith
 Roberts
 Snyder
 Fitch
 Cruz
 Angelli
(10 rows)

--9.- Volem mostrar el nom, només el nom dels venedors, en majúscules.


training=# select upper(split_part(nombre,' ',1)) from repventas ;
 upper 
-------
 BILL
 MARY
 SUE
 SAM
 BOB
 DAN
 TOM
 LARRY
 PAUL
 NANCY
(10 rows)


--10.- Mostreu els valors del camp nombre de manera que
--'Bill Adams' sorti com 'B. Adams'.
 
training=# select concat(left(nombre,1),'. ',split_part(nombre,' ',2)) from repventas;
   concat   
------------
 B. Adams
 M. Jones
 S. Smith
 S. Clark
 B. Smith
 D. Roberts
 T. Snyder
 L. Fitch
 P. Cruz
 N. Angelli
(10 rows)



--11.- Mostreu els valors del camp nombre de manera que
--'Bill Adams' sorti com 'Adams, Bill'.

----------------
 Adams, Bill
 Jones, Mary
 Smith, Sue
 Clark, Sam
 Smith, Bob
 Roberts, Dan
 Snyder, Tom
 Fitch, Larry
 Cruz, Paul
 Angelli, Nancy
(10 rows)

training=# select concat(split_part(nombre,' ',1),', ',split_part(nombre,' ',2)) from repventas;
     concat     
----------------
 Bill, Adams
 Mary, Jones
 Sue, Smith
 Sam, Clark
 Bob, Smith
 Dan, Roberts
 Tom, Snyder
 Larry, Fitch
 Paul, Cruz
 Nancy, Angelli
(10 rows)

 
--12.- Volem mostrar el camp descripcion de la taula productos però que en
--comptes de sortir espais en blanc, volem subratllats ('_').
training=# select replace(descripcion,' ','_') from productos ;
      replace      
-------------------
 V_Stago_Trinquete
 Extractor
 Reductor
 Plate
 Riostra_2-Tm
 Articulo_Tipo_3
 Articulo_Tipo_4
 Manivela
 Perno_Riostra
 Reductor
 Bisagra_Izqda.
 Cubierta
 Soporte_Riostra
 Retn
 Articulo_Tipo_1
 Riostra_1-Tm
 Montador
 Reductor
 Articulo_Tipo_2
 Bisagra_Dcha.
 Riostra_1/2-Tm
 Ajustador
 Bancada_Motor
 Retenedor_Riostra
 Pasador_Bisagra
(25 rows)

-------------------
 V_Stago_Trinquete
 Extractor
 Reductor
 Plate
 Riostra_2-Tm
 Articulo_Tipo_3
 Articulo_Tipo_4
 .....

--13.- Volem treure per pantalla la següent columna, que conté el nom i les
--vendes:
   vendes dels empleats
---------------------------
 Bill Adams..... 367911,00
 Mary Jones..... 392725,00
 Sue Smith...... 474050,00
 Sam Clark...... 299912,00
 Bob Smith...... 142594,00
 Dan Roberts.... 305673,00
 Tom Snyder.....  75985,00
 Larry Fitch.... 361865,00
 Paul Cruz...... 286775,00
 Nancy Angelli.. 186042,00
(10 rows)

training=# select concat(rpad(nombre,20,'.'),' ',ventas) from  repventas ;
             concat             
--------------------------------
 Bill Adams.......... 367911.00
 Mary Jones.......... 392725.00
 Sue Smith........... 474050.00
 Sam Clark........... 299912.00
 Bob Smith........... 142594.00
 Dan Roberts......... 305673.00
 Tom Snyder.......... 75985.00
 Larry Fitch......... 361865.00
 Paul Cruz........... 286775.00
 Nancy Angelli....... 186042.00
(10 rows)


--14.- Treieu per pantalla el temps total que fa que estan contractats els
--treballadors
 
    nombre     |
---------------+-------------------------------
 Bill Adams    | 15 years 6 days
 Mary Jones    | 13 years 4 mons 6 days
 Sue Smith     | 16 years 2 mons 8 days
 Sam Clark     | 14 years 8 mons 4 days
 Bob Smith     | 15 years 8 mons 30 days
 Dan Roberts   | 16 years 3 mons 29 days
 Tom Snyder    | 13 years 1 mon 5 days
 Larry Fitch   | 13 years 4 mons 6 days
 Paul Cruz     | 15 years 11 mons 17 days
 Nancy Angelli | 14 years 3 mons 4 days
(10 rows)

training=# select nombre,age(contrato) from repventas ;
    nombre     |           age           
---------------+-------------------------
 Bill Adams    | 29 years 1 mon 12 days
 Mary Jones    | 27 years 5 mons 12 days
 Sue Smith     | 30 years 3 mons 14 days
 Sam Clark     | 28 years 9 mons 10 days
 Bob Smith     | 29 years 10 mons 5 days
 Dan Roberts   | 30 years 5 mons 4 days
 Tom Snyder    | 27 years 2 mons 11 days
 Larry Fitch   | 27 years 5 mons 12 days
 Paul Cruz     | 30 years 23 days
 Nancy Angelli | 28 years 4 mons 10 days
(10 rows)

---importe for str in date need timestam if already in dat not need

training=# select nombre,age(timestamp '2017-03-21') from repventas ;
    nombre     |  age   
---------------+--------
 Bill Adams    | 3 days
 Mary Jones    | 3 days
 Sue Smith     | 3 days
 Sam Clark     | 3 days
 Bob Smith     | 3 days
 Dan Roberts   | 3 days
 Tom Snyder    | 3 days
 Larry Fitch   | 3 days
 Paul Cruz     | 3 days
 Nancy Angelli | 3 days
(10 rows)

--26.- Mostrar el nom dels venedors i la data del seu contracte. La data s'ha de mostrar usant el següent format:
--	dia_de_la_setmana  dia_numèric, mes_numeric del any_4digits
	per exemple:
	   Bill Adams    | Friday 12, 02 del 1988
	 
training=# select nombre,to_char(contrato,'day'),concat(extract(day from contrato),' , ',extract(month from contrato),' del ',extract(year from contrato)) from repventas;
    nombre     |  to_char  |      concat      
---------------+-----------+------------------
 Bill Adams    | friday    | 12 , 2 del 1988
 Mary Jones    | thursday  | 12 , 10 del 1989
 Sue Smith     | wednesday | 10 , 12 del 1986
 Sam Clark     | tuesday   | 14 , 6 del 1988
 Bob Smith     | tuesday   | 19 , 5 del 1987
 Dan Roberts   | monday    | 20 , 10 del 1986
 Tom Snyder    | saturday  | 13 , 1 del 1990
 Larry Fitch   | thursday  | 12 , 10 del 1989
 Paul Cruz     | sunday    | 1 , 3 del 1987
 Nancy Angelli | monday    | 14 , 11 del 1988
(10 rows)


--27.- LListar el nom dels treballadors i la data del seu contracte mostrant-la amb el següent format:
--		Dia_de_la_setmana dia_mes_numeric, mes_text del any_4digits
	per exemple
                Bill Adams    | Friday    12, February del 1988
                
                
training=# select nombre,to_char(contrato,'day'),to_char(contrato,'month'),concat(extract(day from contrato),' , ',extract(month from contrato),' del ',extract(year from contrato)) from repventas;
    nombre     |  to_char  |  to_char  |      concat      
---------------+-----------+-----------+------------------
 Bill Adams    | friday    | february  | 12 , 2 del 1988
 Mary Jones    | thursday  | october   | 12 , 10 del 1989
 Sue Smith     | wednesday | december  | 10 , 12 del 1986
 Sam Clark     | tuesday   | june      | 14 , 6 del 1988
 Bob Smith     | tuesday   | may       | 19 , 5 del 1987
 Dan Roberts   | monday    | october   | 20 , 10 del 1986
 Tom Snyder    | saturday  | january   | 13 , 1 del 1990
 Larry Fitch   | thursday  | october   | 12 , 10 del 1989
 Paul Cruz     | sunday    | march     | 1 , 3 del 1987
 Nancy Angelli | monday    | november  | 14 , 11 del 1988
(10 rows)

                
--29.-  Cal fer un llistat dels productes dels quals les existències  són inferiors al total d'unitats venudes d'aquell producte els darrers 60 dies, a comptar des de la data actual. 
--Cal mostrar els codis de fabricant i de producte, les existències, i les unitats totals venudes dels darrers 60 dies.


--30- Com l'exercici 29 però en comptes de 60 dies ara es volen dos mesos naturals: productes venuts durant el mes 
--actual o durant l'anterior (si no som a fi de mes seràn menys de 60 dies).

