-- 1.- Quina és la comanda promig de cada venedor?

training=# select rep,avg(importe) from pedidos group by rep ;
 rep |          avg           
-----+------------------------
 106 |     16479.000000000000
 107 | 11477.3333333333333333
 102 |  5694.0000000000000000
 108 |  8376.1428571428571429
 105 |  7865.4000000000000000
 109 |  3552.5000000000000000
 103 |  1350.0000000000000000
 101 |  8876.0000000000000000
 110 | 11566.0000000000000000
(9 rows)


-- 2.- Quin és el rang de quotes asignades a cada oficina? ( es a dir, el mínim i el màxim)

training=# select oficina_rep,max(cuota),min(cuota) from repventas group by oficina_rep order by oficina_rep;
 oficina_rep |    max    |    min    
-------------+-----------+-----------
          11 | 300000.00 | 275000.00
          12 | 300000.00 | 200000.00
          13 | 350000.00 | 350000.00
          21 | 350000.00 | 350000.00
          22 | 300000.00 | 300000.00
             |           |          
(6 rows)


-- 3.- Quants venedors estan asignats a cada oficina?

training=# select oficina_rep,count(num_empl) from repventas where oficina_rep is not null group by oficina_rep;
 oficina_rep | count 
-------------+-------
          12 |     3
          21 |     2
          11 |     2
          13 |     1
          22 |     1
(5 rows)


-- 4.- Quants clients són atesos per (tenen com a representant oficial a) cada cada venedor?

training=# select rep_clie,count(num_clie) from clientes group by rep_clie;
 rep_clie | count 
----------+-------
      106 |     2
      107 |     1
      104 |     1
      102 |     4
      108 |     2
      105 |     2
      109 |     2
      103 |     3
      101 |     3
      110 |     1
(10 rows)


-- 5.- Calcula el total de l'import de les comandes per cada venedor i per cada client.

training=# select clie,rep,sum(importe) from pedidos group by clie,rep;
 clie | rep |   sum    
------+-----+----------
 2111 | 105 |  3745.00
 2124 | 107 |  3082.00
 2111 | 103 |  2700.00
 2113 | 101 | 22500.00
 2108 | 101 |   150.00
 2120 | 102 |  3750.00
 2106 | 102 |  4026.00
 2117 | 106 | 31500.00
 2103 | 105 | 35582.00
 2108 | 109 |  7105.00
 2114 | 102 | 15000.00
 2109 | 107 | 31350.00
 2114 | 108 |  7100.00
 2107 | 110 | 23132.00
 2101 | 106 |  1458.00
 2118 | 108 |  3608.00
 2102 | 101 |  3978.00
 2112 | 108 | 47925.00
(18 rows)


-- 6.- El mateix que a la qüestió anterior, però ordenat per client i dintre de client per venedor.

training=# select clie,rep,sum(importe) from pedidos group by clie,rep order by clie,rep;
 clie | rep |   sum    
------+-----+----------
 2101 | 106 |  1458.00
 2102 | 101 |  3978.00
 2103 | 105 | 35582.00
 2106 | 102 |  4026.00
 2107 | 110 | 23132.00
 2108 | 101 |   150.00
 2108 | 109 |  7105.00
 2109 | 107 | 31350.00
 2111 | 103 |  2700.00
 2111 | 105 |  3745.00
 2112 | 108 | 47925.00
 2113 | 101 | 22500.00
 2114 | 102 | 15000.00
 2114 | 108 |  7100.00
 2117 | 106 | 31500.00
 2118 | 108 |  3608.00
 2120 | 102 |  3750.00
 2124 | 107 |  3082.00
(18 rows)


-- 7.- Calcula les comandes (imports) totals per a cada venedor.

training=# select rep,sum(importe) from pedidos group by rep order by rep;
 rep |   sum    
-----+----------
 101 | 26628.00
 102 | 22776.00
 103 |  2700.00
 105 | 39327.00
 106 | 32958.00
 107 | 34432.00
 108 | 58633.00
 109 |  7105.00
 110 | 23132.00
(9 rows)


-- 8.- Quin és l'import promig de les comandes per cada venedor, les comandes dels quals sumen més de 30 000?

training=# select rep,avg(importe) as promig from pedidos group by rep having sum(importe) >30000 order by rep;
 rep |         promig         
-----+------------------------
 105 |  7865.4000000000000000
 106 |     16479.000000000000
 107 | 11477.3333333333333333
 108 |  8376.1428571428571429
(4 rows)



-- 9.- Per cada oficina amb 2 o més persones, calculeu la quota 
---total i les vendes totals per a tots els venedors que treballen a la oficina 
---no feam ----(volem que sorti la ciutat de l'oficina a la consulta) 

training=# select oficina_rep,count(num_empl),sum(cuota),sum(ventas) from repventas group by oficina_rep having count(num_empl) >= 2;
 oficina_rep | count |    sum    |    sum    
-------------+-------+-----------+-----------
          12 |     3 | 775000.00 | 735042.00
          21 |     2 | 700000.00 | 835915.00
          11 |     2 | 575000.00 | 692637.00
(3 rows)
						mejor es
						
training=# select oficina_rep,count(num_empl),sum(cuota),sum(ventas) from repventas where oficina_rep is not null group by oficina_rep having count(num_empl) >= 2;
 oficina_rep | count |    sum    |    sum    
-------------+-------+-----------+-----------
          12 |     3 | 775000.00 | 735042.00
          21 |     2 | 700000.00 | 835915.00
          11 |     2 | 575000.00 | 692637.00
(3 rows)
