
--- PRÀCTICA 11.  UNION  +  LEFT JOIN + WHERE

---1. Llistar els productes amb existències no superiors a 200 i no inferiors a 20 de la fàbrica aci o de la fabrica imm, i els nom dels clients que han comprat aquests productes. Si algun producte no l'ha comprat ningú ha de sortir sense nom de client.
training=# select clientes.empresa,descripcion from clientes join pedidos on num_clie = clie right join productos on id_fab = fab and id_producto = producto where (existencias <= 200 and existencias >= 20) and id_fab in ('aci','imm') ;
      empresa      |    descripcion    
-------------------+-------------------
 First Corp.       | Articulo Tipo 4
 Ace International | Montador
 Acme Mfg.         | Articulo Tipo 4
 Acme Mfg.         | Articulo Tipo 4
 Orion Corp        | Montador
 Acme Mfg.         | Articulo Tipo 2
 Zetacorp          | Riostra 1/2-Tm
 Midwest Systems   | Articulo Tipo 2
 Holm & Landis     | Ajustador
 Acme Mfg.         | Extractor
 JCP Inc.          | Ajustador
                   | Perno Riostra
                   | Retenedor Riostra
(13 rows)
						----		or   --------


training=# select clientes.empresa,descripcion from productos left join pedidos on id_fab = fab and id_producto = producto left join clientes on clie = num_clie where (existencias <= 200 and existencias >= 20) and id_fab in ('aci','imm') ;
      empresa      |    descripcion    
-------------------+-------------------
 First Corp.       | Articulo Tipo 4
 Ace International | Montador
 Acme Mfg.         | Articulo Tipo 4
 Acme Mfg.         | Articulo Tipo 4
 Orion Corp        | Montador
 Acme Mfg.         | Articulo Tipo 2
 Zetacorp          | Riostra 1/2-Tm
 Midwest Systems   | Articulo Tipo 2
 Holm & Landis     | Ajustador
 Acme Mfg.         | Extractor
 JCP Inc.          | Ajustador
                   | Perno Riostra
                   | Retenedor Riostra
(13 rows)


training=# select 'producto:',descripcion,'-----' from productos left join pedidos on id_fab = fab and id_producto = producto left join clientes on clie = num_clie where (existencias <= 200 and existencias >= 20) and id_fab in ('aci','imm') 
union
select 'clientes:',descripcion,empresa from productos left join pedidos on id_fab = fab and id_producto = producto left join clientes on clie = num_clie where (existencias <= 200 and existencias >= 20) and id_fab in ('aci','imm') order by 2,1 desc;
 ?column?  |    descripcion    |     ?column?      
-----------+-------------------+-------------------
 producto: | Ajustador         | -----
 clientes: | Ajustador         | JCP Inc.
 clientes: | Ajustador         | Holm & Landis
 producto: | Articulo Tipo 2   | -----
 clientes: | Articulo Tipo 2   | Acme Mfg.
 clientes: | Articulo Tipo 2   | Midwest Systems
 producto: | Articulo Tipo 4   | -----
 clientes: | Articulo Tipo 4   | First Corp.
 clientes: | Articulo Tipo 4   | Acme Mfg.
 producto: | Extractor         | -----
 clientes: | Extractor         | Acme Mfg.
 producto: | Montador          | -----
 clientes: | Montador          | Orion Corp
 clientes: | Montador          | Ace International
 producto: | Perno Riostra     | -----
 clientes: | Perno Riostra     | 
 producto: | Retenedor Riostra | -----
 clientes: | Retenedor Riostra | 
 producto: | Riostra 1/2-Tm    | -----
 clientes: | Riostra 1/2-Tm    | Zetacorp
(20 rows)

---2. Llistar la ciutat de cada oficina, el número total de treballadors, el nom del cap de l'oficina i el nom de cadascun dels treballadors (incloent al cap com a treballador).

-----algo asi-----

training=# select '1 oficina:',ciudad,null,null,count(num_empl) from repventas join oficinas on oficina_rep = oficina group by oficina 
union
select '2 nom del director:',ciudad,dir.nombre,null,null from oficinas join repventas dir on oficinas.dir = dir.num_empl
union
select '3 nom de empl:',ciudad,null,nombre,null from repventas left join oficinas on oficina_rep = oficina order by 2;
      ?column?       |   ciudad    |  ?column?   |   ?column?    | count 
---------------------+-------------+-------------+---------------+-------
 1 oficina:          | Atlanta     |             |               |     1
 2 nom del director: | Atlanta     | Bill Adams  |               |      
 3 nom de empl:      | Atlanta     |             | Bill Adams    |      
 2 nom del director: | Chicago     | Bob Smith   |               |      
 3 nom de empl:      | Chicago     |             | Dan Roberts   |      
 3 nom de empl:      | Chicago     |             | Bob Smith     |      
 3 nom de empl:      | Chicago     |             | Paul Cruz     |      
 1 oficina:          | Chicago     |             |               |     3
 2 nom del director: | Denver      | Larry Fitch |               |      
 3 nom de empl:      | Denver      |             | Nancy Angelli |      
 1 oficina:          | Denver      |             |               |     1
 2 nom del director: | Los Angeles | Larry Fitch |               |      
 3 nom de empl:      | Los Angeles |             | Larry Fitch   |      
 3 nom de empl:      | Los Angeles |             | Sue Smith     |      
 1 oficina:          | Los Angeles |             |               |     2
 2 nom del director: | New York    | Sam Clark   |               |      
 1 oficina:          | New York    |             |               |     2
 3 nom de empl:      | New York    |             | Sam Clark     |      
 3 nom de empl:      | New York    |             | Mary Jones    |      
 3 nom de empl:      |             |             | Tom Snyder    |      
(20 rows)

								---	right version ----
training=# select '2 total empl:',ciudad,null,count(num_empl),null from repventas join oficinas on oficina_rep = oficina group by oficina 
union
select '1 nom del director:',ciudad,dir.nombre,null,null from oficinas join repventas dir on oficinas.dir = dir.num_empl
union
select '3 nom de empl:',ciudad,null,null,nombre from repventas left join oficinas on oficina_rep = oficina order by 2,1;
      ?column?       |   ciudad    |  ?column?   | count |   ?column?    
---------------------+-------------+-------------+-------+---------------
 1 nom del director: | Atlanta     | Bill Adams  |       | 
 2 total empl:       | Atlanta     |             |     1 | 
 3 nom de empl:      | Atlanta     |             |       | Bill Adams
 1 nom del director: | Chicago     | Bob Smith   |       | 
 2 total empl:       | Chicago     |             |     3 | 
 3 nom de empl:      | Chicago     |             |       | Paul Cruz
 3 nom de empl:      | Chicago     |             |       | Dan Roberts
 3 nom de empl:      | Chicago     |             |       | Bob Smith
 1 nom del director: | Denver      | Larry Fitch |       | 
 2 total empl:       | Denver      |             |     1 | 
 3 nom de empl:      | Denver      |             |       | Nancy Angelli
 1 nom del director: | Los Angeles | Larry Fitch |       | 
 2 total empl:       | Los Angeles |             |     2 | 
 3 nom de empl:      | Los Angeles |             |       | Sue Smith
 3 nom de empl:      | Los Angeles |             |       | Larry Fitch
 1 nom del director: | New York    | Sam Clark   |       | 
 2 total empl:       | New York    |             |     2 | 
 3 nom de empl:      | New York    |             |       | Mary Jones
 3 nom de empl:      | New York    |             |       | Sam Clark
 3 nom de empl:      |             |             |       | Tom Snyder
(20 rows)

									
								

---3. Llistar els noms i preus dels productes de la fàbrica imm i de la fàbrica rei que contenen 'gr' o 'tr' en el seu nom i que valen entre 900 i 5000€, i els noms dels venedors que han venut aquests productes. Si algun producte no l'ha comprat ningú ha de sortir sense nom venedor.

training=# select '1 producto:',descripcion,null,null,precio from productos where id_fab in ('imm','rei') and ( descripcion like '%gr%' or descripcion like '%tr%' ) and (precio >= 900 and precio <= 5000)
union
select '2 clientes:',descripcion,id_producto,null,precio from productos where id_fab in ('imm','rei') and ( descripcion like '%gr%' or descripcion like '%tr%' ) and (precio >= 900 and precio <= 5000)
 order by 2,1 ;
  ?column?   |  descripcion   | ?column? | ?column? | precio  
-------------+----------------+----------+----------+---------
 1 producto: | Bisagra Dcha.  |          |          | 4500.00
 2 clientes: | Bisagra Dcha.  | 2a44r    |          | 4500.00
 1 producto: | Bisagra Izqda. |          |          | 4500.00
 2 clientes: | Bisagra Izqda. | 2a44l    |          | 4500.00
 1 producto: | Riostra 1/2-Tm |          |          |  975.00
 2 clientes: | Riostra 1/2-Tm | 773c     |          |  975.00
 1 producto: | Riostra 1-Tm   |          |          | 1425.00
 2 clientes: | Riostra 1-Tm   | 775c     |          | 1425.00
 1 producto: | Riostra 2-Tm   |          |          | 1875.00
 2 clientes: | Riostra 2-Tm   | 779c     |          | 1875.00
(10 rows)


				-----right one ----
				
training=# select '1 producto:',descripcion,precio,null from productos where id_fab in ('imm','rei') and ( descripcion like '%gr%' or descripcion like '%tr%' ) and (precio >= 900 and precio <= 5000)
union
select '2 venador:',descripcion,null,nombre from productos left join pedidos on id_fab =fab and id_producto = producto left join repventas on rep = num_empl where id_fab in ('imm','rei') and ( descripcion like '%gr%' or descripcion like '%tr%' ) and (precio >= 900 and precio <= 5000)
 order by 2,1 ;
  ?column?   |  descripcion   | precio  |   ?column?    
-------------+----------------+---------+---------------
 1 producto: | Bisagra Dcha.  | 4500.00 | 
 2 venador:  | Bisagra Dcha.  |         | Larry Fitch
 2 venador:  | Bisagra Dcha.  |         | Dan Roberts
 1 producto: | Bisagra Izqda. | 4500.00 | 
 2 venador:  | Bisagra Izqda. |         | Sam Clark
 1 producto: | Riostra 1/2-Tm |  975.00 | 
 2 venador:  | Riostra 1/2-Tm |         | Larry Fitch
 1 producto: | Riostra 1-Tm   | 1425.00 | 
 2 venador:  | Riostra 1-Tm   |         | Nancy Angelli
 1 producto: | Riostra 2-Tm   | 1875.00 | 
 2 venador:  | Riostra 2-Tm   |         | Sue Smith
 2 venador:  | Riostra 2-Tm   |         | Mary Jones
(12 rows)


---4. Llistar els noms dels productes, el número total de ventes que s'ha fet d'aquell producte, 
la quantitat total d'unitats que s'han venut d'aquell producte, i el nom de cada client que l'ha comprat.

training=# select '1 producto:',descripcion,count(num_pedido),null,null,null from productos join pedidos on id_fab = fab and id_producto = producto group by id_fab,id_producto
union
select '3 totral:',descripcion,null,null,null,sum(cant) from productos join pedidos on id_fab = fab and id_producto = producto group by id_fab,id_producto
union
select '2 clientes:',descripcion,null,empresa,num_pedido, null from productos join pedidos on id_fab = fab and
 id_producto = producto join clientes on clie = num_clie order by 2,1;
  ?column?   |    descripcion    | count |     ?column?      | ?column? 
-------------+-------------------+-------+-------------------+----------
 1 producto: | Ajustador         |     2 |                   |         
 2 clientes: | Ajustador         |       | Holm & Landis     |         
 2 clientes: | Ajustador         |       | JCP Inc.          |         
 3 totral:   | Ajustador         |       |                   |       30
 1 producto: | Articulo Tipo 2   |     2 |                   |         
 2 clientes: | Articulo Tipo 2   |       | Acme Mfg.         |         
 2 clientes: | Articulo Tipo 2   |       | Midwest Systems   |         
 3 totral:   | Articulo Tipo 2   |       |                   |       64
 1 producto: | Articulo Tipo 3   |     1 |                   |         
 2 clientes: | Articulo Tipo 3   |       | JCP Inc.          |         
 3 totral:   | Articulo Tipo 3   |       |                   |       35
 1 producto: | Articulo Tipo 4   |     3 |                   |         
 2 clientes: | Articulo Tipo 4   |       | Acme Mfg.         |         
 2 clientes: | Articulo Tipo 4   |       | First Corp.       |         
 3 totral:   | Articulo Tipo 4   |       |                   |       68
 1 producto: | Bancada Motor     |     2 |                   |         
 2 clientes: | Bancada Motor     |       | Peter Brothers    |         
 2 clientes: | Bancada Motor     |       | Jones Mfg.        |         
 3 totral:   | Bancada Motor     |       |                   |       16
 1 producto: | Bisagra Dcha.     |     2 |                   |         
 2 clientes: | Bisagra Dcha.     |       | Ian & Schmidt     |         
 2 clientes: | Bisagra Dcha.     |       | Zetacorp          |         
 3 totral:   | Bisagra Dcha.     |       |                   |       15
 1 producto: | Bisagra Izqda.    |     1 |                   |         
 2 clientes: | Bisagra Izqda.    |       | J.P. Sinclair     |         
 3 totral:   | Bisagra Izqda.    |       |                   |        7
 1 producto: | Cubierta          |     1 |                   |         
 2 clientes: | Cubierta          |       | Holm & Landis     |         
 3 totral:   | Cubierta          |       |                   |       10
 1 producto: | Extractor         |     1 |                   |         
 2 clientes: | Extractor         |       | Acme Mfg.         |         
 3 totral:   | Extractor         |       |                   |       11
 1 producto: | Manivela          |     2 |                   |         
 2 clientes: | Manivela          |       | Midwest Systems   |         
 2 clientes: | Manivela          |       | Peter Brothers    |         
 3 totral:   | Manivela          |       |                   |        2
 1 producto: | Montador          |     2 |                   |         
 2 clientes: | Montador          |       | Orion Corp        |         
 2 clientes: | Montador          |       | Ace International |         
 3 totral:   | Montador          |       |                   |       15
 1 producto: | Pasador Bisagra   |     1 |                   |         
 2 clientes: | Pasador Bisagra   |       | JCP Inc.          |         
 3 totral:   | Pasador Bisagra   |       |                   |        6
 1 producto: | Reductor          |     3 |                   |         
 2 clientes: | Reductor          |       | Midwest Systems   |         
 2 clientes: | Reductor          |       | Orion Corp        |         
 2 clientes: | Reductor          |       | Fred Lewis Corp.  |         
 3 totral:   | Reductor          |       |                   |       28
 1 producto: | Riostra 1/2-Tm    |     1 |                   |         
 2 clientes: | Riostra 1/2-Tm    |       | Zetacorp          |         
 3 totral:   | Riostra 1/2-Tm    |       |                   |        3
 1 producto: | Riostra 1-Tm      |     1 |                   |         
 2 clientes: | Riostra 1-Tm      |       | Chen Associates   |         
 3 totral:   | Riostra 1-Tm      |       |                   |       22
 1 producto: | Riostra 2-Tm      |     2 |                   |         
 2 clientes: | Riostra 2-Tm      |       | Holm & Landis     |         
 2 clientes: | Riostra 2-Tm      |       | Rico Enterprises  |         
 3 totral:   | Riostra 2-Tm      |       |                   |        5
 1 producto: | V Stago Trinquete |     2 |                   |         
 2 clientes: | V Stago Trinquete |       | Ace International |         
 2 clientes: | V Stago Trinquete |       | Fred Lewis Corp.  |         
 3 totral:   | V Stago Trinquete |       |                   |       32
(62 rows)

training=# 
									-------or----------------
training=# select '1 producto:',descripcion,count(num_pedido),null,null from productos join pedidos on id_fab = fab and id_producto = producto group by id_fab,id_producto
union
select '3 totral:',descripcion,null,'total>>>',sum(cant) from productos join pedidos on id_fab = fab and id_producto = producto group by id_fab,id_producto
union
select '2 clientes:',descripcion,null,empresa,cant from productos join pedidos on id_fab = fab and id_producto = producto join clientes on clie = num_clie order by 2,1;
  ?column?   |    descripcion    | count |     ?column?      | ?column? 
-------------+-------------------+-------+-------------------+----------
 1 producto: | Ajustador         |     2 |                   |         
 2 clientes: | Ajustador         |       | Holm & Landis     |        6
 2 clientes: | Ajustador         |       | JCP Inc.          |       24
 3 totral:   | Ajustador         |       | total>>>          |       30
 1 producto: | Articulo Tipo 2   |     2 |                   |         
 2 clientes: | Articulo Tipo 2   |       | Acme Mfg.         |       54
 2 clientes: | Articulo Tipo 2   |       | Midwest Systems   |       10
 3 totral:   | Articulo Tipo 2   |       | total>>>          |       64
 1 producto: | Articulo Tipo 3   |     1 |                   |         
 2 clientes: | Articulo Tipo 3   |       | JCP Inc.          |       35
 3 totral:   | Articulo Tipo 3   |       | total>>>          |       35
 1 producto: | Articulo Tipo 4   |     3 |                   |         
 2 clientes: | Articulo Tipo 4   |       | First Corp.       |       34
 2 clientes: | Articulo Tipo 4   |       | Acme Mfg.         |       28
 2 clientes: | Articulo Tipo 4   |       | Acme Mfg.         |        6
 3 totral:   | Articulo Tipo 4   |       | total>>>          |       68
 1 producto: | Bancada Motor     |     2 |                   |         
 2 clientes: | Bancada Motor     |       | Peter Brothers    |       10
 2 clientes: | Bancada Motor     |       | Jones Mfg.        |        6
 3 totral:   | Bancada Motor     |       | total>>>          |       16
 1 producto: | Bisagra Dcha.     |     2 |                   |         
 2 clientes: | Bisagra Dcha.     |       | Ian & Schmidt     |        5
 2 clientes: | Bisagra Dcha.     |       | Zetacorp          |       10
 3 totral:   | Bisagra Dcha.     |       | total>>>          |       15
 1 producto: | Bisagra Izqda.    |     1 |                   |         
 2 clientes: | Bisagra Izqda.    |       | J.P. Sinclair     |        7
 3 totral:   | Bisagra Izqda.    |       | total>>>          |        7
 1 producto: | Cubierta          |     1 |                   |         
 2 clientes: | Cubierta          |       | Holm & Landis     |       10
 3 totral:   | Cubierta          |       | total>>>          |       10
 1 producto: | Extractor         |     1 |                   |         
 2 clientes: | Extractor         |       | Acme Mfg.         |       11
 3 totral:   | Extractor         |       | total>>>          |       11
 1 producto: | Manivela          |     2 |                   |         
 2 clientes: | Manivela          |       | Peter Brothers    |        1
 2 clientes: | Manivela          |       | Midwest Systems   |        1
 3 totral:   | Manivela          |       | total>>>          |        2
 1 producto: | Montador          |     2 |                   |         
 2 clientes: | Montador          |       | Ace International |        9
 2 clientes: | Montador          |       | Orion Corp        |        6
 3 totral:   | Montador          |       | total>>>          |       15
 1 producto: | Pasador Bisagra   |     1 |                   |         
 2 clientes: | Pasador Bisagra   |       | JCP Inc.          |        6
 3 totral:   | Pasador Bisagra   |       | total>>>          |        6
 1 producto: | Reductor          |     3 |                   |         
 2 clientes: | Reductor          |       | Fred Lewis Corp.  |        6
 2 clientes: | Reductor          |       | Orion Corp        |       20
 2 clientes: | Reductor          |       | Midwest Systems   |        2
 3 totral:   | Reductor          |       | total>>>          |       28
 1 producto: | Riostra 1/2-Tm    |     1 |                   |         
 2 clientes: | Riostra 1/2-Tm    |       | Zetacorp          |        3
 3 totral:   | Riostra 1/2-Tm    |       | total>>>          |        3
 1 producto: | Riostra 1-Tm      |     1 |                   |         
 2 clientes: | Riostra 1-Tm      |       | Chen Associates   |       22
 3 totral:   | Riostra 1-Tm      |       | total>>>          |       22
 1 producto: | Riostra 2-Tm      |     2 |                   |         
 2 clientes: | Riostra 2-Tm      |       | Holm & Landis     |        3
 2 clientes: | Riostra 2-Tm      |       | Rico Enterprises  |        2
 3 totral:   | Riostra 2-Tm      |       | total>>>          |        5
 1 producto: | V Stago Trinquete |     2 |                   |         
 2 clientes: | V Stago Trinquete |       | Fred Lewis Corp.  |       24
 2 clientes: | V Stago Trinquete |       | Ace International |        8
 3 totral:   | V Stago Trinquete |       | total>>>          |       32
(63 rows)


---5. Llistar els poductes que costen més de 1000 o no són ni de la fàbrica imm ni de la fàbrica rei, ni de la fàbrica ací, i el total que n'ha comprat cada client. Si algun producte no l'ha comprat ningú ha de sortir sense nom de client.


training=# select '1 producto:',descripcion,null,null from productos 
union
select '2 clientes:',descripcion,empresa,null from productos left join pedidos on id_fab = fab and id_producto = producto join clientes on clie = num_clie
union
select '3 total:',descripcion,empresa,count(*) from productos left join pedidos on id_fab = fab and id_producto = producto join clientes on clie = num_clie group by id_fab,id_producto,num_clie;
ERROR:  UNION types text and bigint cannot be matched
LINE 5: select '3 total:',descripcion,empresa,count(*) from producto...
                                              ^
				------also result but fault one line (*)------
training=# select '1 clientes:',descripcion,empresa,null from productos join pedidos on id_fab = fab and id_producto = producto join clientes on clie = num_clie;
  ?column?   |    descripcion    |      empresa      | ?column? 
-------------+-------------------+-------------------+----------
 1 clientes: | Bisagra Izqda.    | J.P. Sinclair     | 
 1 clientes: | Articulo Tipo 3   | JCP Inc.          | 
 1 clientes: | Bancada Motor     | Jones Mfg.        | 
 1 clientes: | Articulo Tipo 4   | First Corp.       | 
 1 clientes: | Montador          | Ace International | 
 1 clientes: | Bisagra Dcha.     | Zetacorp          | 
 1 clientes: | Articulo Tipo 4   | Acme Mfg.         | 
 1 clientes: | Manivela          | Midwest Systems   | 
 1 clientes: | Cubierta          | Holm & Landis     | 
 1 clientes: | Manivela          | Peter Brothers    | 
 1 clientes: | Articulo Tipo 4   | Acme Mfg.         | 
 1 clientes: | Reductor          | Orion Corp        | 
 1 clientes: | Bancada Motor     | Peter Brothers    | 
 1 clientes: | Montador          | Orion Corp        | 
 1 clientes: | Articulo Tipo 2   | Acme Mfg.         | 
 1 clientes: | Riostra 1/2-Tm    | Zetacorp          | 
 1 clientes: | Riostra 1-Tm      | Chen Associates   | 
 1 clientes: | V Stago Trinquete | Ace International | 
 1 clientes: | Articulo Tipo 2   | Midwest Systems   | 
 1 clientes: | Pasador Bisagra   | JCP Inc.          | 
 1 clientes: | Ajustador         | Holm & Landis     | 
 1 clientes: | Riostra 2-Tm      | Rico Enterprises  | 
 1 clientes: | V Stago Trinquete | Fred Lewis Corp.  | 
 1 clientes: | Reductor          | Fred Lewis Corp.  | 
 1 clientes: | Riostra 2-Tm      | Holm & Landis     | 
 1 clientes: | Reductor          | Midwest Systems   | 
 1 clientes: | Extractor         | Acme Mfg.         | 
 1 clientes: | Ajustador         | JCP Inc.          | 
 1 clientes: | Bisagra Dcha.     | Ian & Schmidt     | 
(29 rows)


				
---6. Llistar els codis de fabricants, el número total de productes d'aquell fabricant i el nom de cadascun dels productes.




---7. Llistar els venedors i els seus imports totals de ventes, que tinguin més de 30 anys i treballin a l'oficina 12 i els que tinguin més de 20 anys i treballin a l'oficina 21. Llistar els clients a qui ha venut cadascun d'aquests venedors
