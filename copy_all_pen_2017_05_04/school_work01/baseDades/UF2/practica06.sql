----LLISTA D'EXERCICIS DE SQL . PRÀCTICA 6
================================================== 




---- 21.- Es desitja un llistat didentificadors de fabricants de productes. Només volem tenir en compte els productes de preu superior a 54. Només volem que apareguin els fabricants amb un nombre total d'unitats superior a 300.

training=# select productos.id_fab,sum(productos.existencias) from productos where precio > 54 group by productos.id_fab having sum(existencias) > 300; 
 id_fab | sum 
--------+-----
 aci    | 843
(1 row)


---22.Es desitja un llistat dels productes amb les seves descripcions, ordenat per la suma total d'imports facturats (pedidos) de cada producte de l'any 1989.

training=# select productos.id_fab,productos.descripcion,sum(importe) from productos,pedidos where pedidos.fab = productos.id_fab and pedidos.producto = productos.id_producto group by productos.id_fab,productos.id_producto;
 id_fab |    descripcion    |   sum    
--------+-------------------+----------
 rei    | Pasador Bisagra   |  2100.00
 aci    | Articulo Tipo 3   |  3745.00
 rei    | V Stago Trinquete |  2528.00
 aci    | Ajustador         |   750.00
 fea    | Cubierta          |  1480.00
 bic    | Manivela          |  1304.00
 imm    | Riostra 1-Tm      | 31350.00
 aci    | Montador          | 37500.00
 aci    | Articulo Tipo 2   |  4864.00
 imm    | Riostra 2-Tm      |  9375.00
 rei    | Bisagra Izqda.    | 31500.00
 qsa    | Reductor          | 10006.00
 aci    | Articulo Tipo 4   |  7956.00
 rei    | Bisagra Dcha.     | 67500.00
 imm    | Riostra 1/2-Tm    |  2925.00
 fea    | Bancada Motor     |  3888.00
 aci    | Extractor         | 27500.00
(17 rows)



----23. Per a cada director (de personal, no d'oficina) excepte per al gerent (el venedor que no té director), vull saber el total de vendes dels seus subordinats. Mostreu codi i nom dels directors.

training=# select director_repventas.num_empl,director_repventas.nombre,sum(emplado_repventas.ventas),sum(pedidos.importe) from repventas director_repventas,repventas emplado_repventas,pedidos where emplado_repventas.director = director_repventas.num_empl and emplado_repventas.num_empl = pedidos.rep group by director_repventas.num_empl;
 num_empl |   nombre    |    sum     |   sum    
----------+-------------+------------+----------
      106 | Sam Clark   | 3318505.00 | 65738.00
      108 | Larry Fitch | 2454326.00 | 57208.00
      101 | Dan Roberts |  151970.00 | 23132.00
      104 | Bob Smith   | 3330124.00 | 68655.00
(4 rows)


----24. Quins són els 5 productes que han estat venuts a més clients diferents? Mostreu el número de clients per cada producte. A igualtat de nombre de clients es volen ordenats per ordre decreixent d'existències i, a igualtat d'existències, per descripció. Mostreu tots els camps pels quals s'ordena.

training=# select id_fab,id_producto,descripcion,existencias,count(distinct clie) as "num clientes diferentes" from pedidos join productos on fab = id_fab and producto = id_producto group by id_fab,id_producto order by 5 desc,3,2 limit 5;
 id_fab | id_producto |   descripcion   | existencias | num clientes diferentes 
--------+-------------+-----------------+-------------+-------------------------
 qsa    | xk47        | Reductor        |          38 |                       3
 aci    | 4100x       | Ajustador       |          37 |                       2
 aci    | 41002       | Articulo Tipo 2 |         167 |                       2
 aci    | 41004       | Articulo Tipo 4 |         139 |                       2
 fea    | 114         | Bancada Motor   |          15 |                       2
(5 rows)


----25. Es vol llistar el clients (codi i empresa) tals que no hagin comprat cap tipus de frontissa ("bisagra" en castellà, figura a la descripció) i hagin comprat articles de més d'un fabricant diferent.





----26. Llisteu les oficines per ordre descendent de nombre total de clients diferents amb comandes (pedidos) realizades pels venedors d'aquella oficina, i, a igualtat de clients, ordenat per ordre ascendent del nom del director de l'oficina. Només s'ha de mostrar el codi i la ciutat de l'oficina.


training=# select ciudad,dir.nombre,count(distinct clie) from oficinas join repventas on oficina = oficina_rep join pedidos on num_empl = rep join repventas dir on oficinas.dir = dir.num_empl group by ciudad,dir.nombre;
   ciudad    |   nombre    | count 
-------------+-------------+-------
 Atlanta     | Bill Adams  |     2
 Chicago     | Bob Smith   |     4
 Denver      | Larry Fitch |     2
 Los Angeles | Larry Fitch |     5
 New York    | Sam Clark   |     3
(5 rows)



----27.Llista totes les comandes mostrant el seu número, import, nom de client i límit de crèdit. 



  
----28.Llista cada un dels venedors i la ciutat i regió on treballen 




----29.Llista les oficines, i els noms i títols dels directors. 



----30.Llista les oficines, noms i titols del seus directors amb un objectiu superior a 600.000. 




-----31.Llista els venedors de les oficines de la regió est. 



-----32.Llista totes les comandes, mostrant els imports i les descripcions del producte. 



-----33.Llista les comandes superiors a 25.000, incloent el nom del venedor que va servir la comanda i el nom del client que el va sol·licitar. 



----34.Llista les comandes superiors a 25000, mostrant el client que va demanar la comanda i el nom del venedor que té assignat el client. 


---35.Llista les comandes superiors a 25000, mostrant el nom del client que el va 
---	ordenar, el venedor associat al client, i l'oficina on el venedor treballa. 



----- 35bis.- Llista les comandes superiors a 25000, mostrant el nom del client que el 
---va ordenar, el venedor associat al client, i l'oficina on el venedor treballa. També cal que mostris la descripció del producte. 



--- *36.Trobar totes les comandes rebudes en els dies en que un nou venedor va ser contractat. 



--- *37.Llista totes les combinacions de venedors i oficines on la quota del venedor és superior a l'objectiu de l'oficina. 




--- *38.Mostra el nom, les vendes i l'oficina de cada venedor. 



-- *39.Informa sobre tots els venedors i les oficines en les que treballen. 



--- *40.Informa sobre tots els venedors (tota la informació de repventas) més la ciutat i regió on treballen. 



--- (*)41.Llista el nom dels venedors i el del seu director. 



---- 42.Llista els venedors amb una quota superior a la dels seus directors. 



--- 43.Llista totes les combinacions possibles de venedors i ciutats. 



--- 44.Llistar el nom de l'empresa i totes les comandes fetes pel client 2.103. 


training=# select empresa,pedidos.* from pedidos join clientes on clie = num_clie where num_clie =2103;
  empresa  | num_pedido | fecha_pedido | clie | rep | fab | producto | cant | importe  
-----------+------------+--------------+------+-----+-----+----------+------+----------
 Acme Mfg. |     112963 | 1989-12-17   | 2103 | 105 | aci | 41004    |   28 |  3276.00
 Acme Mfg. |     112983 | 1989-12-27   | 2103 | 105 | aci | 41004    |    6 |   702.00
 Acme Mfg. |     113027 | 1990-01-22   | 2103 | 105 | aci | 41002    |   54 |  4104.00
 Acme Mfg. |     112987 | 1989-12-31   | 2103 | 105 | aci | 4100y    |   11 | 27500.00
(4 rows)


-- 45.Llista els venedors i les oficines en què treballen. 



--- 46.Llista els venedors i les ciutats en què treballen. 



--- 47.Fes que la consulta anterior mostri les dades dels deu venedors. 


