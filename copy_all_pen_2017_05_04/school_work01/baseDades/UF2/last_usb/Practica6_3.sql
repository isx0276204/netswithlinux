--1. Llistar l'identificador de totes les comandes. Mostrar també el nom del director del representant de vendes que ha realitzat la comanda, si aquest existeix.


training=# select num_pedido,dir.nombre from pedidos join repventas on rep = num_empl join repventas dir on repventas.director = dir.num_empl;
 num_pedido |   nombre    
------------+-------------
     113012 | Bob Smith
     113051 | Sam Clark
     112968 | Bob Smith
     110036 | Dan Roberts
     113045 | Sam Clark
     112963 | Bob Smith
     113013 | Sam Clark
     113058 | Sam Clark
     112997 | Larry Fitch
     112983 | Bob Smith
     113024 | Sam Clark
     113062 | Larry Fitch
     112979 | Larry Fitch
     113027 | Bob Smith
     113007 | Sam Clark
     113069 | Larry Fitch
     113034 | Dan Roberts
     112992 | Sam Clark
     112975 | Bob Smith
     113055 | Bob Smith
     113048 | Larry Fitch
     112993 | Larry Fitch
     113065 | Larry Fitch
     113003 | Sam Clark
     113049 | Sam Clark
     112987 | Bob Smith
     113057 | Bob Smith
     113042 | Bob Smith
(28 rows)


--2. Llistar aquells clients que tenen un representant de vendes assignat. Mostrar el nom de l'empresa client, l'identificador i el nom del --representant, el numero d'oficina i la ciutat en la que treballa el representant,en cas de tenir una oficina assignada.


training=# select num_clie,empresa,num_empl,nombre,oficina,ciudad from clientes left join repventas on rep_clie = num_empl left join oficinas on oficina_rep = oficina;
 num_clie |      empresa      | num_empl |    nombre     | oficina |   ciudad    
----------+-------------------+----------+---------------+---------+-------------
     2111 | JCP Inc.          |      103 | Paul Cruz     |      12 | Chicago
     2102 | First Corp.       |      101 | Dan Roberts   |      12 | Chicago
     2103 | Acme Mfg.         |      105 | Bill Adams    |      13 | Atlanta
     2123 | Carter & Sons     |      102 | Sue Smith     |      21 | Los Angeles
     2107 | Ace International |      110 | Tom Snyder    |         | 
     2115 | Smithson Corp.    |      101 | Dan Roberts   |      12 | Chicago
     2101 | Jones Mfg.        |      106 | Sam Clark     |      11 | New York
     2112 | Zetacorp          |      108 | Larry Fitch   |      21 | Los Angeles
     2121 | QMA Assoc.        |      103 | Paul Cruz     |      12 | Chicago
     2114 | Orion Corp        |      102 | Sue Smith     |      21 | Los Angeles
     2124 | Peter Brothers    |      107 | Nancy Angelli |      22 | Denver
     2108 | Holm & Landis     |      109 | Mary Jones    |      11 | New York
     2117 | J.P. Sinclair     |      106 | Sam Clark     |      11 | New York
     2122 | Three-Way Lines   |      105 | Bill Adams    |      13 | Atlanta
     2120 | Rico Enterprises  |      102 | Sue Smith     |      21 | Los Angeles
     2106 | Fred Lewis Corp.  |      102 | Sue Smith     |      21 | Los Angeles
     2119 | Solomon Inc.      |      109 | Mary Jones    |      11 | New York
     2118 | Midwest Systems   |      108 | Larry Fitch   |      21 | Los Angeles
     2113 | Ian & Schmidt     |      104 | Bob Smith     |      12 | Chicago
     2109 | Chen Associates   |      103 | Paul Cruz     |      12 | Chicago
     2105 | AAA Investments   |      101 | Dan Roberts   |      12 | Chicago
(21 rows)


--3. Llistar aquelles comandes que tinguin associat un producte i tambÃ© tinguin associat un representant de vendes. Mostrar l'identificador --i l'import de la comanda, el nom del representant de vendes i la descripciÃ³ del producte.


training=# select num_pedido,nombre,descripcion from pedidos join repventas on rep = num_empl join productos on id_fab = fab and id_producto = producto;
 num_pedido |    nombre     |    descripcion    
------------+---------------+-------------------
     112961 | Sam Clark     | Bisagra Izqda.
     113012 | Bill Adams    | Articulo Tipo 3
     112989 | Sam Clark     | Bancada Motor
     112968 | Dan Roberts   | Articulo Tipo 4
     110036 | Tom Snyder    | Montador
     113045 | Larry Fitch   | Bisagra Dcha.
     112963 | Bill Adams    | Articulo Tipo 4
     113013 | Larry Fitch   | Manivela
     113058 | Mary Jones    | Cubierta
     112997 | Nancy Angelli | Manivela
     112983 | Bill Adams    | Articulo Tipo 4
     113024 | Larry Fitch   | Reductor
     113062 | Nancy Angelli | Bancada Motor
     112979 | Sue Smith     | Montador
     113027 | Bill Adams    | Articulo Tipo 2
     113007 | Larry Fitch   | Riostra 1/2-Tm
     113069 | Nancy Angelli | Riostra 1-Tm
     113034 | Tom Snyder    | V Stago Trinquete
     112992 | Larry Fitch   | Articulo Tipo 2
     112975 | Paul Cruz     | Pasador Bisagra
     113055 | Dan Roberts   | Ajustador
     113048 | Sue Smith     | Riostra 2-Tm
     112993 | Sue Smith     | V Stago Trinquete
     113065 | Sue Smith     | Reductor
     113003 | Mary Jones    | Riostra 2-Tm
     113049 | Larry Fitch   | Reductor
     112987 | Bill Adams    | Extractor
     113057 | Paul Cruz     | Ajustador
     113042 | Dan Roberts   | Bisagra Dcha.
(29 rows)


--4. Llistar aquells representants de vendes que tinguin assignada una oficina. Mostrar l'identificador del representant de vendes, la ciutat de l'oficina i, en cas que l'oficina tingui director, el nom del director de l'oficina. 
SELECT empl.num_empl,                                                          



--5. Llistar els empleats que tinguin un director assignat. Mostrar el nom de l'empleat, el nom del director de l'empleat com a "dirempl". --En cas que l'empleat tingui assignada una oficina,mostrar tambÃ© la ciutat de l'oficina en la que treballa l'empleat. Si l'oficina te assignat un director mostrar tambÃ© el nom del director de l'oficina com a "dirofi".



--6. Llistar les comandes que tinguin un import superiora 30000 i tambÃ©
--llistar les comandes de productes amb un preu superior a 1000. Mostrar l'identificador i la data de la comanda, ladescripciÃ³ i les existÃ¨ncies del producte.



--7.  Llistar tots els client amb un lÃ­mit de crÃ¨dit superior o igual a 50000. Mostrar el nom de l'empresa client, el lÃ­mit de crÃ¨dit i el --nom del representant de vendes assignat al client, en cas de tenir-ne.



--8. Llistar els oficines que tenen un director amb una quota entre 300000 i 400000. Mostrar la ciutat de l'oficina, el nom i la quota del --director.


