
1- Mostrar, de totes les comandes, l'import, el nom del client, el nom del venedor i el nom del producte '.

select importe,empresa,nombre,descripcion form pedidos join clientes on clie=num_clie 
join repventas on num_empl=rep join productos on fab=id_fab and producto=id_producto

2- Mostrar les comandes dels clients que tenen un limit de crèdit superior a 50000 i que tenen un nom que comença amb 'S' o acaba abm 's. Mostrar l'import de la comanda, el nom del client, el nom del venedor i el nom del producte. Mostrar els resultats ordenats per import de comanda.


training=# select importe,empresa,nombre,descripcion from pedidos join clientes on clie=num_clie 
join repventas on num_empl=rep join productos on fab=id_fab and producto=id_producto where limite_credito > 50000 
and (empresa like 'S%' or empresa like '%s') order by importe;
 importe |     empresa     |   nombre    |   descripcion   
---------+-----------------+-------------+-----------------
  150.00 | Holm & Landis   | Dan Roberts | Ajustador
  652.00 | Midwest Systems | Larry Fitch | Manivela
  760.00 | Midwest Systems | Larry Fitch | Articulo Tipo 2
  776.00 | Midwest Systems | Larry Fitch | Reductor
 1480.00 | Holm & Landis   | Mary Jones  | Cubierta
 5625.00 | Holm & Landis   | Mary Jones  | Riostra 2-Tm
(6 rows)



3- Mostrar el codi, el nom, el codi i la ciutat de l'oficina de cada representant de ventes que té director i el codi i el nom del seu director'.

training=# select repventas.num_empl,repventas.nombre,oficinas.oficina,oficinas.ciudad,dir_repventas.num_empl,dir_repventas.nombre from repventas join oficinas on repventas.oficina_rep = oficinas.oficina join repventas dir_repventas on repventas.director = dir_repventas.num_empl; 
 num_empl |    nombre     | oficina |   ciudad    | num_empl |   nombre    
----------+---------------+---------+-------------+----------+-------------
      108 | Larry Fitch   |      21 | Los Angeles |      106 | Sam Clark
      104 | Bob Smith     |      12 | Chicago     |      106 | Sam Clark
      109 | Mary Jones    |      11 | New York    |      106 | Sam Clark
      103 | Paul Cruz     |      12 | Chicago     |      104 | Bob Smith
      101 | Dan Roberts   |      12 | Chicago     |      104 | Bob Smith
      105 | Bill Adams    |      13 | Atlanta     |      104 | Bob Smith
      107 | Nancy Angelli |      22 | Denver      |      108 | Larry Fitch
      102 | Sue Smith     |      21 | Los Angeles |      108 | Larry Fitch
(8 rows)

						or
						
						
training=# select repventas.num_empl,repventas.nombre,oficinas.oficina,oficinas.ciudad,dir_repventas.num_empl,dir_repventas.nombre from repventas left join oficinas on repventas.oficina_rep = oficinas.oficina left join repventas dir_repventas on repventas.director = dir_repventas.num_empl; 
 num_empl |    nombre     | oficina |   ciudad    | num_empl |   nombre    
----------+---------------+---------+-------------+----------+-------------
      105 | Bill Adams    |      13 | Atlanta     |      104 | Bob Smith
      109 | Mary Jones    |      11 | New York    |      106 | Sam Clark
      102 | Sue Smith     |      21 | Los Angeles |      108 | Larry Fitch
      106 | Sam Clark     |      11 | New York    |          | 
      104 | Bob Smith     |      12 | Chicago     |      106 | Sam Clark
      101 | Dan Roberts   |      12 | Chicago     |      104 | Bob Smith
      110 | Tom Snyder    |         |             |      101 | Dan Roberts
      108 | Larry Fitch   |      21 | Los Angeles |      106 | Sam Clark
      103 | Paul Cruz     |      12 | Chicago     |      104 | Bob Smith
      107 | Nancy Angelli |      22 | Denver      |      108 | Larry Fitch
(10 rows)


4- Mostrar el codi i els noms dels clients que tenen representants de ventes de les oficines de New York i Chicago i que tenen un nom d'empresa amb dues 'i'(minúscules o majúscules) o dues 'e'(minúscules o majúscules). Mostrar també el nom dels representants de ventes i la ciutat de l'oficina.

training=# select num_clie,empresa,nombre,ciudad from clientes join repventas on rep_clie = num_empl join oficinas on oficina_rep = oficina
training-# where (ciudad = 'New York' or ciudad = 'Chicago') and (empresa ilike '%e%e%' or empresa ilike '%i%i%');
 num_clie |     empresa     |   nombre    |  ciudad  
----------+-----------------+-------------+----------
     2117 | J.P. Sinclair   | Sam Clark   | New York
     2113 | Ian & Schmidt   | Bob Smith   | Chicago
     2109 | Chen Associates | Paul Cruz   | Chicago
     2105 | AAA Investments | Dan Roberts | Chicago
(4 rows)

training=# select num_clie,empresa,nombre,ciudad from clientes join repventas on rep_clie = num_empl join oficinas on oficina_rep = oficina
where (ciudad = 'New York' or ciudad = 'Chicago') and (empresa ilike '%e%' or empresa ilike '%i%');
 num_clie |     empresa     |   nombre    |  ciudad  
----------+-----------------+-------------+----------
     2111 | JCP Inc.        | Paul Cruz   | Chicago
     2102 | First Corp.     | Dan Roberts | Chicago
     2115 | Smithson Corp.  | Dan Roberts | Chicago
     2101 | Jones Mfg.      | Sam Clark   | New York
     2108 | Holm & Landis   | Mary Jones  | New York
     2117 | J.P. Sinclair   | Sam Clark   | New York
     2119 | Solomon Inc.    | Mary Jones  | New York
     2113 | Ian & Schmidt   | Bob Smith   | Chicago
     2109 | Chen Associates | Paul Cruz   | Chicago
     2105 | AAA Investments | Dan Roberts | Chicago
(10 rows)




5- Mostrar els noms dels representants de ventes, la ciutat de la seva oficina i el nom del director de la seva oficina.

training=# select treballador.nombre,ciudad,capoficina.nombre from repventas treballador join oficinas on oficina_rep = oficina join repventas capoficina on dir = capoficina.num_empl;
    nombre     |   ciudad    |   nombre    
---------------+-------------+-------------
 Bill Adams    | Atlanta     | Bill Adams
 Sam Clark     | New York    | Sam Clark
 Mary Jones    | New York    | Sam Clark
 Paul Cruz     | Chicago     | Bob Smith
 Dan Roberts   | Chicago     | Bob Smith
 Bob Smith     | Chicago     | Bob Smith
 Nancy Angelli | Denver      | Larry Fitch
 Larry Fitch   | Los Angeles | Larry Fitch
 Sue Smith     | Los Angeles | Larry Fitch
(9 rows)


6- Mostrar els noms dels representants de ventes, la ciutat de la seva oficina, el nom del director de la seva oficina i la ciutat de l'oficina d'aquest director.

training=# select treballador.nombre,oficinas.ciudad,capoficina.nombre,oficina_capoficina.ciudad from repventas treballador join oficinas on oficina_rep = oficina join repventas capoficina on dir = capoficina.num_empl join oficinas oficina_capoficina on capoficina.oficina_rep = oficina_capoficina.oficina;
    nombre     |   ciudad    |   nombre    |   ciudad    
---------------+-------------+-------------+-------------
 Bill Adams    | Atlanta     | Bill Adams  | Atlanta
 Mary Jones    | New York    | Sam Clark   | New York
 Sue Smith     | Los Angeles | Larry Fitch | Los Angeles
 Sam Clark     | New York    | Sam Clark   | New York
 Bob Smith     | Chicago     | Bob Smith   | Chicago
 Dan Roberts   | Chicago     | Bob Smith   | Chicago
 Larry Fitch   | Los Angeles | Larry Fitch | Los Angeles
 Paul Cruz     | Chicago     | Bob Smith   | Chicago
 Nancy Angelli | Denver      | Larry Fitch | Los Angeles
(9 rows)


7- Mostrar el codi d'oficina, la ciutat i el total de ventes de cada oficina segons l'import de ventes de les comandes dels seus representants.

training=# select oficina,ciudad,sum(importe) from oficinas join repventas on oficina_rep = oficina join pedidos on num_empl=rep group by oficina;
 oficina |   ciudad    |   sum    
---------+-------------+----------
      11 | New York    | 40063.00
      13 | Atlanta     | 39327.00
      12 | Chicago     | 29328.00
      22 | Denver      | 34432.00
      21 | Los Angeles | 81409.00
(5 rows)


8- Mostrar les comandes que els clients han fet a algun representant que no és el que tenen assignat. Mostrar l'import de comanda, el nom del client, el nom del representant associant al client, el nom del representant que ha fet la comanda i el nom del producte'.

training=# select pedidos.num_pedido,clientes.empresa,repventas_pedidos.nombre as rep_pedido,repventas_clientes.nombre as rep_clientes from pedidos,clientes,repventas repventas_pedidos,repventas repventas_clientes where pedidos.clie = clientes.num_clie  and repventas_pedidos.num_empl = pedidos.rep and repventas_clientes.num_empl = clientes.rep_clie and pedidos.rep != clientes.rep_clie ;
 num_pedido |     empresa     |  rep_pedido   | rep_clientes 
------------+-----------------+---------------+--------------
     113012 | JCP Inc.        | Bill Adams    | Paul Cruz
     113024 | Orion Corp      | Larry Fitch   | Sue Smith
     113069 | Chen Associates | Nancy Angelli | Paul Cruz
     113055 | Holm & Landis   | Dan Roberts   | Mary Jones
     113042 | Ian & Schmidt   | Dan Roberts   | Bob Smith
(5 rows)

9- Mostrar el codi, el nom, el codi i la ciutat de l'oficina de cada representant de ventes que té director i el codi i el nom del seu director i el codi i la ciutat de l'oficina del seu director



10 - Mostrar els nom dels 3 representants de ventes que han venut més segons l'import de totes les comandes que han fet. Mostrar també l'import que ha venut cadascú.



