---1.­ Llistar tots els empleats que han estat contractats abans que els seus directors. Mostrar el nom de
---l'empleat com a "empl", la data del contracte del l'empleat com a "empl_fecha", el nom del director
---com a "dir" i la data de contracte del director com a "dir_fecha".




select nombre as "empl",contrato as "empl_fecha",director as "dir", 


training=# select repventas.nombre as "empl",repventas.contrato as "empl_fecha",dir.nombre as "nom de director",dir.contrato from repventas join repventas dir on repventas.director = dir.num_empl where repventas.contrato > dir.contrato;
    empl     | empl_fecha | nom de director |  contrato  
-------------+------------+-----------------+------------
 Bill Adams  | 1988-02-12 | Bob Smith       | 1987-05-19
 Mary Jones  | 1989-10-12 | Sam Clark       | 1988-06-14
 Tom Snyder  | 1990-01-13 | Dan Roberts     | 1986-10-20
 Larry Fitch | 1989-10-12 | Sam Clark       | 1988-06-14
(4 rows)



---2. Llistar les comandes dels clients els quals la segona lletra del nom de l'empresa sigui la lletra
---"c" sense tenir en compte majúscules i minúscules. Mostrar la data de la comanda i el nom de
---l'empresa.

select empresa,fecha_pedido from clientes join pedidos on num_clie=clie where empresa ilike '_c%';
      empresa      | fecha_pedido 
-------------------+--------------
 JCP Inc.          | 1990-01-11
 Ace International | 1990-01-30
 Acme Mfg.         | 1989-12-17
 Acme Mfg.         | 1989-12-27
 Acme Mfg.         | 1990-01-22
 Ace International | 1990-01-29
 JCP Inc.          | 1989-12-12
 Acme Mfg.         | 1989-12-31
 JCP Inc.          | 1990-02-18
(9 filas)

----3. Mostrar l'identificador i en nom de l'empresa dels clients. Només mostrar aquells clients que la suma dels imports de les seves comandes 
----sigui menor al limit de crèdit.

select num_clie,empresa,sum(importe),limite_credito
from clientes join pedidos on num_clie=clie 
where  
importe < limite_credito group by num_clie order by num_clie;

 num_clie |      empresa      |   sum    | limite_credito 
----------+-------------------+----------+----------------
     2101 | Jones Mfg.        |  1458.00 |       65000.00
     2102 | First Corp.       |  3978.00 |       65000.00
     2103 | Acme Mfg.         | 35582.00 |       50000.00
     2106 | Fred Lewis Corp.  |  4026.00 |       65000.00
     2107 | Ace International | 23132.00 |       35000.00
     2108 | Holm & Landis     |  7255.00 |       55000.00
     2111 | JCP Inc.          |  6445.00 |       50000.00
     2112 | Zetacorp          | 47925.00 |       50000.00
     2114 | Orion Corp        | 22100.00 |       20000.00
     2117 | J.P. Sinclair     | 31500.00 |       35000.00
     2118 | Midwest Systems   |  3608.00 |       60000.00
     2120 | Rico Enterprises  |  3750.00 |       50000.00
     2124 | Peter Brothers    |  3082.00 |       40000.00
(13 filas)


-----4. Mostrar lidentificador dels clients i un camp anomenat "pedidos". El camp "pedidos" ha de mostrar quantes comandes ha fet cada client.

training=# select num_clie,count(num_pedido) as "pedidos" from clientes join pedidos on clie = num_clie group by num_clie;
 num_clie | pedidos 
----------+---------
     2102 |       1
     2117 |       1
     2112 |       2
     2118 |       4
     2120 |       1
     2106 |       2
     2108 |       3
     2113 |       1
     2107 |       2
     2124 |       2
     2109 |       1
     2111 |       3
     2101 |       1
     2114 |       2
     2103 |       4
(15 rows)


--- 5. Mostrar l'identificador del venedor i un camp anomenat "precio0". El camp "precio0" ha de contenir el preu del producte més car que ha venut.

training=# select num_empl,max(importe/cant) from pedidos join repventas on rep = num_empl group by num_empl;
 num_empl |          max          
----------+-----------------------
      102 | 2500.0000000000000000
      106 | 4500.0000000000000000
      107 | 1425.0000000000000000
      108 | 4500.0000000000000000
      105 | 2500.0000000000000000
      109 | 1875.0000000000000000
      103 |  350.0000000000000000
      101 | 4500.0000000000000000
      110 | 2500.0000000000000000
(9 rows)


--- 6. Mostrar l'identificador i nom dels directors d'empleats i un camp anomenat "empleados". Només han d'aparèixer al llistat els directors d'empleats que tenen empleats al seu càrrec. El camp "empleados" ha de mostrar el nombre d'empleats que té assignat cada director d'empleats.

training=# select repventas.num_empl as "empleados",dir.nombre as "nom de director",dir.num_empl as "identificador de director"from repventas join repventas dir on repventas.director = dir.num_empl order by 3;
 empleados | nom de director | identificador de director 
-----------+-----------------+---------------------------
       110 | Dan Roberts     |                       101
       105 | Bob Smith       |                       104
       101 | Bob Smith       |                       104
       103 | Bob Smith       |                       104
       109 | Sam Clark       |                       106
       108 | Sam Clark       |                       106
       104 | Sam Clark       |                       106
       107 | Larry Fitch     |                       108
       102 | Larry Fitch     |                       108
(9 rows)


----7. Mostrar l'identificador i la ciutat de les oficines i dos camps més, un anomenat "credito1" i l'altre "credito2". Per a cada oficina, el camp "credito1" ha de mostrar el límit de crèdit més petit d'entre tots els clients que el seu representant de vendes treballa a l'oficina. El camp "credito2" ha de ser el mateix però pel límit de crèdit més gran.

training=# select oficina,ciudad,min(limite_credito),max(limite_credito) from oficinas join repventas on dir = num_empl join clientes on rep_clie = num_empl group by oficina;
 oficina |   ciudad    |   min    |   max    
---------+-------------+----------+----------
      11 | New York    | 35000.00 | 65000.00
      13 | Atlanta     | 30000.00 | 50000.00
      12 | Chicago     | 20000.00 | 20000.00
      22 | Denver      | 50000.00 | 60000.00
      21 | Los Angeles | 50000.00 | 60000.00
(5 rows)


---8. Mostrar l'identificador del venedor i l'identificador del client amb un camp anomenat "importe_m". El camp "importe_m" ha de mostrar l'import mig de les comandes que ha realitzat cada venedor a cada client diferent.

training=# select num_empl,num_clie,avg(importe) from repventas join clientes on rep_clie = num_empl join pedidos on rep = num_empl group by num_empl,num_clie order by 1,2;
 num_empl | num_clie |          avg           
----------+----------+------------------------
      101 |     2102 |  8876.0000000000000000
      101 |     2105 |  8876.0000000000000000
      101 |     2115 |  8876.0000000000000000
      102 |     2106 |  5694.0000000000000000
      102 |     2114 |  5694.0000000000000000
      102 |     2120 |  5694.0000000000000000
      102 |     2123 |  5694.0000000000000000
      103 |     2109 |  1350.0000000000000000
      103 |     2111 |  1350.0000000000000000
      103 |     2121 |  1350.0000000000000000
      105 |     2103 |  7865.4000000000000000
      105 |     2122 |  7865.4000000000000000
      106 |     2101 |     16479.000000000000
      106 |     2117 |     16479.000000000000
      107 |     2124 | 11477.3333333333333333
      108 |     2112 |  8376.1428571428571429
      108 |     2118 |  8376.1428571428571429
      109 |     2108 |  3552.5000000000000000
      109 |     2119 |  3552.5000000000000000
      110 |     2107 | 11566.0000000000000000
(20 rows)

