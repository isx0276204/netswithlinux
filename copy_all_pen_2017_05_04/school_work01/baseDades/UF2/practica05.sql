--- SEL·LECCIONAR DADES DE MÉS D'UNA TAULA
	Taula repventas : 10 registres;

	Taula oficinas :  5 registres

	Producte cartesià : tots amb tots 50 registres

	SELECT  num_empl, nombre, oficina_rep, oficina, ciudad
	FROM repventas, oficinas
	ORDER BY oficina ;	
 num_empl |    nombre     | oficina_rep | oficina |   ciudad    
----------+---------------+-------------+---------+------------- 
      105 | Bill Adams    |          		13 |      11 | New York 
      109 | Mary Jones    |       	  	11 |      11 | New York 
      102 | Sue Smith     |        		21 |      11 | New York 
      106 | Sam Clark     |          		11 |      11 | New York 
      104 | Bob Smith     |          		12 |      11 | New York 
      101 | Dan Roberts   |       	  	12 |      11 | New York 
      110 | Tom Snyder    |        			11 | New York 
      108 | Larry Fitch   |        		21 |      11 | New York 
      103 | Paul Cruz     |        		12 |      11 | New York 
      107 | Nancy Angelli |     		22 |      11 | New York 
      105 | Bill Adams    |    		13 |      12 | Chicago 
      109 | Mary Jones    |      		11 |      12 | Chicago 
      102 | Sue Smith     |     		21 |      12 | Chicago 
      106 | Sam Clark     |       		11 |      12 | Chicago 
      104 | Bob Smith     |        		12 |      12 | Chicago 
      101 | Dan Roberts   |      		12 |      12 | Chicago 
      110 | Tom Snyder    |       		     |      12 | Chicago 
      108 | Larry Fitch   |   		21 |      12 | Chicago 
      103 | Paul Cruz     |        		12 |      12 | Chicago 
      107 | Nancy Angelli |      		22 |      12 | Chicago 
      105 | Bill Adams    |      		13 |      13 | Atlanta 
      109 | Mary Jones    |          11 |      13 | Atlanta 
      102 | Sue Smith     |          21 |      13 | Atlanta 
      106 | Sam Clark     |          11 |      13 | Atlanta 
      104 | Bob Smith     |          12 |      13 | Atlanta 
      101 | Dan Roberts   |          12 |      13 | Atlanta 
      110 | Tom Snyder    |             |      13 | Atlanta 
      108 | Larry Fitch   |          21 |      13 | Atlanta 
      103 | Paul Cruz     |          12 |      13 | Atlanta 
      107 | Nancy Angelli |          22 |      13 | Atlanta 
      105 | Bill Adams    |          13 |      21 | Los Angeles 
      109 | Mary Jones    |          11 |      21 | Los Angeles 
      102 | Sue Smith     |          21 |      21 | Los Angeles 
      106 | Sam Clark     |          11 |      21 | Los Angeles 
      104 | Bob Smith     |          12 |      21 | Los Angeles 
      101 | Dan Roberts   |          12 |      21 | Los Angeles 
      110 | Tom Snyder    |             |      21 | Los Angeles 
      108 | Larry Fitch   |          21 |      21 | Los Angeles 
      103 | Paul Cruz     |          12 |      21 | Los Angeles 
      107 | Nancy Angelli |          22 |      21 | Los Angeles 
      105 | Bill Adams    |          13 |      22 | Denver 
      109 | Mary Jones    |          11 |      22 | Denver 
      102 | Sue Smith     |          21 |      22 | Denver 
      106 | Sam Clark     |          11 |      22 | Denver 
      104 | Bob Smith     |          12 |      22 | Denver 
      101 | Dan Roberts   |          12 |      22 | Denver 
      110 | Tom Snyder    |             |      22 | Denver 
      108 | Larry Fitch   |          21 |      22 | Denver 
      103 | Paul Cruz     |          12 |      22 | Denver 
      107 | Nancy Angelli |          22 |      22 | Denver 
(50 rows) 

	Producte cartesià : tots amb tots
	SELECT  num_empl, nombre, oficina_rep, oficina, ciudad
	FROM repventas, oficinas
	WHERE oficina_rep=12
	ORDER BY oficina ;	

	 num_empl |   nombre    | oficina_rep | oficina |   ciudad    
	----------+-------------+-------------+---------+------------- 
      104 | Bob Smith   |        		12 |      11 | New York 
      101 | Dan Roberts |       		12 |      11 | New York 
      103 | Paul Cruz   |         		12 |      11 | New York 
      104 | Bob Smith   |       	   	12 |      12 | Chicago 
      101 | Dan Roberts |         		12 |      12 | Chicago 
      103 | Paul Cruz   |          		12 |      12 | Chicago 
      104 | Bob Smith   |          		12 |      13 | Atlanta 
      101 | Dan Roberts |          	12 |      13 | Atlanta 
      103 | Paul Cruz   |          		12 |      13 | Atlanta 
      104 | Bob Smith   |          		12 |      21 | Los Angeles 
      101 | Dan Roberts |          	12 |      21 | Los Angeles 
      103 | Paul Cruz   |          		12 |      21 | Los Angeles 
     	      104 | Bob Smith   |          		12 |      22 | Denver 
                  101 | Dan Roberts |          	12 |      22 | Denver 
                  103 | Paul Cruz   |         		12 |      22 | Denver 
   (15 rows) 



	SELECT  num_empl, nombre, oficina_rep, oficina, ciudad
	FROM repventas, oficinas
WHERE oficina_rep = oficina
ORDER BY oficina ;

 num_empl |    nombre     | oficina_rep | oficina |   ciudad    
----------+---------------+-------------+---------+------------- 
      109 | Mary Jones    |         	 11 |      11 | New York 
      106 | Sam Clark     |       		 11 |      11 | New York 
      101 | Dan Roberts   |         	 12 |      12 | Chicago 
      103 | Paul Cruz     |         		 12 |      12 | Chicago 
      104 | Bob Smith     |         		 12 |      12 | Chicago 
      105 | Bill Adams    |         		 13 |      13 | Atlanta 
      108 | Larry Fitch   |          	 21 |      21 | Los Angeles 
      102 | Sue Smith     |        		 21 |      21 | Los Angeles 
      107 | Nancy Angelli |       		 22 |      22 | Denver 
(9 rows) 
---- 5.1- Llista la ciutat de les oficines, i el nom i títol dels directors de cada oficina.


training=# select oficinas.ciudad,repventas.titulo,repventas.nombre from oficinas,repventas where oficinas.dir = repventas.num_empl;
   ciudad    |   titulo   |   nombre    
-------------+------------+-------------
 Atlanta     | Rep Ventas | Bill Adams
 New York    | VP Ventas  | Sam Clark
 Chicago     | Dir Ventas | Bob Smith
 Los Angeles | Dir Ventas | Larry Fitch
 Denver      | Dir Ventas | Larry Fitch
(5 rows)

						or
						
training=# select oficinas.ciudad,repventas.titulo,repventas.nombre from oficinas join repventas on oficinas.dir = repventas.num_empl;
   ciudad    |   titulo   |   nombre    
-------------+------------+-------------
 Atlanta     | Rep Ventas | Bill Adams
 New York    | VP Ventas  | Sam Clark
 Chicago     | Dir Ventas | Bob Smith
 Los Angeles | Dir Ventas | Larry Fitch
 Denver      | Dir Ventas | Larry Fitch
(5 rows)


-- 5.2- Llista totes les comandes mostrant el seu número, import, número de client i límit de crèdit.

training=# select pedidos.num_pedido,pedidos.importe,pedidos.clie,clientes.limite_credito from pedidos,clientes where clie = num_clie;
 num_pedido | importe  | clie | limite_credito 
------------+----------+------+----------------
     112961 | 31500.00 | 2117 |       35000.00
     113012 |  3745.00 | 2111 |       50000.00
     112989 |  1458.00 | 2101 |       65000.00
     113051 |  1420.00 | 2118 |       60000.00
     112968 |  3978.00 | 2102 |       65000.00
     110036 | 22500.00 | 2107 |       35000.00
     113045 | 45000.00 | 2112 |       50000.00
     112963 |  3276.00 | 2103 |       50000.00
     113013 |   652.00 | 2118 |       60000.00
     113058 |  1480.00 | 2108 |       55000.00
     112997 |   652.00 | 2124 |       40000.00
     112983 |   702.00 | 2103 |       50000.00
     113024 |  7100.00 | 2114 |       20000.00
     113062 |  2430.00 | 2124 |       40000.00
     112979 | 15000.00 | 2114 |       20000.00
     113027 |  4104.00 | 2103 |       50000.00
     113007 |  2925.00 | 2112 |       50000.00
     113069 | 31350.00 | 2109 |       25000.00
     113034 |   632.00 | 2107 |       35000.00
     112992 |   760.00 | 2118 |       60000.00
     112975 |  2100.00 | 2111 |       50000.00
     113055 |   150.00 | 2108 |       55000.00
     113048 |  3750.00 | 2120 |       50000.00
     112993 |  1896.00 | 2106 |       65000.00
     113065 |  2130.00 | 2106 |       65000.00
     113003 |  5625.00 | 2108 |       55000.00
     113049 |   776.00 | 2118 |       60000.00
     112987 | 27500.00 | 2103 |       50000.00
     113057 |   600.00 | 2111 |       50000.00
     113042 | 22500.00 | 2113 |       20000.00
(30 rows)

					or
	
training=# select pedidos.num_pedido,pedidos.importe,pedidos.clie,clientes.limite_credito from pedidos join clientes on clie = num_clie;
 num_pedido | importe  | clie | limite_credito 
------------+----------+------+----------------
     112961 | 31500.00 | 2117 |       35000.00
     113012 |  3745.00 | 2111 |       50000.00
     112989 |  1458.00 | 2101 |       65000.00
     113051 |  1420.00 | 2118 |       60000.00
     112968 |  3978.00 | 2102 |       65000.00
     110036 | 22500.00 | 2107 |       35000.00
     113045 | 45000.00 | 2112 |       50000.00
     112963 |  3276.00 | 2103 |       50000.00
     113013 |   652.00 | 2118 |       60000.00
     113058 |  1480.00 | 2108 |       55000.00
     112997 |   652.00 | 2124 |       40000.00
     112983 |   702.00 | 2103 |       50000.00
     113024 |  7100.00 | 2114 |       20000.00
     113062 |  2430.00 | 2124 |       40000.00
     112979 | 15000.00 | 2114 |       20000.00
     113027 |  4104.00 | 2103 |       50000.00
     113007 |  2925.00 | 2112 |       50000.00
     113069 | 31350.00 | 2109 |       25000.00
     113034 |   632.00 | 2107 |       35000.00
     112992 |   760.00 | 2118 |       60000.00
     112975 |  2100.00 | 2111 |       50000.00
     113055 |   150.00 | 2108 |       55000.00
     113048 |  3750.00 | 2120 |       50000.00
     112993 |  1896.00 | 2106 |       65000.00
     113065 |  2130.00 | 2106 |       65000.00
     113003 |  5625.00 | 2108 |       55000.00
     113049 |   776.00 | 2118 |       60000.00
     112987 | 27500.00 | 2103 |       50000.00
     113057 |   600.00 | 2111 |       50000.00
     113042 | 22500.00 | 2113 |       20000.00
(30 rows)


-- 5.3- Llista el número de totes les comandes amb la descripció del producte demanat.

training=# select pedidos.num_pedido,productos.descripcion from pedidos,productos where fab = id_fab and producto = id_producto;
 num_pedido |    descripcion    
------------+-------------------
     112961 | Bisagra Izqda.
     113012 | Articulo Tipo 3
     112989 | Bancada Motor
     112968 | Articulo Tipo 4
     110036 | Montador
     113045 | Bisagra Dcha.
     112963 | Articulo Tipo 4
     113013 | Manivela
     113058 | Cubierta
     112997 | Manivela
     112983 | Articulo Tipo 4
     113024 | Reductor
     113062 | Bancada Motor
     112979 | Montador
     113027 | Articulo Tipo 2
     113007 | Riostra 1/2-Tm
     113069 | Riostra 1-Tm
     113034 | V Stago Trinquete
     112992 | Articulo Tipo 2
     112975 | Pasador Bisagra
     113055 | Ajustador
     113048 | Riostra 2-Tm
     112993 | V Stago Trinquete
     113065 | Reductor
     113003 | Riostra 2-Tm
     113049 | Reductor
     112987 | Extractor
     113057 | Ajustador
     113042 | Bisagra Dcha.
(29 rows)

					or
					
training=# select pedidos.num_pedido,productos.descripcion from pedidos join productos on fab = id_fab and producto = id_producto;
 num_pedido |    descripcion    
------------+-------------------
     112961 | Bisagra Izqda.
     113012 | Articulo Tipo 3
     112989 | Bancada Motor
     112968 | Articulo Tipo 4
     110036 | Montador
     113045 | Bisagra Dcha.
     112963 | Articulo Tipo 4
     113013 | Manivela
     113058 | Cubierta
     112997 | Manivela
     112983 | Articulo Tipo 4
     113024 | Reductor
     113062 | Bancada Motor
     112979 | Montador
     113027 | Articulo Tipo 2
     113007 | Riostra 1/2-Tm
     113069 | Riostra 1-Tm
     113034 | V Stago Trinquete
     112992 | Articulo Tipo 2
     112975 | Pasador Bisagra
     113055 | Ajustador
     113048 | Riostra 2-Tm
     112993 | V Stago Trinquete
     113065 | Reductor
     113003 | Riostra 2-Tm
     113049 | Reductor
     112987 | Extractor
     113057 | Ajustador
     113042 | Bisagra Dcha.
(29 rows)


-- 5.4- Llista el nom de tots els clients amb el nom del representant de vendes assignat.

training=# select clientes.empresa,repventas.nombre from clientes,repventas where clientes.rep_clie = repventas.num_empl;
      empresa      |    nombre     
-------------------+---------------
 JCP Inc.          | Paul Cruz
 First Corp.       | Dan Roberts
 Acme Mfg.         | Bill Adams
 Carter & Sons     | Sue Smith
 Ace International | Tom Snyder
 Smithson Corp.    | Dan Roberts
 Jones Mfg.        | Sam Clark
 Zetacorp          | Larry Fitch
 QMA Assoc.        | Paul Cruz
 Orion Corp        | Sue Smith
 Peter Brothers    | Nancy Angelli
 Holm & Landis     | Mary Jones
 J.P. Sinclair     | Sam Clark
 Three-Way Lines   | Bill Adams
 Rico Enterprises  | Sue Smith
 Fred Lewis Corp.  | Sue Smith
 Solomon Inc.      | Mary Jones
 Midwest Systems   | Larry Fitch
 Ian & Schmidt     | Bob Smith
 Chen Associates   | Paul Cruz
 AAA Investments   | Dan Roberts
(21 rows)

							or
							
training=# select clientes.empresa,repventas.nombre from clientes join repventas on clientes.rep_clie = repventas.num_empl;
      empresa      |    nombre     
-------------------+---------------
 JCP Inc.          | Paul Cruz
 First Corp.       | Dan Roberts
 Acme Mfg.         | Bill Adams
 Carter & Sons     | Sue Smith
 Ace International | Tom Snyder
 Smithson Corp.    | Dan Roberts
 Jones Mfg.        | Sam Clark
 Zetacorp          | Larry Fitch
 QMA Assoc.        | Paul Cruz
 Orion Corp        | Sue Smith
 Peter Brothers    | Nancy Angelli
 Holm & Landis     | Mary Jones
 J.P. Sinclair     | Sam Clark
 Three-Way Lines   | Bill Adams
 Rico Enterprises  | Sue Smith
 Fred Lewis Corp.  | Sue Smith
 Solomon Inc.      | Mary Jones
 Midwest Systems   | Larry Fitch
 Ian & Schmidt     | Bob Smith
 Chen Associates   | Paul Cruz
 AAA Investments   | Dan Roberts
(21 rows)


-- 5.5- Llista la data de totes les comandes amb el numero i nom del client de la comanda.

training=# select pedidos.fecha_pedido,clientes.empresa from pedidos,clientes where pedidos.clie = clientes.num_clie;
 fecha_pedido |      empresa      
--------------+-------------------
 1989-12-17   | J.P. Sinclair
 1990-01-11   | JCP Inc.
 1990-01-03   | Jones Mfg.
 1990-02-10   | Midwest Systems
 1989-10-12   | First Corp.
 1990-01-30   | Ace International
 1990-02-02   | Zetacorp
 1989-12-17   | Acme Mfg.
 1990-01-14   | Midwest Systems
 1990-02-23   | Holm & Landis
 1990-01-08   | Peter Brothers
 1989-12-27   | Acme Mfg.
 1990-01-20   | Orion Corp
 1990-02-24   | Peter Brothers
 1989-10-12   | Orion Corp
 1990-01-22   | Acme Mfg.
 1990-01-08   | Zetacorp
 1990-03-02   | Chen Associates
 1990-01-29   | Ace International
 1989-11-04   | Midwest Systems
 1989-12-12   | JCP Inc.
 1990-02-15   | Holm & Landis
 1990-02-10   | Rico Enterprises
 1989-01-04   | Fred Lewis Corp.
 1990-02-27   | Fred Lewis Corp.
 1990-01-25   | Holm & Landis
 1990-02-10   | Midwest Systems
 1989-12-31   | Acme Mfg.
 1990-02-18   | JCP Inc.
 1990-02-02   | Ian & Schmidt
(30 rows)



-- 5.6- Llista les oficines, noms i títols del seus directors amb un objectiu superior a 600.000.

training=# select oficinas.oficina,repventas.titulo,repventas.nombre,oficinas.objetivo from oficinas,repventas where repventas.num_empl = oficinas.dir and objetivo > 600000;
 oficina |   titulo   |   nombre    | objetivo  
---------+------------+-------------+-----------
      12 | Dir Ventas | Bob Smith   | 800000.00
      21 | Dir Ventas | Larry Fitch | 725000.00
(2 rows)

										or
										
training=# select oficinas.oficina,repventas.titulo,repventas.nombre,oficinas.objetivo from oficinas join repventas on repventas.num_empl = oficinas.dir where objetivo > 600000;
 oficina |   titulo   |   nombre    | objetivo  
---------+------------+-------------+-----------
      12 | Dir Ventas | Bob Smith   | 800000.00
      21 | Dir Ventas | Larry Fitch | 725000.00
(2 rows)


-- 5.7- Llista els venedors de les oficines de la regió est.

training=# select repventas.num_empl,repventas.nombre from repventas,oficinas where repventas.oficina_rep = oficinas.oficina and region = 'Este';
 num_empl |   nombre    
----------+-------------
      105 | Bill Adams
      109 | Mary Jones
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      103 | Paul Cruz
(6 rows)
							or

training=# select repventas.num_empl,repventas.nombre from repventas join oficinas on repventas.oficina_rep = oficinas.oficina where region = 'Este';
 num_empl |   nombre    
----------+-------------
      105 | Bill Adams
      109 | Mary Jones
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      103 | Paul Cruz
(6 rows)



-- 5.8- Llista les comandes superiors a 25000, incloent el nom del venedor que va servir la comanda i el nom del client que el va sol·licitar.

training=# select repventas.nombre,clientes.empresa from pedidos,repventas,clientes where pedidos.clie = clientes.num_clie and pedidos.rep = repventas.num_empl and pedidos.importe > 25000;
    nombre     |     empresa     
---------------+-----------------
 Bill Adams    | Acme Mfg.
 Larry Fitch   | Zetacorp
 Sam Clark     | J.P. Sinclair
 Nancy Angelli | Chen Associates
(4 rows)

									or
									
training=# select repventas.nombre,clientes.empresa from pedidos join clientes on pedidos.clie = clientes.num_clie join repventas on  pedidos.rep = repventas.num_empl where pedidos.importe > 25000;
    nombre     |     empresa     
---------------+-----------------
 Bill Adams    | Acme Mfg.
 Larry Fitch   | Zetacorp
 Sam Clark     | J.P. Sinclair
 Nancy Angelli | Chen Associates
(4 rows)


-- 5.9- Llista les comandes superiors a 25000, mostrant el client que va servir la comanda i el nom del venedor que té assignat el client.

training=# select pedidos.num_pedido,clientes.empresa,pedidos.importe,repventas.nombre from pedidos,clientes,repventas where pedidos.clie = clientes.num_clie and clientes.rep_clie = repventas.num_empl and pedidos.importe > 25000;
 num_pedido |     empresa     | importe  |   nombre    
------------+-----------------+----------+-------------
     112987 | Acme Mfg.       | 27500.00 | Bill Adams
     113045 | Zetacorp        | 45000.00 | Larry Fitch
     112961 | J.P. Sinclair   | 31500.00 | Sam Clark
     113069 | Chen Associates | 31350.00 | Paul Cruz
(4 rows)

							or
							
training=# select pedidos.num_pedido,clientes.empresa,pedidos.importe,repventas.nombre from pedidos join clientes on pedidos.clie = clientes.num_clie join repventas on clientes.rep_clie = repventas.num_empl where pedidos.importe > 25000;
 num_pedido |     empresa     | importe  |   nombre    
------------+-----------------+----------+-------------
     112987 | Acme Mfg.       | 27500.00 | Bill Adams
     113045 | Zetacorp        | 45000.00 | Larry Fitch
     112961 | J.P. Sinclair   | 31500.00 | Sam Clark
     113069 | Chen Associates | 31350.00 | Paul Cruz
(4 rows)





---- 5.10- Llista les comandes superiors a 25000, mostrant el nom del client que el va ordenar, el venedor associat al client, i l'oficina on el venedor treballa.

training=# select pedidos.num_pedido,clientes.empresa,pedidos.importe,repventas.nombre,oficinas.oficina from pedidos,clientes,repventas,oficinas where pedidos.clie = clientes.num_clie and clientes.rep_clie = repventas.num_empl and oficinas.oficina = repventas.oficina_rep and pedidos.importe > 25000;
 num_pedido |     empresa     | importe  |   nombre    | oficina 
------------+-----------------+----------+-------------+---------
     112961 | J.P. Sinclair   | 31500.00 | Sam Clark   |      11
     113045 | Zetacorp        | 45000.00 | Larry Fitch |      21
     113069 | Chen Associates | 31350.00 | Paul Cruz   |      12
     112987 | Acme Mfg.       | 27500.00 | Bill Adams  |      13
(4 rows)




-- 5.11- Llista totes les combinacions de venedors i oficines on la quota del venedor és superior a l'objectiu de l'oficina.

training=# select * from oficinas,repventas where oficinas.objetivo < repventas.cuota ;
 oficina | ciudad | region | dir | objetivo  |  ventas   | num_empl |   nombre    | edad | oficina_rep |   titulo   |  contrato  | director 
|   cuota   |  ventas   
---------+--------+--------+-----+-----------+-----------+----------+-------------+------+-------------+------------+------------+----------
+-----------+-----------
      22 | Denver | Oeste  | 108 | 300000.00 | 186042.00 |      105 | Bill Adams  |   37 |          13 | Rep Ventas | 1988-02-12 |      104 
| 350000.00 | 367911.00
      22 | Denver | Oeste  | 108 | 300000.00 | 186042.00 |      102 | Sue Smith   |   48 |          21 | Rep Ventas | 1986-12-10 |      108 
| 350000.00 | 474050.00
      22 | Denver | Oeste  | 108 | 300000.00 | 186042.00 |      108 | Larry Fitch |   62 |          21 | Dir Ventas | 1989-10-12 |      106 
| 350000.00 | 361865.00
(3 rows)


-- 5.12- Informa sobre tots els venedors i les oficines en les que treballen.

training=# select repventas.num_empl,oficinas.oficina from repventas,oficinas where repventas.oficina_rep = oficinas.oficina;
 num_empl | oficina 
----------+---------
      105 |      13
      109 |      11
      102 |      21
      106 |      11
      104 |      12
      101 |      12
      108 |      21
      103 |      12
      107 |      22
(9 rows)


-- 5.13- Llista els venedors amb una quota superior a la dels seus directors.

training=# select * from repventas,repventas dir where repventas.director = dir.num_empl and repventas.cuota > dir.cuota;
 num_empl |   nombre    | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   | num_empl |  nombre   | edad | of
icina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+-------------+------+-------------+------------+------------+----------+-----------+-----------+----------+-----------+------+---
----------+------------+------------+----------+-----------+-----------
      105 | Bill Adams  |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00 |      104 | Bob Smith |   33 |   
       12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00
      109 | Mary Jones  |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00 |      106 | Sam Clark |   52 |   
       11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00
      101 | Dan Roberts |   45 |          12 | Rep Ventas | 1986-10-20 |      104 | 300000.00 | 305673.00 |      104 | Bob Smith |   33 |   
       12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00
      108 | Larry Fitch |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00 |      106 | Sam Clark |   52 |   
       11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00
      103 | Paul Cruz   |   29 |          12 | Rep Ventas | 1987-03-01 |      104 | 275000.00 | 286775.00 |      104 | Bob Smith |   33 |   
       12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00
(5 rows)


-- 5.14- Llistar el nom de l'empresa i totes les comandes fetes pel client 2103.

training=# select clientes.empresa,pedidos.num_pedido from clientes,pedidos where pedidos.clie = clientes.num_clie and clientes.num_clie = '2103'; 
  empresa  | num_pedido 
-----------+------------
 Acme Mfg. |     112963
 Acme Mfg. |     112983
 Acme Mfg. |     113027
 Acme Mfg. |     112987
(4 rows)



-- 5.15- Llista aquelles comandes que el seu import sigui superior a 10000, mostrant el numero de comanda, els imports i les descripcions del producte.

training=# select pedidos.num_pedido,pedidos.importe,productos.descripcion from pedidos,productos where productos.id_fab = pedidos.fab and productos.id_producto = pedidos.producto and pedidos.importe > 10000;
 num_pedido | importe  |  descripcion   
------------+----------+----------------
     112987 | 27500.00 | Extractor
     112961 | 31500.00 | Bisagra Izqda.
     113069 | 31350.00 | Riostra 1-Tm
     112979 | 15000.00 | Montador
     110036 | 22500.00 | Montador
     113042 | 22500.00 | Bisagra Dcha.
     113045 | 45000.00 | Bisagra Dcha.
(7 rows)


-- 5.16- Llista les comandes superiors a 25000, mostrant el nom del client que la va demanar, el venedor associat al client, i l'oficina on el venedor treballa. També cal mostar la descripció del producte.

training=# select clientes.empresa,repventas.num_empl,repventas.nombre,oficinas.oficina,oficinas.ciudad,productos.descripcion from pedidos,clientes,repventas,oficinas,productos where pedidos.clie = clientes.num_clie and clientes.rep_clie = repventas.num_empl and repventas.oficina_rep = oficinas.oficina and productos.id_fab = pedidos.fab and productos.id_producto =pedidos.producto and pedidos.importe > 25000;
     empresa     | num_empl |   nombre    | oficina |   ciudad    |  descripcion   
-----------------+----------+-------------+---------+-------------+----------------
 Acme Mfg.       |      105 | Bill Adams  |      13 | Atlanta     | Extractor
 Zetacorp        |      108 | Larry Fitch |      21 | Los Angeles | Bisagra Dcha.
 J.P. Sinclair   |      106 | Sam Clark   |      11 | New York    | Bisagra Izqda.
 Chen Associates |      103 | Paul Cruz   |      12 | Chicago     | Riostra 1-Tm
(4 rows)


-- 5.17- Trobar totes les comandes rebudes en els dies en que un nou venedor va ser contractat. Per cada comanda mostrar un cop el número, import i data de la comanda.

-------algo asi-------

training=# select pedidos.num_pedido,pedidos.importe,pedidos.fecha_pedido,repventas.contrato from pedidos,repventas where pedidos.rep = repventas.num_empl order by repventas.contrato desc limit 1;
 num_pedido | importe  | fecha_pedido |  contrato  
------------+----------+--------------+------------
     110036 | 22500.00 | 1990-01-30   | 1990-01-13
(1 row)


-- 5.18- Mostra el nom, les vendes dels treballadors que tenen assignada una oficina, amb la ciutat i l'objectiu de l'oficina de cada venedor.

training=# select repventas.nombre,repventas.ventas,oficinas.ciudad,oficinas.objetivo from repventas,oficinas where repventas.oficina_rep = oficinas.oficina and repventas.oficina_rep is not null;
    nombre     |  ventas   |   ciudad    | objetivo  
---------------+-----------+-------------+-----------
 Bill Adams    | 367911.00 | Atlanta     | 350000.00
 Mary Jones    | 392725.00 | New York    | 575000.00
 Sue Smith     | 474050.00 | Los Angeles | 725000.00
 Sam Clark     | 299912.00 | New York    | 575000.00
 Bob Smith     | 142594.00 | Chicago     | 800000.00
 Dan Roberts   | 305673.00 | Chicago     | 800000.00
 Larry Fitch   | 361865.00 | Los Angeles | 725000.00
 Paul Cruz     | 286775.00 | Chicago     | 800000.00
 Nancy Angelli | 186042.00 | Denver      | 300000.00
(9 rows)


-- 5.19- Llista el nom de tots els venedors i el del seu director en cas de tenir-ne. El camp que conté el nom del treballador s'ha d'identificar amb "empleado" i el camp que conté el nom del director amb "director".

training=# select table_empledo.nombre as empledo,table_director.nombre as director from repventas table_empledo,repventas table_director where table_empledo.num_empl = table_director.director;
   empledo   |   director    
-------------+---------------
 Sam Clark   | Larry Fitch
 Sam Clark   | Bob Smith
 Sam Clark   | Mary Jones
 Bob Smith   | Paul Cruz
 Bob Smith   | Dan Roberts
 Bob Smith   | Bill Adams
 Dan Roberts | Tom Snyder
 Larry Fitch | Nancy Angelli
 Larry Fitch | Sue Smith
(9 rows)



-- 5.20- Llista totes les combinacions possibles de venedors i ciutats.

training=# select repventas.nombre,oficinas.ciudad from repventas,oficinas;
    nombre     |   ciudad    
---------------+-------------
 Bill Adams    | Denver
 Bill Adams    | New York
 Bill Adams    | Chicago
 Bill Adams    | Atlanta
 Bill Adams    | Los Angeles
 Mary Jones    | Denver
 Mary Jones    | New York
 Mary Jones    | Chicago
 Mary Jones    | Atlanta
 Mary Jones    | Los Angeles
 Sue Smith     | Denver
 Sue Smith     | New York
 Sue Smith     | Chicago
 Sue Smith     | Atlanta
 Sue Smith     | Los Angeles
 Sam Clark     | Denver
 Sam Clark     | New York
 Sam Clark     | Chicago
 Sam Clark     | Atlanta
 Sam Clark     | Los Angeles
 Bob Smith     | Denver
 Bob Smith     | New York
 Bob Smith     | Chicago
 Bob Smith     | Atlanta
 Bob Smith     | Los Angeles
 Dan Roberts   | Denver
 Dan Roberts   | New York
 Dan Roberts   | Chicago
 Dan Roberts   | Atlanta
 Dan Roberts   | Los Angeles
 Tom Snyder    | Denver
 Tom Snyder    | New York
 Tom Snyder    | Chicago
 Tom Snyder    | Atlanta
 Tom Snyder    | Los Angeles
 Larry Fitch   | Denver
 Larry Fitch   | New York
 Larry Fitch   | Chicago
 Larry Fitch   | Atlanta
 Larry Fitch   | Los Angeles
 Paul Cruz     | Denver
 Paul Cruz     | New York
 Paul Cruz     | Chicago
 Paul Cruz     | Atlanta
 Paul Cruz     | Los Angeles
 Nancy Angelli | Denver
 Nancy Angelli | New York
 Nancy Angelli | Chicago
 Nancy Angelli | Atlanta
 Nancy Angelli | Los Angeles
(50 rows)



-- 5.21- Per a cada venedor, mostrar el nom, les vendes i la ciutat de l'oficina en cas de tenir-ne una d'assignada.

training=# select repventas.num_empl,repventas.nombre,oficinas.ciudad,oficinas.ventas from repventas,oficinas where repventas.oficina_rep = oficinas.oficina and repventas.oficina_rep is not null;
 num_empl |    nombre     |   ciudad    |  ventas   
----------+---------------+-------------+-----------
      105 | Bill Adams    | Atlanta     | 367911.00
      109 | Mary Jones    | New York    | 692637.00
      102 | Sue Smith     | Los Angeles | 835915.00
      106 | Sam Clark     | New York    | 692637.00
      104 | Bob Smith     | Chicago     | 735042.00
      101 | Dan Roberts   | Chicago     | 735042.00
      108 | Larry Fitch   | Los Angeles | 835915.00
      103 | Paul Cruz     | Chicago     | 735042.00
      107 | Nancy Angelli | Denver      | 186042.00
(9 rows)

										or
										
training=# select repventas.num_empl,repventas.nombre,repventas.ventas,sum(importe) from repventas,oficinas,pedidos where repventas.oficina_rep = oficinas.oficina and pedidos.rep = repventas.num_empl and repventas.oficina_rep is not null group by repventas.num_empl; num_empl |    nombre     |  ventas   |   sum    
----------+---------------+-----------+----------
      102 | Sue Smith     | 474050.00 | 22776.00
      106 | Sam Clark     | 299912.00 | 32958.00
      101 | Dan Roberts   | 305673.00 | 26628.00
      107 | Nancy Angelli | 186042.00 | 34432.00
      108 | Larry Fitch   | 361865.00 | 58633.00
      105 | Bill Adams    | 367911.00 | 39327.00
      109 | Mary Jones    | 392725.00 |  7105.00
      103 | Paul Cruz     | 286775.00 |  2700.00
(8 rows)



-- 5.22- Mostra les comandes de productes que tenen unes existències inferiors a 10. Llistar el numero de comanda, la data de la comanda, el nom del client que ha fet la comanda, identificador del fabricant i l'identificador de producte de la comanda.

training=# select pedidos.num_pedido,pedidos.fecha_pedido,clientes.empresa,pedidos.producto,pedidos.fab from pedidos,clientes,productos where pedidos.clie = clientes.num_clie and pedidos.producto = productos.id_producto and pedidos.fab = productos.id_fab and productos.existencias < 10; 
 num_pedido | fecha_pedido |     empresa      | producto | fab 
------------+--------------+------------------+----------+-----
     113013 | 1990-01-14   | Midwest Systems  | 41003    | bic
     112997 | 1990-01-08   | Peter Brothers   | 41003    | bic
     113069 | 1990-03-02   | Chen Associates  | 775c     | imm
     113048 | 1990-02-10   | Rico Enterprises | 779c     | imm
     113003 | 1990-01-25   | Holm & Landis    | 779c     | imm
(5 rows)


-- 5.23- Llista les 5 comandes amb un import superior. Mostrar l'identificador de la comanda, import de la comanda, preu del producte, nom del client, nom del representant de vendes que va efectuar la comanda i ciutat de l'oficina, en cas de tenir oficina assignada.

training=# select pedidos.num_pedido,pedidos.importe,clientes.empresa,productos.precio,repventas.nombre,oficinas.ciudad from pedidos,clientes,productos,repventas,oficinas where pedidos.clie = clientes.num_clie and pedidos.producto = productos.id_producto and pedidos.fab = productos.id_fab and repventas.num_empl = pedidos.rep and repventas.oficina_rep = oficinas.oficina and repventas.oficina_rep is not null; 
 num_pedido | importe  |     empresa      | precio  |    nombre     |   ciudad    
------------+----------+------------------+---------+---------------+-------------
     112961 | 31500.00 | J.P. Sinclair    | 4500.00 | Sam Clark     | New York
     113012 |  3745.00 | JCP Inc.         |  107.00 | Bill Adams    | Atlanta
     112989 |  1458.00 | Jones Mfg.       |  243.00 | Sam Clark     | New York
     112968 |  3978.00 | First Corp.      |  117.00 | Dan Roberts   | Chicago
     113045 | 45000.00 | Zetacorp         | 4500.00 | Larry Fitch   | Los Angeles
     112963 |  3276.00 | Acme Mfg.        |  117.00 | Bill Adams    | Atlanta
     113013 |   652.00 | Midwest Systems  |  652.00 | Larry Fitch   | Los Angeles
     113058 |  1480.00 | Holm & Landis    |  148.00 | Mary Jones    | New York
     112997 |   652.00 | Peter Brothers   |  652.00 | Nancy Angelli | Denver
     112983 |   702.00 | Acme Mfg.        |  117.00 | Bill Adams    | Atlanta
     113024 |  7100.00 | Orion Corp       |  355.00 | Larry Fitch   | Los Angeles
     113062 |  2430.00 | Peter Brothers   |  243.00 | Nancy Angelli | Denver
     112979 | 15000.00 | Orion Corp       | 2500.00 | Sue Smith     | Los Angeles
     113027 |  4104.00 | Acme Mfg.        |   76.00 | Bill Adams    | Atlanta
     113007 |  2925.00 | Zetacorp         |  975.00 | Larry Fitch   | Los Angeles
     113069 | 31350.00 | Chen Associates  | 1425.00 | Nancy Angelli | Denver
     112992 |   760.00 | Midwest Systems  |   76.00 | Larry Fitch   | Los Angeles
     112975 |  2100.00 | JCP Inc.         |  350.00 | Paul Cruz     | Chicago
     113055 |   150.00 | Holm & Landis    |   25.00 | Dan Roberts   | Chicago
     113048 |  3750.00 | Rico Enterprises | 1875.00 | Sue Smith     | Los Angeles
     112993 |  1896.00 | Fred Lewis Corp. |   79.00 | Sue Smith     | Los Angeles
     113065 |  2130.00 | Fred Lewis Corp. |  355.00 | Sue Smith     | Los Angeles
     113003 |  5625.00 | Holm & Landis    | 1875.00 | Mary Jones    | New York
     113049 |   776.00 | Midwest Systems  |  355.00 | Larry Fitch   | Los Angeles
     112987 | 27500.00 | Acme Mfg.        | 2750.00 | Bill Adams    | Atlanta
     113057 |   600.00 | JCP Inc.         |   25.00 | Paul Cruz     | Chicago
     113042 | 22500.00 | Ian & Schmidt    | 4500.00 | Dan Roberts   | Chicago
(27 rows)

							or ok
							
training=# select pedidos.num_pedido,pedidos.importe,clientes.empresa,productos.precio,repventas.nombre,oficinas.ciudad from pedidos,clientes,productos,repventas,oficinas where pedidos.clie = clientes.num_clie and pedidos.producto = productos.id_producto and pedidos.fab = productos.id_fab and repventas.num_empl = pedidos.rep and repventas.oficina_rep = oficinas.oficina and repventas.oficina_rep is not null order by precio desc limit 5; 
 num_pedido | importe  |    empresa    | precio  |   nombre    |   ciudad    
------------+----------+---------------+---------+-------------+-------------
     113042 | 22500.00 | Ian & Schmidt | 4500.00 | Dan Roberts | Chicago
     113045 | 45000.00 | Zetacorp      | 4500.00 | Larry Fitch | Los Angeles
     112961 | 31500.00 | J.P. Sinclair | 4500.00 | Sam Clark   | New York
     112987 | 27500.00 | Acme Mfg.     | 2750.00 | Bill Adams  | Atlanta
     112979 | 15000.00 | Orion Corp    | 2500.00 | Sue Smith   | Los Angeles
(5 rows)

-- 5.24- Llista les comandes que han estat preses per un representant de vendes que no és l'actual representant de vendes del client pel que s'ha realitzat la comanda. Mostrar el número de comanda, el nom del client, el nom de l'actual representant de vendes del client com a "rep_cliente" i el nom del representant de vendes que va realitzar la comanda com a "rep_pedido".


	


-- 5.25- Llista les comandes amb un import superior a 5000 i també aquelles comandes realitzades per un client amb un crèdit inferior a 30000. Mostrar l'identificador de la comanda, el nom del client i el nom del representant de vendes que va prendre la comanda.

training=# select pedidos.num_pedido,clientes.empresa,repventas.nombre from pedidos,clientes,repventas where pedidos.clie = clientes.num_clie and pedidos.rep = repventas.num_empl and pedidos.importe > 5000 and clientes.limite_credito > 30000;
 num_pedido |      empresa      |   nombre    
------------+-------------------+-------------
     112987 | Acme Mfg.         | Bill Adams
     113003 | Holm & Landis     | Mary Jones
     112961 | J.P. Sinclair     | Sam Clark
     110036 | Ace International | Tom Snyder
     113045 | Zetacorp          | Larry Fitch
(5 rows)

