--- 1- Mostra el codi dels clients que han fet alguna comanda juntament amb l'import mínim que han gastat i l'import màxim. Llista-ho ordenat pel codi del client.

  training=# select clie,min(importe),max(importe) from pedidos group by clie order by clie;
 clie |   min    |   max    
------+----------+----------
 2101 |  1458.00 |  1458.00
 2102 |  3978.00 |  3978.00
 2103 |   702.00 | 27500.00
 2106 |  1896.00 |  2130.00
 2107 |   632.00 | 22500.00
 2108 |   150.00 |  5625.00
 2109 | 31350.00 | 31350.00
 2111 |   600.00 |  3745.00
 2112 |  2925.00 | 45000.00
 2113 | 22500.00 | 22500.00
 2114 |  7100.00 | 15000.00
 2117 | 31500.00 | 31500.00
 2118 |   652.00 |  1420.00
 2120 |  3750.00 |  3750.00
 2124 |   652.00 |  2430.00
(15 rows)
  

---- 2- Mostra els clie dels clients que han fet alguna comanda juntament amb l'import mínim que han gastat i l'import màxim. Llista-ho ordenat pel nom del client.
        
training=# select clie,max(importe),min(importe) from pedidos group by clie order by clie;
 clie |   max    |   min    
------+----------+----------
 2101 |  1458.00 |  1458.00
 2102 |  3978.00 |  3978.00
 2103 | 27500.00 |   702.00
 2106 |  2130.00 |  1896.00
 2107 | 22500.00 |   632.00
 2108 |  5625.00 |   150.00
 2109 | 31350.00 | 31350.00
 2111 |  3745.00 |   600.00
 2112 | 45000.00 |  2925.00
 2113 | 22500.00 | 22500.00
 2114 | 15000.00 |  7100.00
 2117 | 31500.00 | 31500.00
 2118 |  1420.00 |   652.00
 2120 |  3750.00 |  3750.00
 2124 |  2430.00 |   652.00
(15 rows)
										or
										
			join


---- 3- Calcula els diners gastats per cada client excloent les comandes inferiors a 1000. S'ha de mostrar el codi del client i la suma, ordenats pel codi del client.

---   Es pot agrupar per més d'un camp:

---    Diners gastats per cada client.
      

---  Diners gastats per cada client segons el venedor.
       
        
--- Diners gastats per cada client segons el venedor. Mostra el nom del client i el nom del venedor.
       
        
training=# select clie,sum(importe) from pedidos where importe > 1000 group by clie order by clie;
 clie |   sum    
------+----------
 2101 |  1458.00
 2102 |  3978.00
 2103 | 34880.00
 2106 |  4026.00
 2107 | 22500.00
 2108 |  7105.00
 2109 | 31350.00
 2111 |  5845.00
 2112 | 47925.00
 2113 | 22500.00
 2114 | 22100.00
 2117 | 31500.00
 2118 |  1420.00
 2120 |  3750.00
 2124 |  2430.00
(15 rows)
							also


training=# select clie,rep,sum(importe) from pedidos where importe >= 1000 group by clie,rep order by clie,rep;
 clie | rep |   sum    
------+-----+----------
 2101 | 106 |  1458.00
 2102 | 101 |  3978.00
 2103 | 105 | 34880.00
 2106 | 102 |  4026.00
 2107 | 110 | 22500.00
 2108 | 109 |  7105.00
 2109 | 107 | 31350.00
 2111 | 103 |  2100.00
 2111 | 105 |  3745.00
 2112 | 108 | 47925.00
 2113 | 101 | 22500.00
 2114 | 102 | 15000.00
 2114 | 108 |  7100.00
 2117 | 106 | 31500.00
 2118 | 108 |  1420.00
 2120 | 102 |  3750.00
 2124 | 107 |  2430.00
(17 rows)


---- 4- Mostra el codi dels clients que han fet alguna comanda juntament amb l'import mínim que han gastat i l'import màxim. Només s'han de llistar aquells clients que l'import mínim sigui superior a 10000. Llista-ho ordenat pel codi del client.
      
training=# select clie,min(importe),max(importe) from pedidos group by clie having min(importe) > 10000 order by clie;
 clie |   min    |   max    
------+----------+----------
 2109 | 31350.00 | 31350.00
 2113 | 22500.00 | 22500.00
 2117 | 31500.00 | 31500.00
(3 rows)



----- 5- Mostra els codis dels clients que han fet alguna comanda juntament amb l'import mínim que han gastat i l'import màxim. Només s'han de llistar aquells clients que l'import mínim sigui superior a 10000. Llista-ho ordenat pel nom del client.

training=# select clie,min(importe),max(importe) from pedidos group by clie having min(importe) > 10000 order by clie;
 clie |   min    |   max    
------+----------+----------
 2109 | 31350.00 | 31350.00
 2113 | 22500.00 | 22500.00
 2117 | 31500.00 | 31500.00
(3 rows)


        
------ 6- Mostra els codis dels clients que han fet alguna comanda juntament amb l'import mínim que han gastat i l'import màxim. Només s'han de llistar aquells clients que l'import mínim sigui superior a 10000 i que tinguin la lletra 't' al seu nom. Llista-ho ordenat pel nom del client.
       
        
training=# select clie,max(importe),min(importe),empresa from pedidos join clientes on num_clie = clie where importe > 10000 and empresa like '%t%' group by clie,empresa;
 clie |   max    |   min    |      empresa      
------+----------+----------+-------------------
 2113 | 22500.00 | 22500.00 | Ian & Schmidt
 2112 | 45000.00 | 45000.00 | Zetacorp
 2109 | 31350.00 | 31350.00 | Chen Associates
 2107 | 22500.00 | 22500.00 | Ace International
(4 rows)



