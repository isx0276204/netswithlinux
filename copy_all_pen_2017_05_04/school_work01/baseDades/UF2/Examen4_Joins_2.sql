1. Per cada comanda que tenim, visualitzar la ciurar de l'oficina del representant de ventes  que l'ha fet,
 el nom del representant que l'ha fet, l''import i el nom de l'empresa que ha comprat el producte. Mostrar-ho
 ordenat pels 2 primers camps demanats.
 


2. Mostrar la quantitat de comandes que cada representant ha fet per client i l'import total de comandes que cada venedor ha venut a cada client.
 Mostrar la ciutat de l'oficina del representant de ventes, el nom del representant, 
 el nom del client, la quantitat de comandes representant-client i
  el total de l''import de les seves comandes per client.
 Ordenar resulats per ciutat, representant i client.
 
 
  

3. Mostrar la quantitat de representants de ventes que hi ha a la regió Este i  la quantitat de 
 representants de ventes que hi ha a la regió Oeste.
 


4. Mostrar la quantitat de representatns de ventes que hi ha a les diferents ciutats-oficines.
Mostrar la regió, la ciutat i el número de representants.

 

5. Mostrar la quantitat de representants de ventes que hi ha a les diferents ciutats-oficines
 i la quantitat de clients que tenen associats als representants d''aquestes ciutats-oficines.
Mostrar la regió, la ciutat, el número de representants i el número de clients.



6. Mostrar els noms de les empreses que han comprat productes de les fàbriques imm i rei amb un preu
de catàleg (no de compra) inferior a 80 o superior a 1000. Mostrar l'empresa' el fabricant, el codi
de producte i el preu


7. Mostrar les empreses amb un nom que no comenci ni per I ni per J que entre totes les seves compres ens han comprat per un total
superior a 20000



8. Mostrar les comandes que els clients han fet a representants de ventes que no són el que tenen assignat.
Mostrar l'import de la comanda, el nom del client, el nom del representant de ventes que ha fet 
la comanda i el nom del representant de ventes que el client té assignat.'



9. Mostrar les dades del producte (5 camps) del qual se n'han venut més unitats.'



10. Per cada representant de ventes mostrar el seu nom, el nom del seu cap i el nom del cap
de l''oficina on treballa.



