---- 1 ¿Quina es la quota mitjana y las vendes mitjanas de tots els empleats?

training=# select avg(cuota) as mitjana_de_cuota,avg(ventas) from repventas ;
  mitjana_de_cuota   |         avg         
---------------------+---------------------
 300000.000000000000 | 289353.200000000000
(1 row)


----- 2 Trobar l'import mitjà de comandes,l'import total de comandes i el preu mig de venda.

training=# select avg(importe) as mitjaja_del_importe,sum(importe) as suma_del_importe,avg(importe/cant) as mitjaja_del_preu from pedidos ;  
mitjaja_del_importe  | suma_del_importe |   mitjaja_del_preu    
-----------------------+------------------+-----------------------
 8256.3666666666666667 |        247691.00 | 1056.9666666666666667
(1 row)


----- 3 Trobar el preu mitjà dels productes del fabricant aci.

training=# select id_fab,avg(precio) from productos where id_fab= 'aci' group by id_fab;
 id_fab |         avg          
--------+----------------------
 aci    | 804.2857142857142857
(1 row)




----- 4 Quin es l'import total de les comandes realizades per l'empleat amb identificador 105?

training=# select sum(importe) as preu from pedidos where rep = 105;
   preu   
----------
 39327.00
(1 row)


------ 5 Trobar la data en que es va realitzar la primera comando.

training=# select min(fecha_pedido) from pedidos ;
    min     
------------
 1989-01-04
(1 row)
						or
training=# select fecha_pedido from pedidos order by fecha_pedido limit 1;
 fecha_pedido 
--------------
 1989-01-04
(1 row)



------ 6 Trobar quantes comandes hi ha de més 25000.

training=# select * from pedidos where importe > 25000;
 num_pedido | fecha_pedido | clie | rep | fab | producto | cant | importe  
------------+--------------+------+-----+-----+----------+------+----------
     112961 | 1989-12-17   | 2117 | 106 | rei | 2a44l    |    7 | 31500.00
     113045 | 1990-02-02   | 2112 | 108 | rei | 2a44r    |   10 | 45000.00
     113069 | 1990-03-02   | 2109 | 107 | imm | 775c     |   22 | 31350.00
     112987 | 1989-12-31   | 2103 | 105 | aci | 4100y    |   11 | 27500.00
(4 rows)


------ 7 Llistar quants empleats estan assignats a cada oficina, indicar el identificador d'oficina, i quants té assignats.


training=# select oficina_rep,count(oficina_rep) from repventas group by oficina_rep having count(oficina_rep) > 0;
 oficina_rep | count 
-------------+-------
          12 |     3
          21 |     2
          11 |     2
          13 |     1
          22 |     1
(5 rows)

------ 8 Per a cada empleat, identificador, import venut a cada client, identificador de client.

training=# select rep,clie,sum(importe) from pedidos group by rep,clie;
 rep | clie |   sum    
-----+------+----------
 109 | 2108 |  7105.00
 110 | 2107 | 23132.00
 106 | 2117 | 31500.00
 102 | 2120 |  3750.00
 102 | 2106 |  4026.00
 108 | 2112 | 47925.00
 108 | 2118 |  3608.00
 107 | 2109 | 31350.00
 105 | 2111 |  3745.00
 101 | 2102 |  3978.00
 105 | 2103 | 35582.00
 108 | 2114 |  7100.00
 103 | 2111 |  2700.00
 107 | 2124 |  3082.00
 101 | 2108 |   150.00
 101 | 2113 | 22500.00
 102 | 2114 | 15000.00
 106 | 2101 |  1458.00
(18 rows)


------ 9 Per a cada empleat on les comandes sumin més de 30000, trobar l'import mitjà de cada comanda.

training=# select rep,sum(importe),avg(importe) from pedidos group by rep having sum(importe) > 30000;
 rep |   sum    |          avg           
-----+----------+------------------------
 106 | 32958.00 |     16479.000000000000
 107 | 34432.00 | 11477.3333333333333333
 108 | 58633.00 |  8376.1428571428571429
 105 | 39327.00 |  7865.4000000000000000
(4 rows)


------ 10 Quina quantitat d'oficines tenen empleats amb vendes superiors a la seva quota.

training=# select oficina_rep,count(num_empl),sum(cuota) as sum_del_cuota,sum(ventas)  as sum_del_ventas from repventas where ventas > cuota group by oficina_rep ;
 oficina_rep | count | sum_del_cuota | sum_del_ventas 
-------------+-------+---------------+----------------
          11 |     2 |     575000.00 |      692637.00
          21 |     2 |     700000.00 |      835915.00
          13 |     1 |     350000.00 |      367911.00
          12 |     2 |     575000.00 |      592448.00
(4 rows)

						right
training=# select count(distinct oficina_rep) from repventas where ventas > cuota;;
 count 
-------
     4
(1 row)
