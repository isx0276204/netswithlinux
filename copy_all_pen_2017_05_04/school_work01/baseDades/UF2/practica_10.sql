UNION
https://www.postgresql.org/docs/9.4/static/sql-select.html#SQL-UNION

SELECT column_name(s) FROM table1
UNION
SELECT column_name(s) FROM table2;


The UNION operator is used to combine the result-set of two or more SELECT statements.

Notice that each SELECT statement within the UNION must have the same number of columns. The columns must also have similar data types. Also, the columns in each SELECT statement must be in the same order.


SELECT empresa FROM clientes
UNION
SELECT nombre FROM repventas;


SELECT empresa, rep_clie FROM clientes
UNION
SELECT nombre, num_empl FROM repventas;


SELECT empresa FROM clientes
UNION
SELECT nombre FROM repventas
UNION
SELECT ciudad FROM oficinas;

Llistar les empreses, el seu venedor assignat, i, per cada venta realitzada llistar el nom de l'empresa i el venedor que l'ha fet. 

select 'Client' , empresa, rep_clie, 'VENEDOR ASSIGNAT' as relacio FROM clientes
UNION
select 'Venta', empresa, rep, 'VEVEDOR QUE HA FET LA VENTA' FROM pedidos JOIN clientes ON clie=num_clie
ORDER BY empresa,1;


SELECT 'Compra : ', empresa, fecha_pedido as data, importe from pedidos JOIN clientes on clie=num_clie 
 UNION 
SELECT 'Total :', empresa, current_date as data, sum(importe) FROM pedidos JOIN clientes ON clie=num_clie GROUP BY empresa 
UNION 
SELECT 'Total General :', '--------', current_date as data, sum(importe) FROM pedidos ORDER BY empresa, data; 

    ?column?     |      empresa      |    data    |  importe  
-----------------+-------------------+------------+----------- 
 Total General : | --------          | 2015-12-02 | 247691.00 
 Compra :        | Ace International | 1990-01-29 |    632.00 
 Compra :        | Ace International | 1990-01-30 |  22500.00 
 Total :         | Ace International | 2015-12-02 |  23132.00 
 Compra :        | Acme Mfg.         | 1989-12-17 |   3276.00 
 Compra :        | Acme Mfg.         | 1989-12-27 |    702.00 
 Compra :        | Acme Mfg.         | 1989-12-31 |  27500.00 
 Compra :        | Acme Mfg.         | 1990-01-22 |   4104.00 
 Total :         | Acme Mfg.         | 2015-12-02 |  35582.00 
 Compra :        | Chen Associates   | 1990-03-02 |  31350.00 
 Total :         | Chen Associates   | 2015-12-02 |  31350.00 
 Compra :        | First Corp.       | 1989-10-12 |   3978.00 
 Total :         | First Corp.       | 2015-12-02 |   3978.00 
 Compra :        | Fred Lewis Corp.  | 1989-01-04 |   1896.00 
 Compra :        | Fred Lewis Corp.  | 1990-02-27 |   2130.00 
 Total :         | Fred Lewis Corp.  | 2015-12-02 |   4026.00 
 Compra :        | Holm & Landis     | 1990-01-25 |   5625.00 
 Compra :        | Holm & Landis     | 1990-02-15 |    150.00 
 Compra :        | Holm & Landis     | 1990-02-23 |   1480.00 
 Total :         | Holm & Landis     | 2015-12-02 |   7255.00 
 Compra :        | Ian & Schmidt     | 1990-02-02 |  22500.00 
 Total :         | Ian & Schmidt     | 2015-12-02 |  22500.00 
 Compra :        | JCP Inc.          | 1989-12-12 |   2100.00 
 Compra :        | JCP Inc.          | 1990-01-11 |   3745.00 
 Compra :        | JCP Inc.          | 1990-02-18 |    600.00 
 Total :         | JCP Inc.          | 2015-12-02 |   6445.00 
 Compra :        | Jones Mfg.        | 1990-01-03 |   1458.00 
 Total :         | Jones Mfg.        | 2015-12-02 |   1458.00 
 Compra :        | J.P. Sinclair     | 1989-12-17 |  31500.00 
 Total :         | J.P. Sinclair     | 2015-12-02 |  31500.00 
 Compra :        | Midwest Systems   | 1989-11-04 |    760.00 
 Compra :        | Midwest Systems   | 1990-01-14 |    652.00 
 Compra :        | Midwest Systems   | 1990-02-10 |   1420.00 
 Compra :        | Midwest Systems   | 1990-02-10 |    776.00 
 Total :         | Midwest Systems   | 2015-12-02 |   3608.00 
 Compra :        | Orion Corp        | 1989-10-12 |  15000.00 
 Compra :        | Orion Corp        | 1990-01-20 |   7100.00 
 Total :         | Orion Corp        | 2015-12-02 |  22100.00 
 Compra :        | Peter Brothers    | 1990-01-08 |    652.00 
 Compra :        | Peter Brothers    | 1990-02-24 |   2430.00 
 Total :         | Peter Brothers    | 2015-12-02 |   3082.00 
 Compra :        | Rico Enterprises  | 1990-02-10 |   3750.00 
 Total :         | Rico Enterprises  | 2015-12-02 |   3750.00 
 Compra :        | Zetacorp          | 1990-01-08 |   2925.00 
 Compra :        | Zetacorp          | 1990-02-02 |  45000.00 
 Total :         | Zetacorp          | 2015-12-02 |  47925.00 
(46 rows) 

Mostrar l'import de cada compra i el total per client:

SELECT 'Compra : ', empresa, importe from pedidos JOIN clientes on clie=num_clie 
UNION
SELECT 'Total :', empresa, sum(importe) FROM pedidos JOIN clientes ON clie=num_clie GROUP BY empresa
ORDER BY empresa; 

 ?column?  |      empresa      | importe  
-----------+-------------------+---------- 
 Compra :  | Ace International |   632.00 
 Compra :  | Ace International | 22500.00 
 Total :   | Ace International | 23132.00 
 Compra :  | Acme Mfg.         |  3276.00 
 Compra :  | Acme Mfg.         |  4104.00 
 Compra :  | Acme Mfg.         |   702.00 
 Total :   | Acme Mfg.         | 35582.00 
 Compra :  | Acme Mfg.         | 27500.00 
 Compra :  | Chen Associates   | 31350.00 
 Total :   | Chen Associates   | 31350.00 
 Compra :  | First Corp.       |  3978.00 
 Total :   | First Corp.       |  3978.00 
 Compra :  | Fred Lewis Corp.  |  2130.00 
 Compra :  | Fred Lewis Corp.  |  1896.00 
 Total :   | Fred Lewis Corp.  |  4026.00 
 Compra :  | Holm & Landis     |  1480.00 
 Compra :  | Holm & Landis     |   150.00 
 Compra :  | Holm & Landis     |  5625.00 
 Total :   | Holm & Landis     |  7255.00 
 Compra :  | Ian & Schmidt     | 22500.00 
 Total :   | Ian & Schmidt     | 22500.00 
 Compra :  | JCP Inc.          |  2100.00 
 Total :   | JCP Inc.          |  6445.00 
 Compra :  | JCP Inc.          |  3745.00 
 Compra :  | JCP Inc.          |   600.00 
 Total :   | Jones Mfg.        |  1458.00 
 Compra :  | Jones Mfg.        |  1458.00 
 Compra :  | J.P. Sinclair     | 31500.00 
 Total :   | J.P. Sinclair     | 31500.00 
 Compra :  | Midwest Systems   |   760.00 
 Compra :  | Midwest Systems   |  1420.00 
 Compra :  | Midwest Systems   |   776.00 
 Total :   | Midwest Systems   |  3608.00 
 Compra :  | Midwest Systems   |   652.00 
 Total :   | Orion Corp        | 22100.00 
 Compra :  | Orion Corp        | 15000.00 
 Compra :  | Orion Corp        |  7100.00 
 Total :   | Peter Brothers    |  3082.00 
 Compra :  | Peter Brothers    |  2430.00 
 Compra :  | Peter Brothers    |   652.00 
 Total :   | Rico Enterprises  |  3750.00 
 Compra :  | Rico Enterprises  |  3750.00 
 Compra :  | Zetacorp          | 45000.00 
 Total :   | Zetacorp          | 47925.00 
 Compra :  | Zetacorp          |  2925.00 
(45 rows) 


Mostrar l'import total de cada compra i el total per client a data d'avui, ordenat per client i data:

SELECT 'Compra : ', empresa, fecha_pedido as data, importe from pedidos JOIN clientes on clie=num_clie 
 UNION 
SELECT 'Total :', empresa, current_date as data, sum(importe) FROM pedidos JOIN clientes ON clie=num_clie GROUP BY empresa 
ORDER BY empresa, data;

 ?column?  |      empresa      |    data    | importe  
-----------+-------------------+------------+---------- 
 Compra :  | Ace International | 1990-01-29 |   632.00 
 Compra :  | Ace International | 1990-01-30 | 22500.00 
 Total :   | Ace International | 2015-12-02 | 23132.00 
 Compra :  | Acme Mfg.         | 1989-12-17 |  3276.00 
 Compra :  | Acme Mfg.         | 1989-12-27 |   702.00 
 Compra :  | Acme Mfg.         | 1989-12-31 | 27500.00 
 Compra :  | Acme Mfg.         | 1990-01-22 |  4104.00 
 Total :   | Acme Mfg.         | 2015-12-02 | 35582.00 
 Compra :  | Chen Associates   | 1990-03-02 | 31350.00 
 Total :   | Chen Associates   | 2015-12-02 | 31350.00 
 Compra :  | First Corp.       | 1989-10-12 |  3978.00 
 Total :   | First Corp.       | 2015-12-02 |  3978.00 
 Compra :  | Fred Lewis Corp.  | 1989-01-04 |  1896.00 
 Compra :  | Fred Lewis Corp.  | 1990-02-27 |  2130.00 
 Total :   | Fred Lewis Corp.  | 2015-12-02 |  4026.00 
 Compra :  | Holm & Landis     | 1990-01-25 |  5625.00 
 Compra :  | Holm & Landis     | 1990-02-15 |   150.00 
 Compra :  | Holm & Landis     | 1990-02-23 |  1480.00 
 Total :   | Holm & Landis     | 2015-12-02 |  7255.00 
 Compra :  | Ian & Schmidt     | 1990-02-02 | 22500.00 
 Total :   | Ian & Schmidt     | 2015-12-02 | 22500.00 
 Compra :  | JCP Inc.          | 1989-12-12 |  2100.00 
 Compra :  | JCP Inc.          | 1990-01-11 |  3745.00 
 Compra :  | JCP Inc.          | 1990-02-18 |   600.00 
 Total :   | JCP Inc.          | 2015-12-02 |  6445.00 
 Compra :  | Jones Mfg.        | 1990-01-03 |  1458.00 
 Total :   | Jones Mfg.        | 2015-12-02 |  1458.00 
 Compra :  | J.P. Sinclair     | 1989-12-17 | 31500.00 
 Total :   | J.P. Sinclair     | 2015-12-02 | 31500.00 
 Compra :  | Midwest Systems   | 1989-11-04 |   760.00 
 Compra :  | Midwest Systems   | 1990-01-14 |   652.00 
 Compra :  | Midwest Systems   | 1990-02-10 |   776.00 
 Compra :  | Midwest Systems   | 1990-02-10 |  1420.00 
 Total :   | Midwest Systems   | 2015-12-02 |  3608.00 
 Compra :  | Orion Corp        | 1989-10-12 | 15000.00 
 Compra :  | Orion Corp        | 1990-01-20 |  7100.00 
 Total :   | Orion Corp        | 2015-12-02 | 22100.00 
 Compra :  | Peter Brothers    | 1990-01-08 |   652.00 
 Compra :  | Peter Brothers    | 1990-02-24 |  2430.00 
 Total :   | Peter Brothers    | 2015-12-02 |  3082.00 
 Compra :  | Rico Enterprises  | 1990-02-10 |  3750.00 
 Total :   | Rico Enterprises  | 2015-12-02 |  3750.00 
 Compra :  | Zetacorp          | 1990-01-08 |  2925.00 
 Compra :  | Zetacorp          | 1990-02-02 | 45000.00 
 Total :   | Zetacorp          | 2015-12-02 | 47925.00 
(45 rows)


Afegir el total general :

SELECT 'Compra : ', empresa, fecha_pedido as data, importe from pedidos JOIN clientes on clie=num_clie 
UNION 
SELECT 'Total :', empresa, current_date as data, sum(importe) FROM pedidos JOIN clientes ON clie=num_clie GROUP BY empresa UNION SELECT 'Total General :', '--------', current_date as data, sum(importe) FROM pedidos
ORDER BY empresa, data; 

    ?column?     |      empresa      |    data    |  importe  
-----------------+-------------------+------------+----------- 
 Total General : | --------          | 2015-12-02 | 247691.00 
 Compra :        | Ace International | 1990-01-29 |    632.00 
 Compra :        | Ace International | 1990-01-30 |  22500.00 
 Total :         | Ace International | 2015-12-02 |  23132.00 
 Compra :        | Acme Mfg.         | 1989-12-17 |   3276.00 
 Compra :        | Acme Mfg.         | 1989-12-27 |    702.00 
 Compra :        | Acme Mfg.         | 1989-12-31 |  27500.00 
 Compra :        | Acme Mfg.         | 1990-01-22 |   4104.00 
 Total :         | Acme Mfg.         | 2015-12-02 |  35582.00 
 Compra :        | Chen Associates   | 1990-03-02 |  31350.00 
 Total :         | Chen Associates   | 2015-12-02 |  31350.00 
 Compra :        | First Corp.       | 1989-10-12 |   3978.00 
 Total :         | First Corp.       | 2015-12-02 |   3978.00 
 Compra :        | Fred Lewis Corp.  | 1989-01-04 |   1896.00 
 Compra :        | Fred Lewis Corp.  | 1990-02-27 |   2130.00 
 Total :         | Fred Lewis Corp.  | 2015-12-02 |   4026.00 
 Compra :        | Holm & Landis     | 1990-01-25 |   5625.00 
 Compra :        | Holm & Landis     | 1990-02-15 |    150.00 
 Compra :        | Holm & Landis     | 1990-02-23 |   1480.00 
 Total :         | Holm & Landis     | 2015-12-02 |   7255.00 
 Compra :        | Ian & Schmidt     | 1990-02-02 |  22500.00 
 Total :         | Ian & Schmidt     | 2015-12-02 |  22500.00 
 Compra :        | JCP Inc.          | 1989-12-12 |   2100.00 
 Compra :        | JCP Inc.          | 1990-01-11 |   3745.00 
 Compra :        | JCP Inc.          | 1990-02-18 |    600.00 
 Total :         | JCP Inc.          | 2015-12-02 |   6445.00 
 Compra :        | Jones Mfg.        | 1990-01-03 |   1458.00 
 Total :         | Jones Mfg.        | 2015-12-02 |   1458.00 
 Compra :        | J.P. Sinclair     | 1989-12-17 |  31500.00 
 Total :         | J.P. Sinclair     | 2015-12-02 |  31500.00 
 Compra :        | Midwest Systems   | 1989-11-04 |    760.00 
 Compra :        | Midwest Systems   | 1990-01-14 |    652.00 
 Compra :        | Midwest Systems   | 1990-02-10 |   1420.00 
 Compra :        | Midwest Systems   | 1990-02-10 |    776.00 
 Total :         | Midwest Systems   | 2015-12-02 |   3608.00 
 Compra :        | Orion Corp        | 1989-10-12 |  15000.00 
 Compra :        | Orion Corp        | 1990-01-20 |   7100.00 
 Total :         | Orion Corp        | 2015-12-02 |  22100.00 
 Compra :        | Peter Brothers    | 1990-01-08 |    652.00 
 Compra :        | Peter Brothers    | 1990-02-24 |   2430.00 
 Total :         | Peter Brothers    | 2015-12-02 |   3082.00 
 Compra :        | Rico Enterprises  | 1990-02-10 |   3750.00 
 Total :         | Rico Enterprises  | 2015-12-02 |   3750.00 
 Compra :        | Zetacorp          | 1990-01-08 |   2925.00 
 Compra :        | Zetacorp          | 1990-02-02 |  45000.00 
 Total :         | Zetacorp          | 2015-12-02 |  47925.00 
(46 rows) 


----##################################################################################################################################################################################################

---1. Mostar les ventes individuals dels productes dels fabricants 'aci' i 'rei' que comencin per 'Bisagra' o 'Articulo'. Mostrar també el total venut d'aquests productes.

training=# select 'producto: ',id_producto,id_fab,fecha_pedido,precio from productos join pedidos on id_fab = fab and id_producto = producto where (id_fab='rei' or id_fab='aci') and (descripcion like 'Bisagra%' or descripcion like 'Articulo%')
union
select 'total: ',id_producto,id_fab,current_date,sum(precio) from productos left join pedidos on id_fab = fab and id_producto = producto where (id_fab='rei' or id_fab='aci') and (descripcion like 'Bisagra%' or descripcion like 'Articulo%') group by id_producto,id_fab order by 2,1;
  ?column?  | id_producto | id_fab | fecha_pedido | precio  
------------+-------------+--------+--------------+---------
 producto:  | 2a44l       | rei    | 1989-12-17   | 4500.00
 total:     | 2a44l       | rei    | 2017-01-20   | 4500.00
 producto:  | 2a44r       | rei    | 1990-02-02   | 4500.00
 total:     | 2a44r       | rei    | 2017-01-20   | 9000.00
 total:     | 41001       | aci    | 2017-01-20   |   55.00
 producto:  | 41002       | aci    | 1990-01-22   |   76.00
 producto:  | 41002       | aci    | 1989-11-04   |   76.00
 total:     | 41002       | aci    | 2017-01-20   |  152.00
 producto:  | 41003       | aci    | 1990-01-11   |  107.00
 total:     | 41003       | aci    | 2017-01-20   |  107.00
 producto:  | 41004       | aci    | 1989-12-27   |  117.00
 producto:  | 41004       | aci    | 1989-10-12   |  117.00
 producto:  | 41004       | aci    | 1989-12-17   |  117.00
 total:     | 41004       | aci    | 2017-01-20   |  351.00
(14 rows)


----2. Mostrar les ventes individuals fetes pels venedors de New York i de Chicago que superin els 2500€. Mostrar també el total de ventes de cada oficina.



----3. Mostrar quantes ventes ha fet cada venedor, la mitjana de numero de ventes dels venedors de cada oficina i el numero de ventes total.

training=# select 'ventas:',ciudad,rep,count(*) from oficinas join repventas on oficina = oficina_rep join pedidos on rep = num_empl group by ciudad,rep
union
select 'apromedio ventas',ciudad,null,count(*)/count(distinct rep) :: float from oficinas join repventas on oficina = oficina_rep join pedidos on rep = num_empl group by ciudad
union select 'oficinas numero ventas ',ciudad,null,count(*) from oficinas join repventas on oficina = oficina_rep join pedidos on rep = num_empl group by ciudad
 order by 2,1 desc;
        ?column?         |   ciudad    | rep | count 
-------------------------+-------------+-----+-------
 ventas:                 | Atlanta     | 105 |     5
 oficinas numero ventas  | Atlanta     |     |     5
 apromedio ventas        | Atlanta     |     |     5
 ventas:                 | Chicago     | 103 |     2
 ventas:                 | Chicago     | 101 |     3
 oficinas numero ventas  | Chicago     |     |     5
 apromedio ventas        | Chicago     |     |   2.5
 ventas:                 | Denver      | 107 |     3
 oficinas numero ventas  | Denver      |     |     3
 apromedio ventas        | Denver      |     |     3
 ventas:                 | Los Angeles | 102 |     4
 ventas:                 | Los Angeles | 108 |     7
 oficinas numero ventas  | Los Angeles |     |    11
 apromedio ventas        | Los Angeles |     |   5.5
 ventas:                 | New York    | 109 |     2
 ventas:                 | New York    | 106 |     2
 oficinas numero ventas  | New York    |     |     4
 apromedio ventas        | New York    |     |     2
(18 rows)



----4. Mostrar les compres de productes de la fabrica 'aci' que han fet els clients del Bill Adams i el Dan Roberts. Mostrar també l'import total per cada client.

training=# select 'compras',clie,producto,importe from pedidos join clientes on clie = num_clie join repventas on rep = num_empl where fab = 'aci' and nombre in ('Bill Adams','Dan Roberts')
union
select 'total:',clie,null,sum(importe) from pedidos join clientes on clie = num_clie join repventas on rep = num_empl where fab = 'aci' and nombre in ('Bill Adams','Dan Roberts') group by clie
training-# order by 2,1;
 ?column? | clie | producto | importe  
----------+------+----------+----------
 compras  | 2102 | 41004    |  3978.00
 total:   | 2102 |          |  3978.00
 compras  | 2103 | 41004    |   702.00
 compras  | 2103 | 4100y    | 27500.00
 compras  | 2103 | 41004    |  3276.00
 compras  | 2103 | 41002    |  4104.00
 total:   | 2103 |          | 35582.00
 compras  | 2108 | 4100x    |   150.00
 total:   | 2108 |          |   150.00
 compras  | 2111 | 41003    |  3745.00
 total:   | 2111 |          |  3745.00
(11 rows)

training=# 


----5. Mostrar el total de ventes de cada oficina i el total de ventes de cada regió

training=# 
select 'oficina:',oficina,region,sum(importe) from oficinas join repventas on oficina_rep = oficina join pedidos on rep = num_empl group by oficina
union
select 'region:',null,region,sum(importe) from oficinas join repventas on oficina_rep = oficina join pedidos on rep = num_empl group by 3
order by 3,1;
 ?column? | oficina | region |    sum    
----------+---------+--------+-----------
 oficina: |      12 | Este   |  29328.00
 oficina: |      13 | Este   |  39327.00
 oficina: |      11 | Este   |  40063.00
 region:  |         | Este   | 108718.00
 oficina: |      21 | Oeste  |  81409.00
 oficina: |      22 | Oeste  |  34432.00
 region:  |         | Oeste  | 115841.00
(7 rows)


----6. Mostrar els noms dels venedors de cada ciutat i el numero total de venedors per ciutat

training=# select 'nombre:',ciudad,nombre,null from oficinas join repventas on oficina_rep = oficina group by ciudad,nombre
union
select 'total:',ciudad,'--',count(num_empl) from oficinas join repventas on oficina_rep = oficina group by ciudad
training-# order by 2,1;
 ?column? |   ciudad    |    nombre     | ?column? 
----------+-------------+---------------+----------
 nombre:  | Atlanta     | Bill Adams    |         
 total:   | Atlanta     | --            |        1
 nombre:  | Chicago     | Paul Cruz     |         
 nombre:  | Chicago     | Dan Roberts   |         
 nombre:  | Chicago     | Bob Smith     |         
 total:   | Chicago     | --            |        3
 nombre:  | Denver      | Nancy Angelli |         
 total:   | Denver      | --            |        1
 nombre:  | Los Angeles | Larry Fitch   |         
 nombre:  | Los Angeles | Sue Smith     |         
 total:   | Los Angeles | --            |        2
 nombre:  | New York    | Sam Clark     |         
 nombre:  | New York    | Mary Jones    |         
 total:   | New York    | --            |        2
(14 rows)



----7. Mostrat els noms dels clients de cada ciutat i el numero total de clients per ciutat.

training=# select 'nombre:',ciudad,empresa,null from oficinas join repventas on oficina_rep = oficina join clientes on rep_clie = num_empl group by ciudad,empresa
union
select 'total:',ciudad,'----',count(num_clie) from oficinas join repventas on oficina_rep = oficina join clientes on rep_clie = num_empl group by ciudad
union select 'ZZZZ','---','----',count(num_clie) from oficinas join repventas on oficina_rep = oficina join clientes on rep_clie = num_empl order by 2,1;
 ?column? |   ciudad    |     empresa      | ?column? 
----------+-------------+------------------+----------
 ZZZZ     | ---         | ----             |       20
 nombre:  | Atlanta     | Three-Way Lines  |         
 nombre:  | Atlanta     | Acme Mfg.        |         
 total:   | Atlanta     | ----             |        2
 nombre:  | Chicago     | First Corp.      |         
 nombre:  | Chicago     | Ian & Schmidt    |         
 nombre:  | Chicago     | Smithson Corp.   |         
 nombre:  | Chicago     | Chen Associates  |         
 nombre:  | Chicago     | AAA Investments  |         
 nombre:  | Chicago     | JCP Inc.         |         
 nombre:  | Chicago     | QMA Assoc.       |         
 total:   | Chicago     | ----             |        7
 nombre:  | Denver      | Peter Brothers   |         
 total:   | Denver      | ----             |        1
 nombre:  | Los Angeles | Midwest Systems  |         
 nombre:  | Los Angeles | Carter & Sons    |         
 nombre:  | Los Angeles | Zetacorp         |         
 nombre:  | Los Angeles | Rico Enterprises |         
 nombre:  | Los Angeles | Orion Corp       |         
 nombre:  | Los Angeles | Fred Lewis Corp. |         
 total:   | Los Angeles | ----             |        6
 nombre:  | New York    | Holm & Landis    |         
 nombre:  | New York    | J.P. Sinclair    |         
 nombre:  | New York    | Solomon Inc.     |         
 nombre:  | New York    | Jones Mfg.       |         
 total:   | New York    | ----             |        4
(26 rows)


----8. Mostrat els noms dels treballadors que son -caps- d'algú, els noms dels seus -subordinats- i el numero de treballadors que té assignat cada cap.
training=# select 'nombre:',dir.nombre,repventas.nombre,null from repventas join repventas dir on dir.num_empl = repventas.director
union
select 'total:',dir.nombre,'----',count(repventas.num_empl) from repventas join repventas dir on dir.num_empl = repventas.director
group by 2 order by 2,1;
 ?column? |   nombre    |    nombre     | ?column? 
----------+-------------+---------------+----------
 nombre:  | Bob Smith   | Paul Cruz     |         
 nombre:  | Bob Smith   | Dan Roberts   |         
 nombre:  | Bob Smith   | Bill Adams    |         
 total:   | Bob Smith   | ----          |        3
 nombre:  | Dan Roberts | Tom Snyder    |         
 total:   | Dan Roberts | ----          |        1
 nombre:  | Larry Fitch | Nancy Angelli |         
 nombre:  | Larry Fitch | Sue Smith     |         
 total:   | Larry Fitch | ----          |        2
 nombre:  | Sam Clark   | Bob Smith     |         
 nombre:  | Sam Clark   | Mary Jones    |         
 nombre:  | Sam Clark   | Larry Fitch   |         
 total:   | Sam Clark   | ----          |        3
(13 rows)
