---1. Mostrar el número de comada i nom del director 
----del representant de vendes que ha realitzat 
---la comanda, si aquest existeix.



--- 2.Llista les comandes superiors a 10000, 
--- mostrant el nom del client que el va 
----- ordenar, el venedor associat al client,
---- i l'oficina on el venedor treballa. 


--- 3.-Nom, vendes i ciutat oficina dels venedors. 



---4.­ Llistar tots els empleats que han estat 
---contractats abans que els seus directors.
--- Mostrar el nom de ---l'empleat com a "empl",
--- la data del contracte del l'empleat
--- com a "empl_fecha", el nom del director
---com a "dir" i la data de contracte del director
--- com a "dir_fecha".



---5. Llistar les comandes dels clients 
---els quals la segona lletra del nom de l'empresa 
---sigui la lletra 
---"c" sense tenir en compte majúscules i minúscules.
--- Mostrar la data de la comanda i el nom de
---l'empresa.

training=# select fecha_pedido,empresa from pedidos join clientes on clie = num_clie where empresa ilike '_c%';
 fecha_pedido |      empresa      
--------------+-------------------
 1990-01-11   | JCP Inc.
 1990-01-30   | Ace International
 1989-12-17   | Acme Mfg.
 1989-12-27   | Acme Mfg.
 1990-01-22   | Acme Mfg.
 1990-01-29   | Ace International
 1989-12-12   | JCP Inc.
 1989-12-31   | Acme Mfg.
 1990-02-18   | JCP Inc.
(9 rows)


---6. Mostrar l'identificador i en nom de l'empresa
--- dels clients. Només mostrar aquells clients que 
---la suma dels imports de les seves comandes sigui
--- menor al limit de crèdit.




--- 7.-Nom venedor i nom director del venedor 

training=# select repventas.nombre,dir.nombre from repventas join repventas dir on repventas.director = dir.num_empl;
    nombre     |   nombre    
---------------+-------------
 Bill Adams    | Bob Smith
 Mary Jones    | Sam Clark
 Sue Smith     | Larry Fitch
 Bob Smith     | Sam Clark
 Dan Roberts   | Bob Smith
 Tom Snyder    | Dan Roberts
 Larry Fitch   | Sam Clark
 Paul Cruz     | Bob Smith
 Nancy Angelli | Larry Fitch
(9 rows)



-- 8. Mostrar l'identificador i la 
--- ciutat de les oficines i 
--- el número de clients de cada oficina.



-- 9 Mostrar les quantitats
---venudes de cada producte. 

training=# select producto,sum(cant) from pedidos group by producto;
 producto | sum 
----------+-----
 2a44r    |  15
 773c     |   3
 41004    |  68
 114      |  16
 779c     |   5
 4100z    |  15
 112      |  10
 xk47     |  28
 775c     |  22
 2a45c    |  32
 2a44g    |   6
 2a44l    |   7
 k47      |   4
 41002    |  64
 41003    |  37
 4100x    |  30
 4100y    |  11
(17 rows)



--- 10.-Nom venedor, quota venedor,
--- nom i quota del seu director. Nomes mostrat
--- els venedors
--- amb quota superior a la de seu director, 

training=# select repventas.nombre,repventas.cuota,dir.nombre,dir.cuota from repventas join repventas dir on repventas.director = dir.num_empl where repventas.cuota > dir.cuota;
   nombre    |   cuota   |  nombre   |   cuota   
-------------+-----------+-----------+-----------
 Bill Adams  | 350000.00 | Bob Smith | 200000.00
 Mary Jones  | 300000.00 | Sam Clark | 275000.00
 Dan Roberts | 300000.00 | Bob Smith | 200000.00
 Larry Fitch | 350000.00 | Sam Clark | 275000.00
 Paul Cruz   | 275000.00 | Bob Smith | 200000.00
(5 rows)



--- 11.-Nom venedor, ciutat de la seva oficina,
--- nom del seu director i nom de la ciutat
--- de la seva oficina 
--- dels venedors que treballen en ciutats diferents
--- de les dels seus directors. 

training=# select repventas.nombre,oficinas.ciudad,dir.nombre,dir_ofi.ciudad from repventas join repventas dir on repventas.director = dir.num_empl join oficinas on repventas.oficina_rep = oficinas.oficina join oficinas dir_ofi on dir.oficina_rep = dir_ofi.oficina;
    nombre     |   ciudad    |   nombre    |   ciudad    
---------------+-------------+-------------+-------------
 Larry Fitch   | Los Angeles | Sam Clark   | New York
 Bob Smith     | Chicago     | Sam Clark   | New York
 Mary Jones    | New York    | Sam Clark   | New York
 Paul Cruz     | Chicago     | Bob Smith   | Chicago
 Dan Roberts   | Chicago     | Bob Smith   | Chicago
 Bill Adams    | Atlanta     | Bob Smith   | Chicago
 Nancy Angelli | Denver      | Larry Fitch | Los Angeles
 Sue Smith     | Los Angeles | Larry Fitch | Los Angeles
(8 rows)


 
--- 12. Llistar els empleats que tinguin un director assignat. 
---Mostrar el nom de l'empleat,
-- el nom del director de l'empleat com a "dirEmpl". 
---En cas que l'empleat tingui assignada una oficina, mostrar també la ciutat de l'oficina 
---en la que treballa l'empleat. Si l'oficina té assignat un director mostrar també el nom del 
---director de l'oficina com a "dirOficina".

--- 13.-Nom venedor, ciutat de la seva oficina, nom del seu director i nom de la ciutat de la seva oficina dels venedors que treballen en ciutats diferents de les dels seus directors. 

