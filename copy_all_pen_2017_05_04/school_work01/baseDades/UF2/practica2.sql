­PRÀCTICA 1. M02. Exercicis
Exercicis WHERE:
1. Alumnat que es digui Anna
practica1=# select * from alumnes where nomalumne = 'anna';
 idalumne | nomalumne | cognomsalumne |  datanaix  | curs | idcicle 
----------+-----------+---------------+------------+------+---------
       7 | anna      | ok            | 1966-12-12 |    1 | asix 
       9 | anna      | gurdiola      | 1988-12-06 |    1 | dam  
(2 rows)

2. Alumnat que es digui Guardiola
practica1=# select * from alumnes where nomalumne = 'gurdiola';
 idalumne | nomalumne | cognomsalumne |  datanaix  | curs | idcicle 
----------+-----------+---------------+------------+------+---------
       8 | gurdiola  | sache         | 1970-06-10 |    2 | isix 
(1 row)
3. Alumnat de 1r
practica1=# select * from alumnes where curs = 1;
 idalumne | nomalumne | cognomsalumne |  datanaix  | curs | idcicle 
----------+-----------+---------------+------------+------+---------
        1 | parveen   | parveeen      | 1992-12-05 |    1 | hisix
        2 | marc      | perez         | 1990-06-25 |    1 | asx  
        7 | anna      | ok            | 1966-12-12 |    1 | asix 
        9 | anna      | gurdiola      | 1988-12-06 |    1 | dam  
       10 | joan      | carlos        | 1988-02-02 |    1 | daw  
       13 | joan      | josep         | 1966-12-26 |    1 | daw  
(6 rows)
4. Alumnat que faci un curs superior a 1r
practica1=# select * from alumnes where curs > 1;
 idalumne | nomalumne | cognomsalumne |  datanaix  | curs | idcicle 
----------+-----------+---------------+------------+------+---------
        4 | migel     | amido         | 1992-08-16 |    2 | hisix
        3 | ivan      | madero        | 1990-03-15 |    2 | hisix
        5 | ana       | ok            | 1970-02-15 |    2 | hisix
        6 | marta     | hiiii         | 2001-05-16 |    2 | asx  
        8 | gurdiola  | sache         | 1970-06-10 |    2 | isix 
       11 | joan      | manel         | 1990-05-30 |    2 | daw  
       12 | joan      | josep         | 1992-05-12 |    2 | dam  
       14 | maria     | josep         | 1992-12-16 |    2 | daw  
(8 rows)
5. Alumnat que faci un curs inferior a 3r
practica1=# select * from alumnes where curs < 3;
 idalumne | nomalumne | cognomsalumne |  datanaix  | curs | idcicle 
----------+-----------+---------------+------------+------+---------
        1 | parveen   | parveeen      | 1992-12-05 |    1 | hisix
        2 | marc      | perez         | 1990-06-25 |    1 | asx  
        4 | migel     | amido         | 1992-08-16 |    2 | hisix
        3 | ivan      | madero        | 1990-03-15 |    2 | hisix
        5 | ana       | ok            | 1970-02-15 |    2 | hisix
        6 | marta     | hiiii         | 2001-05-16 |    2 | asx  
        7 | anna      | ok            | 1966-12-12 |    1 | asix 
        8 | gurdiola  | sache         | 1970-06-10 |    2 | isix 
        9 | anna      | gurdiola      | 1988-12-06 |    1 | dam  
       10 | joan      | carlos        | 1988-02-02 |    1 | daw  
       11 | joan      | manel         | 1990-05-30 |    2 | daw  
       12 | joan      | josep         | 1992-05-12 |    2 | dam  
       13 | joan      | josep         | 1966-12-26 |    1 | daw  
                    14 | maria     | josep         | 1992-12-16 |    2 | daw  
(14 rows)
6. Alumnat nascut el 27 de setembre del 1997
practica1=# select * from alumnes where datanaix = '1997-10-27';
 idalumne | nomalumne | cognomsalumne | datanaix | curs | idcicle 
----------+-----------+---------------+----------+------+---------
(0 rows)

7. Alumnat de 1r que es digui Guardiola
practica1=# select * from alumnes where curs = 1 and nomalumne = 'gurdiola';
 idalumne | nomalumne | cognomsalumne | datanaix | curs | idcicle 
----------+-----------+---------------+----------+------+---------
(0 rows)
8. Alumnat de 1r que es digui Anna Guardiola
practica1=# select * from alumnes where curs = 1 and nomalumne = 'anna' and cognomsalumne = 'gurgiola';
idalumne | nomalumne | cognomsalumne | datanaix | curs | idcicle 
----------+-----------+---------------+----------+------+---------
(0 rows)
9. Alumnat de 1r i de 2n (Atenció!!!!! : volem l'alumnat de primer + l'alumnat de 2n, no volem alumnat que faci primer i 2n a l'hora')
­
practica1=# select * from alumnes where curs = 1 or curs = 2 ;
 idalumne | nomalumne | cognomsalumne |  datanaix  | curs | idcicle 
----------+-----------+---------------+------------+------+---------
        1 | parveen   | parveeen      | 1992-12-05 |    1 | hisix
        2 | marc      | perez         | 1990-06-25 |    1 | asx  
        4 | migel     | amido         | 1992-08-16 |    2 | hisix
        3 | ivan      | madero        | 1990-03-15 |    2 | hisix
        5 | ana       | ok            | 1970-02-15 |    2 | hisix
        6 | marta     | hiiii         | 2001-05-16 |    2 | asx  
        7 | anna      | ok            | 1966-12-12 |    1 | asix 
        8 | gurdiola  | sache         | 1970-06-10 |    2 | isix 
        9 | anna      | gurdiola      | 1988-12-06 |    1 | dam  
       10 | joan      | carlos        | 1988-02-02 |    1 | daw  
       11 | joan      | manel         | 1990-05-30 |    2 | daw  
       12 | joan      | josep         | 1992-05-12 |    2 | dam  
       13 | joan      | josep         | 1966-12-26 |    1 | daw  
       14 | maria     | josep         | 1992-12-16 |    2 | daw  
       15 | parveen   | parveeen      | 1992-05-12 |    2 | hisix
(15 rows)


10. Alumnat que estudiï ASIX o ISIX o DAM o DAW
­
practica1=# select * from alumnes where idcicle = 'isix' or idcicle = 'asix' or idcicle = 'dam' or idcicle = 'daw';
 idalumne | nomalumne | cognomsalumne |  datanaix  | curs | idcicle 
----------+-----------+---------------+------------+------+---------
        7 | anna      | ok            | 1966-12-12 |    1 | asix 
        8 | gurdiola  | sache         | 1970-06-10 |    2 | isix 
        9 | anna      | gurdiola      | 1988-12-06 |    1 | dam  
      10 | joan      | carlos        | 1988-02-02 |    1 | daw  
       11 | joan      | manel         | 1990-05-30 |    2 | daw  
       12 | joan      | josep         | 1992-05-12 |    2 | dam  
       13 | joan      | josep         | 1966-12-26 |    1 | daw  
       14 | maria     | josep         | 1992-12-16 |    2 | daw  
(8 rows)

11. Alumnat que estudiï ASIX i ISIX i DAM i DAW
practica1=# select * from alumnes where idcicle = 'isix' and idcicle = 'asix' and idcicle = 'dam' and idcicle = 'daw';
 idalumne | nomalumne | cognomsalumne | datanaix | curs | idcicle 
----------+-----------+---------------+----------+------+---------
(0 rows)

12. Alumnat que es digui Joan o Juan o 'Joan Carles' o 'Joan Manel' o 'Joan Josep'

practica1=# select * from alumnes where nomalumne like 'joan' or nomalumne like 'juan' or nomalumne like 'Joan carles' or nomalumne like 'joan manel' or nomalumne like 'joan josep';
 idalumne | nomalumne | cognomsalumne |  datanaix  | curs | idcicle 
----------+-----------+---------------+------------+------+---------
       10 | joan      | carlos        | 1988-02-02 |    1 | daw  
       11 | joan      | manel         | 1990-05-30 |    2 | daw  
       12 | joan      | josep         | 1992-05-12 |    2 | dam  
       13 | joan      | josep         | 1966-12-26 |    1 | daw  
(4 rows)




13. Alumnat que es  digui Josep i Pep i 'Josep Maria' i 'Josep Joan'

practica1=# select * from alumnes where nomalumne like 'josep' and nomalumne like 'pep' and nomalumne like 'Josep maria' and nomalumne like 'josep joan';
 idalumne | nomalumne | cognomsalumne | datanaix | curs | idcicle 
----------+-----------+---------------+----------+------+---------
(0 rows)



14. Alumnat de 1r d'ASIX'

practica1=# select * from alumnes where curs = 1 and idcicle = 'asix';
 idalumne | nomalumne | cognomsalumne |  datanaix  | curs | idcicle 
----------+-----------+---------------+------------+------+---------
        7 | anna      | ok            | 1966-12-12 |    1 | asix 
(1 row)


15. Alumant de 1r d'ASIX I de 2n d'ASIX 
practica1=# select * from alumnes where (curs = 1 or curs = 2) and idcicle = 'asix';
 idalumne | nomalumne | cognomsalumne |  datanaix  | curs | idcicle 
----------+-----------+---------------+------------+------+---------
        7 | anna      | ok            | 1966-12-12 |    1 | asix 
(1 row)


16. Alumnat amb un nom que contingui per A

practica1=# select * from alumnes where nomalumne like '%A%';
 idalumne | nomalumne | cognomsalumne | datanaix | curs | idcicle 
----------+-----------+---------------+----------+------+---------
(0 rows)


17. Alumnat amb un nom que acabi en 'e'

practica1=# select * from alumnes where nomalumne like '%e';
 idalumne | nomalumne | cognomsalumne | datanaix | curs | idcicle 
----------+-----------+---------------+----------+------+---------
(0 rows)



18. Alumnat amb un nom que contingui per A i acabi per 'e'

practica1=# select * from alumnes where nomalumne like '%A%e';
 idalumne | nomalumne | cognomsalumne | datanaix | curs | idcicle 
----------+-----------+---------------+----------+------+---------
(0 rows)



19. Alumnat amb un nom que sigui 'Ann' i una lletra més

practica1=# select * from alumnes where nomalumne like 'Ann_';
 idalumne | nomalumne | cognomsalumne | datanaix | curs | idcicle 
----------+-----------+---------------+----------+------+---------
(0 rows)




20. Alumnat amb un nom que contingui per A o per E i acabi en 'A' o en 'E'

practica1=# select * from alumnes where (nomalumne like '%A%' or nomalumne like '%E%') and (nomalumne like '%A' or nomalumne like '%E');
 idalumne | nomalumne | cognomsalumne | datanaix | curs | idcicle 
----------+-----------+---------------+----------+------+---------
(0 rows)

21. Alumnat amb un nom que comenci per A o per E i que després de la primera lletra aparegui una 'A' o una 'E'

select * from clientes where empresa like 'e%a%' or empreas like 'e%e% or empresa like 'a%e%' or empreas like 'a%a% ;


22.alumnat amb nom que comenci per 'p',que acabi en 'a' i que contingui una 'r'.

select * from alumnes where nomalumne ilike 'p%r%a';

23. alumnat com 22 amb un cognom de mes de 5 lletra faci 'asix' -1r o 'daw' -2n.

select * from alumnes where nomalumne ilike 'p%r%a' and cognomalume ilike '_____%' and ((curs = 1 and idcicle = 'asix') or (curs =2 and idcicle = 'daw')) ;

				or
select * from alumnes where nomalumne ilike 'p%r%a' and char_lenght(cognomalume) >= 5 and ((curs = 1 and idcicle = 'asix') or (curs =2 and idcicle = 'daw')) ;
