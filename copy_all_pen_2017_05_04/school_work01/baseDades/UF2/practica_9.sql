PRÀCTICA 9

-- 1. Llistar quants clients té assignats cada treballador-venedor-representant. Mostrar el codi del representant, el nom del representant i la quantitat de clients, ordenant de major quantitat de clients a menor.
 



-- 2. Llistar el nom de cada producte diferent, el seu preu i quantes unitats se n'han venut. No cal mostrar productes sense nom.

training=# select id_fab,id_producto,descripcion,precio,sum(cant) from p
pedidos           pg_catalog.       pg_temp_1.        pg_toast.         pg_toast_temp_1.  productos         public.
training=# select id_fab,id_producto,descripcion,precio,sum(cant) from productos left join pedidos on id_fab = fab and id_producto = producto group by id_fab,id_producto;
 id_fab | id_producto |    descripcion    | precio  | sum 
--------+-------------+-------------------+---------+-----
 rei    | 2a44g       | Pasador Bisagra   |  350.00 |   6
 rei    | 2a45c       | V Stago Trinquete |   79.00 |  32
 fea    | 112         | Cubierta          |  148.00 |  10
 bic    | 41003       | Manivela          |  652.00 |   2
 aci    | 4100z       | Montador          | 2500.00 |  15
 aci    | 41004       | Articulo Tipo 4   |  117.00 |  68
 rei    | 2a44r       | Bisagra Dcha.     | 4500.00 |  15
 fea    | 114         | Bancada Motor     |  243.00 |  16
 qsa    | xk48a       | Reductor          |  117.00 |    
 imm    | 887x        | Retenedor Riostra |  475.00 |    
 aci    | 4100y       | Extractor         | 2750.00 |  11
 aci    | 41001       | Articulo Tipo 1   |   55.00 |    
 aci    | 41003       | Articulo Tipo 3   |  107.00 |  35
 aci    | 4100x       | Ajustador         |   25.00 |  30
 bic    | 41672       | Plate             |  180.00 |    
 imm    | 775c        | Riostra 1-Tm      | 1425.00 |  22
 bic    | 41089       | Retn              |  225.00 |    
 aci    | 41002       | Articulo Tipo 2   |   76.00 |  64
 qsa    | xk48        | Reductor          |  134.00 |    
 imm    | 779c        | Riostra 2-Tm      | 1875.00 |   5
 imm    | 887h        | Soporte Riostra   |   54.00 |    
 rei    | 2a44l       | Bisagra Izqda.    | 4500.00 |   7
 qsa    | xk47        | Reductor          |  355.00 |  28
 imm    | 773c        | Riostra 1/2-Tm    |  975.00 |   3
 imm    | 887p        | Perno Riostra     |  250.00 |    
(25 rows)




-- 3. Llistar el nom de cada producte diferent, el seu preu i quantes unitats se n'han venut segons el representant que l'ha venut (volem saber quantes unitats n'ha venut cada representant). Mostrar el nom del venedor. No cal mostrar productes sense nom.




-- 4. Llistar el nom de cada producte diferent, el seu preu i quantes unitats se n'han venut segons el el client que l'ha comprat (volem saber quantes unitats n'ha comprat cada client). Mostrar el nom del client. No cal mostrar productes sense nom.





--5. Mostrar quants treballadors tenim a cada regió.

training=# select region,count(num_empl) from repventas left join oficinas on oficina_rep = oficina group by region;
 region | count 
--------+-------
        |     1
 Este   |     6
 Oeste  |     3
(3 rows)




--6. Mostrar el noms dels 3  clients que ens han comprat més tenint en compte l'import.






--7. Mostrar els 3 noms de productes que s'han venut més tenint en compte la quantitat.







--– 8. Mostrat els 3 noms de venedors que han venut menys tenint en compte l'import.







-- 9. Mostrar totes les dades possibles relacionades amb les comandes que han fet els clients que tenen un nom que no comença ni amb 'A' ni amb 'S' de productes dels fabricants acabats amb ' sa ' o en ' mm'  i que ha fet algun venendor de Denver.

training=# select * from pedidos join clientes on clie = num_clie left join productos on id_fab = fab and id_producto = producto join repventas on rep = num_empl left join oficinas on oficina_rep = oficina where (empresa not like 'S%'and empresa not like 'A%') and (id_fab like '%mm' or id_fab like '%sa') and ( ciudad='Denver');
 num_pedido | fecha_pedido | clie | rep | fab | producto | cant | importe  | num_clie |     empresa     | rep_clie | limite_credito | id_fab
 | id_producto | descripcion  | precio  | existencias | num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |
   cuota   |  ventas   | oficina | ciudad | region | dir | objetivo  |  ventas   
------------+--------------+------+-----+-----+----------+------+----------+----------+-----------------+----------+----------------+-------
-+-------------+--------------+---------+-------------+----------+---------------+------+-------------+------------+------------+----------+
-----------+-----------+---------+--------+--------+-----+-----------+-----------
     113069 | 1990-03-02   | 2109 | 107 | imm | 775c     |   22 | 31350.00 |     2109 | Chen Associates |      103 |       25000.00 | imm   
 | 775c        | Riostra 1-Tm | 1425.00 |           5 |      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 |
 300000.00 | 186042.00 |      22 | Denver | Oeste  | 108 | 300000.00 | 186042.00
(1 row)







--10. Mostrar, per cada comanda, les següents dades ordenades per client:
-- Nom de producte
-- Codi i nom del venedor que va vendre la comanda
-- Ciutat de l'oficina del venedor que va vendre la comanda
-- Codi i nom del cap del venedor que va vendre la comanda
-- Codi i nom del client que va comprar la comanda
-- Codi i nom del venedor assignat al client
-- Ciutat de l'oficina del venedor assignat al client
-- Codi i nom del cap del venedor assignat al client
-- Import de la comanda

----algo asi----no complet---
training=# select num_pedidos,descripcion,rep.num_empl as num_venedor,rep.nombre as nom_de_venador from pedidos left join productos on id_fab = fab and id_producto = producto join repventas rep on rep=rep.num_empl left join oficinas 
