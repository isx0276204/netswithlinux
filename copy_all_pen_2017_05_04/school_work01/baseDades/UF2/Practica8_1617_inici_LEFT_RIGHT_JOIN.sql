LEFT JOIN / RIGHT JOIN / FULL JOIN

SELECT COUNT(*) FROM repventas;
 count 
-------
    10
(1 row)

SELECT nombre, ciudad FROM repventas JOIN oficinas ON oficina_rep=oficina;
    nombre     |   ciudad    
---------------+-------------
 Bill Adams    | Atlanta
 Mary Jones    | New York
 Sue Smith     | Los Angeles
 Sam Clark     | New York
 Bob Smith     | Chicago
 Dan Roberts   | Chicago
 Larry Fitch   | Los Angeles
 Paul Cruz     | Chicago
 Nancy Angelli | Denver
(9 rows)

SELECT nombre, ciudad FROM repventas LEFT JOIN oficinas ON oficina_rep=oficina;
    nombre     |   ciudad    
---------------+-------------
 Bill Adams    | Atlanta
 Mary Jones    | New York
 Sue Smith     | Los Angeles
 Sam Clark     | New York
 Bob Smith     | Chicago
 Dan Roberts   | Chicago
 Tom Snyder    | 
 Larry Fitch   | Los Angeles
 Paul Cruz     | Chicago
 Nancy Angelli | Denver
(10 rows)

SELECT nombre, director FROM repventas;
    nombre     | director 
---------------+----------
 Bill Adams    |      104
 Mary Jones    |      106
 Sue Smith     |      108
 Sam Clark     |         
 Bob Smith     |      106
 Dan Roberts   |      104
 Tom Snyder    |      101
 Larry Fitch   |      106
 Paul Cruz     |      104
 Nancy Angelli |      108
(10 rows)

SELECT treb.nombre, treb.director, cap.nombre as jefe FROM repventas treb JOIN repventas cap ON treb.director=cap.num_empl;
    nombre     | director |    jefe     
---------------+----------+-------------
 Bill Adams    |      104 | Bob Smith
 Mary Jones    |      106 | Sam Clark
 Sue Smith     |      108 | Larry Fitch
 Bob Smith     |      106 | Sam Clark
 Dan Roberts   |      104 | Bob Smith
 Tom Snyder    |      101 | Dan Roberts
 Larry Fitch   |      106 | Sam Clark
 Paul Cruz     |      104 | Bob Smith
 Nancy Angelli |      108 | Larry Fitch
(9 rows)

SELECT treb.nombre, treb.director, cap.nombre as jefe FROM repventas treb LEFT JOIN repventas cap ON treb.director=cap.num_empl;
    nombre     | director |    jefe     
---------------+----------+-------------
 Bill Adams    |      104 | Bob Smith
 Mary Jones    |      106 | Sam Clark
 Sue Smith     |      108 | Larry Fitch
 Sam Clark     |          | 
 Bob Smith     |      106 | Sam Clark
 Dan Roberts   |      104 | Bob Smith
 Tom Snyder    |      101 | Dan Roberts
 Larry Fitch   |      106 | Sam Clark
 Paul Cruz     |      104 | Bob Smith
 Nancy Angelli |      108 | Larry Fitch
(10 rows)

SELECT COUNT(*) FROM CLIENTES;
 count 
-------
    21
(1 row)

SELECT empresa, COUNT(num_pedido) from clientes JOIN pedidos ON num_clie=clie GROUP BY num_clie;
      empresa      | count 
-------------------+-------
 First Corp.       |     1
 Zetacorp          |     2
 J.P. Sinclair     |     1
 Midwest Systems   |     4
 Rico Enterprises  |     1
 Fred Lewis Corp.  |     2
 Holm & Landis     |     3
 Ian & Schmidt     |     1
 Ace International |     2
 Peter Brothers    |     2
 Chen Associates   |     1
 JCP Inc.          |     3
 Jones Mfg.        |     1
 Orion Corp        |     2
 Acme Mfg.         |     4
(15 rows)

SELECT empresa, COUNT(num_pedido) from clientes LEFT JOIN pedidos ON num_clie=clie GROUP BY num_clie;
      empresa      | count 
-------------------+-------
 First Corp.       |     1
 Zetacorp          |     2
 J.P. Sinclair     |     1
 Midwest Systems   |     4
 Smithson Corp.    |     0
 AAA Investments   |     0
 Carter & Sons     |     0
 Rico Enterprises  |     1
 Fred Lewis Corp.  |     2
 Holm & Landis     |     3
 Ian & Schmidt     |     1
 Ace International |     2
 Peter Brothers    |     2
 Solomon Inc.      |     0
 Three-Way Lines   |     0
 Chen Associates   |     1
 QMA Assoc.        |     0
 JCP Inc.          |     3
 Jones Mfg.        |     1
 Orion Corp        |     2
 Acme Mfg.         |     4
(21 rows)

SELECT empresa, COUNT(num_pedido) from pedidos RIGHT JOIN clientes ON num_clie=clie GROUP BY num_clie;
      empresa      | count 
-------------------+-------
 First Corp.       |     1
 Zetacorp          |     2
 J.P. Sinclair     |     1
 Midwest Systems   |     4
 Smithson Corp.    |     0
 AAA Investments   |     0
 Carter & Sons     |     0
 Rico Enterprises  |     1
 Fred Lewis Corp.  |     2
 Holm & Landis     |     3
 Ian & Schmidt     |     1
 Ace International |     2
 Peter Brothers    |     2
 Solomon Inc.      |     0
 Three-Way Lines   |     0
 Chen Associates   |     1
 QMA Assoc.        |     0
 JCP Inc.          |     3
 Jones Mfg.        |     1
 Orion Corp        |     2
 Acme Mfg.         |     4
(21 rows)


