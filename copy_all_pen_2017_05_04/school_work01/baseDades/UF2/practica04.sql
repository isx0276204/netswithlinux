PRÀCTICA 4. M02								4/4

SQL Aggregate Functions
SQL aggregate functions return a single value, calculated from values in a column.
Useful aggregate functions:
AVG() - Returns the average value
COUNT() - Returns the number of rows
MAX() - Returns the largest value
MIN() - Returns the smallest value
SUM() - Returns the sum

SELECT avg(importe) FROM pedidos
SELECT avg(edad) FROM repventas 
SELECT count(*) FROM repventas 
SELECT max(importe), min(importe) FROM pedidos ;
SELECT avg(importe), sum(importe) FROM pedidos ;

SELECT MIN(importe) as minim, MAX(importe) as maxim, AVG(importe), SUM(importe) FROM pedidos ;
GROUP BY
HAVING
SELECT column_name, aggregate_function(column_name)
FROM table_name
WHERE column_name operator value
GROUP BY column_name
HAVING aggregate_function(column_name) operator value; 

SELECT rep, count(*)  from pedidos 
GROUP BY rep;

SELECT rep, count(*)  from pedidos 
GROUP BY rep
HAVING count(*) > 2
ORDER BY rep ;

SELECT rep, sum(importe) as total_importe  FROM pedidos 

GROUP BY rep
HAVING total_importe BETWEEN 1000 AND 4000
ORDER BY rep ;

SELECT rep, clie, count(*) FROM pedidos GROUP BY rep,clie; 

Funcions : COUNT(*), MIN() i MAX(), AVG(), SUM()


1. Fabricant, número i import de les comandes que el seu import oscil·li entre 10000 i 39999, i ordenat per fabricant de forma ascendent i pel número descendent.
training=# select fab,num_pedido,importe from pedidos where importe between 1000 and 3999 order by fab,num_pedido desc;
 fab | num_pedido | importe 
-----+------------+---------
 aci |     113012 | 3745.00
 aci |     112968 | 3978.00
 aci |     112963 | 3276.00
 fea |     113062 | 2430.00
 fea |     113058 | 1480.00
 fea |     112989 | 1458.00
 imm |     113048 | 3750.00
 imm |     113007 | 2925.00
 qsa |     113065 | 2130.00
 qsa |     113051 | 1420.00
 rei |     112993 | 1896.00
 rei |     112975 | 2100.00
(12 rows)



2. Identificador i nom dels venedors, amb l'identificador de l'oficina d aquells venedors que tenen una oficina assignada .

training=# select num_empl,nombre from repventas where oficina_rep is not null;
 num_empl |    nombre     
----------+---------------
      105 | Bill Adams
      109 | Mary Jones
      102 | Sue Smith
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      108 | Larry Fitch
      103 | Paul Cruz
      107 | Nancy Angelli
(9 rows)


3. Trobar la data en que es va realitzar la primera i la última comanda.

training=# select min(fecha_pedido)as primer,max(fecha_pedido)as ultima from pedidos ;
   primer   |   ultima   
------------+------------
 1989-01-04 | 1990-03-02
(1 row)


4. Llistar quants empleats estan assignats a cada oficina, indicar el identificador d'oficina, i quants té assignats, no s'han de mostrar els nulls.


training=# select oficina_rep,count(oficina_rep) from repventas where oficina_rep is not null group by oficina_rep;
 oficina_rep | count 
-------------+-------
          12 |     3
          21 |     2
          11 |     2
          13 |     1
          22 |     1
(5 rows)


5. Per a cada empleat que tingui més d'una' comanda a algún client que sumi més de 2000, trobar la mitja per a cada empleat i client.

 
 
 training=# select clie,rep,sum(importe),avg(importe) from pedidos group by clie,rep having sum(importe) > 2000;
 clie | rep |   sum    |          avg           
------+-----+----------+------------------------
 2111 | 105 |  3745.00 |  3745.0000000000000000
 2124 | 107 |  3082.00 |  1541.0000000000000000
 2111 | 103 |  2700.00 |  1350.0000000000000000
 2113 | 101 | 22500.00 |     22500.000000000000
 2120 | 102 |  3750.00 |  3750.0000000000000000
 2106 | 102 |  4026.00 |  2013.0000000000000000
 2117 | 106 | 31500.00 |     31500.000000000000
 2103 | 105 | 35582.00 |  8895.5000000000000000
 2108 | 109 |  7105.00 |  3552.5000000000000000
 2114 | 102 | 15000.00 | 15000.0000000000000000
 2109 | 107 | 31350.00 |     31350.000000000000
 2114 | 108 |  7100.00 |  7100.0000000000000000
 2107 | 110 | 23132.00 | 11566.0000000000000000
 2118 | 108 |  3608.00 |   902.0000000000000000
 2102 | 101 |  3978.00 |  3978.0000000000000000
 2112 | 108 | 47925.00 |     23962.500000000000
(16 rows)



6. Trobar el fabricant, producte i preu dels productes on el seu identificador comenci i acabi per 4.

training=# select id_fab,id_producto,precio from productos where id_producto like '4%4';
 id_fab | id_producto | precio 
--------+-------------+--------
 aci    | 41004       | 117.00
(1 row)




7. Trobar el fabricant, producte i preu dels productes amb un preu superior a 100 i unes existemcias menors de 10. i mostralo ordenat per fabricant en ordre descencent, i per id_producte de forma ascendent.

training=# select id_fab,id_producto,precio from productos where precio > 100 and existencias < 10 order by id_fab desc,id_producto;
 id_fab | id_producto | precio  
--------+-------------+---------
 imm    | 775c        | 1425.00
 imm    | 779c        | 1875.00
 bic    | 41003       |  652.00
 bic    | 41672       |  180.00
(4 rows)


8. Identificador fabricant, producte i descripció dels productes amb un preu superior a 1000 i siguin del fabricant amb identificador rei o les existències siguin superiors a 20.

training=# select id_fab,id_producto,descripcion from productos where precio > 1000 and ( existencias > 20 or  id_fab = 'rei');
 id_fab | id_producto |  descripcion   
--------+-------------+----------------
 aci    | 4100y       | Extractor
 rei    | 2a44l       | Bisagra Izqda.
 aci    | 4100z       | Montador
 rei    | 2a44r       | Bisagra Dcha.
(4 rows)


9. Identificador i ciutat de les oficines de la regió est amb unes vendes inferiors a 700000 o de la regió oest amb unes vendes inferiors a 600000.

training=# select oficina,ciudad,ventas,region from oficinas where (region = 'Este' and ventas < 700000) or (region = 'Oeste' and ventas < 600000); 
 oficina |  ciudad  |  ventas   | region 
---------+----------+-----------+--------
      22 | Denver   | 186042.00 | Oeste
      11 | New York | 692637.00 | Este
      13 | Atlanta  | 367911.00 | Este
(3 rows)




10. Identificador del fabricant, identificació i descripció dels productes on l identificador del fabricant és "rei" o el preu és menor a 500.


training=# select id_fab,id_producto,descripcion from productos where id_fab = 'rei' or precio < 500;
 id_fab | id_producto |    descripcion    
--------+-------------+-------------------
 rei    | 2a45c       | V Stago Trinquete
 qsa    | xk47        | Reductor
 bic    | 41672       | Plate
 aci    | 41003       | Articulo Tipo 3
 aci    | 41004       | Articulo Tipo 4
 imm    | 887p        | Perno Riostra
 qsa    | xk48        | Reductor
 rei    | 2a44l       | Bisagra Izqda.
 fea    | 112         | Cubierta
 imm    | 887h        | Soporte Riostra
 bic    | 41089       | Retn
 aci    | 41001       | Articulo Tipo 1
 qsa    | xk48a       | Reductor
 aci    | 41002       | Articulo Tipo 2
 rei    | 2a44r       | Bisagra Dcha.
 aci    | 4100x       | Ajustador
 fea    | 114         | Bancada Motor
 imm    | 887x        | Retenedor Riostra
 rei    | 2a44g       | Pasador Bisagra
(19 rows)

11. Identificador dels clients que el seu nom no conté " Corp." o " Inc." amb crèdit major a 30000.

training=# select * from clientes where not (empresa ilike '%inc.' or empresa ilike '%corp.') and limite_credito > 30000;
 num_clie |      empresa      | rep_clie | limite_credito 
----------+-------------------+----------+----------------
     2103 | Acme Mfg.         |      105 |       50000.00
     2123 | Carter & Sons     |      102 |       40000.00
     2107 | Ace International |      110 |       35000.00
     2101 | Jones Mfg.        |      106 |       65000.00
     2112 | Zetacorp          |      108 |       50000.00
     2121 | QMA Assoc.        |      103 |       45000.00
     2124 | Peter Brothers    |      107 |       40000.00
     2108 | Holm & Landis     |      109 |       55000.00
     2117 | J.P. Sinclair     |      106 |       35000.00
     2120 | Rico Enterprises  |      102 |       50000.00
     2118 | Midwest Systems   |      108 |       60000.00
     2105 | AAA Investments   |      101 |       45000.00
(12 rows)


12. Identificador i mitja d edad per a cada oficina.

training=# select oficina_rep,avg(edad) from repventas group by oficina_rep;
 oficina_rep |         avg         
-------------+---------------------
             | 41.0000000000000000
          12 | 35.6666666666666667
          21 | 55.0000000000000000
          11 | 41.5000000000000000
          13 | 37.0000000000000000
          22 | 49.0000000000000000
(6 rows)


13. Regió i diferència entre la mitjana aritmètica dels objectius i la de les vendes, per a cada regió.

training=# select region,avg(ventas)-avg(objetivo) as diferencia from oficinas group by region;
 region |     diferencia     
--------+--------------------
 Este   | 23530.000000000000
 Oeste  | -1521.500000000000
(2 filas)



14. Trobar l import mitjà de comandes,l import total de comandes i el preu mig de venda.

training=# select avg(importe),sum(importe),avg(importe/cant) as mid_preu from pedidos ;
          avg          |    sum    |       mid_preu        
-----------------------+-----------+-----------------------
 8256.3666666666666667 | 247691.00 | 1056.9666666666666667
(1 row)


15. Ciutat oficines i diferència entre vendes i objectius ordenada per aquesta diferència. 

training=# select ciudad,ventas - objetivo as difference from oficinas order by difference;
   ciudad    | difference 
-------------+------------
 Denver      | -113958.00
 Chicago     |  -64958.00
 Atlanta     |   17911.00
 Los Angeles |  110915.00
 New York    |  117637.00
(5 rows)


16. Número comanda, data comanda, codi fabricant, codi producte i import comandes per comandes entre els dies '1989-09-1' i '1989-12-31'.

training=# select num_pedido,fecha_pedido,fab,producto,importe from pedidos where fecha_pedido between '1989-09-01' and '1989-12-31';
 num_pedido | fecha_pedido | fab | producto | importe  
------------+--------------+-----+----------+----------
     112961 | 1989-12-17   | rei | 2a44l    | 31500.00
     112968 | 1989-10-12   | aci | 41004    |  3978.00
     112963 | 1989-12-17   | aci | 41004    |  3276.00
     112983 | 1989-12-27   | aci | 41004    |   702.00
     112979 | 1989-10-12   | aci | 4100z    | 15000.00
     112992 | 1989-11-04   | aci | 41002    |   760.00
     112975 | 1989-12-12   | rei | 2a44g    |  2100.00
     112987 | 1989-12-31   | aci | 4100y    | 27500.00
(8 rows)


17. Ciutat oficines, regions i diferència entre vendes i objectius ordenades per regió i per diferència entre vendes i objectius de major a menor. 

training=# select oficina,region,(ventas - objetivo) as diff from oficinas order by region,diff desc;
 oficina | region |    diff    
---------+--------+------------
      11 | Este   |  117637.00
      13 | Este   |   17911.00
      12 | Este   |  -64958.00
      21 | Oeste  |  110915.00
      22 | Oeste  | -113958.00
(5 rows)


18. Número total de comandes.

training=# select count(*) from pedidos ;
 count 
-------
    30
(1 row)


19. Nom i data de contracte dels empleats que les seves vendes siguin superiors a 200000 i mostrar ordenat per contracte de més nou a més vell.

training=# select nombre,contrato from repventas where ventas > 200000 order by contrato desc;
   nombre    |  contrato  
-------------+------------
 Larry Fitch | 1989-10-12
 Mary Jones  | 1989-10-12
 Sam Clark   | 1988-06-14
 Bill Adams  | 1988-02-12
 Paul Cruz   | 1987-03-01
 Sue Smith   | 1986-12-10
 Dan Roberts | 1986-10-20
(7 rows)


20. Codi i nom de les tres ciutats que tinguin unes vendes superiors. 

training=# select oficina,ciudad,ventas from oficinas order by ventas desc limit 3;
 oficina |   ciudad    |  ventas   
---------+-------------+-----------
      21 | Los Angeles | 835915.00
      12 | Chicago     | 735042.00
      11 | New York    | 692637.00
(3 rows)
