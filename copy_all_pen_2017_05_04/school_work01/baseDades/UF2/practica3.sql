1. Importar la BASE DE DADES TRAINING:
A Linux :
cp /home/groups/inf/public/ASX/M02-BaseDades/training/training.sql /tmp
Passar (amb el LibreOffice) CLIENTES.DAT a CLIENTES.CSV
Passar (amb el LibreOffice) OFICINAS.DAT a OFICINAS.CSV
A PSQL:
CREATE DATABASE training;
\c training

Crear l'estructura de la BASE DE DADES
\i /tmp/training.sql

Amb l'ordre COPY importar les dades dels fitxers .dat i .csv

Buscar Sintaxis de l'ordre COPY de POSTGRES
Atenció a l'opció DELIMITER.

		COPY productos FROM /tmp/productos.dat
		COPY clientes FROM /tmp/clientes.csv
	
2. Consultar la BASE DE DADES TRAINING:
SELECT * FROM taula;

SELECT * FROM productos;
SELECT * FROM pedidos;

Llista de taules:
	\d
Estructura de taula : \d taula 	llista els camps de la taula
	\d pedidos
\d clientes
(rows) 	numero de registres/files:
SELECT count(*) FROM taula;
SELECT count(*) FROM productos;
SELECT count(*) FROM pedidos;


Seleccionar camps: SELECT col1, col3 FROM taula;
	
	SELECT num_pedido, fecha_pedido, rep FROM pedidos;

	Ordenació de camps:

	SELECT fecha_pedido, num_pedido, rep
	FROM pedidos 
ORDER BY fecha_pedido;

	Canvi de nom del camp:
		SELECT num_pedido AS "IDorder", fecha_pedido AS "Date"
		FROM pedidos;

	WHERE
	AND
	OR
	=
	<>
	>
	<
	>=
<=


		SELECT * 
		FROM pedidos 
		WHERE num_pedido = 113024;
	
		SELECT * 
		FROM pedidos 
		WHERE fecha_pedido < '1990-1-1';
	BETWEEN

		SELECT * 
FROM pedidos 
WHERE importe BETWEEN 500 AND 600;

SELECT * 
FROM pedidos 
WHERE importe >= 500 AND importe <= 600;

	LIKE
	_ 1 caracter
	% varis caracters o cap
'abc' LIKE 'abc'    true

'abc' LIKE 'a%'     true

'abc' LIKE '_b_'    true

'abc' LIKE 'c'      false

		SELECT * 
FROM productos 
WHERE description LIKE 'A%';

	SELECT oficina, region 
FROM oficinas 
WHERE region LIKE 'este';

	SELECT oficina, region 
FROM oficinas 
WHERE region LIKE '_ste';

ILIKE  –-  LIKE case sensitive
	SELECT oficina, region 
FROM oficinas 
WHERE region ILIKE 'este';
	
IN
	SELECT * 
FROM productos 
WHERE id_fab IN('imm','fea');

	ORDER BY camp 
	ORDER BY camp1 DESC, camp2 ASC
SELECT id_fab, precio, descripcion
FROM productos
ORDER BY id_fab ASC, precio DESC;

SELECT id_fab, precio, descripcion 
FROM productos 
ORDER BY 1 ASC, 2 DESC;

	LIMIT
		SELECT id_producto, descripcion 
FROM productos 
ORDER BY precio DESC
LIMIT 5;
	OFFSET
SELECT id_producto, descripcion 
FROM productos 
ORDER BY precio DESC
LIMIT 5 OFFSET 10;


Columnes virtuals usant operands matemàtics

	SELECT num_empl, nombre, (ventas/cuota) AS ratio 
	FROM repventas 
	WHERE (ventas/cuota) > 1;

Unir text amb || i inserir text amb ''

	SELECT nombre || ' es identificat amb ' || num_empl AS "Treballador"
	FROM repventas;

DISTINCT per obtenir resultats únics

	SELECT DISCTINCT id_fab 
	FROM productos;

NOT
	SELECT nombre, ventas
FROM repventas 
WHERE NOT ventas > 300000;

	SELECT nombre, ventas 
FROM repventas
 	WHERE ventas <= 300000

IS NULL

	SELECT nombre 
FROM repventas 
WHERE cuota IS NULL;

	SELECT nombre
FROM repventas
WHERE cuota = NULL;

SELECT nombre
FROM repventas 
WHERE cuota IS NOT NULL;

	SELECT nombre 
FROM repventas
WHERE cuota <> NULL;



Precedencia d'operadors'
	*, /
	+, -
	IS, IS NULL, NOT NULL
	IN, BETWEEN, LIKE, ILIKE
	<>, =
	NOT
	AND
	OR

Alteració de la precedència amb ()

	SELECT * 
FROM productos 
WHERE id_fab = 'imm' AND existencias < 10 OR precio > 4000;

	SELECT * 
FROM productos 
WHERE id_fab = 'imm' AND ( existencias < 10 OR precio > 4000 );

###############################################################################################################################
###############################################################################################################################


-- 2.1- Els identificadors de les oficines amb la seva ciutat, els objectius i les vendes reals.

training=# select ciudad,objetivo,ventas from oficinas ;
   ciudad    | objetivo  |  ventas   
-------------+-----------+-----------
 Denver      | 300000.00 | 186042.00
 New York    | 575000.00 | 692637.00
 Chicago     | 800000.00 | 735042.00
 Atlanta     | 350000.00 | 367911.00
 Los Angeles | 725000.00 | 835915.00
(5 rows)


-- 2.2- Els identificadors de les oficines de la regió est amb la seva ciutat, els objectius i les vendes reals.

training=# select region,objetivo,ventas from oficinas where region = 'Este';
 region | objetivo  |  ventas   
--------+-----------+-----------
 Este   | 575000.00 | 692637.00
 Este   | 800000.00 | 735042.00
 Este   | 350000.00 | 367911.00
(3 rows)


-- 2.3- Les ciutats en ordre alfabètic de les oficines de la regió est amb els objectius i les vendes reals.

training=# select ciudad,region,objetivo,ventas from oficinas where region = 'Este' order by ciudad asc;
  ciudad  | region | objetivo  |  ventas   
----------+--------+-----------+-----------
 Atlanta  | Este   | 350000.00 | 367911.00
 Chicago  | Este   | 800000.00 | 735042.00
 New York | Este   | 575000.00 | 692637.00
(3 rows)


-- 2.4- Les ciutats, els objectius i les vendes d'aquelles oficines que les seves vendes superin els seus objectius.

training=# select ciudad,objetivo,ventas from oficinas where ventas > objetivo;
   ciudad    | objetivo  |  ventas   
-------------+-----------+-----------
 New York    | 575000.00 | 692637.00
 Atlanta     | 350000.00 | 367911.00
 Los Angeles | 725000.00 | 835915.00
(3 rows)


-- 2.5- Nom, quota i vendes de l'empleat representant de vendes número 107.

training=# select nombre,cuota,ventas from repventas where num_empl = 107;
    nombre     |   cuota   |  ventas   
---------------+-----------+-----------
 Nancy Angelli | 300000.00 | 186042.00
(1 row)



-- 2.6- Nom i data de contracte dels representants de vendes amb vendes superiors a 300000.

training=# select nombre,contrato from repventas where ventas > 300000;
   nombre    |  contrato  
-------------+------------
 Bill Adams  | 1988-02-12
 Mary Jones  | 1989-10-12
 Sue Smith   | 1986-12-10
 Dan Roberts | 1986-10-20
 Larry Fitch | 1989-10-12
(5 rows)


-- 2.7- Nom dels representants de vendes dirigits per l'empleat numero 104 Bob Smith.

training=# select nombre from repventas where director = 104;
   nombre    
-------------
 Bill Adams
 Dan Roberts
 Paul Cruz
(3 rows)


-- 2.8- Nom dels venedors i data de contracte d'aquells que han estat contractats abans del 1988.

training=# select nombre,contrato from repventas where contrato < '1988-01-01';
   nombre    |  contrato  
-------------+------------
 Sue Smith   | 1986-12-10
 Bob Smith   | 1987-05-19
 Dan Roberts | 1986-10-20
 Paul Cruz   | 1987-03-01
(4 rows)


-- 2.9- Identificador de les oficines i ciutat d'aquelles oficines que el seu objectiu és diferent a 800000.

training=# select oficina,ciudad from oficinas where objetivo <> 800000;
 oficina |   ciudad    
---------+-------------
      22 | Denver
      11 | New York
      13 | Atlanta
      21 | Los Angeles
(4 rows)


-- 2.10- Nom de l'empresa i limit de crèdit del client número 2107.

training=# select empresa,limite_credito from clientes where num_clie = 2107;
      empresa      | limite_credito 
-------------------+----------------
 Ace International |       35000.00
(1 row)


-- 2.11- id_fab com a "Identificador del fabricant", id_producto com a "Identificador del producte" i descripcion com a "descripció" dels productes.

training=# select id_fab as "identificador del fabricant",id_producto as "identifidor del producto",descripcion as "descripcio" from productos ;
;
 identificador del fabricant | identifidor del producto |    descripcio     
-----------------------------+--------------------------+-------------------
 rei                         | 2a45c                    | V Stago Trinquete
 aci                         | 4100y                    | Extractor
 qsa                         | xk47                     | Reductor
 bic                         | 41672                    | Plate
 imm                         | 779c                     | Riostra 2-Tm
 aci                         | 41003                    | Articulo Tipo 3
 aci                         | 41004                    | Articulo Tipo 4
 bic                         | 41003                    | Manivela
 imm                         | 887p                     | Perno Riostra
 qsa                         | xk48                     | Reductor
 rei                         | 2a44l                    | Bisagra Izqda.
 fea                         | 112                      | Cubierta
 imm                         | 887h                     | Soporte Riostra
 bic                         | 41089                    | Retn
 aci                         | 41001                    | Articulo Tipo 1
 imm                         | 775c                     | Riostra 1-Tm
 aci                         | 4100z                    | Montador
 qsa                         | xk48a                    | Reductor
 aci                         | 41002                    | Articulo Tipo 2
 rei                         | 2a44r                    | Bisagra Dcha.
 imm                         | 773c                     | Riostra 1/2-Tm
 aci                         | 4100x                    | Ajustador
 fea                         | 114                      | Bancada Motor
 imm                         | 887x                     | Retenedor Riostra
 rei                         | 2a44g                    | Pasador Bisagra
(25 rows)


-- 2.12- Identificador del fabricant, identificador del producte i descripció del producte d'aquells productes que el seu identificador de fabricant acabi amb la lletra i.

training=# select id_fab,id_producto,descripcion from productos where id_fab like '%i';
 id_fab | id_producto |    descripcion    
--------+-------------+-------------------
 rei    | 2a45c       | V Stago Trinquete
 aci    | 4100y       | Extractor
 aci    | 41003       | Articulo Tipo 3
 aci    | 41004       | Articulo Tipo 4
 rei    | 2a44l       | Bisagra Izqda.
 aci    | 41001       | Articulo Tipo 1
 aci    | 4100z       | Montador
 aci    | 41002       | Articulo Tipo 2
 rei    | 2a44r       | Bisagra Dcha.
 aci    | 4100x       | Ajustador
 rei    | 2a44g       | Pasador Bisagra
(11 rows)


-- 2.13- Nom i identificador dels venedors que estan per sota la quota i tenen vendes inferiors a 300000.

training=# select nombre,num_empl from repventas where ventas < cuota or ventas < 300000;
    nombre     | num_empl 
---------------+----------
 Sam Clark     |      106
 Bob Smith     |      104
 Tom Snyder    |      110
 Paul Cruz     |      103
 Nancy Angelli |      107
(5 rows)


-- 2.14- Identificador i nom dels venedors que treballen a les oficines 11 o 13.

training=# select num_empl,nombre from repventas where oficina_rep = 11 or oficina_rep = 13;
 num_empl |   nombre   
----------+------------
      105 | Bill Adams
      109 | Mary Jones
      106 | Sam Clark
(3 rows)


-- 2.15- Identificador, descripció i preu dels productes ordenats del més car al més barat.

training=# select id_producto,descripcion,precio from productos order by precio desc; 
 id_producto |    descripcion    | precio  
-------------+-------------------+---------
 2a44r       | Bisagra Dcha.     | 4500.00
 2a44l       | Bisagra Izqda.    | 4500.00
 4100y       | Extractor         | 2750.00
 4100z       | Montador          | 2500.00
 779c        | Riostra 2-Tm      | 1875.00
 775c        | Riostra 1-Tm      | 1425.00
 773c        | Riostra 1/2-Tm    |  975.00
 41003       | Manivela          |  652.00
 887x        | Retenedor Riostra |  475.00
 xk47        | Reductor          |  355.00
 2a44g       | Pasador Bisagra   |  350.00
 887p        | Perno Riostra     |  250.00
 114         | Bancada Motor     |  243.00
 41089       | Retn              |  225.00
 41672       | Plate             |  180.00
 112         | Cubierta          |  148.00
 xk48        | Reductor          |  134.00
 xk48a       | Reductor          |  117.00
 41004       | Articulo Tipo 4   |  117.00
 41003       | Articulo Tipo 3   |  107.00
 2a45c       | V Stago Trinquete |   79.00
 41002       | Articulo Tipo 2   |   76.00
 41001       | Articulo Tipo 1   |   55.00
 887h        | Soporte Riostra   |   54.00
 4100x       | Ajustador         |   25.00
(25 rows)


-- 2.16- Identificador i descripció de producte amb el valor d'inventari (existencies * preu).

training=# select id_producto,descripcion,precio,existencias * precio as inventario from productos;
 id_producto |    descripcion    | precio  | inventario 
-------------+-------------------+---------+------------
 2a45c       | V Stago Trinquete |   79.00 |   16590.00
 4100y       | Extractor         | 2750.00 |   68750.00
 xk47        | Reductor          |  355.00 |   13490.00
 41672       | Plate             |  180.00 |       0.00
 779c        | Riostra 2-Tm      | 1875.00 |   16875.00
 41003       | Articulo Tipo 3   |  107.00 |   22149.00
 41004       | Articulo Tipo 4   |  117.00 |   16263.00
 41003       | Manivela          |  652.00 |    1956.00
 887p        | Perno Riostra     |  250.00 |    6000.00
 xk48        | Reductor          |  134.00 |   27202.00
 2a44l       | Bisagra Izqda.    | 4500.00 |   54000.00
 112         | Cubierta          |  148.00 |   17020.00
 887h        | Soporte Riostra   |   54.00 |   12042.00
 41089       | Retn              |  225.00 |   17550.00
 41001       | Articulo Tipo 1   |   55.00 |   15235.00
 775c        | Riostra 1-Tm      | 1425.00 |    7125.00
 4100z       | Montador          | 2500.00 |   70000.00
 xk48a       | Reductor          |  117.00 |    4329.00
 41002       | Articulo Tipo 2   |   76.00 |   12692.00
 2a44r       | Bisagra Dcha.     | 4500.00 |   54000.00
 773c        | Riostra 1/2-Tm    |  975.00 |   27300.00
 4100x       | Ajustador         |   25.00 |     925.00
 114         | Bancada Motor     |  243.00 |    3645.00
 887x        | Retenedor Riostra |  475.00 |   15200.00
 2a44g       | Pasador Bisagra   |  350.00 |    4900.00
(25 rows)


-- 2.17- Vendes de cada oficina en una sola columna i format amb format "<ciutat> te unes vendes de <vendes>", exemple "Denver te unes vendes de 186042.00".

training=# select ciudad || 'te unes vendes de' || ventas from oficinas ;
               ?column?                
---------------------------------------
 Denverte unes vendes de186042.00
 New Yorkte unes vendes de692637.00
 Chicagote unes vendes de735042.00
 Atlantate unes vendes de367911.00
 Los Angeleste unes vendes de835915.00
(5 rows)


-- 2.18- Codis d'empleats que són directors d'oficines.

training=# select num_empl from repventas where titulo = 'Dir Ventas';
 num_empl 
----------
      104
      108
(2 rows)


-- 2.19- Identificador i ciutat de les oficines que tinguin ventes per sota el 80% del seu objectiu.

training=# select oficina,ciudad from oficinas where ventas < objetivo*80/100; 
 oficina | ciudad 
---------+--------
      22 | Denver
(1 row)


-- 2.20- Identificador, ciutat i director de les oficines que no siguin dirigides per l'empleat 108.
training=# select oficina,ciudad,dir from oficinas where dir <> 108 ;
 oficina |  ciudad  | dir 
---------+----------+-----
      11 | New York | 106
      12 | Chicago  | 104
      13 | Atlanta  | 105
(3 rows)



-- 2.21- Identificadors i noms dels venedors amb vendes entre el 80% i el 120% de llur quota.


training=# select num_empl,nombre from repventas where ventas between (cuota*0.8) and (cuota*1.2);
 num_empl |   nombre    
----------+-------------
      105 | Bill Adams
      106 | Sam Clark
      101 | Dan Roberts
      108 | Larry Fitch
      103 | Paul Cruz
(5 rows)

-- 2.22- Identificador, vendes i ciutat de cada oficina ordenades alfabèticament per regió i, dintre de cada regió ordenades per ciutat.

training=# select oficina,ventas,ciudad from oficinas order by region,ciudad;
 oficina |  ventas   |   ciudad    
---------+-----------+-------------
      13 | 367911.00 | Atlanta
      12 | 735042.00 | Chicago
      11 | 692637.00 | New York
      22 | 186042.00 | Denver
      21 | 835915.00 | Los Angeles
(5 rows)


-- 2.23- Llista d'oficines classificades alfabèticament per regió i, per cada regió, en ordre descendent de rendiment de vendes (vendes-objectiu).


training=# select region,ventas,objetivo,(ventas-objetivo) as rendiment_ventas from oficinas order by region,rendiment_ventas desc;
 region |  ventas   | objetivo  | rendiment_ventas 
--------+-----------+-----------+------------------
 Este   | 692637.00 | 575000.00 |        117637.00
 Este   | 367911.00 | 350000.00 |         17911.00
 Este   | 735042.00 | 800000.00 |        -64958.00
 Oeste  | 835915.00 | 725000.00 |        110915.00
 Oeste  | 186042.00 | 300000.00 |       -113958.00
(5 rows)


-- 2.24- Codi i nom dels tres venedors que tinguin unes vendes superiors.

training=# select num_empl,nombre from repventas order by ventas desc limit 3;
 num_empl |   nombre   
----------+------------
      102 | Sue Smith
      109 | Mary Jones
      105 | Bill Adams
(3 rows)


-- 2.25- Nom i data de contracte dels empleats que les seves vendes siguin superiors a 500000.

training=# select nombre,contrato from repventas where ventas > 500000;
 nombre | contrato 
--------+----------
(0 rows)



-- 2.26- Nom i quota actual dels venedors amb el calcul d'una "nova possible quota" que serà la quota de cada venedor augmentada un 3 per cent de les seves propies vendes.

training=# select nombre,cuota,(cuota + (ventas*0.03)) as nova_cuota from repventas; 
    nombre     |   cuota   | nova_cuota  
---------------+-----------+-------------
 Bill Adams    | 350000.00 | 361037.3300
 Mary Jones    | 300000.00 | 311781.7500
 Sue Smith     | 350000.00 | 364221.5000
 Sam Clark     | 275000.00 | 283997.3600
 Bob Smith     | 200000.00 | 204277.8200
 Dan Roberts   | 300000.00 | 309170.1900
 Tom Snyder    |           |            
 Larry Fitch   | 350000.00 | 360855.9500
 Paul Cruz     | 275000.00 | 283603.2500
 Nancy Angelli | 300000.00 | 305581.2600
(10 rows)



-- 2.27- Identificador i nom de les oficines que les seves vendes estan per sota del 80% de l'objectiu.

training=# select oficina,ciudad from oficinas where ventas < (objetivo*0.80);
 oficina | ciudad 
---------+--------
      22 | Denver
(1 row)


-- 2.28- Numero i import de les comandes que el seu import oscil·li entre 20000 i 29999.

training=# select num_pedido,importe from pedidos where importe between 20000 and 29999;
 num_pedido | importe  
------------+----------
     110036 | 22500.00
     112987 | 27500.00
     113042 | 22500.00
(3 rows)


-- 2.29- Nom, ventes i quota dels venedors que les seves vendes no estan entre el 80% i el 120% de la seva quota.

training=# select nombre,ventas,cuota from repventas where ventas not between (cuota*0.8) and (cuota*1.2);
    nombre     |  ventas   |   cuota   
---------------+-----------+-----------
 Mary Jones    | 392725.00 | 300000.00
 Sue Smith     | 474050.00 | 350000.00
 Bob Smith     | 142594.00 | 200000.00
 Nancy Angelli | 186042.00 | 300000.00
(4 rows)


-- 2.30- Nom de l'empresa i el seu limit de crèdit, de les empreses que el seu nom comença per Smith.

training=# select empresa,limite_credito from clientes where empresa like 'Smith%';
    empresa     | limite_credito 
----------------+----------------
 Smithson Corp. |       20000.00
(1 row)


-- 2.31- Identificador i nom dels venedors que no tenen assignada oficina.

training=# select num_empl,nombre from repventas where oficina_rep is null;
 num_empl |   nombre   
----------+------------
      110 | Tom Snyder
(1 row)


-- 2.32- Identificador i nom dels venedors, amb l'identificador de l'oficina d'aquells venedors que tenen una oficina assignada.

training=# select num_empl,nombre from repventas where oficina_rep is not null;
 num_empl |    nombre     
----------+---------------
      105 | Bill Adams
      109 | Mary Jones
      102 | Sue Smith
      106 | Sam Clark
      104 | Bob Smith
      101 | Dan Roberts
      108 | Larry Fitch
      103 | Paul Cruz
      107 | Nancy Angelli
(9 rows)
training=# select num_empl,nombre from repventas where oficina_rep in (11,12,22) and (contrato > '1988-06-01' and director is null) ; 
num_empl |  nombre   
----------+-----------
      106 | Sam Clark
(1 row)


-- 2.33- Identificador i descripció dels productes del fabricant identificat per imm dels quals hi hagin existències superiors o iguals 200, també del fabricant bic amb existències superiors o iguals a 50.

training=# select id_fab,descripcion from productos where (id_fab = 'imm' and existencias >= 200) or ( id_fab = 'bic' and existencias >= 50);
 id_fab |   descripcion   
--------+-----------------
 imm    | Soporte Riostra
 bic    | Retn
(2 rows)



-- 2.34- Identificador i nom dels venedors que treballen a les oficines 11, 12 o 22 i compleixen algun dels següents supòsits:
-- a) han estat contractats a partir de juny del 1988 i no tenen director
-- b) estan per sobre la quota però tenen vendes de 600000 o menors.

(a)
training=# select num_empl,nombre from repventas where oficina_rep in (11,12,22) and (contrato > '1988-06-01' and director is null) ; 
num_empl |  nombre   
----------+-----------
      106 | Sam Clark
(1 row)


(b)

training=# select num_empl,nombre from repventas where oficina_rep in (11,12,22) and (ventas > cuota and ventas < 600000);
 num_empl |   nombre    
----------+-------------
      109 | Mary Jones
      106 | Sam Clark
      101 | Dan Roberts
      103 | Paul Cruz
(4 rows)

					or
training=# select num_empl,nombre from repventas where oficina_rep in (11,12,22) and ((contrato > '1988-06-01' and director is null) or (ventas > cuota and ventas < 600000));
 num_empl |   nombre    
----------+-------------
      109 | Mary Jones
      106 | Sam Clark
      101 | Dan Roberts
      103 | Paul Cruz
(4 rows)



-- 2.35- Identificador i descripció dels productes amb un preu superior a 1000 i siguin del fabricant amb identificador rei o les existències siguin superiors a 20.

training=# select id_fab,descripcion from productos where (precio > 1000) and (id_fab = 'rei' or existencias > 20);
 id_fab |  descripcion   
--------+----------------
 aci    | Extractor
 rei    | Bisagra Izqda.
 aci    | Montador
 rei    | Bisagra Dcha.
(4 rows)


-- 2.36- Identificador del fabricant,identificador i descripció dels productes amb fabricats pels fabricants que tenen una lletra qualsevol, una lletra 'i' i una altre lletra qualsevol com a identificador de fabricant.

training=# select id_fab,id_producto,descripcion from productos where id_fab like '_i_';
 id_fab | id_producto | descripcion 
--------+-------------+-------------
 bic    | 41672       | Plate
 bic    | 41003       | Manivela
 bic    | 41089       | Retn
(3 rows)


-- 2.37- Identificador i descripció dels productes que la seva descripció comenÃ§a per "art" sense tenir en compte les majúscules i minúscules.

training=# select id_producto,descripcion from productos where descripcion ilike 'art%';
 id_producto |   descripcion   
-------------+-----------------
 41003       | Articulo Tipo 3
 41004       | Articulo Tipo 4
 41001       | Articulo Tipo 1
 41002       | Articulo Tipo 2
(4 rows)


-- 2.38- Identificador i nom dels clients que la segona lletra del nom sigui una "a" minúscula o majuscula.


training=# select num_empl,nombre from repventas where nombre ilike '_a%';
 num_empl |    nombre     
----------+---------------
      109 | Mary Jones
      106 | Sam Clark
      101 | Dan Roberts
      108 | Larry Fitch
      103 | Paul Cruz
      107 | Nancy Angelli
(6 rows)

-- 2.39- Identificador i ciutat de les oficines que compleixen algun dels següents supòsits:
-- a) És de la regió est amb unes vendes inferiors a 700000.
-- b) És de la regió oest amb unes vendes inferiors a 600000.

(a)
training=# select oficina,ciudad from oficinas where region = 'Este' and ventas < 700000;
 oficina |  ciudad  
---------+----------
      11 | New York
      13 | Atlanta
(2 rows)

(b)
training=# select oficina,ciudad from oficinas where region = 'Oeste' and ventas < 600000;
 oficina | ciudad 
---------+--------
      22 | Denver
(1 row)
					or
					
training=# select oficina,ciudad from oficinas where (region = 'Este' and ventas < 700000) or (region = 'Oeste' and ventas < 600000);
 oficina |  ciudad  
---------+----------
      22 | Denver
      11 | New York
      13 | Atlanta
(3 rows)

					

-- 2.40- Identificador del fabricant, identificació i descripció dels productes que compleixen tots els següents supòsits:
-- a) L'identificador del fabricant és "imm" o el preu és menor a 500.
-- b) Les existències són inferiors a 5 o el producte te l'identificador 41003.  


training=# select id_fab,id_producto,descripcion from productos where (id_fab = 'imm' or precio < 500) and (existencias > 5 or id_producto = '41003');
 id_fab | id_producto |    descripcion    
--------+-------------+-------------------
 rei    | 2a45c       | V Stago Trinquete
 qsa    | xk47        | Reductor
 imm    | 779c        | Riostra 2-Tm
 aci    | 41003       | Articulo Tipo 3
 aci    | 41004       | Articulo Tipo 4
 imm    | 887p        | Perno Riostra
 qsa    | xk48        | Reductor
 fea    | 112         | Cubierta
 imm    | 887h        | Soporte Riostra
 bic    | 41089       | Retn
 aci    | 41001       | Articulo Tipo 1
 qsa    | xk48a       | Reductor
 aci    | 41002       | Articulo Tipo 2
 imm    | 773c        | Riostra 1/2-Tm
 aci    | 4100x       | Ajustador
 fea    | 114         | Bancada Motor
 imm    | 887x        | Retenedor Riostra
 rei    | 2a44g       | Pasador Bisagra
(18 rows)


-- 2.41- Identificador de les comandes del fabricant amb identificador "rei" amb una quantitat superior o igual a 10 o amb un import superior o igual a 10000.


training=# select num_pedido from pedidos where fab = 'rei' and (cant >= 10 or importe >= 10000);
 num_pedido 
------------
     112961
     113045
     112993
     113042
(4 rows)



-- 2.42- Data de les comandes amb una quantitat superior a 20 i un import superior a 1000 dels clients 2102, 2106 i 2109.

training=# select fecha_pedido from pedidos where cant > 20 and importe > 1000 and (clie in (2102,2106,2109));
 fecha_pedido 
--------------
 1989-10-12
 1990-03-02
 1989-01-04
(3 rows)


-- 2.43- Identificador dels clients que el seu nom no conté " Corp." o " Inc." amb crèdit major a 30000.

training=# select num_clie from clientes where not (empresa ilike '%corp.%' or empresa ilike '%inc.%') and limite_credito > 30000 ; 
 num_clie 
----------
     2103
     2123
     2107
     2101
     2112
     2121
     2124
     2108
     2117
     2120
     2118
     2105
(12 rows)


-- 2.44- Identificador dels representants de vendes majors de 40 anys amb vendes inferiors a 400000.


training=# select num_empl from repventas where edad > 40 and ventas < 400000;
 num_empl 
----------
      106
      101
      110
      108
      107
(5 rows)

-- 2.45- Identificador dels representants de vendes menors de 35 anys amb vendes superiors a 350000.

training=# select num_empl from repventas where edad < 35 and ventas > 350000;
 num_empl 
----------
      109
(1 row)

---2.46 producto que tinguin una 'a' al codi i que sigain del fabricant 'fea'.
training=# select * from productos where id_fab = 'fea' and id_producto ilike '%a%';
 id_fab | id_producto | descripcion | precio | existencias 
--------+-------------+-------------+--------+-------------
(0 rows)

---2.47 producto del fabricante 'imm' o del fabricant 'fea'.

training=# select * from productos where id_fab = 'imm' or id_fab = 'fea';
 id_fab | id_producto |    descripcion    | precio  | existencias 
--------+-------------+-------------------+---------+-------------
 imm    | 779c        | Riostra 2-Tm      | 1875.00 |           9
 imm    | 887p        | Perno Riostra     |  250.00 |          24
 fea    | 112         | Cubierta          |  148.00 |         115
 imm    | 887h        | Soporte Riostra   |   54.00 |         223
 imm    | 775c        | Riostra 1-Tm      | 1425.00 |           5
 imm    | 773c        | Riostra 1/2-Tm    |  975.00 |          28
 fea    | 114         | Bancada Motor     |  243.00 |          15
 imm    | 887x        | Retenedor Riostra |  475.00 |          32
(8 rows)


---2.48 producto que tinguin una 'c' al codi i que siquin del fabridcant 'fea' o del fabricant 'imm'.
training=# select * from productos where id_producto like '%c%' and (id_fab = 'fea' or id_fab = 'imm');
 id_fab | id_producto |  descripcion   | precio  | existencias 
--------+-------------+----------------+---------+-------------
 imm    | 779c        | Riostra 2-Tm   | 1875.00 |           9
 imm    | 775c        | Riostra 1-Tm   | 1425.00 |           5
 imm    | 773c        | Riostra 1/2-Tm |  975.00 |          28
(3 rows)
