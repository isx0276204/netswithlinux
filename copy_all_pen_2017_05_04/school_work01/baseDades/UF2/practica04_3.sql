Consultes simples o monotaula

-- 4.1- Quina és la quota promig mostrada com a "prom_cuota" i la venda promig mostrades com a "prom_ventas" dels venedors?

training=# select avg(cuota) as prom_mig,avg(ventas) as prom_mig from repventas ;
      prom_mig       |      prom_mig       
---------------------+---------------------
 300000.000000000000 | 289353.200000000000
(1 row)


-- 4.2- Quin és el rendiment de quota promig dels venedors (percentatge de les vendes respecte la quota)?

training=# select (sum(ventas)*100)/sum(cuota) as prom_mig from repventas ;
       prom_mig       
----------------------
 107.1678518518518519
(1 row)


-- 4.3- Quines són les quotes totals com a "t_cuota" i vendes totals com a "t_ventas" de tots els venedors?

training=# select sum(cuota) as t_cuota,sum(ventas) as t_ventas from repventas ;
  t_cuota   |  t_ventas  
------------+------------
 2700000.00 | 2893532.00
(1 row)


-- 4.4- Calcula el preu mig dels productes del fabricant amb identificador "aci".

training=# select avg(precio) from productos where id_fab = 'aci';
         avg          
----------------------
 804.2857142857142857
(1 row)


-- 4.5- Quines són les quotes assignades mínima i màxima?

training=# select min(cuota),max(cuota) from repventas ;
    min    |    max    
-----------+-----------
 200000.00 | 350000.00
(1 row)


-- 4.6- Quina és la data de comanda més antiga?

training=# select min(fecha_pedido) from pedidos ;
    min     
------------
 1989-01-04
(1 row)



-- 4.7- Quin és el major rendiment de vendes de tots els venedors?


training=# select max(ventas/cuota) from repventas ;
        max         
--------------------
 1.3544285714285714
(1 row)


-- 4.8- Quants clients hi ha?

training=# select count(num_clie) from clientes ;
 count 
-------
    21
(1 row)

						or
training=# select count(distinct num_clie) from clientes ;
 count 
-------
    21
(1 row)


-- 4.9- Quants venedors superen la seva quota?

training=# select count(num_empl) from repventas  where ventas > cuota;
 count 
-------
     7
(1 row)


-- 4.10- Quantes comandes de més de 25000 hi ha en els registres?

training=# select count(importe) from pedidos where importe > 25000;
 count 
-------
     4
(1 row)


-- 4.11-



-- 4.12- Trobar l'import mitjà de les comandes, l'import total de les comandes, l'import mitjà de les comandes com a percentatge del límit de crèdit del client i l'import mitjà de comandes com a percentatge de la quota del venedor.

--- no doing

-- 4.13- Compta les files que hi ha a repventas, les files del camp vendes i les del camp quota.

training=# select count(*),count(cuota),count(ventas) from repventas ;
 count | count | count 
-------+-------+-------
    10 |     9 |    10
(1 row)


-- 4.14- Mostra que la suma de restar (vendes menys quota) és diferent que sumar vendes i restar-li la suma de quotes.

training=# select sum(ventas - cuota),sum(ventas) -sum(cuota) from repventas; 
    sum    | ?column?  
-----------+-----------
 117547.00 | 193532.00
(1 row)


-- 4.15- Quants títols diferents tenen els venedors?

training=# select count(distinct titulo) from repventas ;
 count 
-------
     3
(1 row)



-- 4.16- Quantes oficines de vendes tenen venedors que superen les seves quotes?


training=# select count(distinct oficina_rep) from repventas where ventas > cuota;
 count 
-------
     4
(1 row)


-- 4.17- Seleccionar de la taula clients quants clients diferents i venedors diferents hi ha.

training=# select count(distinct rep_clie) from clientes ;
 count 
-------
    10
(1 row)
					or

training=# select count(distinct num_clie),count(distinct rep_clie) from clientes ;
 count | count 
-------+-------
    21 |    10
(1 row)

-- 4.18- De la taula comandes seleccionar quantes comandes diferents i clients diferents hi ha

training=# select count(distinct clie) from pedidos;
 count 
-------
    15
(1 row)
					or
training=# select count(*),count(distinct clie) from pedidos;
 count | count 
-------+-------
    30 |    15
(1 row)


-- 4.19- Calcular la mitjana dels imports de les comandes.

training=# select avg(importe/cant) from pedidos ;
          avg          
-----------------------
 1056.9666666666666667
(1 row)


--- 4.20- Calcula la mitjana de l'import d'una comanda realitzada pel client amb codi 2120.

training=# select round(avg(importe),2) from pedidos where clie > 2120;
  round  
---------
 1541.00
(1 row)


-- 4.21- Quina és la comanda promig de cada venedor?

training=# select rep,avg(importe) from pedidos group by rep;
 rep |          avg           
-----+------------------------
 106 |     16479.000000000000
 107 | 11477.3333333333333333
 102 |  5694.0000000000000000
 108 |  8376.1428571428571429
 105 |  7865.4000000000000000
 109 |  3552.5000000000000000
 103 |  1350.0000000000000000
 101 |  8876.0000000000000000
 110 | 11566.0000000000000000
(9 rows)


-- 4.22- Quin és el rang (màxim i mínim) de quotes dels venedors per cada oficina?

training=# select oficina_rep,min(cuota),max(cuota) from repventas group by oficina_rep;
 oficina_rep |    min    |    max    
-------------+-----------+-----------
             |           |          
          12 | 200000.00 | 300000.00
          21 | 350000.00 | 350000.00
          11 | 275000.00 | 300000.00
          13 | 350000.00 | 350000.00
          22 | 300000.00 | 300000.00
(6 rows)


-- 4.23- Quants venedors estan asignats a cada oficina?

training=# select oficina_rep,count(num_empl) from repventas group by oficina_rep;
 oficina_rep | count 
-------------+-------
             |     1
          12 |     3
          21 |     2
          11 |     2
          13 |     1
          22 |     111
(6 rows)


-- 4.24- Per cada venedor calcular quants clients diferents ha atès (ha fet comandes)?

training=# select rep_clie,count(num_clie) from clientes group by rep_clie;
 rep_clie | count 
----------+-------
      106 |     2
      107 |     1
      104 |     1
      102 |     4
      108 |     2
      105 |     2
      109 |     2
      103 |     3
      101 |     3
      110 |     1
(10 rows)

					right
training=# select rep, count(distinct clie) from pedidos group by rep;
 rep | count 
-----+-------
 101 |     3
 102 |     3
 103 |     1
 105 |     2
 106 |     2
 107 |     2
 108 |     3
 109 |     1
 110 |     1
(9 rows)

-- 4.25- Calcula el total dels imports de les comandes fetes per cada client a cada venedor.

training=# select clie,rep,sum(importe) from pedidos group by clie,rep;
 clie | rep |   sum    
------+-----+----------
 2111 | 105 |  3745.00
 2124 | 107 |  3082.00
 2111 | 103 |  2700.00
 2113 | 101 | 22500.00
 2108 | 101 |   150.00
 2120 | 102 |  3750.00
 2106 | 102 |  4026.00
 2117 | 106 | 31500.00
 2103 | 105 | 35582.00
 2108 | 109 |  7105.00
 2114 | 102 | 15000.00
 2109 | 107 | 31350.00
 2114 | 108 |  7100.00
 2107 | 110 | 23132.00
 2101 | 106 |  1458.00
 2118 | 108 |  3608.00
 2102 | 101 |  3978.00
 2112 | 108 | 47925.00
(18 rows)


-- 4.26- El mateix que a la qüestió anterior, però ordenat per client i dintre de client per venedor.

training=# select clie,rep,sum(importe) from pedidos group by clie,rep order by clie,rep;
 clie | rep |   sum    
------+-----+----------
 2101 | 106 |  1458.00
 2102 | 101 |  3978.00
 2103 | 105 | 35582.00
 2106 | 102 |  4026.00
 2107 | 110 | 23132.00
 2108 | 101 |   150.00
 2108 | 109 |  7105.00
 2109 | 107 | 31350.00
 2111 | 103 |  2700.00
 2111 | 105 |  3745.00
 2112 | 108 | 47925.00
 2113 | 101 | 22500.00
 2114 | 102 | 15000.00
 2114 | 108 |  7100.00
 2117 | 106 | 31500.00
 2118 | 108 |  3608.00
 2120 | 102 |  3750.00
 2124 | 107 |  3082.00
(18 rows)


-- 4.27- Calcula les comandes totals per a cada venedor.

training=# select rep,sum(importe) from pedidos group by rep;
 rep |   sum    
-----+----------
 106 | 32958.00
 107 | 34432.00
 102 | 22776.00
 108 | 58633.00
 105 | 39327.00
 109 |  7105.00
 103 |  2700.00
 101 | 26628.00
 110 | 23132.00
(9 rows)


-- 4.28- Quin és l'import promig de les comandes per cada venedor que les seves comandes sumen més de 30000?

training=# select rep,avg(importe) from pedidos group by rep having sum(importe) > 30000;
 rep |          avg           
-----+------------------------
 106 |     16479.000000000000
 107 | 11477.3333333333333333
 108 |  8376.1428571428571429
 105 |  7865.4000000000000000
(4 rows)



-- 4.29- Per cada oficina amb dos o més empleats, calcular la quota total i les vendes totals per a tots els venedors que treballen a la oficina .


training=# select oficina_rep,count(num_empl),sum(cuota),sum(ventas) from repventas where oficina_rep is not null group by oficina_rep having count(num_empl) >= 2;
 oficina_rep | count |    sum    |    sum    
-------------+-------+-----------+-----------
          12 |     3 | 775000.00 | 735042.00
          21 |     2 | 700000.00 | 835915.00
          11 |     2 | 575000.00 | 692637.00
(3 rows)

no-- 4.30- Mostra el preu, les existències i la quantitat total de les comandes de cada producte per als quals la quantitat total demanada està per sobre del 75% de les existències.



-- 4.31- Es desitja un llistat d'identificadors de fabricants de productes. Només volem tenir en compte els productes de preu superior a 54. Només volem que apareguin els fabricants amb un nombre total d'unitats superior a 300.

training=# select id_fab,sum(existencias) from productos where precio > 54 group by id_fab having sum(existencias) > 300;
 id_fab | sum 
--------+-----
 aci    | 843
(1 row)
