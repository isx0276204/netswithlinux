------5
training=# select pedidos.num_pedido,pedidos.importe,pedidos.fab,pedidos.producto,productos.descripcion,clientes.empresa from pedidos,productos,clientes where pedidos.fab = productos.id_fab and pedidos.producto = productos.id_producto and pedidos.clie =clientes.num_clie order by productos.descripcion;
 num_pedido | importe  | fab | producto |    descripcion    |      empresa      
------------+----------+-----+----------+-------------------+-------------------
     113057 |   600.00 | aci | 4100x    | Ajustador         | JCP Inc.
     113055 |   150.00 | aci | 4100x    | Ajustador         | Holm & Landis
     113027 |  4104.00 | aci | 41002    | Articulo Tipo 2   | Acme Mfg.
     112992 |   760.00 | aci | 41002    | Articulo Tipo 2   | Midwest Systems
     113012 |  3745.00 | aci | 41003    | Articulo Tipo 3   | JCP Inc.
     112983 |   702.00 | aci | 41004    | Articulo Tipo 4   | Acme Mfg.
     112963 |  3276.00 | aci | 41004    | Articulo Tipo 4   | Acme Mfg.
     112968 |  3978.00 | aci | 41004    | Articulo Tipo 4   | First Corp.
     112989 |  1458.00 | fea | 114      | Bancada Motor     | Jones Mfg.
     113062 |  2430.00 | fea | 114      | Bancada Motor     | Peter Brothers
     113042 | 22500.00 | rei | 2a44r    | Bisagra Dcha.     | Ian & Schmidt
     113045 | 45000.00 | rei | 2a44r    | Bisagra Dcha.     | Zetacorp
     112961 | 31500.00 | rei | 2a44l    | Bisagra Izqda.    | J.P. Sinclair
     113058 |  1480.00 | fea | 112      | Cubierta          | Holm & Landis
     112987 | 27500.00 | aci | 4100y    | Extractor         | Acme Mfg.
     113013 |   652.00 | bic | 41003    | Manivela          | Midwest Systems
     112997 |   652.00 | bic | 41003    | Manivela          | Peter Brothers
     112979 | 15000.00 | aci | 4100z    | Montador          | Orion Corp
     110036 | 22500.00 | aci | 4100z    | Montador          | Ace International
     112975 |  2100.00 | rei | 2a44g    | Pasador Bisagra   | JCP Inc.
     113065 |  2130.00 | qsa | xk47     | Reductor          | Fred Lewis Corp.
     113024 |  7100.00 | qsa | xk47     | Reductor          | Orion Corp
     113049 |   776.00 | qsa | xk47     | Reductor          | Midwest Systems
     113007 |  2925.00 | imm | 773c     | Riostra 1/2-Tm    | Zetacorp
     113069 | 31350.00 | imm | 775c     | Riostra 1-Tm      | Chen Associates
     113003 |  5625.00 | imm | 779c     | Riostra 2-Tm      | Holm & Landis
     113048 |  3750.00 | imm | 779c     | Riostra 2-Tm      | Rico Enterprises
     113034 |   632.00 | rei | 2a45c    | V Stago Trinquete | Ace International
     112993 |  1896.00 | rei | 2a45c    | V Stago Trinquete | Fred Lewis Corp.
(29 rows)


-----4
training=# select pedidos.num_pedido,pedidos.importe,pedidos.fab,pedidos.producto,productos.descripcion from pedidos,productos where pedidos.fab = productos.id_fab and pedidos.producto = productos.id_producto order by productos.descripcion; 
num_pedido | importe  | fab | producto |    descripcion    
------------+----------+-----+----------+-------------------
     113057 |   600.00 | aci | 4100x    | Ajustador
     113055 |   150.00 | aci | 4100x    | Ajustador
     113027 |  4104.00 | aci | 41002    | Articulo Tipo 2
     112992 |   760.00 | aci | 41002    | Articulo Tipo 2
     113012 |  3745.00 | aci | 41003    | Articulo Tipo 3
     112983 |   702.00 | aci | 41004    | Articulo Tipo 4
     112963 |  3276.00 | aci | 41004    | Articulo Tipo 4
     112968 |  3978.00 | aci | 41004    | Articulo Tipo 4
     112989 |  1458.00 | fea | 114      | Bancada Motor
     113062 |  2430.00 | fea | 114      | Bancada Motor
     113042 | 22500.00 | rei | 2a44r    | Bisagra Dcha.
     113045 | 45000.00 | rei | 2a44r    | Bisagra Dcha.
     112961 | 31500.00 | rei | 2a44l    | Bisagra Izqda.
     113058 |  1480.00 | fea | 112      | Cubierta
     112987 | 27500.00 | aci | 4100y    | Extractor
     113013 |   652.00 | bic | 41003    | Manivela
     112997 |   652.00 | bic | 41003    | Manivela
     112979 | 15000.00 | aci | 4100z    | Montador
     110036 | 22500.00 | aci | 4100z    | Montador
     112975 |  2100.00 | rei | 2a44g    | Pasador Bisagra
     113065 |  2130.00 | qsa | xk47     | Reductor
     113024 |  7100.00 | qsa | xk47     | Reductor
     113049 |   776.00 | qsa | xk47     | Reductor
     113007 |  2925.00 | imm | 773c     | Riostra 1/2-Tm
     113069 | 31350.00 | imm | 775c     | Riostra 1-Tm
     113003 |  5625.00 | imm | 779c     | Riostra 2-Tm
     113048 |  3750.00 | imm | 779c     | Riostra 2-Tm
     113034 |   632.00 | rei | 2a45c    | V Stago Trinquete
     112993 |  1896.00 | rei | 2a45c    | V Stago Trinquete
(29 rows)


-------3
training=# select pedidos.num_pedido,pedidos.importe,pedidos.clie,pedidos.rep,repventas.nombre from pedidos,repventas where rep = num_empl order by repventas.nombre,pedidos.clie;
 num_pedido | importe  | clie | rep |    nombre     
------------+----------+------+-----+---------------
     112983 |   702.00 | 2103 | 105 | Bill Adams
     112987 | 27500.00 | 2103 | 105 | Bill Adams
     112963 |  3276.00 | 2103 | 105 | Bill Adams
     113027 |  4104.00 | 2103 | 105 | Bill Adams
     113012 |  3745.00 | 2111 | 105 | Bill Adams
     112968 |  3978.00 | 2102 | 101 | Dan Roberts
     113055 |   150.00 | 2108 | 101 | Dan Roberts
     113042 | 22500.00 | 2113 | 101 | Dan Roberts
     113045 | 45000.00 | 2112 | 108 | Larry Fitch
     113007 |  2925.00 | 2112 | 108 | Larry Fitch
     113024 |  7100.00 | 2114 | 108 | Larry Fitch
     113013 |   652.00 | 2118 | 108 | Larry Fitch
     112992 |   760.00 | 2118 | 108 | Larry Fitch
     113051 |  1420.00 | 2118 | 108 | Larry Fitch
     113049 |   776.00 | 2118 | 108 | Larry Fitch
     113058 |  1480.00 | 2108 | 109 | Mary Jones
     113003 |  5625.00 | 2108 | 109 | Mary Jones
     113069 | 31350.00 | 2109 | 107 | Nancy Angelli
     112997 |   652.00 | 2124 | 107 | Nancy Angelli
     113062 |  2430.00 | 2124 | 107 | Nancy Angelli
     113057 |   600.00 | 2111 | 103 | Paul Cruz
     112975 |  2100.00 | 2111 | 103 | Paul Cruz
     112989 |  1458.00 | 2101 | 106 | Sam Clark
     112961 | 31500.00 | 2117 | 106 | Sam Clark
     112993 |  1896.00 | 2106 | 102 | Sue Smith
     113065 |  2130.00 | 2106 | 102 | Sue Smith
     112979 | 15000.00 | 2114 | 102 | Sue Smith
     113048 |  3750.00 | 2120 | 102 | Sue Smith
     110036 | 22500.00 | 2107 | 110 | Tom Snyder
     113034 |   632.00 | 2107 | 110 | Tom Snyder
(30 rows)

------2
training=# select pedidos.num_pedido,pedidos.importe,pedidos.clie,pedidos.rep,repventas.nombre from pedidos,repventas where rep = num_empl order by repventas.nombre;
 num_pedido | importe  | clie | rep |    nombre     
------------+----------+------+-----+---------------
     112983 |   702.00 | 2103 | 105 | Bill Adams
     112963 |  3276.00 | 2103 | 105 | Bill Adams
     112987 | 27500.00 | 2103 | 105 | Bill Adams
     113027 |  4104.00 | 2103 | 105 | Bill Adams
     113012 |  3745.00 | 2111 | 105 | Bill Adams
     113042 | 22500.00 | 2113 | 101 | Dan Roberts
     112968 |  3978.00 | 2102 | 101 | Dan Roberts
     113055 |   150.00 | 2108 | 101 | Dan Roberts
     113045 | 45000.00 | 2112 | 108 | Larry Fitch
     113024 |  7100.00 | 2114 | 108 | Larry Fitch
     113051 |  1420.00 | 2118 | 108 | Larry Fitch
     113007 |  2925.00 | 2112 | 108 | Larry Fitch
     112992 |   760.00 | 2118 | 108 | Larry Fitch
     113049 |   776.00 | 2118 | 108 | Larry Fitch
     113013 |   652.00 | 2118 | 108 | Larry Fitch
     113003 |  5625.00 | 2108 | 109 | Mary Jones
     113058 |  1480.00 | 2108 | 109 | Mary Jones
     113062 |  2430.00 | 2124 | 107 | Nancy Angelli
     113069 | 31350.00 | 2109 | 107 | Nancy Angelli
     112997 |   652.00 | 2124 | 107 | Nancy Angelli
     112975 |  2100.00 | 2111 | 103 | Paul Cruz
     113057 |   600.00 | 2111 | 103 | Paul Cruz
     112961 | 31500.00 | 2117 | 106 | Sam Clark
     112989 |  1458.00 | 2101 | 106 | Sam Clark
     112979 | 15000.00 | 2114 | 102 | Sue Smith
     113048 |  3750.00 | 2120 | 102 | Sue Smith
     112993 |  1896.00 | 2106 | 102 | Sue Smith
     113065 |  2130.00 | 2106 | 102 | Sue Smith
     110036 | 22500.00 | 2107 | 110 | Tom Snyder
     113034 |   632.00 | 2107 | 110 | Tom Snyder
(30 rows)

----1
training=# select pedidos.num_pedido,pedidos.importe,clientes.num_clie,clientes.empresa from clientes,pedidos where num_clie = clie order by clientes.empresa;
 num_pedido | importe  | num_clie |      empresa      
------------+----------+----------+-------------------
     113034 |   632.00 |     2107 | Ace International
     110036 | 22500.00 |     2107 | Ace International
     112963 |  3276.00 |     2103 | Acme Mfg.
     112987 | 27500.00 |     2103 | Acme Mfg.
     113027 |  4104.00 |     2103 | Acme Mfg.
     112983 |   702.00 |     2103 | Acme Mfg.
     113069 | 31350.00 |     2109 | Chen Associates
     112968 |  3978.00 |     2102 | First Corp.
     112993 |  1896.00 |     2106 | Fred Lewis Corp.
     113065 |  2130.00 |     2106 | Fred Lewis Corp.
     113055 |   150.00 |     2108 | Holm & Landis
     113058 |  1480.00 |     2108 | Holm & Landis
     113003 |  5625.00 |     2108 | Holm & Landis
     113042 | 22500.00 |     2113 | Ian & Schmidt
     113012 |  3745.00 |     2111 | JCP Inc.
     112975 |  2100.00 |     2111 | JCP Inc.
     113057 |   600.00 |     2111 | JCP Inc.
     112989 |  1458.00 |     2101 | Jones Mfg.
     112961 | 31500.00 |     2117 | J.P. Sinclair
     112992 |   760.00 |     2118 | Midwest Systems
     113051 |  1420.00 |     2118 | Midwest Systems
     113013 |   652.00 |     2118 | Midwest Systems
     113049 |   776.00 |     2118 | Midwest Systems
     112979 | 15000.00 |     2114 | Orion Corp
     113024 |  7100.00 |     2114 | Orion Corp
     112997 |   652.00 |     2124 | Peter Brothers
     113062 |  2430.00 |     2124 | Peter Brothers
     113048 |  3750.00 |     2120 | Rico Enterprises
     113045 | 45000.00 |     2112 | Zetacorp
     113007 |  2925.00 |     2112 | Zetacorp
(30 rows)
