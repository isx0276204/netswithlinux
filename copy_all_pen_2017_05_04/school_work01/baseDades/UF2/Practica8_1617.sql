-- 1- Llista el número de totes les comandes amb la descripció del producte demanat.

ERROR:

SELECT NUM_PEDIDO, IMPORTE, DESCRIPCION
	FROM PEDIDOS, PRODUCTOS
WHERE FAB=ID_FAB AND PRODUCTO=ID_PRODUCTO;

Qüestió: Trobes quelcom extrany?

Resp: Surten 29 en comptes de 30 ----> Hem trobat una "inconsistència"
 ( Un dels productes que surten a una comanda no el venem!!!) 
 Probablement es tracta d'una errada en l'entrada d'informació'.


ALERTA:

SELECT num_pedido,descripcion FROM pedidos 
LEFT JOIN productos ON id_fab=fab AND id_producto = producto;

 num_pedido |    descripcion    
------------+-------------------
     113027 | Articulo Tipo 2
     112992 | Articulo Tipo 2
     113012 | Articulo Tipo 3
     112963 | Articulo Tipo 4
     112983 | Articulo Tipo 4
     112968 | Articulo Tipo 4
     113055 | Ajustador
     113057 | Ajustador
     112987 | Extractor
     110036 | Montador
     112979 | Montador
     112997 | Manivela
     113013 | Manivela
     113058 | Cubierta
     113062 | Bancada Motor
     112989 | Bancada Motor
     113007 | Riostra 1/2-Tm
     113069 | Riostra 1-Tm
     113048 | Riostra 2-Tm
     113003 | Riostra 2-Tm
     113051 | 
     113065 | Reductor
     113024 | Reductor
     113049 | Reductor
     112975 | Pasador Bisagra
     112961 | Bisagra Izqda.
     113045 | Bisagra Dcha.
     113042 | Bisagra Dcha.
     112993 | V Stago Trinquete
     113034 | V Stago Trinquete
(30 rows)

-- 2- Llista el nom de tots els venedors i el del seu director en cas de tenir-ne.
-- El camp que conté el nom del treballador s'ha d'identificar amb "empleado" 
-- i el camp que conté el nom del director amb "director".

ALERTA: Sam Clark no té director

training=# select repventas.nombre as empleado,dir.nombre as director from repventas left join repventas dir on repventas.director = dir.num_empl;
   empleado    |  director   
---------------+-------------
 Bill Adams    | Bob Smith
 Mary Jones    | Sam Clark
 Sue Smith     | Larry Fitch
 Sam Clark     | 
 Bob Smith     | Sam Clark
 Dan Roberts   | Bob Smith
 Tom Snyder    | Dan Roberts
 Larry Fitch   | Sam Clark
 Paul Cruz     | Bob Smith
 Nancy Angelli | Larry Fitch
(10 rows)


-- 3- Per a cada venedor, mostrar el nom, les vendes i la ciutat de l'oficina
-- en cas de tenir-ne una d'assignada.


training=# select nombre,sum(importe),ciudad from repventas left join oficinas on oficina_rep = oficina left join pedidos on rep = num_empl group by nombre,ciudad order by nombre;
    nombre     |   sum    |   ciudad    
---------------+----------+-------------
 Bill Adams    | 39327.00 | Atlanta
 Bob Smith     |          | Chicago
 Dan Roberts   | 26628.00 | Chicago
 Larry Fitch   | 58633.00 | Los Angeles
 Mary Jones    |  7105.00 | New York
 Nancy Angelli | 34432.00 | Denver
 Paul Cruz     |  2700.00 | Chicago
 Sam Clark     | 32958.00 | New York
 Sue Smith     | 22776.00 | Los Angeles
 Tom Snyder    | 23132.00 | 
(10 rows)



 -- 4- Llista les 5 comandes amb un import superior. 
-- Mostrar l'identificador de la comanda, import de la comanda, preu del producte,
-- nom del client, nom del representant de vendes que va efectuar la comanda 
-- i ciutat de l'oficina, en cas de tenir oficina assignada.

ALERTA: Tom Snyder no te oficina assignada i hi ha una comanda amb un producte inexistent

training=# select pedidos.num_pedido,pedidos.importe,productos.precio,clientes.empresa,repventas.nombre,oficinas.ciudad from pedidos left join productos on id_producto = producto and id_fab = fab join clientes on clie = num_clie join repventas on rep = num_empl left join oficinas on oficina_rep = oficina;
 num_pedido | importe  | precio  |      empresa      |    nombre     |   ciudad    
------------+----------+---------+-------------------+---------------+-------------
     112993 |  1896.00 |   79.00 | Fred Lewis Corp.  | Sue Smith     | Los Angeles
     113034 |   632.00 |   79.00 | Ace International | Tom Snyder    | 
     112987 | 27500.00 | 2750.00 | Acme Mfg.         | Bill Adams    | Atlanta
     113049 |   776.00 |  355.00 | Midwest Systems   | Larry Fitch   | Los Angeles
     113065 |  2130.00 |  355.00 | Fred Lewis Corp.  | Sue Smith     | Los Angeles
     113024 |  7100.00 |  355.00 | Orion Corp        | Larry Fitch   | Los Angeles
     113048 |  3750.00 | 1875.00 | Rico Enterprises  | Sue Smith     | Los Angeles
     113003 |  5625.00 | 1875.00 | Holm & Landis     | Mary Jones    | New York
     113012 |  3745.00 |  107.00 | JCP Inc.          | Bill Adams    | Atlanta
     112963 |  3276.00 |  117.00 | Acme Mfg.         | Bill Adams    | Atlanta
     112983 |   702.00 |  117.00 | Acme Mfg.         | Bill Adams    | Atlanta
     112968 |  3978.00 |  117.00 | First Corp.       | Dan Roberts   | Chicago
     113013 |   652.00 |  652.00 | Midwest Systems   | Larry Fitch   | Los Angeles
     112997 |   652.00 |  652.00 | Peter Brothers    | Nancy Angelli | Denver
     112961 | 31500.00 | 4500.00 | J.P. Sinclair     | Sam Clark     | New York
     113058 |  1480.00 |  148.00 | Holm & Landis     | Mary Jones    | New York
     113069 | 31350.00 | 1425.00 | Chen Associates   | Nancy Angelli | Denver
     112979 | 15000.00 | 2500.00 | Orion Corp        | Sue Smith     | Los Angeles
     110036 | 22500.00 | 2500.00 | Ace International | Tom Snyder    | 
     112992 |   760.00 |   76.00 | Midwest Systems   | Larry Fitch   | Los Angeles
     113027 |  4104.00 |   76.00 | Acme Mfg.         | Bill Adams    | Atlanta
     113042 | 22500.00 | 4500.00 | Ian & Schmidt     | Dan Roberts   | Chicago
     113045 | 45000.00 | 4500.00 | Zetacorp          | Larry Fitch   | Los Angeles
     113007 |  2925.00 |  975.00 | Zetacorp          | Larry Fitch   | Los Angeles
     113055 |   150.00 |   25.00 | Holm & Landis     | Dan Roberts   | Chicago
     113057 |   600.00 |   25.00 | JCP Inc.          | Paul Cruz     | Chicago
     113062 |  2430.00 |  243.00 | Peter Brothers    | Nancy Angelli | Denver
     112989 |  1458.00 |  243.00 | Jones Mfg.        | Sam Clark     | New York
     112975 |  2100.00 |  350.00 | JCP Inc.          | Paul Cruz     | Chicago
     113051 |  1420.00 |         | Midwest Systems   | Larry Fitch   | Los Angeles
(30 rows)


-- 5- Llista les comandes que han estat preses per un representant de vendes
-- que no és l'actual representant de vendes del client pel que s'ha realitzat la comanda. 
-- Mostrar el número de comanda, el nom del client, 
-- el nom de l'actual representant de vendes del client com a "rep_cliente"
-- i el nom del representant de vendes que va realitzar la comanda com a "rep_pedido".


training=# select pedidos.num_pedido,clientes.empresa,repventas.nombre,repventas_clie.nombre,repventas.nombre from pedidos join clientes on clie = num_clie join repventas on rep = num_empl join repventas repventas_clie on repventas_clie.num_empl = rep_clie where rep_clie != rep;
 num_pedido |     empresa     |    nombre     |   nombre   |    nombre     
------------+-----------------+---------------+------------+---------------
     113012 | JCP Inc.        | Bill Adams    | Paul Cruz  | Bill Adams
     113024 | Orion Corp      | Larry Fitch   | Sue Smith  | Larry Fitch
     113069 | Chen Associates | Nancy Angelli | Paul Cruz  | Nancy Angelli
     113055 | Holm & Landis   | Dan Roberts   | Mary Jones | Dan Roberts
     113042 | Ian & Schmidt   | Dan Roberts   | Bob Smith  | Dan Roberts
(5 rows)


-- 6- Llista les comandes amb un import superior a 5000
-- i també aquelles comandes realitzades per un client amb un crèdit inferior a 30000. 
-- Mostrar l'identificador de la comanda, el nom del client
-- i el nom del representant de vendes que va prendre la comanda.


training=# select pedidos.num_pedido,clientes.empresa,repventas.nombre,repventas.nombre,importe,limite_credito from pedidos join clientes on clie = num_clie join repventas on rep = num_empl where limite_credito < 30000 and importe > 5000;
 num_pedido |     empresa     |    nombre     |    nombre     | importe  | limite_credito 
------------+-----------------+---------------+---------------+----------+----------------
     112979 | Orion Corp      | Sue Smith     | Sue Smith     | 15000.00 |       20000.00
     113042 | Ian & Schmidt   | Dan Roberts   | Dan Roberts   | 22500.00 |       20000.00
     113024 | Orion Corp      | Larry Fitch   | Larry Fitch   |  7100.00 |       20000.00
     113069 | Chen Associates | Nancy Angelli | Nancy Angelli | 31350.00 |       25000.00
(4 rows)





-- 7. Mostrar l'identificador i en nom de l'empresa dels clients.
-- Només mostrar aquells clients que la suma dels imports de les seves
-- comandes sigui menor al limit de crèdit.

training=# select num_clie,limite_credito,sum(importe) from clientes join pedidos on clie = num_clie group by num_clie having limite_credito > sum(importe);
 num_clie | limite_credito |   sum    
----------+----------------+----------
     2102 |       65000.00 |  3978.00
     2117 |       35000.00 | 31500.00
     2112 |       50000.00 | 47925.00
     2118 |       60000.00 |  3608.00
     2120 |       50000.00 |  3750.00
     2106 |       65000.00 |  4026.00
     2108 |       55000.00 |  7255.00
     2107 |       35000.00 | 23132.00
     2124 |       40000.00 |  3082.00
     2111 |       50000.00 |  6445.00
     2101 |       65000.00 |  1458.00
     2103 |       50000.00 | 35582.00
(12 rows)


-- 8. Mostrar l'identificador dels clients i un camp anomenat "pedidos".
-- El camp "pedidos" ha de mostrar quantes comandes ha fet cada client.



-- 9. Mostrar l'identificador i la ciutat de les oficines i dos camps més, 
-- un anomenat "credito1" i l'altre "credito2".
-- Per a cada oficina, el camp "credito1" ha de mostrar el límit de 
-- crèdit més petit d'entre tots els clients que el seu representant 
-- de vendes treballa a l'oficina. 
-- El camp "credito2" ha de ser el mateix però pel límit de crèdit més gran.

training=# select oficina,ciudad,min(limite_credito),max(limite_credito) from oficinas left join repventas on oficina_rep = oficina left join clientes on rep_clie = num_empl group by oficina;
 oficina |   ciudad    |   min    |   max    
---------+-------------+----------+----------
      11 | New York    | 25000.00 | 65000.00
      12 | Chicago     | 20000.00 | 65000.00
      13 | Atlanta     | 30000.00 | 50000.00
      22 | Denver      | 40000.00 | 40000.00
      21 | Los Angeles | 20000.00 | 65000.00
(5 rows)

