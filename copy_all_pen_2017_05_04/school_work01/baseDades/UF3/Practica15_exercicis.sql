---1. Selecciona els treballadors que han venut menys 
--quantitat de productes que la Sue Smith

training=# select nombre,ventas,sum(cant) from repventas left join pedidos on num_empl = rep group by num_empl having sum(cant) < ( select sum(cant) from repventas join pedidos on num_empl = rep where nombre = 'Sue Smith');
    nombre     |  ventas   | sum 
---------------+-----------+-----
 Sam Clark     | 299912.00 |  13
 Nancy Angelli | 186042.00 |  33
 Mary Jones    | 392725.00 |  13
 Paul Cruz     | 286775.00 |  30
 Tom Snyder    |  75985.00 |  17
(5 rows)



---2. Llista els treballadors que han venut mes en import
--- que la Sue Smith, la Mary Jones i els Bill Adams

training=# select nombre,sum(importe) from pedidos join repventas on num_empl = rep group by nombre having sum(importe) > all (select sum(importe) from repventas join pedidos on rep = num_empl where nombre in ('Sue Smith','Mary Jones','Bill Adams') group by num_empl);
   nombre    |   sum    
-------------+----------
 Larry Fitch | 58633.00
(1 row)



---3. Llista els treballadors que han venut mes que alguns dels seguents : 
---Sue Smith, la Mary Jones i els Bill Adams

training=# select nombre,sum(importe) from pedidos join repventas on num_empl = rep group by nombre having sum(importe) > any (select sum(importe) from repventas join pedidos on rep = num_empl where nombre in ('Sue Smith','Mary Jones','Bill Adams') group by num_empl);
    nombre     |   sum    
---------------+----------
 Larry Fitch   | 58633.00
 Dan Roberts   | 26628.00
 Bill Adams    | 39327.00
 Nancy Angelli | 34432.00
 Sue Smith     | 22776.00
 Tom Snyder    | 23132.00
 Sam Clark     | 32958.00
(7 rows)


---4. Llista els treballadors que han fet mes comandes que els seus directors.

training=# select repventas.num_empl as empleat,count(num_pedido),dir.num_empl from repventas join repventas dir on repventas.director = dir.num_empl join pedidos on rep = repventas.num_empl group by 1,3 having count(num_pedido) > (select count(num_pedido) from pedidos where dir.num_empl = pedidos.rep);
 empleat | count | num_empl 
---------+-------+----------
     103 |     2 |      104
     105 |     5 |      104
     108 |     7 |      106
     101 |     3 |      104
(4 rows)



---5. Llista els treballadors que en el r[anking de ventes estan
 ---entre el Dan Roberts i la Bill Adams
training=# select nombre from repventas where ventas in ( select ventas from repventas where ventas between (select ventas from repventas where nombre = 'Dan Roberts') and (select ventas from repventas where nombre = 'Bill Adams'));
   nombre    
-------------
 Bill Adams
 Dan Roberts
 Larry Fitch
(3 rows)

								--------or------
				
				
training=# select nombre from repventas join pedidos on rep = num_empl group by num_empl having sum(importe) in ( select sum(importe) from repventas join pedidos on rep = num_empl group by num_empl having sum(importe) between (select sum(importe) from repventas join pedidos on rep = num_empl where nombre = 'Dan Roberts') and (select sum(importe) from repventas join pedidos on rep = num_empl where nombre = 'Bill Adams'));
    nombre     
---------------
 Sam Clark
 Nancy Angelli
 Bill Adams
 Dan Roberts
(4 rows)

						--------right----
						
training=# select num_empl, nombre, sum(importe) from repventas join pedidos on rep = num_empl group by num_empl having sum(importe) between (select sum(importe) from repventas join pedidos on rep = num_empl where nombre = 'Dan Roberts') and (select sum(importe) from repventas join pedidos on rep = num_empl where nombre = 'Bill Adams');
 num_empl |    nombre     |   sum    
----------+---------------+----------
      106 | Sam Clark     | 32958.00
      107 | Nancy Angelli | 34432.00
      105 | Bill Adams    | 39327.00
      101 | Dan Roberts   | 26628.00
(4 rows)
