
---6. Mostra les oficines (codi i ciutat) tals que el seu objectiu sigui inferior o igual a les quotes de tots els seus treballadors.

training=# select oficina,ciudad,cuota,objetivo from oficinas join repventas on oficina_rep = oficina where objetivo <= cuota ;
 oficina | ciudad  |   cuota   | objetivo  
---------+---------+-----------+-----------
      13 | Atlanta | 350000.00 | 350000.00
      22 | Denver  | 300000.00 | 300000.00
(2 rows)

training=# select oficina,ciudad from oficinas where oficinas.objetivo <= all (select cuota from repventas where repventas.oficina_rep = oficinas.oficina);
 oficina | ciudad  
---------+---------
      22 | Denver
      13 | Atlanta
(2 rows)


---7. Llista els representants de vendes (codi de treballador i nom) que tenen un director més jove que algun dels seus empleats.

training=# select empl.num_empl,empl.nombre,empl.edad,dir.edad from repventas empl join repventas dir on empl.director = dir.num_empl where empl.edad > dir.edad ;
 num_empl |   nombre    | edad | edad 
----------+-------------+------+------
      105 | Bill Adams  |   37 |   33
      101 | Dan Roberts |   45 |   33
      108 | Larry Fitch |   62 |   52
(3 rows)
------right one-----
training=# select e.num_empl,e.nombre,e.director from repventas e join  repventas dir on e.director = dir.num_empl and dir.edad < any ( select edad from repventas where director = dir.num_empl); 
 num_empl |   nombre    | director 
----------+-------------+----------
      108 | Larry Fitch |      106
      104 | Bob Smith   |      106
      109 | Mary Jones  |      106
      103 | Paul Cruz   |      104
      101 | Dan Roberts |      104
      105 | Bill Adams  |      104
(6 rows)

---8. Mostrar el codi de treballador, el seu nom i un camp anomenat i_m. El camp i_m és l'import més gran de les comandes que ha fet aquest treballador. Només s'han de llistar els treballadors que tinguin tots els clients amb alguna comanda amb import superior a la mitjana dels imports de totes les comandes.

training=# select nombre,(select max(importe) as i_m from pedidos where num_empl = rep ) from repventas ;
    nombre     |   i_m    
---------------+----------
 Bill Adams    | 27500.00
 Mary Jones    |  5625.00
 Sue Smith     | 15000.00
 Sam Clark     | 31500.00
 Bob Smith     |         
 Dan Roberts   | 22500.00
 Tom Snyder    | 22500.00
 Larry Fitch   | 45000.00
 Paul Cruz     |  2100.00
 Nancy Angelli | 31350.00
(10 rows)

		----right one----
training=# select nombre,(select max(importe) as i_m from pedidos where num_empl = rep ) from repventas where (select avg(importe) from pedidos) < all (select max(importe) from pedidos right join clientes on clie = num_clie where num_empl = rep_clie group by clie);
   nombre   |   i_m    
------------+----------
 Bob Smith  |         
 Tom Snyder | 22500.00
(2 rows)


---9. Mostra el codi de fabricant i de producte i un camp de nom n_p. n_p és el nombre de comandes que s'han fet d'aquell producte. Només s'han de llistar aquells productes tals que se n'ha fet alguna comanda amb una quantitat inferior a les seves existències. En el llistat només han d''aparèixer els tres productes amb més comandes, ordenats per codi de fabricant i de producte.



---10. Mostra el codi de client, el nom de client, un camp c_r i un camp n_p. El camp c_r ha de mostrar la quota del representant de vendes del client. El camp n_p ha demostrar el nombre de comandes que ha fet aquest client. Només s'han de mostrar els clients que l'import total de totes les seves comandes sigui superior a la mitjana de l''import de totes les comandes.


                                     
