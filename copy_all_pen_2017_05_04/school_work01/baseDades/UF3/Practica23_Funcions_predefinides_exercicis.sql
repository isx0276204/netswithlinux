-- --------------------------------------------
--    Funcions predefinides
-- --------------------------------------------

-- 10.1- Mostreu la longitud de la cadena "hola que tal"


-- 10.2- Mostreu la longitud dels valors del camp "id_producto".


-- 10.3- Mostrar la longitud dels valors del camp "descripcion".


-- 10.4- Mostreu els noms dels venedors en majúscules.


-- 10.5- Mostreu els noms dels venedors en minúscules.


-- 10.6- Trobeu on és la posició de l'espai en blanc de la cadena 'potser 7'.


-- 10.7- Volem mostrar el nom, només el nom dels venedors sense el cognom, en majúscules.


-- 10.8- Crear una vista que mostri l'identificador dels representants de vendes i en columnes separades el nom i el cognom.


-- 10.9- Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'B. Adams'.
 

-- 10.10- Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'Adams, Bill'.


-- 10.11- Volem mostrar el camp descripcion de la taula productos però que en comptes de sortir espais en blanc, volem subratllats ('_').


-- 10.12- Volem treure per pantalla una columna, que conté el nom i les vendes, amb els següent estil:
--   vendes dels empleats
-- ---------------------------
--  Bill Adams..... 367911,00
--  Mary Jones..... 392725,00
--  Sue Smith...... 474050,00
--  Sam Clark...... 299912,00
--  Bob Smith...... 142594,00
--  Dan Roberts.... 305673,00
--  Tom Snyder.....  75985,00
--  Larry Fitch.... 361865,00
--  Paul Cruz...... 286775,00
--  Nancy Angelli.. 186042,00
-- (10 rows)


-- 10.13- Treieu per pantalla el temps total que fa que estan contractats els treballadors, ordenat pels cognoms dels treballadors amb un estil semblant al següent:
--       nombre  |     tiempo_trabajando
--    -----------+-------------------------
--    Mary Jones | 13 years 4 months 6 days

 
-- 10.14- Cal fer un llistat dels productes dels quals les existències són inferiors al total d'unitats venudes d'aquell producte els darrers 60 dies, a comptar des de la data actual. Cal mostrar els codis de fabricant i de producte, les existències, i les unitats totals venudes dels darrers 60 dies.


-- 10.15- Com l'exercici anterior però en comptes de 60 dies ara es volen aquells productes venuts durant el mes actual o durant l'anterior.


-- 10.16- Per fer un estudi previ de la campanya de Nadal es vol un llistat on, per cada any del qual hi hagi comandes a la base de dades, el nombre de clients diferents que hagin fet comandes en el mes de desembre d'aquell any. Cal mostrar l'any i el número de clients, ordenat ascendent per anys.


-- 10.17- Llisteu codi(s) i descripció dels productes. La descripció ha d'aperèixer en majúscules. Ha d'estar ordenat per la longitud de les descripcions (les més curtes primer).


-- 10.18- LListar el nom dels treballadors i la data del seu contracte mostrant-la amb el següent format:
-- Dia_de_la_setmana dia_mes_numeric, mes_text del any_4digits
-- per exemple:
-- Bill Adams    | Friday    12, February del 1988


-- 10.19- Modificar els imports de les comandes que s'han realitzat durant l'estiu augmentant-lo un 20% i arrodonint a l'alça el resultat.


-- 10.20- Mostar les dades de les oficines llistant en primera instància aquelles oficines que tenen una desviació entre vendes i objectius més gran.


-- 10.21- Llistar les dades d'aquells representants de vendes que tenen un identificador senar i són directors d'algun representants de vendes.


