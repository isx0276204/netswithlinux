----1- Llistar els venedors que treballen en oficines que superen el seu objectiu. Mostrar també les següents dades de l'oficina: ciutat i la diferència entre les vendes i l'objectiu. Ordenar el resultat per aquest últim valor. Proposa dos sentències SQL, una amb subconsultes i una sense.



----a) barrejant multitaula i subconsulta:



----b) només amb multitaula 



----2- Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. Sense usar consultes multitaula.



----3- Llistar els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. Mostrant també la ciutat de l'oficina on treballa l'empleat i l'identificador del director de la oficina. Proposa dos sentències SQL, una amb subconsultes i una sense.



----a) barrejant multitaula i subconsulta:



----b) només amb multitaula



----4- Llistar tots els clients que han realitzat comandes del productes de la família ACI Widgets entre gener i juny del 1990. Els productes de la famíla ACI Widgets són aquells que tenen identificador de fabricant "aci" i que l'identificador del producte comença per "4100".



----5- Llistar els clients que no tenen cap comanda.



----6- Llistar els clients que tenen assignat el venedor que porta més temps a contractat.



----7- Llistar els clients assignats a Sue Smith que no han fet cap comanda amb un import superior a 30000. Proposa una sentència SQL sense usar multitaula i una altre en que s'usi multitaula i subconsultes.



----8- Llistar l'identificador i el nom dels directors d'empleats que tenen més de 40 anys i que dirigeixen un venedor que té unes vendes superiors a la seva pròpia quota.



----9- Llista d'oficines on hi hagi algun venedor tal que la seva quota representi més del 50% de l'objectiu de l'oficina



----10- Llista d'oficines on tots els venedors tinguin la seva quota superior al 55% de l'objectiu de l'oficina.



