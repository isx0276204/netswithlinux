Exercicis 

----1- Quins són els productes amb un preu inferior al 10% del preu del producte més car? I els d'un preu inferior al 5% del producte més car?


training=# select descripcion from productos where precio < (select max(precio) from productos)*0.1; 
    descripcion    
-------------------
 V Stago Trinquete
 Reductor
 Plate
 Articulo Tipo 3
 Articulo Tipo 4
 Perno Riostra
 Reductor
 Cubierta
 Soporte Riostra
 Retn
 Articulo Tipo 1
 Reductor
 Articulo Tipo 2
 Ajustador
 Bancada Motor
 Pasador Bisagra
(16 rows)


----2- Quin és el 2n client que compra més (en import)?


training=# select clie from pedidos group by clie order by sum(importe) limit 1 offset 1;
 clie 
------
 2124
(1 row)


training=# select * from clientes where num_clie in ( select clie from pedidos group by clie order by sum(importe) limit 2) and num_clie not in (select clie from pedidos group by clie order by sum(importe) limit 1); 
 num_clie |    empresa     | rep_clie | limite_credito 
----------+----------------+----------+----------------
     2124 | Peter Brothers |      107 |       40000.00
(1 row)


----3- Quin és el 3r client que compra menys (en import)?

training=# select * from clientes where num_clie in ( select clie from pedidos group by clie order by sum(importe) limit 3) and num_clie not in (select clie from pedidos group by clie order by sum(importe) limit 2); 
 num_clie |     empresa     | rep_clie | limite_credito 
----------+-----------------+----------+----------------
     2118 | Midwest Systems |      108 |       60000.00
(1 row)

----4- Dels 10 productes més venuts, quin és el més car?


training=# select id_producto, id_fab, descripcion, precio from productos where id_producto in (select id_producto from productos join pedidos on id_fab = fab and id_producto = producto
group by id_fab,id_producto order by sum(cant) desc limit 10) order by precio desc limit 1;
 id_producto | id_fab |  descripcion  | precio  
-------------+--------+---------------+---------
 2a44r       | rei    | Bisagra Dcha. | 4500.00
(1 row)

----5- Llista els  clients que tenen un total d'import gastat amb una diferència màxima del 40%  de l'import que ha gastat el millor client.

training=# select sum(importe) from pedidos group by clie having sum(importe) > (select sum(importe) from pedidos group by clie order by sum(importe) desc limit 1)*0.60;
   sum    
----------
 31500.00
 47925.00
 31350.00
 35582.00
(4 rows)
