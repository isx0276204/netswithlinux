-- --------------------------------------------
--    Estructura
-- --------------------------------------------

-- 8.1- Crear una base de dades anomenada "biblioteca". Dins aquesta base de dades
-- crear una taula anomenada 	 amb els camps següents:
-- - "ref": clau primaria numèrica que identifica els llibres i s'ha d'assignar automàticament.
-- - "titol": títol del llibre.
-- - "editorial": nom de l'editorial.
-- - "autor": identificador de l'autor, clau forana, per defecte ha de
-- referenciar a primer valor de la taula autors que simbolitzar l'autor "Anònim".
-- Crear una altre taula anomenada "autors" amb els següents camps:
-- - "autor": identificador de l'autor.
-- - "nom": Nom i cognoms de l'autor.
-- - "nacionalitatnacionalitat": Nacionalitat de l'autor.
-- Ambdues taules han de mantenir integritat referencial. Cal que si es trenca
-- la integritat per delete d'autor, la clau forana del llibre apunti a "Anònim".
-- Si es trenca la integritat per insert/update s'ha d'impedir l'alta/modificació.
-- Cal inserir l'autor "Anònim" a la taula autors.



-- 8.2- A la base biblioteca cal crear una taula anomenada "socis"
-- amb els següents camps:
-- - num_soci: clau primària
-- - nom: nom i cognoms del soci.
-- - dni: DNI del soci.
-- També una taula anomenada préstecs amb els següents camps:
-- - ref: clau forana, que fa referència al llibre prestat.
-- - soci: clau forana, que fa referència al soci.
-- - data_prestec: data en que s'ha realitzat el préstec.
-- No cal que préstecs tingui clau principal ja que només és una taula de relació.
-- En eliminar un llibre cal que s'eliminin els seus préstecs automàticament.
-- No s'ha de poder eliminar un soci amb préstecs pendents.



-- 8.3- A la base de dades training crear una taula anomenada "repventas_baja" que tingui la mateixa estructura que la taula repventas i a més a més un camp anomenat "baja" que pugui contenir la data en que un representant de vendes s'ha donat de baixa.



-- 8.4- A la base de dades training crear una taula anomenada "productos_sin_pedidos" omplerta amb aquells productes que no han tingut mai cap comanda. A continuació esborrar de la taula "productos" aquells productes que estan en aquesta nova taula.



-- 8.5- A la base de dades training crear una taula temporal que substitueixi la taula "clientes" però només ha de contenir aquells clients que no han fet comandes i tenen assignat un representant de vendes amb unes vendes inferiors al 110% de la seva quota.



-- 8.6- Escriu les sentències necessàries per a crear l'estructura de l'esquema proporcionat de la base de dades training. Justifica les accions a realitzar en modificar/actualitzar les claus primàries.

C

-- 8.7- Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que afegeixi un camp anomenat "nif" a la taula "clientes" que permeti emmagatzemar el NIF de cada client. També s'ha de procurar que el NIF de cada client sigui únic.



-- 8.8- Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que afegeixi un camp anomenat "tel" a la taula "clientes" que permeti emmagatzemar el número de telèfon de cada client. També s'ha de procurar que aquest contingui 9 xifres.



-- 8.9- Escriu les sentències necessàries per modificar la base de dades training proporcionada. Cal que s'impedeixi que els noms dels representants de vendes i els noms dels clients estiguin buits, és a dir que ni siguin nuls ni continguin la cadena buida.



-- 8.10- Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que procuri que l'edat dels representants de vendes no sigui inferior a 18 anys ni superior a 65.



-- 8.11- Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que esborri el camp "titulo" de la taula "repventas" esborrant també les possibles restriccions i referències que tingui.



-- 8.12- Escriu les sentències necessàries per modificar la base de dades training proporcionada per tal que aquesta tingui integritat referencial. Justifica les accions a realitzar per modificar les dades.


