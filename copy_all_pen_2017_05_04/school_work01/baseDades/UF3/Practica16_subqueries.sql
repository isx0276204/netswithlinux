-------------------------------------------------------------------------------
--          Subconsultes
-------------------------------------------------------------------------------


-- 5.1- Llista els venedors que tinguin una quota igual o inferior a l'objectiu de l'oficina de vendes d'Atlanta.

training=# select nombre,cuota from repventas where cuota <= ( select objetivo from oficinas where ciudad = 'Atlanta');
    nombre     |   cuota   
---------------+-----------
 Bill Adams    | 350000.00
 Mary Jones    | 300000.00
 Sue Smith     | 350000.00
 Sam Clark     | 275000.00
 Bob Smith     | 200000.00
 Dan Roberts   | 300000.00
 Larry Fitch   | 350000.00
 Paul Cruz     | 275000.00
 Nancy Angelli | 300000.00
(9 rows)




-- 5.2- Tots els clients, identificador i nom de l'empresa, que han estat atesos per (que han fet comanda amb) Bill Adams.

training=# select num_clie,empresa from clientes where exists ( select * from repventas where rep_clie = num_empl and nombre = 'Bill Adams');
 num_clie |     empresa     
----------+-----------------
     2103 | Acme Mfg.
     2122 | Three-Way Lines
(2 rows)

					------right solution------
training=# select num_clie,empresa from clientes where num_clie in ( select clie from pedidos where rep = (select num_empl from repventas where nombre = 'Bill Adams'));
 num_clie |  empresa  
----------+-----------
     2111 | JCP Inc.
     2103 | Acme Mfg.
(2 rows)

-- 5.3- Venedors amb quotes que siguin iguals o superiors a l'objectiu de llur oficina de vendes.

training=# select nombre,cuota from repventas where cuota >= ( select objetivo from oficinas where oficina = oficina_rep);
    nombre     |   cuota   
---------------+-----------
 Bill Adams    | 350000.00
 Nancy Angelli | 300000.00
(2 rows)


-- 5.4- Mostrar l'identificador de l'oficina i la ciutat de les oficines on l'objectiu de vendes de l'oficina excedeix la suma de quotes dels venedors d'aquella oficina.

training=# select oficina,ciudad from oficinas where objetivo > ( select sum(cuota)  from repventas where oficina = oficina_rep) ;
 oficina |   ciudad    
---------+-------------
      12 | Chicago
      21 | Los Angeles
(2 rows)


-- 5.5- Llista dels productes del fabricant amb identificador "aci" que les existències superen les existències del producte amb identificador de producte "41004" i identificador de fabricant "aci".

training=# select descripcion,id_fab,id_producto from productos where id_fab = 'aci' and existencias >= ( select existencias from productos  where id_fab = 'aci' and id_producto = '41004');
   descripcion   | id_fab | id_producto 
-----------------+--------+-------------
 Articulo Tipo 3 | aci    | 41003
 Articulo Tipo 4 | aci    | 41004
 Articulo Tipo 1 | aci    | 41001
 Articulo Tipo 2 | aci    | 41002
(4 rows)

			------right-----
training=# select descripcion,id_fab,id_producto from productos where id_fab = 'aci' and existencias > ( select existencias from productos  where id_fab = 'aci' and id_producto = '41004');
   descripcion   | id_fab | id_producto 
-----------------+--------+-------------
 Articulo Tipo 3 | aci    | 41003
 Articulo Tipo 1 | aci    | 41001
 Articulo Tipo 2 | aci    | 41002
(3 rows)


-- 5.6- Llistar els venedors que han acceptat una comanda que representa més del 10% de la seva quota.

training=# select nombre from repventas join pedidos on rep = num_empl where importe > 0.1 * cuota ;
    nombre     
---------------
 Sam Clark
 Larry Fitch
 Nancy Angelli
(3 rows)


training=# select nombre from repventas where 0.1 * cuota < any (select importe from pedidos where rep = num_empl);
    nombre     
---------------
 Sam Clark
 Larry Fitch
 Nancy Angelli
(3 rows)

training=# select distinct rep from pedidos where importe > ( select cuota * 0.1 from repventas where rep = num_empl);
 rep 
-----
 106
 107
 108
(3 rows)




-- 5.7- Llistar el nom i l'edat de totes les persones de l'equip de vendes que no dirigeixen una oficina.

training=# select nombre,edad from repventas where num_empl not in ( select dir from oficinas ) ;
    nombre     | edad 
---------------+------
 Mary Jones    |   31
 Sue Smith     |   48
 Dan Roberts   |   45
 Tom Snyder    |   41
 Paul Cruz     |   29
 Nancy Angelli |   49
(6 rows)


-- 5.8- Llistar aquelles oficines, i els seus objectius, que tots els seus venedors tinguin unes vendes que superen el 50% de l'objectiu de l'oficina.
----de l'oficina no superi cap de les ventas dels venedors d'aquella oficina.

training=# select oficina,ciudad,objetivo from oficinas where 0.5 * objetivo < all (select ventas from repventas where oficina_rep = oficina) ;
 oficina |  ciudad  | objetivo  
---------+----------+-----------
      22 | Denver   | 300000.00
      11 | New York | 575000.00
      13 | Atlanta  | 350000.00
(3 rows)

---

-- 5.9- Llistar aquells clients que els seus representants de vendes estàn assignats a oficines de la regió est.

training=# select num_clie,empresa from clientes join repventas on num_empl = rep_clie join oficinas on oficina_rep = oficina where region = 'Este';
 num_clie |     empresa     
----------+-----------------
     2111 | JCP Inc.
     2102 | First Corp.
     2103 | Acme Mfg.
     2115 | Smithson Corp.
     2101 | Jones Mfg.
     2121 | QMA Assoc.
     2108 | Holm & Landis
     2117 | J.P. Sinclair
     2122 | Three-Way Lines
     2119 | Solomon Inc.
     2113 | Ian & Schmidt
     2109 | Chen Associates
     2105 | AAA Investments
(13 rows)


-------subqurry----

training=# select num_clie,empresa from clientes where rep_clie in ( select num_empl from repventas where oficina_rep in ( select oficina from oficinas where region = 'Este'));
 num_clie |     empresa     
----------+-----------------
     2111 | JCP Inc.
     2102 | First Corp.
     2103 | Acme Mfg.
     2115 | Smithson Corp.
     2101 | Jones Mfg.
     2121 | QMA Assoc.
     2108 | Holm & Landis
     2117 | J.P. Sinclair
     2122 | Three-Way Lines
     2119 | Solomon Inc.
     2113 | Ian & Schmidt
     2109 | Chen Associates
     2105 | AAA Investments
(13 rows)


-- 5.10- Llistar els venedors que treballen en oficines que superen el seu objectiu.
training=# select num_empl,nombre from repventas where oficina_rep in ( select oficina from oficinas where ventas > objetivo);
 num_empl |   nombre    
----------+-------------
      105 | Bill Adams
      109 | Mary Jones
      102 | Sue Smith
      106 | Sam Clark
      108 | Larry Fitch
(5 rows)




-- 5.11- Llistar els venedors que treballen en oficines que superen el seu objectiu. Mostrar també les següents dades de l'oficina: ciutat i la diferència entre les vendes i l'objectiu. Ordenar el resultat per aquest últim valor. Proposa dues sentències SQL, una amb subconsultes i una sense.


-- 5.12- Llista els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. Sense usar consultes multitaula.



-- 5.13- Llistar els venedors que no treballen en oficines dirigides per Larry Fitch, o que no treballen a cap oficina. Mostrant també la ciutat de l'oficina on treballa l'empleat i l'identificador del director de la oficina. Proposa dues sentències SQL, una amb subconsultes i una sense.



-- 5.14- Llistar tots els clients que han realitzat comandes del productes de la família ACI Widgets entre gener i juny del 1990. Els productes de la famíla ACI Widgets són aquells que tenen identificador de fabricant "aci" i que l'identificador del producte comença per "4100".



-- 5.15- Llistar els clients que no tenen cap comanda.

;

-- 5.16- Llistar els clients que tenen assignat el venedor que porta més temps a contractat.




-- 5.17- Llistar els clients assignats a Sue Smith que no han fet cap comanda amb un import superior a 30000. Proposa una sentència SQL sense usar multitaula i una altre en que s'usi multitaula i subconsultes.

training=# select empresa from clientes where rep_clie = ( select num_empl from repventas where nombre = 'Sue Smith') and not exists (select num_pedido from pedidos where clie = num_clie and pedidos.importe > 30000); 
     empresa      
------------------
 Carter & Sons
 Orion Corp
 Rico Enterprises
 Fred Lewis Corp.
(4 rows)

-- 5.18- Llistar l'identificador i el nom dels directors d'empleats que tenen més de 40 anys i que dirigeixen un venedor que té unes vendes superiors a la seva pròpia quota.

training=# select distinct dir.num_empl,dir.nombre as director,treb.num_empl,treb.nombre as treballador from repventas treb join repventas dir on treb.director = dir.num_empl where dir.edad > 40 and (treb.ventas > treb.cuota);
 num_empl |  director   | num_empl | treballador 
----------+-------------+----------+-------------
      106 | Sam Clark   |      108 | Larry Fitch
      106 | Sam Clark   |      109 | Mary Jones
      108 | Larry Fitch |      102 | Sue Smith
(3 rows)



-- 5.19- Llista d'oficines on hi hagi algun venedor tal que la seva quota representi més del 50% de l'objectiu de l'oficina

training=# select ciudad from oficinas where objetivo * 0.55 < all ( select cuota from repventas where oficina_rep = oficina);
 ciudad  
---------
 Denver
 Atlanta
(2 rows)



-- 5.20- Llista d'oficines on tots els venedors tinguin la seva quota superior al 55% de l'objectiu de l'oficina.



-- 5.21- Transforma el següent JOIN a una comanda amb subconsultes:
-- SELECT num_pedido, importe, clie, num_clie, limite_credito
-- FROM pedidos JOIN clientes
-- ON clie = num_clie;



-- 5.22- Transforma el següent JOIN a una comanda amb subconsultes:
-- SELECT empl.nombre, empl.cuota, dir.nombre, dir.cuota
-- FROM repventas AS empl JOIN repventas AS dir
-- ON empl.director = dir.num_empl
-- WHERE empl.cuota > dir.cuota;



-- 5.23- Transforma la següent consulta amb un ANY a una consulta amb un EXISTS i també en una altre consulta amb un ALL:
-- SELECT oficina FROM oficinas WHERE ventas*0.8 < ANY (SELECT ventas FROM repventas WHERE oficina_rep = oficina);

training=# select oficina from oficinas where exists ( select * from repventas where oficina_rep = oficina and oficinas.ventas * 0.8 < ventas);
 oficina 
---------
      22
      13
(2 rows)


training=# select oficina from oficinas where ventas * 0.8 <all ( select ventas from repventas where oficina_rep = oficina);
 oficina 
---------
      22
      13
(2 rows)

   
-- 5.24- Transforma la següent consulta amb un ALL a una consutla amb un EXISTS i també en una altre consulta amb un ANY:
-- SELECT num_clie FROM clientes WHERE limite_credito < ALL (SELECT importe FROM pedidos WHERE num_clie = clie);



-- 5.25- Transforma la següent consulta amb un EXISTS a una consulta amb un ALL i també a una altre consulta amb un ANY:
-- SELECT num_clie, empresa FROM clientes WHERE EXISTS (SELECT * FROM repventas WHERE rep_clie = num_empl AND edad BETWEEN 40 AND 50);


-- 5.26- Transforma la següent consulta amb subconsultes a una consulta sense subconsultes.
-- SELECT * FROM productos WHERE id_fab IN(SELECT fab FROM pedidos WHERE cant > 30) AND id_producto IN(SELECT producto FROM pedidos WHERE cant > 30);


-- 5.27- Transforma la següent consulta amb subconsultes a una consulta sense subconsultes.
-- SELECT num_empl, nombre FROM repventas WHERE num_empl = ANY ( SELECT rep_clie FROM clientes WHERE empresa LIKE '%Inc.');



-- 5.28- Transforma la següent consulta amb un IN a una consulta amb un EXISTS i també a una altre consulta amb un ALL.
-- SELECT num_empl, nombre FROM repventas WHERE num_empl IN(SELECT director FROM repventas);



-- 5.29- Modifica la següent consulta perquè mostri la ciutat de l'oficina, proposa una consulta simplificada.
-- SELECT num_pedido FROM pedidos WHERE rep IN
-- (
--     SELECT num_empl FROM repventas WHERE ventas >
--     (
--         SELECT avg(ventas) FROM repventas
--     )
--     AND oficina_rep IN
--     (
--         SELECT oficina FROM oficinas WHERE region ILIKE 'este'
--     )
-- );



-- 5.30- Transforma la següent consulta amb subconsultes a una consulta amb les mínimes subconsultes possibles.
-- SELECT num_clie, empresa, (SELECT nombre FROM repventas WHERE rep_clie = num_empl) AS rep_nombre FROM clientes WHERE rep_clie = ANY (SELECT num_empl FROM repventas WHERE ventas > (SELECT MAX(cuota) FROM repventas));



