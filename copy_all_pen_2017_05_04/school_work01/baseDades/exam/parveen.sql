-------1

training=# select producto,count(num_pedido),sum(importe) from pedidos where (producto like '4%' and (producto like '%2' or producto like '%3' or producto like '%4')) group by producto having count(num_pedido) >= 3;
 producto | count |   sum   
----------+-------+---------
 41003    |     3 | 5049.00
 41004    |     3 | 7956.00
(2 rows)


-------2

training=# select count(distinct clie) from pedidos where (cant > 7 and (fab = 'aci' or fab = 'rei')) ;
 count 
-------
     7
(1 row)


-------3

training=# select distinct clie,count(num_pedido) from pedidos where (cant > 7 and (fab = 'aci' or fab = 'rei')) group by clie having count(num_pedido) >= 2;
 clie | count 
------+-------
 2107 |     2
 2103 |     3
 2111 |     2
(3 rows)



------4
training=# select id_fab,sum(existencias) from productos group by id_fab order by sum(existencias) desc limit 1;
 id_fab | sum 
--------+-----
 aci    | 880
(1 row)

------5

training=# select descripcion,max(precio) from productos group by descripcion having count(descripcion) >= 2;
 descripcion |  max   
-------------+--------
 Reductor    | 355.00
(1 row)

------6

training=# select oficina_rep,count(num_empl),sum(cuota) from repventas where (nombre like '% A%' or nombre like '% S%') and (nombre like 'T%' or nombre like 'S%' or nombre like 'B%' or nombre like 'N%') and (oficina_rep is not null) group by oficina_rep;
 oficina_rep | count |    sum    
-------------+-------+-----------
          13 |     1 | 350000.00
          12 |     1 | 200000.00
          22 |     1 | 300000.00
          21 |     1 | 350000.00
(4 rows)


------7
	
training=# select rep_clie,count(num_clie) from clientes where empresa like '%o%' and empresa like'%i%' group by rep_clie having count(num_clie) > 2;
 rep_clie | count 
----------+-------
      102 |     3
(1 row)


----8
training=# select clie,count(num_pedido) from pedidos group by clie having count(num_pedido) >= 3;
 clie | count 
------+-------
 2118 |     4
 2108 |     3
 2111 |     3
 2103 |     4
(4 rows)

------9

training=# select clie,producto,count(num_pedido) from pedidos group by clie,producto having count(num_pedido) > 1;
 clie | producto | count 
------+----------+-------
 2103 | 41004    |     2
(1 row)

------10

training=# select producto,count(num_pedido),min(importe),sum(cant),sum(importe) from pedidos group by producto having count(num_pedido) > 1 order by sum(importe) limit 1;
 producto | count |  min   | sum |  sum   
----------+-------+--------+-----+--------
 4100x    |     2 | 150.00 |  30 | 750.00
(1 row)


