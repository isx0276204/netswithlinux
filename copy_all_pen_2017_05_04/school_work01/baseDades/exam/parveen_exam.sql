----1.1
training=# select id_fab,id_producto,descripcion,(select count(num_pedido) from pedidos where id_fab = fab and id_producto = producto),( select count(distinct clie) from pedidos where id_fab = fab and id_producto = producto )from productos group by id_fab,id_producto order by 4 desc;
 id_fab | id_producto |    descripcion    | count | count 
--------+-------------+-------------------+-------+-------
 qsa    | xk47        | Reductor          |     3 |     3
 aci    | 41004       | Articulo Tipo 4   |     3 |     2
 bic    | 41003       | Manivela          |     2 |     2
 aci    | 4100x       | Ajustador         |     2 |     2
 aci    | 4100z       | Montador          |     2 |     2
 rei    | 2a44r       | Bisagra Dcha.     |     2 |     2
 fea    | 114         | Bancada Motor     |     2 |     2
 rei    | 2a45c       | V Stago Trinquete |     2 |     2
 imm    | 779c        | Riostra 2-Tm      |     2 |     2
 aci    | 41002       | Articulo Tipo 2   |     2 |     2
 rei    | 2a44g       | Pasador Bisagra   |     1 |     1
 fea    | 112         | Cubierta          |     1 |     1
 aci    | 4100y       | Extractor         |     1 |     1
 aci    | 41003       | Articulo Tipo 3   |     1 |     1
 imm    | 775c        | Riostra 1-Tm      |     1 |     1
 rei    | 2a44l       | Bisagra Izqda.    |     1 |     1
 imm    | 773c        | Riostra 1/2-Tm    |     1 |     1
 aci    | 41001       | Articulo Tipo 1   |     0 |     0
 qsa    | xk48        | Reductor          |     0 |     0
 imm    | 887x        | Retenedor Riostra |     0 |     0
 imm    | 887h        | Soporte Riostra   |     0 |     0
 qsa    | xk48a       | Reductor          |     0 |     0
 bic    | 41672       | Plate             |     0 |     0
 bic    | 41089       | Retn              |     0 |     0
 imm    | 887p        | Perno Riostra     |     0 |     0
(25 rows)


----1.2
training=# select id_fab,id_producto,descripcion,count(num_pedido),count(distinct clie) from productos left join pedidos on  id_fab = fab and id_producto=producto group by id_fab,id_producto order by 4 desc;
 id_fab | id_producto |    descripcion    | count | count 
--------+-------------+-------------------+-------+-------
 qsa    | xk47        | Reductor          |     3 |     3
 aci    | 41004       | Articulo Tipo 4   |     3 |     2
 aci    | 4100x       | Ajustador         |     2 |     2
 imm    | 779c        | Riostra 2-Tm      |     2 |     2
 aci    | 41002       | Articulo Tipo 2   |     2 |     2
 aci    | 4100z       | Montador          |     2 |     2
 bic    | 41003       | Manivela          |     2 |     2
 rei    | 2a45c       | V Stago Trinquete |     2 |     2
 rei    | 2a44r       | Bisagra Dcha.     |     2 |     2
 fea    | 114         | Bancada Motor     |     2 |     2
 imm    | 773c        | Riostra 1/2-Tm    |     1 |     1
 aci    | 41003       | Articulo Tipo 3   |     1 |     1
 aci    | 4100y       | Extractor         |     1 |     1
 fea    | 112         | Cubierta          |     1 |     1
 imm    | 775c        | Riostra 1-Tm      |     1 |     1
 rei    | 2a44g       | Pasador Bisagra   |     1 |     1
 rei    | 2a44l       | Bisagra Izqda.    |     1 |     1
 imm    | 887x        | Retenedor Riostra |     0 |     0
 aci    | 41001       | Articulo Tipo 1   |     0 |     0
 qsa    | xk48        | Reductor          |     0 |     0
 qsa    | xk48a       | Reductor          |     0 |     0
 bic    | 41672       | Plate             |     0 |     0
 bic    | 41089       | Retn              |     0 |     0
 imm    | 887h        | Soporte Riostra   |     0 |     0
 imm    | 887p        | Perno Riostra     |     0 |     0
(25 rows)


----2.1
training=# select num_clie,empresa,(select sum(cant) from pedidos where clie = num_clie) from clientes where num_clie in (select clie from pedidos where fab = 'imm' or fab = 'aci' ) group by num_clie order by 3 desc;
 num_clie |      empresa      | sum 
----------+-------------------+-----
     2103 | Acme Mfg.         |  99
     2111 | JCP Inc.          |  65
     2102 | First Corp.       |  34
     2114 | Orion Corp        |  26
     2109 | Chen Associates   |  22
     2108 | Holm & Landis     |  19
     2118 | Midwest Systems   |  17
     2107 | Ace International |  17
     2112 | Zetacorp          |  13
     2120 | Rico Enterprises  |   2
(10 rows)


----2.2
training=# select num_clie,empresa,sum(cant) from clientes join pedidos on clie =num_clie join productos on id_fab = fab and id_producto = producto where fab = 'imm' or fab = 'aci' group by num_clie order by 3 desc;
 num_clie |      empresa      | sum 
----------+-------------------+-----
     2103 | Acme Mfg.         |  99
     2111 | JCP Inc.          |  59
     2102 | First Corp.       |  34
     2109 | Chen Associates   |  22
     2118 | Midwest Systems   |  10
     2107 | Ace International |   9
     2108 | Holm & Landis     |   9
     2114 | Orion Corp        |   6
     2112 | Zetacorp          |   3
     2120 | Rico Enterprises  |   2
(10 rows)
			--------or easy one------
training=# select num_clie,empresa,sum(cant) from clientes join pedidos on clie =num_clie where fab = 'imm' or fab = 'aci' group by num_clie order by 3 desc;
 num_clie |      empresa      | sum 
----------+-------------------+-----
     2103 | Acme Mfg.         |  99
     2111 | JCP Inc.          |  59
     2102 | First Corp.       |  34
     2109 | Chen Associates   |  22
     2118 | Midwest Systems   |  10
     2108 | Holm & Landis     |   9
     2107 | Ace International |   9
     2114 | Orion Corp        |   6
     2112 | Zetacorp          |   3
     2120 | Rico Enterprises  |   2
(10 rows)

----3.1

----3.2
training=# select id_fab,count( distinct id_producto) as total_producto,count(distinct producto),sum(existencias),sum(cant) from productos left join pedidos on id_fab = fab and id_producto = producto group by id_fab;
 id_fab | total_producto | count | sum  | sum 
--------+----------------+-------+------+-----
 aci    |              7 |     6 | 1390 | 223
 bic    |              3 |     1 |   84 |   2
 fea    |              2 |     2 |  145 |  26
 imm    |              6 |     3 |  330 |  30
 qsa    |              3 |     1 |  354 |  28
 rei    |              4 |     4 |  470 |  60
(6 rows)

		----right one----
		

training=# select id_fab,count (distinct id_producto) as total_producto,count (distinct producto),(select sum (p2.existencias) from productos p2 where p2.id_fab = productos.id_fab),sum(cant) from productos left join pedidos on id_fab = fab and id_producto = producto group by id_fab;
 id_fab | total_producto | count | sum | sum 
--------+----------------+-------+-----+-----
 aci    |              7 |     6 | 880 | 223
 bic    |              3 |     1 |  81 |   2
 fea    |              2 |     2 | 130 |  26
 imm    |              6 |     3 | 321 |  30
 qsa    |              3 |     1 | 278 |  28
 rei    |              4 |     4 | 248 |  60
(6 rows)

----4.1

training=# select nombre,(select count(num_pedido) from pedidos where rep = num_empl),(select sum(cant) from pedidos where rep = num_empl),(select count(distinct clie) from pedidos where rep = num_empl),(select count(num_clie) from clientes where rep_clie = num_empl) from oficinas join repventas on oficina_rep = oficina where ciudad in ('New York','Chicago') group by num_empl;
   nombre    | count | sum | count | count 
-------------+-------+-----+-------+-------
 Dan Roberts |     3 |  45 |     3 |     3
 Paul Cruz   |     2 |  30 |     1 |     3
 Bob Smith   |     0 |     |     0 |     1
 Sam Clark   |     2 |  13 |     2 |     2
 Mary Jones  |     2 |  13 |     1 |     2
(5 rows)

---algo asi---
select (select nombre from repventas where oficina = oficina_rep),(select count(num_pedido) from pedidos where rep in (select num_empl from repventas where oficina = oficina_rep)),(select sum(cant) from pedidos where rep in (select num_empl from repventas where oficina =oficina_rep)),(select count(distinct clie) from pedidos where rep in (select num_empl from repventas where oficina =oficina_rep)),(select count(num_clie) from clientes where rep_clie in (select num_empl from repventas where oficina = oficina_rep)) from oficinas where ciudad in ('New York','Chicago') group by nombre;

----4.2

training=# select nombre,count(num_pedido),sum(cant),count(distinct clie),(select count(num_clie) from clientes where rep_clie = num_empl) from oficinas join repventas on oficina_rep = oficina left join pedidos on rep = num_empl where ciudad in ('New York','Chicago') group by num_empl;
   nombre    | count | sum | count | count 
-------------+-------+-----+-------+-------
 Dan Roberts |     3 |  45 |     3 |     3
 Paul Cruz   |     2 |  30 |     1 |     3
 Bob Smith   |     0 |     |     0 |     1
 Sam Clark   |     2 |  13 |     2 |     2
 Mary Jones  |     2 |  13 |     1 |     2
(5 rows)


----5.1
training=# select id_fab,id_producto,(select sum(importe) from pedidos where fab = id_fab and id_producto = producto)/(select sum(importe) from pedidos)*100 from productos where id_fab in (select fab from pedidos) and id_producto in (select producto from pedidos ) group by id_fab,id_producto order by 3 desc;
 id_fab | id_producto |        ?column?         
--------+-------------+-------------------------
 rei    | 2a44r       | 27.25169667044826013100
 aci    | 4100z       | 15.13983148358236673900
 rei    | 2a44l       | 12.71745844620918806100
 imm    | 775c        | 12.65689912027485859400
 aci    | 4100y       | 11.10254308796040227500
 qsa    | xk47        |  4.03971076865933764200
 imm    | 779c        |  3.78495787089559168500
 aci    | 41004       |  3.21206664755683492700
 aci    | 41002       |  1.96373707563052351500
 fea    | 114         |  1.56969772821781978400
 aci    | 41003       |  1.51196450416042569200
 imm    | 773c        |  1.18090685571942460600
 rei    | 2a45c       |  1.02062650641323261600
 rei    | 2a44g       |  0.84783056308061253700
 fea    | 112         |  0.59751868255205074100
 bic    | 41003       |  0.52646240678910416600
 aci    | 4100x       |  0.30279662967164733500
(17 rows)

-------correcta es

training=# select id_fab,id_producto,(select sum(importe) from pedidos where fab = id_fab and id_producto = producto)/(select sum(importe) from pedidos)*100 from productos where ( id_fab in (select fab from pedidos) or id_fab not in (select fab from pedidos) )and (id_producto in (select producto from pedidos) or id_producto not in (select producto from pedidos) ) group by id_fab,id_producto order by 3 desc;

 id_fab | id_producto |        ?column?         
--------+-------------+-------------------------
 bic    | 41672       |                        
 aci    | 41001       |                        
 imm    | 887x        |                        
 bic    | 41089       |                        
 imm    | 887h        |                        
 imm    | 887p        |                        
 qsa    | xk48        |                        
 qsa    | xk48a       |                        
 rei    | 2a44r       | 27.25169667044826013100
 aci    | 4100z       | 15.13983148358236673900
 rei    | 2a44l       | 12.71745844620918806100
 imm    | 775c        | 12.65689912027485859400
 aci    | 4100y       | 11.10254308796040227500
 qsa    | xk47        |  4.03971076865933764200
 imm    | 779c        |  3.78495787089559168500
 aci    | 41004       |  3.21206664755683492700
 aci    | 41002       |  1.96373707563052351500
 fea    | 114         |  1.56969772821781978400
 aci    | 41003       |  1.51196450416042569200
 imm    | 773c        |  1.18090685571942460600
 rei    | 2a45c       |  1.02062650641323261600
 rei    | 2a44g       |  0.84783056308061253700
 fea    | 112         |  0.59751868255205074100
 bic    | 41003       |  0.52646240678910416600
 aci    | 4100x       |  0.30279662967164733500
(25 rows)

----------more easy ------
training=# select id_fab,id_producto,(select sum(importe) from pedidos where fab = id_fab and id_producto = producto)/(select sum(importe) from pedidos)*100 from productos group by id_fab,id_producto order by 3 desc; id_fab | id_producto |        ?column?         
--------+-------------+-------------------------
 qsa    | xk48        |                        
 imm    | 887p        |                        
 qsa    | xk48a       |                        
 imm    | 887x        |                        
 aci    | 41001       |                        
 imm    | 887h        |                        
 bic    | 41672       |                        
 bic    | 41089       |                        
 rei    | 2a44r       | 27.25169667044826013100
 aci    | 4100z       | 15.13983148358236673900
 rei    | 2a44l       | 12.71745844620918806100
 imm    | 775c        | 12.65689912027485859400
 aci    | 4100y       | 11.10254308796040227500
 qsa    | xk47        |  4.03971076865933764200
 imm    | 779c        |  3.78495787089559168500
 aci    | 41004       |  3.21206664755683492700
 aci    | 41002       |  1.96373707563052351500
 fea    | 114         |  1.56969772821781978400
 aci    | 41003       |  1.51196450416042569200
 imm    | 773c        |  1.18090685571942460600
 rei    | 2a45c       |  1.02062650641323261600
 rei    | 2a44g       |  0.84783056308061253700
 fea    | 112         |  0.59751868255205074100
 bic    | 41003       |  0.52646240678910416600
 aci    | 4100x       |  0.30279662967164733500
(25 rows)



----5.2
training=# select id_fab,id_producto,sum(importe)/(select sum(importe) from pedidos)*100 from productos left join pedidos on id_fab=fab and id_producto = producto group by id_fab,id_producto order by 3 desc;
 id_fab | id_producto |        ?column?         
--------+-------------+-------------------------
 qsa    | xk48        |                        
 imm    | 887p        |                        
 qsa    | xk48a       |                        
 imm    | 887x        |                        
 aci    | 41001       |                        
 imm    | 887h        |                        
 bic    | 41672       |                        
 bic    | 41089       |                        
 rei    | 2a44r       | 27.25169667044826013100
 aci    | 4100z       | 15.13983148358236673900
 rei    | 2a44l       | 12.71745844620918806100
 imm    | 775c        | 12.65689912027485859400
 aci    | 4100y       | 11.10254308796040227500
 qsa    | xk47        |  4.03971076865933764200
 imm    | 779c        |  3.78495787089559168500
 aci    | 41004       |  3.21206664755683492700
 aci    | 41002       |  1.96373707563052351500
 fea    | 114         |  1.56969772821781978400
 aci    | 41003       |  1.51196450416042569200
 imm    | 773c        |  1.18090685571942460600
 rei    | 2a45c       |  1.02062650641323261600
 rei    | 2a44g       |  0.84783056308061253700
 fea    | 112         |  0.59751868255205074100
 bic    | 41003       |  0.52646240678910416600
 aci    | 4100x       |  0.30279662967164733500
(25 rows)
