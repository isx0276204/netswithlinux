#! /bin/bash
# $ su -
# # wget ftp://gandhi/pub/ptf20.sh
# # bash ptf20.sh
# acceptar la llicència (enter, espais, Yes)
# acceptar directori [/opt/pt] enter
# acceptar ennlaç Yes
# $ packettracer &
#
# -------------------------------------------------------
# Instal·lació del Packettracer
# --------------------------------------------------------
# (0) Iniciar sessió com a root
#	$ su - 
# (1) Descarregar el tar.gz a /tmp
#	# cd /tmp
#	# cp /home/groups/inf/public/ASX/M00-Imatges/M07-cisco/CiscoPacketTracer6.0.1forLinux-notutorials /tmp
# (2) Desempaquetar-lo des de /tmp 
#	# tar xvxf CiscoPacketTracer6.0.1forLinux-notutorials
# (3) Executar l'instal·lador de dins del directòri desempaquetat     
# 	# cd PacketTracer601
#	# ./install  	
# -------------------------------------------------------
# Post Instal·lació del Packettracer
# --------------------------------------------------------
# (4) Acabar d'instal·lar paquets:
# 	# yum install libacpi.i686 libacpi.x86_64 libglib-2.0.so.0 libfreetype.so.6 libfontconfig.so.1 openssl-libs libssh libX11.i686 libXrender.so.1 libXext.so.6
# 	# wget ftp://gandhi/pub/libcrypto.so.1.0.0
#       # cp libcrypto.so.1.0.0 /lib/.
#       # rm libcrypto.so.1.0.0 ptf20.sh
# ----------------------------------------------------------
# su -
cd /tmp
cp ./CiscoPacketTracer6.0.1forLinux-notutorials /tmp
tar xvxf CiscoPacketTracer6.0.1forLinux-notutorials
cd PacketTracer601
./install
yum --setopt=protected_multilib=false -y install libacpi.i686 libacpi.x86_64 libglib-2.0.so.0 libfreetype.so.6 libfontconfig.so.1 openssl-libs libssh libX11.i686 libXrender.so.1 libXext.so.6
cp /tmp/libcrypto.so.1.0.0 /lib/.
cp /tmp/ptf20.desktop /usr/share/applications/ptf20.desktop
echo "instalacio finalitzada, escriu:  packettracer"

