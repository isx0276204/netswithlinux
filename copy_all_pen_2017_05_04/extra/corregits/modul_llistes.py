# !/usr/bin/python
# -*-coding: utf-8-*-

# Nom autor: Tania Alvarenga
# Isx434324
# Data 02 Feb 16
# Versió Python 2.7.5

# Descripció: MIS FUNCIONES DE LLISTES! 
#Falta optimizar los de cerca_sequencial i crear una funcion que converteix
#los str del sys.arg a integer ignorant els que no son reals (amb una nova llista si cal)

import modul_datas
import modul_mates

#NO ES NECESSÂRIA
def frequencia_longs(llista) :
	""" Esta funcion es per dirme la frecuencia de longituds de paraules en una llista
	    Cuantas paraules tenen cada longitud(la longitud es la posicio en la llista y el element
	    es la quantitat de paraules que tenen aquesta longitud).
		
	Paramentros entrada :
	Param llista:  llista de strings
	
	Parametros sortida:
	llista_lens: llista de enters
	""" 

	llista_lens = [0]
	long_llista=0
	len_per_augmentar = 0
	
	for paraula in llista :
		longitud = len(paraula)
		# Si encuentro una palabra mas larga que la len de mi lista,
		# aumento su longitud con 0s
		if long_llista < longitud:
			len_per_augmentar = longitud - long_llista
			llista_lens += len_per_augmentar * [0]  
			long_llista = longitud
			
		llista_lens[longitud] += 1
	
	return llista_lens



def frequencia_texts(llista_text,a_buscar) :
	""" Esta funcion es per dirme la frecuencia que te un text(chars_buscar) en un text (llista_text).

	Paramentros entrada :
	Param llista_text:  string o llista de strings
	Param a_buscar: strings o llista de strings	segons l'entrada
	
	Parametros sortida:
	llista_lens: llista de enters
	""" 
	
	llista_final = [0] * len(a_buscar)
	
	for lletra in llista_text :
		num = 0
		while num < len(a_buscar) :
			if lletra == a_buscar[num] :
				llista_final[num] += 1
			num += 1		
	
	return llista_final
	
	

def histograma(llista, capcalera, caracter) :
	""" Esta funcion es per dibuixar un histograma amb una llista introduida
		la capcalera del dibuix y el caracter que vull que dibuixi. 
		
	Paramentros entrada :
	Param llista:  llista de enters positius
	Param capcalera: llista strings
	Param caracter: caracter amb el que vull dibuixar
	
	Parametros sortida:
	res return (dibuix)
	""" 
	
	num = 0
	for element in llista :
		asteriscs = caracter * element
		print capcalera[num], ': ', asteriscs 
		num += 1
	return


def introduir_ordenadament(llista_ordenada, element) :
	""" Esta funcion es per introduir un element en una llista (ordenada) de la posicio que li toca
	per que la llista continui ordenada.
	
	Paramentros entrada :
	Param llista: llista nums/cadenes 
	Param element: num/cadena
	
	Parametros sortida: llista de nums/cadenes
	"""
	
	#Buscar la posicio on hauria d'anar
	pos = busca_posicio(llista_ordenada,element)
	
	#Retallar la llista en dos
	part1 = llista_ordenada[:pos]
	part2 = llista_ordenada[pos:]
	
	#Sumar les 3 llistes
	llista_final = part1 + [element] + part2
	
	return llista_final 


def busca_posicio(llista, insertar) :
	""" Esta funcion es per trobar la posicio on hauria d'anar un element
	en una llista ordenada.
	
	Paramentros entrada :
	Param llista: ints/floats/strs
	Param insertar: ints/floats/strs
	
	Parametros sortida: int >= 0
	"""
	
	posicio = 0
	while posicio < len(llista) and insertar > llista[posicio] :
		posicio += 1
	return 	posicio

def cerca_sequencial(llista, element) :
	"""NO ORDENADA- Esta funcion es per saber si un element esta dins d'una llista
	si es asi, mostrar la posicio on esta, si no returnar -1.
	
	Paramentros entrada :
	Param llista: ints/floats/strs
	Param element: ints/floats/strs
	
	Parametros sortida: int >= 0 (-1 si el element no hi es)
	"""
	cont = 0
	while cont < len(llista) and llista[cont] != element: 
		cont += 1
	
	#mirem si l'hem trobat o no
	
	if cont == len(llista):
		cont = -1
	return cont
	
def cerca_sequencial_ordenada(llista, element) :
	"""NO ORDENADA- Esta funcion es per saber si un element esta dins d'una llista
	ORDENADA ASCENDENT
	si es asi, mostrar la posicio on esta, si no returnar -1.
	
	Paramentros entrada :
	Param llista: ints/floats/strs
	Param element: ints/floats/strs
	
	Parametros sortida: int >= 0 (-1 si el element no hi es)
	"""
	cont = 0
	posicio = -1
	while cont < len(llista) and llista[cont] > element: 
		if llista[cont] == element:
			posicio = cont
		cont += 1
	
	return cont

def compara_segons_camp(llista1,llista2) :
	""" Esta funcion es per comparar una llista de llistes segons el 'CAMP'
	de les subllistes.
	
	Paramentros entrada :
	Param llista: ints/floats/strs de dos elements com a minim
	
	Parametros sortida: 1 , 0, -1  
	"""
	CAMP = 1
	if llista1[CAMP] > llista2[CAMP] :
		return 1
	if llista1[CAMP] < llista2[CAMP] :
		return -1
	return 0
	

def convert_a_num(llista) :
	""" Aquesta funció es per convertir els elements d'una llista a integers
	(si no son reales omitirlos).
	
	Paramentros entrada :
	Param llista: ints/floats/strs (no buida)
	
	Parametros sortida: llista de integers
	"""
	llista_final = []
	
	for element in llista :
		if modul_mates.real(element) :
			element = float(element)
			llista_final.append(element)
	return llista_final




# Test Driver
if __name__ == '__main__' :
	if False :
		print frequencia_texts('hola que tal estas','a')  
		print frequencia_texts(['hola','que','tal','estas'],['hola', 'world'])    
		print frequencia_texts(['veure','a','hello','world'],['veure','hello','tania'])       
		print frequencia_texts(['si que funciona??','mirem','eucalipto','arbre'],['arbre','gabri','mirem','que'])
		print frequencia_texts(['holaaaa','que','mouse','dedos'],['que'])
	if False :
		print busca_posicio([27,65,100,103],70)
		print busca_posicio([27,65,100,103],1000)
		print busca_posicio([27,65,100,103],20)
	if False :
		print introduir_ordenadament([27,65,100,103],70)
		print introduir_ordenadament([27,65,100,103],1000)
		print introduir_ordenadament([27,65,100,103],20)
	if True :
		print cerca_sequencial([150,98,3,-45],150)   #No ordenada
		print cerca_sequencial([150,98,3,-45],98)
		print cerca_sequencial([150,98,3,-45],3)
		print cerca_sequencial([150,98,3,-45],-45)
		print cerca_sequencial([150,98,3,-45],33)
		print cerca_sequencial([98,150,340,-450],98)  #Ordenada
		print cerca_sequencial([9,150,156,890,891],150)
		print cerca_sequencial([-100,-87,-50,-45],-50)
		print cerca_sequencial([-100,-87,-50,-45],-45)
		print cerca_sequencial([150,98,3,-45],33)
		
		
		
	if False :
		print convert_a_int(['96','a','40.5','9','0'])
	if False :
		print compara_segons_camp(['stacey cordero','59'],['zazz mencias','21'])
		print compara_segons_camp(['zazz mencias','21'],['stacey cordero','59'])
		print compara_segons_camp(['stacey cordero','59'],['stacey cordero','59'])
		
