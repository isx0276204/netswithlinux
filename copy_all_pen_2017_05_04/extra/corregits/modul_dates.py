# !/usr/bin/python
# -*-coding: utf-8-*-

# Nom autor: Tania Alvarenga
# Isx434324
# Data 01 April 16
# Versió Python 2.7.5

# Descripció: MIS FUNCIONES DE DATES! amb format correct dd/mm/aa

import time

def dia(fecha) :
	""" Esta funcion es per mostrar el dia d'una data introduida
	
	Paramentros entrada :
	Param fecha:  string (data format correct)
	
	Parametros sortida:
	Int (positius)
	"""
	return int(fecha.split('/')[0])

def mes(fecha) :
	""" Esta funcion es per mostrar el mes d'una data introduida
	
	Paramentros entrada :
	Param fecha:  string (data format correct)
	
	Parametros sortida:
	Int (months)
	"""
	return int(fecha.split('/')[1])

def anyo(fecha) :
	""" Esta funcion es per mostrar l'any d'una data introduida
	
	Paramentros entrada :
	Param fecha:  string (data format correct)
	
	Parametros sortida:
	Int (positius)
	"""
	return int(fecha.split('/')[2])

def format_data_correct(fecha) :
	""" Esta funcion es per saber si el format d'una data es valida. (dd/mm/aa )
	
	Paramentros entrada :
	Param fecha:  string
	
	Parametros sortida:
	Booleana
	"""
	
	format_correct = True
	
	#longitud 10
	if len(fecha) != 8 :
		return False
	
	# 3 camps
	llista_camps = fecha.split('/')
	if len(llista_camps) != 3 :
		return False
	
	# cada camp isdigit amb longitud correcte
	if len(llista_camps[0]) != 2 or not llista_camps[0].isdigit:
		return False
	if len(llista_camps[1]) != 2 or not llista_camps[1].isdigit:
		return False
	if len(llista_camps[2]) != 2 or not llista_camps[2].isdigit:
		return False
			
	return format_correct	
		
def data_valida(fecha) :
	""" Esta funcion es per saber si una data es valida. (dd/mm/aa)
	
	Paramentros entrada :
	Param fecha:  string (data)
	
	Parametros sortida:
	Booleana
	"""
	
	data_correcta = True
	
	if not format_data_correct(fecha) :
		return False
	
	day = dia(fecha)
	month = mes(fecha)
	year = anyo(fecha)
		
	if not (day >= 1 and day <= ultim_dia_mes(month,year)
	   and month >= 1 and month <= 12 and year >= 1 ):
		data_correcta = False

	return data_correcta

def ultim_dia_mes(mes,anyo) :
	""" Esta funcion es per saber si un num(dia) es l'ultim del mes
		
	Paramentros entrada :
	Param mes:  int (positiu)
	Param anyo:  int (positiu)	
	
	Parametros sortida:
	Int positiu
	"""
	mesos = [0,31,28,31,30,31,30,31,31,30,31,30,31]
	
	ultim_dia = mesos[mes]
	
	if mes == 2 and es_de_traspas(anyo):
		ultim_dia = 29
		
	return ultim_dia

def es_de_traspas(anyo) :
	""" Esta funcion es per saber si un any es de traspas
		
	Paramentros entrada :
	Param fecha:  int (positiu)
		
	Parametros sortida:
	Booleana
	"""
	
	# https://es.wikibooks.org/wiki/Algoritmo_bisiesto
	return (anyo>=1) and (anyo%4==0) and (not(anyo%100==0) or (anyo%400==0))
		
def dias_anyo(anyo) :
	""" Esta funcion es per saber quants dias te un any
		
	Paramentros entrada :
	Param fecha:  int (positiu)
		
	Parametros sortida:
	Int positiu
	"""
	# Es traspas?
	if es_de_traspas(anyo) :
		cant_dias = 366
	else :
		cant_dias = 365
	
	return cant_dias



def crear_data(dia,mes,anyo) :
	""" Esta funcion es per crear una data en un format correct. 
		
	Paramentros entrada :
	Param dia:  int
	Param mes: int
	Param anyo: int
	
	Parametros sortida:
	Param fecha_final: string (data)
	"""
	
	return '%02i/%02i/%04i'%(dia,mes,anyo)
	
	
def dia_seguent(fecha) :
	""" Esta funcion es per donarme el seguent dia d'una data introduida.
		
	Paramentros entrada :
	Param fecha:  str (data amb format correct)	
	
	Parametros sortida:
	dia seguent : str (data amb format correct)
	"""
	
	day = dia(fecha)
	month = mes(fecha)
	year = anyo(fecha)

	#Ultim dia del mes?
	if day == ultim_dia_mes(month,year) :
		day = 1
		#Diciembre
		if month == 12 :
			month = 1
			year += 1
		#Demas meses
		else :
			month += 1
	else :
		day += 1

	return crear_data(day,month,year)
	
	
	
def compara_data(fecha1,fecha2) :
	""" Esta funcion es per saber si la fecha1 es mes gran, mes petita, o igual que la fecha2.
	
	Paramentros entrada : fecha1, fecha2 amb format correct 
	
	Parametros sortida:
	enter (-1,1,0)
	"""
	
	dia1 = dia(fecha1)
	mes1 = mes(fecha1)
	anyo1 = anyo(fecha1)
	
	dia2 = dia(fecha2)
	mes2 = mes(fecha2)
	anyo2 = anyo(fecha2)
	
	valor = 0
	#Año mayor o menor
	if anyo1 > anyo2 :
		valor =  1
	elif anyo1 < anyo2 :
		valor =  -1
	#Años iguales
	else :
		#Meses
		if mes1 > mes2 :
			valor =  1
		elif mes1 < mes2 :
			valor =  -1
		else :
			if dia1 > dia2 :
				valor =  1
			elif dia1 < dia2 :
				valor =  -1
	return valor		
	

def data_avui() :
	""" Esta funcion es per donar-me la data d'avui.
	
	Paramentros entrada : res
	
	Parametros sortida:
	data avui en format correcte
	"""
	return time.strftime('%Y-%m-%d')


def dies_medi(data1,data2) :
	""" Esta funcion es per dirme quants dies hi han entre dues datas.  
	
	Paramentros entrada :
	data1 : data amb format correct
	data2 : data amb format correct
	
	Parametros sortida:
	int > 0 si data1 > data2
	0 si són iguals
	int < 0 si data1 < data2 (els dies han passat al revés)'''
	"""

	data_inicial = data1
	data_final = data2
	dies_positius = False
	
	dies = 0
	#si la data_inicial és data2
	if compara_data(data1,data2) == -1 :
		data_inicial = data2
		data_final = data1
		dies_positius = True
		
	#comptem els dies
	while data_final != data_inicial :
		data_final = dia_seguent(data_final)
		dies += 1
		
	#afegim el signe si cal
	if not dies_positius:
		dies = -dies
	
	return dies
	
	
#Test driver
if __name__ == "__main__" :
	if False :
		print format_data_correct('31/01/1999')  #True
		print format_data_correct('31/02/199t')  #False
		print format_data_correct('1/02/1999')   #False
		print format_data_correct('01/2/1996')   #False
		print format_data_correct('31/02/199')   #False
		print format_data_correct('31/02/19')    #False
		print format_data_correct('31/02/9')     #False
	if True :
		print data_valida('29/02/16')   #True
		print data_valida('29/02/17')   #False
		print data_valida('31/04/95')   #False
		print data_valida('31/05/95')   #True
		print data_valida('32/03/16')   #False
		print data_valida('15/13/16')   #False
		print data_valida('')             #False
	if False :
		print ultim_dia_mes('02')
		print ultim_dia_mes('12')
		print ultim_dia_mes('04')
	if False :
		print crear_data(2,3,915)
		print crear_data(02,5,15)
		print crear_data(12,3,5)
		print crear_data(2,03,1915)
	if False :
		print dia_seguent('28/02/2016')
		print dia_seguent('29/02/2016')
		print dia_seguent('28/02/2015')
		print dia_seguent('31/12/2000')
		print dia_seguent('30/04/2011')
	if False :
		print dia('12/02/1996')
		print dia('16/02/1996')
	if False :
		print data_avui()
	if False :
		print dies_medi('29/03/2016','05/04/2016')   #7
		print dies_medi('05/04/2016','29/03/2016')   #-7
		print dies_medi('29/03/2016','29/03/2016')   #0
		
		
