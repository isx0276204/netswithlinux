#!/usr/bin/python
#-*- coding: utf-8-*-

#capçalera complerta

#FUNCIONS

#FORMAT timestamp ==> 'dd/mm/aaaa  HH:MM:SS'


import dates
import funcions_rellotge
	
	
def extreu_data(timestamp):
	'''Funció que donat un timestamp correcte exteu la part corresponent 
	a la data. Sobre aquesta data podrem utilitzar extreuDia etc..
	Entrada: string que representa un timestamp vàlid
	Sortida: string que representa una data vàlida'''
	return timestamp.split()[0]
	
	
	
def extreu_hora(timestamp):
	'''Funció que donat un timestamp correcte exteu la part corresponent 
	a l'hora de rellotge (hh:mm:ss).
	Entrada: string que representa un timestamp vàlid
	Sortida: string que representa una hora de rellotge vàlida'''
	return timestamp.split()[1]
	
	
def suma_timestamp(timestamp, dies, hora):
	'''funció que donat untimestamp li afegeix la hora de rellotge
	Entrada: string que representa un timestamp, string que representa 
	una hora de rellotge vàlides
	Sortida: string que representa un timestamp'''
	
	#Calculem en quina hora acabarà
	hora1 = extreu_hora(timestamp)
	hores = funcions_rellotge.afegeix_hores(hora1, hora)
	hora_r_final = funcions_rellotge.normalitza_hora_rellotge(hores)
	
	#si la hora és anterior a la del timestamp, és que serà de demà, o sigui
	#que sumem un dia més
	if funcions_rellotge.compara_hora(hora1, hora_r_final) > 0:
		dies = dies + 1
	
	#afegim els dies a la data
	data_final = extreu_data(timestamp)
	for i in range(0, dies):
		data_final = dates.dia_seguent(data_final)
	return crea_timestamp(data_final, hora_r_final)
	
def crea_timestamp(data, hora_r):
	'''Funció que crea un timestamp amb el format correste
	Entrada: str (data en format correcte), str (horade r en format correcte
	Sortida: timestamp en format correcte'''
	return '%s %s' % (data, hora_r)
	
def valida_timestamp(s):
	'''Funció que ens diu si un timestamp és correcte, cridant a les funcions 
	de validar de cada part
	Entrada: str
	sortida: bool'''
	
	#controlem que tingui les 2 parts
	s_list = s.split()
	if len(s_list) != 2:
		return False
	
	#que la primera sigui una data
	if not dates.valida_data(s_list[0]):
		return False
		
	#que la segona sigui una hora de rellotge
	if not funcions_rellotge.valida_hora_rellotge(s_list[1]):
		return False
	
	return True
	
	
	
#test drivers 
if __name__ == '__main__' :
	if True:
		print extreu_data('14/03/2016 11:16:48')
		print extreu_hora('14/03/2016 11:16:48')
		
	if True:
		print suma_timestamp('14/03/2016 11:16:48', 8, '23:45:33')
		print suma_timestamp('14/03/2016 11:16:48', 0, '08:45:33')

	
	
