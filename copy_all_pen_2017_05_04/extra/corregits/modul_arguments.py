# !/usr/bin/python
# -*-coding: utf-8-*-

# Nom autor: Tania Alvarenga
# Isx434324
# Data 20 Abril 16
# Versió Python 2.7.5
import sys

def es_real(nombre) :
	""" Esta funcion es per dir si un numero es real.
	
	Paramentros entrada :
	Param a: cadena amb len mes gran que 0
	
	Parametros sortida:
	Booleana: False, True	

	"""

	#Treure el signe
	if nombre[0] == '-' or nombre[0] == '+' :
		nombre = nombre[1:]

	#Si queda buida no es numero
	if len(nombre) < 1 :
		return False
	
	#Separem pel punt
	parts = nombre.split('.')

	#Si te mes de dos punts
	if len(parts) > 2 :
		return False
		
	if len(parts) == 2 : 
		#ambas partes digits
		if not (( parts[0].isdigit() and parts[1].isdigit() ) or 
		
		#primera digit y la segona buida
		( parts[0].isdigit() and parts[1] == "") or
		
		#primera biuda y segona digit
		( parts[0] == "" and parts[1].isdigit() )) :
			return False
	
	#Si no te ningun punt(len del split es 1)
	if not parts[0].isdigit() and len(parts) == 1:
			return False
	return True

def es_enter(nombre) :
	""" Esta funcion es per dir si un numero es enter.
	
	Paramentros entrada :
	Param a: string
	
	Parametros sortida:
	Booleana False, True	
	
	"""

	#Treure el signe si tinguesi
	if nombre[0] == '-' or nombre[0] == '+':
		nombre = nombre[1:]
	
	return nombre.isdigit()

def es_arg_num(opcio):
	''' em diu si un string te format de opcio numerica amb guio i numero.
	Entrada: str
	Sortida: booleana
	'''
	num_opcio = opcio[1:]
	opcio_num = True
	if opcio[0] != '-' or not num_opcio.isdigit():
		opcio_num = False
	return opcio_num

		

## Test Driver

if __name__=="__main__":
	if __name__=="__main__":
	if True :
		print es_arg_num('-2')
		print es_arg_num('-abc')
		print es_arg_num('-')
		print es_arg_num('45')
		print es_arg_num('uno')
	if False :
		print es_enter('-3')        #True
		print es_enter('3.0')       #False
		print es_enter('1.5')       #False
		print es_enter('0')         #True
		print es_enter('a')         #False
		print es_enter('+1')        #True
		print es_enter('9.1')       #False
		print es_enter('34')        #True
	if False :
		print es_real('+4')         #True
		print es_real('4')          #True
		print es_real('3.2')        #True
		print es_real('-5.25')      #True
		print es_real('.3')         #True
		print es_real('7.')         #True
		print es_real('.')          #False
		print es_real('3.2.7')      #False
		print es_real('a.7')        #False
		print es_real('+.4')        #True
		print es_real('+.')         #False
		print es_real('a')          #False
