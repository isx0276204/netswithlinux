# !/usr/bin/python
# -*-coding: utf-8-*-

# Nom autor: Tania Alvarenga
# Isx434324
# Data 02 Feb 16
# Versió Python 2.7.5

# Descripció: MIS FUNCIONES DE MATES! 

def quadrat(a) :
	""" Esta funcion es para dar el cuadrado de un numero.
	
	Paramentros entrada :
	Param a: entero
	
	Parametros sortida:
	a * a	
	"""
	return a * a
	
	
def es_enter(nombre) :
	""" Esta funcion es per dir si un numero es enter.
	
	Paramentros entrada :
	Param a: string
	
	Parametros sortida:
	Booleana False, True	
	
	"""

	#Treure el signe si tinguesi
	if nombre[0] == '-' or nombre[0] == '+':
		nombre = nombre[1:]
	
	return nombre.isdigit()
	

	
def es_real(nombre) :
	""" Esta funcion es per dir si un numero es real.
	
	Paramentros entrada :
	Param a: cadena amb len mes gran que 0
	
	Parametros sortida:
	Booleana: False, True	

	"""

	#Treure el signe
	if nombre[0] == '-' or nombre[0] == '+' :
		nombre = nombre[1:]

	#Si queda buida no es numero
	if len(nombre) < 1 :
		return False
	
	#Separem pel punt
	parts = nombre.split('.')

	#Si te mes de dos punts
	if len(parts) > 2 :
		return False
		
	if len(parts) == 2 : 
		#ambas partes digits
		if not (( parts[0].isdigit() and parts[1].isdigit() ) or 
		
		#primera digit y la segona buida
		( parts[0].isdigit() and parts[1] == "") or
		
		#primera biuda y segona digit
		( parts[0] == "" and parts[1].isdigit() )) :
			return False
	
	#Si no te ningun punt(len del split es 1)
	if not (parts[0].isdigit()):
			return False
	return True
	
	
		

def cadena_binaria(cadena) :
	""" Esta funcion es per saber si una cadena es binaria.
	
	Paramentros entrada :
	Param a: string no buida
	
	Parametros sortida:
	True, False (booleana)
	"""
	
	# La cadena ha de contener solo '0' o '1'
	for a in cadena :
		if a != '0' and a != '1' :
			return False
	return True
	


def binari_a_decimal(cadena) :
	""" Esta funcion es per convertir una cadena binaria a decimal.
	
	Paramentros entrada :
	Param a: string (ip binaria correcta)
	
	Parametros sortida:
	cadena (string) (nombre decimal) 
	"""
	
	POTENCIA_MAYOR = (len(cadena) - 1 )
	decimal = 0
	for a in cadena :
		decimal += (int(a) *  (2 ** (POTENCIA_MAYOR)))
		POTENCIA_MAYOR -= 1
		
	return decimal


def es_primer(nombre) :
	""" Esta funcion es para dir si un nombre es primer.
	
	Paramentros entrada :
	Param a: un nombre entero positiu
	
	Parametros sortida:
	Boolean
	LLETJA!!!!
	"""
	divisor = 2
	
	while divisor < nombre and primo :
		resta = nombre % divisor
		if resta == 0 :
			return False
		divisor = divisor + 1
	return True
	
	
def es_perfect(nombre) :
	""" Esta funcion es para dir si un nombre es perfect 
	(Suma del seus divisors excepte ell mateix és el mateix nombre)
	
	Paramentros entrada :
	Param a: un nombre entero positiu
	
	Parametros sortida:
	Boolean
	LLETJA!!!!
	"""
	#Trobar la suma del seus divisors
	divisor = 1
	suma = 0
	while divisor < nombre :
		resta = nombre % divisor
		#Si es divisor sumo
		if resta == 0 :
			suma += divisor
		divisor = divisor + 1
	
	#Si la suma no es igual al mateix nombre
	if suma != nombre :
		return False
	
	return True


def esCapicua(n): 
	'''Funcio booleana que diu si un nombre és o no capicua 
	Entrada: Int (nombre enter positiu)
	Sortida: Boolean
	MALAMENT!!!!!
	''' 

	for a in range(0, len(n)): 
		if n[a] != n[-a]: 
			return False 
	return True


#Test Driver
if __name__ == "__main__" :
	if False :
		print es_perfect(0)
		print es_perfect(3)
		print es_perfect(6)
		print es_perfect(28)
		print es_perfect(30)
	if False :
		print quadrat(3)
		print quadrat(-3)
		print quadrat(0)
		print quadrat(2)
	if True :
		print enter('-3')        #True
		print enter('3.0')       #True
		print enter('1.5')       #False
		print enter('0')         #True
		print enter('a')         #False
		print enter('+1')        #True
		print enter('9.1')      #False
		print enter('34')       #True
	if False :
		print esCapicua('1221')
	if False :
		print real('+4')        
		print real('4')
		print real('3.2')
		print real('-5.25')
		print real('.3')
		print real('7.')
		print real('.')
		print real('3.2.7')
		print real('a.7')
		print real('+.4')
		print real('+.')
		
