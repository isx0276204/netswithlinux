#!/usr/bin/python
#-*- coding: utf-8-*-

#CAPÇALERA COMPLERTA


#Funcions que treballen amb un string que representa una hora de rellotge
#vàlida, amb el format
#		hh:mm:ss


	
def valida_format_r(s):
	'''Funció que valida que el format d'una hora de rellotge és hh:mm:ss
	ATENCIÓ: Considerem que el format és correcte si és int:int:int
	(considerarem correcte 23:78:123 i ja la normalitzarem
	Entrada: string
	sortida: Bool'''
	#if len(s) != 8:
	#	return False
	l_str = s.split(':')
	if len(l_str) != 3:
		return False
	for el in l_str:
		if len(el) != 2 or not el.isdigit():
			return False
	return True
	
def valida_hora_rellotge(st):
	'''Funció que diu si una hora en format rellotge és o no vàlida
	Entrada: str
	sortida: bool'''
	if not valida_format_r(st):
		return False
	h = extreu_hora(st)
	m = extreu_minut(st)
	s = extreu_segon(st)
	return  h < 24 and 60 > m and 60 > s  #EL NEGATIUS ELS HE DESCARTAT AMB EL ISDIGIT	
	
def extreu_hora(h_rellotge):
	'''Funció que donada una hora de rellotge correcta extreu la hora
	Entrada: string que representa una hora de rellotge vàlida
	Sortida: int '''
	return int(h_rellotge.split(':')[0])
	

def extreu_minut(h_rellotge):
	'''Funció que donada una hora de rellotge correcta extreu els minuts
	Entrada: string que representa una hora de rellotge vàlida
	Sortida: int '''
	return int(h_rellotge.split(':')[1])

def extreu_segon(h_rellotge):
	'''Funció que donada una hora de rellotge correcta extreu els segons
	Entrada: string que representa una hora de rellotge vàlida
	Sortida: int '''
	return int(h_rellotge.split(':')[2])
	
def crea_hora_rellotge(h, m, s):
	'''Funció que donats una hora un minut i un segon crea una hora de rellotge
	Aquesta hora s'haurà de validar
	Entrada: int, int, int
	sortida: str en format hora de rellotge (hh:mm:ss)'''
	return '%02i:%02i:%02i' % (h, m, s)
	
def segon_seguent(h_rellotge):
	'''Funció que donada una hora de rellotge correcta retorna el segon 
	següent.
	ATENCIÓ: PERQUÈ HO FAIG AIXÍ?
	Entrada: hora de rellotge correcta
	sortida: hora de rellotge correcta'''
	h = afegeix_hores(h_rellotge, '00:00:01')
	return normalitza_hora_rellotge(h)
	
def compara_hora(h1, h2):
	'''Funció que donades dues hores de rellotge vàlides, ens diu quina
	és anterior a l'altra.
	PROBLEMA: Com que les hores de rellotge són circulars, si parlem amb 
	propietat no es pot dir quina va abans que l'altra. Aquesta funció només
	ens pot parlar de quina va abans dins del mateix dia, i la primera 
	sempre serà la hora 00:00:00
	
	Entrada: h1 i h2 són hores de rellotge en format correcte vàlides
	Sortida: int :  1 si h1 és posterior a h2, 0 si són igauls i -1 si h2 és 
	a posterior h1 (sempre dins del mateix dia)'''
	#comparo les hores amb l'operador < de cadenes, 
	
	valor = -1
	if h1 > h2:
		valor =  1
	elif h1 == h2:
		valor =  0
	
	return valor
	

#LES FUNCIONS AFEGEIX HORES I NORMALITZA HORES ES poden substituïr per una
#única funció suma hores de rellotge. S'ha fet així per no repetir la
#feina de la funció segon_seguent	
def afegeix_hores(h1, h2):
	'''Funció que donades dues hores en format correcte, suma segons amb segons, 
	minuts amb minuts i hores amb hores.
	ATENCIÓ: el resultat és una hora en format correcte, però no té perquè
	ser vàlida. Faltaria passar per la funció NORMALITZAR
	Entrada: hora de rellotge en FORMAT correcte (int:int:int)
	Sortida: hora de rellotge en FORMAT correcte (no té perquè ser vàlida)'''
	hores = extreu_hora(h1) + extreu_hora(h2)
	minuts = extreu_minut(h1) + extreu_minut(h2)
	segons = extreu_segon(h1) + extreu_segon(h2)
	return crea_hora_rellotge(hores, minuts, segons)
	
def normalitza_hora_rellotge(hora_r):
	'''Funció que donada una hora de rellotge en format correcte (int:int:int) i la
	normalitza convertint-la en una hora vàlida
	ATENCIO: En aquest cas, podem acceptar nombres de mes o menys de dues xifres
	Entrada: hora de rellotge en format correcte
	Sortida: hora de rellotge vàlida'''
	h = extreu_hora(hora_r)
	m = extreu_minut(hora_r)
	s = extreu_segon(hora_r)
	segons_finals = s % 60
	m = m + s / 60
	minuts_finals = m % 60
	hores_finals = (h + m / 60) % 24
	return crea_hora_rellotge(hores_finals, minuts_finals, segons_finals)
	
			
	
	
#test drivers 
if __name__ == '__main__' :
	if False:
		print extreu_hora('13:03:05')
		print extreu_minut('13:03:05')
		print extreu_segon('13:03:05')
		
	if False:
		print valida_format_r('2:200:67')
		print valida_format_r('02:89:89')
		print valida_format_r('01:32:5t')
		print valida_format_r('23:90:900')
		
	if False:
		print valida_hora_rellotge('02:89:89')
		print valida_hora_rellotge('00:34:56')
		
	if False:
		print crea_hora_rellotge(1, 2, 3)
		
	if True:
		print segon_seguent('23:59:59')
		print segon_seguent('23:59:57')
		
	if False:
		print compara_hora('00:00:12', '01:00:33')
		print compara_hora('00:00:12', '00:00:12')

		print compara_hora('03:30:12', '01:00:33')
		
	if False:
		print afegeix_hores('123:55:33', '00:12:45')

	if False:
		print normalitza_hora_rellotge('24:61:16')
		
	
	
	
