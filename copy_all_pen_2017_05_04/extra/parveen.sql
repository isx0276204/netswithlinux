----1
training=# select descripcion,sum(cant),null,null from productos left join pedidos on id_producto = producto and id_fab = fab left join repventas on rep = num_empl left join oficinas on oficina_rep = oficina group by descripcion
union select descripcion,null,ciudad,sum(cant) from productos left join pedidos on id_producto = producto and id_fab = fab left join repventas on rep = num_empl left join oficinas on oficina_rep = oficina group by descripcion,ciudad
order by 1,3;
    descripcion    | sum |  ?column?   | ?column? 
-------------------+-----+-------------+----------
 Ajustador         |     | Chicago     |       30
 Ajustador         |  30 |             |         
 Articulo Tipo 1   |     |             |         
 Articulo Tipo 2   |     | Atlanta     |       54
 Articulo Tipo 2   |     | Los Angeles |       10
 Articulo Tipo 2   |  64 |             |         
 Articulo Tipo 3   |     | Atlanta     |       35
 Articulo Tipo 3   |  35 |             |         
 Articulo Tipo 4   |     | Atlanta     |       34
 Articulo Tipo 4   |     | Chicago     |       34
 Articulo Tipo 4   |  68 |             |         
 Bancada Motor     |     | Denver      |       10
 Bancada Motor     |     | New York    |        6
 Bancada Motor     |  16 |             |         
 Bisagra Dcha.     |     | Chicago     |        5
 Bisagra Dcha.     |     | Los Angeles |       10
 Bisagra Dcha.     |  15 |             |         
 Bisagra Izqda.    |     | New York    |        7
 Bisagra Izqda.    |   7 |             |         
 Cubierta          |     | New York    |       10
 Cubierta          |  10 |             |         
 Extractor         |     | Atlanta     |       11
 Extractor         |  11 |             |         
 Manivela          |     | Denver      |        1
 Manivela          |     | Los Angeles |        1
 Manivela          |   2 |             |         
 Montador          |     | Los Angeles |        6
 Montador          |  15 |             |         
 Montador          |     |             |        9
 Pasador Bisagra   |     | Chicago     |        6
 Pasador Bisagra   |   6 |             |         
 Perno Riostra     |     |             |         
 Plate             |     |             |         
 Reductor          |     | Los Angeles |       28
 Reductor          |     |             |         
 Reductor          |  28 |             |         
 Retenedor Riostra |     |             |         
 Retn              |     |             |         
 Riostra 1/2-Tm    |     | Los Angeles |        3
 Riostra 1/2-Tm    |   3 |             |         
 Riostra 1-Tm      |     | Denver      |       22
 Riostra 1-Tm      |  22 |             |         
 Riostra 2-Tm      |     | Los Angeles |        2
 Riostra 2-Tm      |     | New York    |        3
 Riostra 2-Tm      |   5 |             |         
 Soporte Riostra   |     |             |         
 V Stago Trinquete |     | Los Angeles |       24
 V Stago Trinquete |     |             |        8
 V Stago Trinquete |  32 |             |         
(49 rows)


-----2

training=# select region,null,null,count(num_clie) from clientes join repventas on rep_clie = num_empl left join oficinas on oficina_rep = oficina group by region
union
select region,ciudad,null,count(num_clie) from clientes join repventas on rep_clie = num_empl left join oficinas on oficina_rep = oficina group by region,ciudad
union
select region,ciudad,nombre,count(num_clie) from clientes join repventas on rep_clie = num_empl left join oficinas on oficina_rep = oficina group by region,ciudad,nombre
order by 1,2,3;
 region |  ?column?   |   ?column?    | count 
--------+-------------+---------------+-------
 Este   | Atlanta     | Bill Adams    |     2
 Este   | Atlanta     |               |     2
 Este   | Chicago     | Bob Smith     |     1
 Este   | Chicago     | Dan Roberts   |     3
 Este   | Chicago     | Paul Cruz     |     3
 Este   | Chicago     |               |     7
 Este   | New York    | Mary Jones    |     2
 Este   | New York    | Sam Clark     |     2
 Este   | New York    |               |     4
 Este   |             |               |    13
 Oeste  | Denver      | Nancy Angelli |     1
 Oeste  | Denver      |               |     1
 Oeste  | Los Angeles | Larry Fitch   |     2
 Oeste  | Los Angeles | Sue Smith     |     4
 Oeste  | Los Angeles |               |     6
 Oeste  |             |               |     7
        |             | Tom Snyder    |     1
        |             |               |     1
(18 rows)




--------3
training=# select 'clase a:',empresa,sum(importe) from clientes join pedidos on clie = num_clie group by empresa having sum(importe) > 30000 
union
select 'clase b:',empresa,sum(importe) from clientes join pedidos on clie = num_clie group by empresa having sum(importe) between 7000 and 30000 
union
select 'clase c:',empresa,sum(importe) from clientes left join pedidos on clie = num_clie group by empresa having sum(importe) is null or sum(importe) < 7000
order by 1,3;
 ?column? |      empresa      |   sum    
----------+-------------------+----------
 clase a: | Chen Associates   | 31350.00
 clase a: | J.P. Sinclair     | 31500.00
 clase a: | Acme Mfg.         | 35582.00
 clase a: | Zetacorp          | 47925.00
 clase b: | Holm & Landis     |  7255.00
 clase b: | Orion Corp        | 22100.00
 clase b: | Ian & Schmidt     | 22500.00
 clase b: | Ace International | 23132.00
 clase c: | Jones Mfg.        |  1458.00
 clase c: | Peter Brothers    |  3082.00
 clase c: | Midwest Systems   |  3608.00
 clase c: | Rico Enterprises  |  3750.00
 clase c: | First Corp.       |  3978.00
 clase c: | Fred Lewis Corp.  |  4026.00
 clase c: | JCP Inc.          |  6445.00
 clase c: | Solomon Inc.      |         
 clase c: | Carter & Sons     |         
 clase c: | QMA Assoc.        |         
 clase c: | Smithson Corp.    |         
 clase c: | Three-Way Lines   |         
 clase c: | AAA Investments   |         
(21 rows)



-----4
training=# select region,count(num_clie),null,null,null from clientes join pedidos on clie = num_clie join repventas on rep = num_empl join oficinas on oficina_rep = oficina group by region
union
select region,null,empresa,count(num_pedido),sum(importe) from clientes join pedidos on clie = num_clie join repventas on rep = num_empl join oficinas on oficina_rep = oficina group by region,empresa
order by 1,4 desc,5
;
 region | count |     ?column?     | ?column? | ?column? 
--------+-------+------------------+----------+----------
 Este   |    14 |                  |          |         
 Este   |       | Acme Mfg.        |        4 | 35582.00
 Este   |       | JCP Inc.         |        3 |  6445.00
 Este   |       | Holm & Landis    |        3 |  7255.00
 Este   |       | Jones Mfg.       |        1 |  1458.00
 Este   |       | First Corp.      |        1 |  3978.00
 Este   |       | Ian & Schmidt    |        1 | 22500.00
 Este   |       | J.P. Sinclair    |        1 | 31500.00
 Oeste  |    14 |                  |          |         
 Oeste  |       | Midwest Systems  |        4 |  3608.00
 Oeste  |       | Peter Brothers   |        2 |  3082.00
 Oeste  |       | Fred Lewis Corp. |        2 |  4026.00
 Oeste  |       | Orion Corp       |        2 | 22100.00
 Oeste  |       | Zetacorp         |        2 | 47925.00
 Oeste  |       | Rico Enterprises |        1 |  3750.00
 Oeste  |       | Chen Associates  |        1 | 31350.00
(16 rows)

