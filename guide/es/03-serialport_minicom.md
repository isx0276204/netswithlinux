# Comunicación de puerto serie con linux

## Identificar puerto de consola y cables

Muchos equipos de red de gama profesional suelen llevar un puerto serie 
para configurarse. La comunicación por puerto serie se realiza enviando 
octetos con un bit de start y opcionalmente algún bit de stop y/o control de errores. 
Lo más habitual es que el puerto serie se configure con 8 bits de datos, 
sin bits de control de errores y con un bit de stop. La velocidad puede 
tomar los siguientes valores: 9600bps, 19200bps, 38400bps, 57600bps, 115200 bps

El conector de puerto serie tradicional suele ser del tipo DB9: 

![serial0](../serial0.png)

Son 9 hilos más la masa, aunque no se usan todos. Por eso algunos 
fabricantes para ahorrar espacio y para que te veas obligado a usar 
sus cables especiales modifican este conector poniendo en el extremo 
donde van los equipos informáticos un RJ45. Otros fabricantes tampoco 
usan un esquema de pins estándar aunque el conector tenga forma de DB9. 
Disponer del cable adecuado es imprescindible para poder conectarnos.

Aquí hay algunas fotos con ejemplos de estos cables:

![serial1](../serial1.png)


Hay que ir con cuidado para identificar bien el puerto de consola y 
no confundirlo con un puerto ethernet. 
Aquí hay algunas fotos de puertos de consola:

![serial2](../serial2.png)


Es habitual encontrar indicaciones de la velocidad del puerto serie, 
aunque no siempre pasa y hay que recurrir al manual o bien ir probando 
diferentes velocidades hasta encontrar una que funciona.

## Minicom 

Hoy en día es habitual que los ordenadores no llevan un puerto serie. 

Si existe un puerto serie en la placa base se accede a través de:

/dev/ttyS0 => primera consola de puerto serie del sistema

Como los puertos serie del sistema no son plug and play, existen siempre
definidos en /dev, aunque no se usen o no existan.

Cuando conectamos un adaptador de puerto serie a USB sí que es plug and play.

Primero hemos de ver si el dispositivo se ha conectado bien al puerto usb.

Antes de conectar obtenemos por ejemplo:

```
$ lsusb 
Bus 002 Device 004: ID 045e:078c Microsoft Corp. 
Bus 002 Device 003: ID 0461:4d64 Primax Electronics, Ltd 
Bus 002 Device 002: ID 8087:0024 Intel Corp. Integrated Rate Matching Hub
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 001 Device 002: ID 8087:0024 Intel Corp. Integrated Rate Matching Hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

Y después de conectar:
```
$ lsusb 
Bus 002 Device 004: ID 045e:078c Microsoft Corp. 
Bus 002 Device 003: ID 0461:4d64 Primax Electronics, Ltd 
Bus 002 Device 005: ID 067b:2303 Prolific Technology, Inc. PL2303 Serial Port
Bus 002 Device 002: ID 8087:0024 Intel Corp. Integrated Rate Matching Hub
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 001 Device 002: ID 8087:0024 Intel Corp. Integrated Rate Matching Hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

Ha aparecido una nueva línea:
```
Bus 002 Device 005: ID 067b:2303 Prolific Technology, Inc. PL2303 Serial Port
```

Los logs del sistema nos informan de como ha ido el proceso en el que
udev ha reconocido este puerto:

```
# journalctl -r |grep -i usb | head -n 50 
[...] kernel: usb 2-1.2: pl2303 converter now attached to ttyUSB0
[...] kernel: usbserial: USB Serial support registered for pl2303
[...] kernel: usbcore: registered new interface driver pl2303
[...] mtp-probe[17291]: checking bus 2, device 5: "/sys/devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.2"
[...] kernel: usb 2-1.2: Manufacturer: Prolific Technology Inc.
[...] kernel: usb 2-1.2: Product: USB-Serial Controller
[...] kernel: usb 2-1.2: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[...] kernel: usb 2-1.2: New USB device found, idVendor=067b, idProduct=2303
[...] kernel: usb 2-1.2: new full-speed USB device number 5 using ehci-pci
```

Ha reconocido el adaptador, ha instalado los drivers y permite
que se pueda acceder como si fuera un puerto serie en **/dev/ttyUSB0**

## Instalar minicom

dnf -y install minicom

Para acceder a los puertos serie he de tener permisos para poder usar
esos puertos. Como root:

Para configurar la velocidad y parámetros del puerto

```
# minicom -s


            +-----[configuration]------+
            | Filenames and paths      |
            | File transfer protocols  |
            | Serial port setup        |
            | Modem and dialing        |
            | Screen and keyboard      |
            | Save setup as dfl        |
            | Save setup as..          |
            | Exit                     |
            | Exit from Minicom        |
            +--------------------------+

```
Seleccionamos con las teclas:
[up | down] : seleccionar menú
[enter] : entrar en el menú
[esc]: salir del menú

Entramos en el menú **Serial port setup**:

    +-----------------------------------------------------------------------+
    | A -    Serial Device      : /dev/modem                                |
    |                                                                       |
    | C -   Callin Program      :                                           |
    | D -  Callout Program      :                                           |
    | E -    Bps/Par/Bits       : 115200 8N1                                |
    | F - Hardware Flow Control : Yes                                       |
    | G - Software Flow Control : No                                        |
    |                                                                       |
    |    Change which setting?                                              |
    +-----------------------------------------------------------------------+

Con la tecla [A] accedes a cambiar la ruta hacia el dispositivo y escribes: 

	/dev/ttyUSB0

para que el cambio quede registrado aprietas la tecla [enter]

Después vamos al menú Bps/Par/Bits apretando la tecla [E], aparece un nuevo menú

```
+---------[Comm Parameters]----------+
|                                    |
|     Current: 115200 8N1            |
| Speed            Parity      Data  |
| A: <next>        L: None     S: 5  |
| B: <prev>        M: Even     T: 6  |
| C:   9600        N: Odd      U: 7  |
| D:  38400        O: Mark     V: 8  |
| E: 115200        P: Space          |
|                                    |
| Stopbits                           |
| W: 1             Q: 8-N-1          |
| X: 2             R: 7-E-1          |
|                                    |
|                                    |
| Choice, or <Enter> to exit?        |
+------------------------------------+

```
Con las teclas [A] y [B] seleccionamos la velocidad que deseemos:

Y depués [Enter] para volver al menú anterior guardando cambios.

Y finalmente deseleccionamos el "Hardware Flow Control" hasta que quede en "No"
con la tecla [F]

Y ha de quedar una configuración somo la siguiente:

```
    +-----------------------------------------------------------------------+
    | A -    Serial Device      : /dev/ttyUSB0                              |
    |                                                                       |
    | C -   Callin Program      :                                           |
    | D -  Callout Program      :                                           |
    | E -    Bps/Par/Bits       : 9600 8N1                                  |
    | F - Hardware Flow Control : No                                        |
    | G - Software Flow Control : No                                        |
    |                                                                       |
    |    Change which setting?                                              |
    +-----------------------------------------------------------------------+

```

Con [Enter] guardamos los cambios en memoria.

Si queremos guardar los cambios para que la configuración quede
persistente en el sistema:

Seleccionamos la opción **Save setup as dfl** para no repetir el 
proceso de configuración cada vez.

Si queremos guardar diferentes configuraciones podemos usar la opción 
** Save setup as.. ** y escribir por ejemplo '9600conf' para acceder a los 
dispositivos con esa configuración en un futuro. 

Para salir de la configuración de minicom y volver a la consola de linux
seleccionamos la opción **Exit from Minicom**

Para acceder a la consola serie ejecutamos minicom sin opciones.

Dentro de la consola de minicom accedemos al menú ejecutando Ctrl+A

Con [Ctrl+A] y luego [H] accedemos a la ayuda (help)

Con [Ctrl+A] y luego [Q] salimos de la consola de minicom





























