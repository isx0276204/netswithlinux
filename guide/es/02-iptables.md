# firewall con iptables

**Iptables** es una utilidad que trabaja con el módulo de kernel 
netfilter. Gestiona reglas de firewall que pueden bloquear y 
modificar paŕametros de los paquetes ip.

**Netfilter** organiza la capa de firewall de linux en 
tablas y cadenas.

## Firewall organizado en tablas y cadenas

Un paquete cuando entra y/o sale por una tarjeta de red sigue
un recorrido de cadenas en las cuales podemos aplicar reglas.

El modelo actual es bastante complejo, y conforme fue evolucionando linux
se fue complicando. Para explicarlo existen diferentes diagramas que ayudan a entender
el recorrido que realiza un paquete por las diferentes cadenas. 

Es mejor empezar por un diagrama simplificado como este:
![chains_1](../iptables_diagram_1.png)

A partir de un ejemplo de una navegación web desde un equipo, a 
través de un router y un servidor, mostraremos el recorrido
de un paquete por las diferentes cadenas en los diferentes equipos.

1. El navegador web del cliente genera una petición http y la quiere 
enviar por la red. Ese paquete ha sido generado desde ese 
equipo y ha de salir a la red, a través de la tabla de routing decide
por qué interface sale. Atraviesa las cadenas de OUTPUT y después las de 
POSTROUTING

2. El paquete llega al router. Como entra en el equipo atraviesa las cadenas
de PREROUTING. A continuación en función de su tabla de routing decide 
que ese paquete no es para él y por qué interfaz de salida se ha de reenviar. 
Si es un paquete que se ha de reenviar y no se lo queda el router pasa
por las cadenas de FORWARD y finalmente sale por POSTROUTING

3. El paquete llega al servidor web, entra por las cadenas de PREROUTING y 
a partir de sus tablas de routing decide que es para el. Ese paquete atraviesa
entonces las cadenas de INPUT y el sistema operativo mira si hay algún proceso
con el puerto 80 (http) escuchando para esa dirección ip (orden: netstat -tlnp).
El servidor web analiza la petición y genera una respuesta (en el diagrama esta
fase es la que llaman Local Process). La respuesta se encapsula en un paquete
de respuesta que sale por las cadenas de OUTPUT, y después por POSTROUTING

4. El paquete llega al router, y atraviesa las cadenas de PREROUTING-FORWARD-POSTROUTING

5. El paquete llega al cliente, entra por PREROUTING y a través de sus 
tablas de routing decide que es para el, atraviesa las cadenas de INPUT
y se lo entrega al navegador web.


iptables funciona en cualquier linux, pero sólo en los equipos que tengan
activados el bit de forwarding y actúen como routers atravesarán la 
cadena **forward** cuando un paquete entre por una interface y se redirija
a otra red a través de otra interface. 



Las cadenas se organizan en 3 tablas importantes en función de la
función que desempeñen las reglas de firewall:

- **filter**: filtrar paquetes. Permite acciones como:
  - **DROP**: tirar, descartar, no informa del rechazo
  - **REJECT**: rechaza, pero informa del rechazo
  - **ACCEPT**: deja pasar
- **nat**: NAT(Network Address Translation). En estas tablas
se permite cambiar direcciones o puertos del paquete.
Acciones que sólo se permiten en NAT:

  - **MASQUERADE**: enmascara una ip interna, permite compartir
una ip pública entre varias ips internas
  - **DNAT**: destination NAT. Redirige los puertos. Modifica 
ip y puerto destino
  - **SNAT**: como MASQUERADE pero con más opciones
  
- **mangle**: se usa para marcar paquetes o hacer logs
  - **MARK**: marca el paquete para poderlo usar en otras reglas o 
  para policy routing o QoS
  - **TOS**: modifica el campo TOS (Type of Service) de la cabecera IP, 
  que se puede usar en otros routers para QoS o Policy Routing
  - **LOG**: genera un log del sistema cuando un paquete cumple esta regla
  
Una orden iptables que añade una regla tiene 3 partes diferenciadas como 
las del ejemplo siguiente:



	iptables -t filter -A FORWARD -d 8.8.8.8 -j ACCEPT
	         |                    |          |
	         |                    |          +--> (3) -j = acción
             |                    |
	         |                    +--> (2) condiciones
             |
             +--> (1) -t = tabla / -A = añadir regla a cadena
    

Pasamos a explicar con más detalle estas tres partes:

- (1) Con -t seleccionamos la tabla (si no se especifica por defecto 
es filter. A continuación se especifica el tipo de operación que queremos
hacer sobre una cadena: 
  - -A CHAIN: Add -- añade una regla a la cadena
  - -D CHAIN: Delete -- borra una regla de la cadena
  - -F CHAIN: Flush -- borra todas las reglas de una cadena, si no se 
especifica la cadena borra todas las reglas de todas las cadenas de la tabla
  - -C CHAIN: Crea cadenas definidas por el usuario
  - -X CHAIN: Borra la cadena, si no se especifica una cadena borra todas 
las cadenas creadas por el usuario de esa tabla

- (2) Ahora se van encadenando opciones que se han de cumplir
para que se desencadene una acción. Están las opciones de condición 
básicas de iptables y las opciones extendidas que se organizan en módulos.
Las opciones básicas de iptables:
	* -i, --in-interface name: interface de entrada
	* -o, --out-interface name: interface de salida
	* -s, --source address[/mask]: dirección ip de host o red origen
	* -d, --destionation address[/mask]: dirección ip de host o red destino
  * -p, --protocol protocol: protocolo de transporte, puede tener subopciones
precedidas de dos guiones, para especificar campos de ese protocolo que se han
de cumplir (por ejemplo para especificar un puerto TCP o UDP)
  * -m, --match match : especifica el módulo que se ha de usar, se pasan las opciones del módulo
precedidas de dos guiones.

- (3) Esta es la parte que define la acción, se suele situar al final y 
viene precedida de la opción -j que decide que acción se ha realizar si
se cumple la regla. No todas las acciones están permitidas en todas las
cadenas como hemos visto anteriormente (ACCEPT, DROP, REJECT, MASQUERADE,
DNAT,MARK,LOG...) 

## Las reglas tienen un orden

Las reglas de iptables se evaluan en orden y es imporante introducirlas
en el orden correcto. 

Hay una regla general por defecto en cada cadena. La acción asociada a esa
regla es la que se ejecuta en caso de que ninguna condición de ninunga
regla anterior se cumpla. Por defecto esta regla siempre es ACCEPT. Si
se evaluan todas las reglas de una cadena y no se cumplen se acepta el 
paquete.

Por ejemplo si queremos acceder al servidor web que está alojado en la
dirección ip 192.168.1.10 y tenemos dos reglas de iptables y el orden
es el siguiente:

```
iptables -t filter -A OUTPUT -d 192.168.1.0/24 -j DROP
iptables -t filter -A OUTPUT -p tcp --dport 80 -j ACCEPT
```

No podremos acceder porque hay una primera regla que descarta el paquete
con destino 192.168.1.10, y la segunda regla no se evalúa

Si alteramos el orden el comportamiento cambia

```
iptables -t filter -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -t filter -A OUTPUT -d 192.168.1.0/24 -j DROP

```

Y como queremos comunicarnos con el puerto 80 dejamos que pase el paquete
y la segunda regla no se evalúa


## Connection tracking

Connection tracking hace referencia al seguimiento de las conexiones
de capa 4 que atraviesan el router. Cuando un host cliente se comunica con un
servidor se abre una conexión que queda establecida por una combinación
de direcciones ips origen y destino y puertos (TCP o UDP) origen y destino.
Esta combinación de ips y puertos también se conoce como socket.

El cliente y el servidor manejan esas conexiones y el kernel lleva un
control del estado de estas conexiones que podemos consultar con la orden
netstat. Por ejemplo, con **netstat -tanp** nos lista las conexiones 
activas y el proceso asociado.

Por ejemplo si navegamos con firefox en una página web y a la vez 
listamos con netstat obtenemos:

```
$ netstat -tnp
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 192.168.43.15:34402     216.58.214.174:443      ESTABLISHED 6110/firefox 
```

Todos los paquetes que se intercambien desde la ip local 192.168.43.15 
y el puerto 34402 con la ip remota 216.58.214.174 y el puerto 443 serán 
manejadas por el proceso de firefox con PID 6110. El estado de este socket 
(o conexión) es ESTABLISHED. 

Este seguimiento de las conexiones también lo puede realizar un router 
intermedio, que puede detectar si la conexión desde una ip y un puerto 
hacia otra ip y un puerto se ha iniciado correctamente. Por ejemplo en 
el caso del protocolo TCP hará un seguimiento de los FLAGS para ver si el 
**handshake** del inicio de conexión se ha realizado. 
Mientras se vayan enviando paquetes y no se envíe una señal de FIN, 
entenderá que la conexión está activa. 

Este seguimiento de conexiones es muy importante en los routers para
poder hacer un seguimiento del NAT (cambio de puertos y direcciones) 
y también para no inspeccionar cada paquete (con el gasto de cpu asociado si el filtro es complejo) y analizar los filtros sólo al inicio de la conexión. Una vez la conexión se ha establecido ya se confía en ella y no es necesario analizar cada paquete de la conexión. 


Para poder entender mejor este funcionamiento del connection-tracking 
y observar como realiza el seguimiento de las conexiones 
podemos utilizar la herramienta conntrack

	dnf -y install conntrack-tools

Nos aseguramos de que el módulo de connection tracking está activo:
	
	modprobe nf_conntrack_ipv4
	
Hay más módulos relacionados con connection tracking para poder hacer
seguimiento de otros protocolos que no sean tcp o udp:

	find /lib/modules/4.7.2-201.fc24.x86_64  -name *conntrack*

Podemos ver como hace el seguimiento de una conexión navegando por una 
página web segura (puerto 443) y ejecutando el comando:

```
  conntrack -L
  conntrack v1.4.2 (conntrack-tools): 29 flow entries have been shown.
  [...]
  tcp      6 431991 ESTABLISHED src=192.168.1.164 dst=104.244.42.72 sport=40028 dport=443 src=104.244.42.72 dst=192.168.1.164 sport=443 dport=40028 [ASSURED] mark=0 use=1
  [...]

```

La salida de este comando nos informa de las ips y puertos antes y después
de hacer nat, en este caso son las mismas, ya que no se les ha aplicado
ninguna regla de nat, y no se modifican las ips ni los puertos.

Lo que nos enseña este comando es que iptables puede usar este seguimiento
de las conexiones. Hay un módulo de iptables denominado conntrack que 
servirá para esto.

Del manual de iptables-extensions extraemos:

   conntrack
       This  module,  when combined with connection tracking, allows access to the con‐
       nection tracking state for this packet/connection.

       States for --ctstate:

       INVALID
              The packet is associated with no known connection.

       NEW    The packet has started a new connection or otherwise  associated  with  a
              connection which has not seen packets in both directions.

       ESTABLISHED
              The packet is associated with a connection which has seen packets in both
              directions.

       RELATED
              The packet is starting a new connection, but is associated with an exist‐
              ing connection, such as an FTP data transfer or an ICMP error.

       UNTRACKED
              The packet is not tracked at all, which happens if you explicitly untrack
              it by using -j CT --notrack in the raw table.

       SNAT   A virtual state, matching if the original source address differs from the
              reply destination.

       DNAT   A  virtual  state,  matching if the original destination differs from the
              reply source.

Imaginemos que en un router sólo queremos dar acceso a un servidor web 
(puerto 80) que está en 192.168.22.10 desde la red 192.168.100.0/24

```
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

iptables -t filter -A FORWARD 
         -d 192.168.22.10 -s 192.168.100.0/24 \
         -p tcp --dport 80 \
         -j ACCEPT  

iptables -t filter -A FORWARD 
         -d 192.168.22.10 \
         -p tcp --dport 80 \
         -j DROP   
```

La primera regla analiza si la conexión ya se ha establecido, en ese caso,
ya no hace falta mirar la segunda y la tercera regla.

Si aparece una nueva petición de conexión la primera regla no se cumple
y se evalua la segunda. Si la ip es de la familia 192.168.100.0/24 se 
le deja pasar y si no pertenece a esa familia de ips actúa la tercera
regla y descarta el paquete (DROP)

El módulo de connection tracking en este caso permite que sólo se evaluen las reglas
en el momento de la conexión, una vez la conexión se ha establecido 
no se vuelven a evaluar las reglas.

	

## Ejemplos en clase
```
iptables -t filter -F
iptables -t filter -X
iptables -t nat -F
iptables -t nat -X
iptables -t nat -A PREROUTING -d 10.43.43.200 -p tcp --dport 5001 -j DNAT --to 10.44.44.15:5001
iptables -t nat -L
iptables -t filter -A INPUT -p tcp --dport 5001 -j DROP
iptables -t filter -A FORWARD -p tcp --dport 5001 -j DROP
iptables -t filter -A FORWARD -d 10.44.44.15 -j DROP
iptables -t filter -A FORWARD -s 10.44.44.15 -j DROP
iptables -t filter -L
```

## Para listar las reglas activas:

Si quieres ver las reglas en el mismo formato en el cual se han de introducir
hay que utilizar la orden iptables-save (a veces no viene instalada por 
defecto en la distribución). Si no contesta nada es que no hay ninguna 
regla de firewall activa.

```
iptables-save
```

Si queremos listar las reglas en un formato con columnas usamos:
```
iptables -L
```

Hay opciones que vale la pena conocer para listar las reglas:
 - **-L**: listamos las reglas,
 - **-n**: nos muestra las ips y los puertos en formato numérico
 - **-v**: nos lista los contadores
de paquetes y bytes que han cumplido esa regla (muy interesante para
ver si nuestra regla está siendo efectiva)
 - **--line-numbers**: muestra la numeración de cada regla, es útil si 
queremos eliminar una regla en concreto

```
iptables -n -L -v --line-numbers
```

Si queremos limitar el listado a una cadena, después de -L ponemos el 
nombre de la cadena 



----------------------------------





Ejemplos:
```
(A): iptables -A INPUT -i eth1 -s 192.168.0.0/24 -j DROP
(B): iptables -A INPUT -i eth0 -p tcp --dport 80 -j DROP
(C): iptables -
```

A. bloquea cualquier paquete hacia el host que entre por la tarjeta
eth1 y con ip origen perteneciente a la red 192.168.0.0/24
B. bloquea cualquier paquete TCP con puerto destino 80 que entre por
qualquier tarjeta en el equipo. Aunque haya un servidor web corriendo


##1. Red compartida para todo

```
         internet
      +---------------------------+
                    |DYN_IP_address
                    |wan
                +--------+
                | Router |                           ((-+
                | ISP    |                              |
                |        |              +-))            |.101
                +--------+              |AP          +-------+
                    |lan                |            |tabletA|
 192.168.1.0/24     |.1                 |.100        +-------+
 ------------------------------------------
         |.200            |.10     |.11               ((-+
         |                |        |                     |
  +-------------+     +------+ +-------                  |.102
  |server       |     |host A| |host B|               +-------+
  |www          |     +------+ +------+               |tabletB|
  |ports 80,443 |                                     +-------+
  +-------------+
```

El router del ISP lo hemos sustituido por un equipo linux que tiene 
dos interfaces: wan y lan. La interfaz wan
le conecta a internet a través de una ip dinámica y la interfaz lan
forma parte de una red 192.168.1.0/24 donde se sitúan un servidor web (.200), dos
equipos de trabajo (.10 y .11), un access point que da acceso por wifi
a dos tablets (.101 y .102)

Si hubiese alguna configuración previa en el firewall y queremos eliminarla
hay que borrar todas las reglas y cadenas cradas por el usuario:
  iptables -t filter -X
  iptables -t filter -F
  iptables -t nat -X
  iptables -t nat -F
  iptables -t mangle -X
  iptables -t mangle -F

Si queremos que todos los equipos de la red interna salgan a internet
hay que hacer enmascaramiento, que convierte las ips privadas en ips 
públicas. Este cambio de IP se realiza cuando los paquetes salen del router, 
por lo tanto la cadena que se ha de usar es POSTROUTING de la tabla nat.

El tráfico que se ha de enmascarar es el tráfico que salga por la interfaz wan

  iptables -t nat -o wan -j -MASQUERADE
  
En este caso también sería válida una regla basada en la ip origen:

  iptables -t nat -s 192.168.1.0/24 -j MASQUERADE
  
Si queremos filtrar que el host1 y el host2 no tengan salida a internet
lo haremos en la cadena FORWARD de la tabla filter:

  iptables -t filter -A FORWARD -s 192.168.1.10 -j REJECT
  iptables -t filter -A FORWARD -s 192.168.1.11 -j REJECT
  
Que se podrían juntar en una sola regla haciendo supernetting:

  iptables -t filter -A FORWARD -s 192.168.1.10/31 -j REJECT

Si queremos que desde internet se pueda consultar el servidor web hay
que redirigir los puertos, cambiando la IP destino de los paquetes que
inicialmente van dirigidos al router par redirigirlos al servidor web. 
Esta redirección implica que el paquete con la petición que viene de 
internet ha de ser ruteado hacia el servidor web, y ese cambio de ip 
se ha de producir antes de tomar la decisión de routing, por tanto se 
usará la cadena PREROUTING de la tabla nat. Como la ip de la interfaz
wan será dinámica, no podemos usarla como condición, por eso usamos
la interfaz de entrada y el número de puerto.

  iptables -t nat -A PREROUTING \
           -i wan -p tcp --dport 80 \
           -j DNAT --to 192.168.1.200:80  
  
  iptables -t nat -A PREROUTING \
           -i wan -p tcp --dport 443 \
           -j DNAT --to 192.168.1.200:443

#############################
```
           internet                                         ((-+
        +---------------------------+                          |
                      |DYN_IP_address          +-))            |.101
                      |inet                    |            +-------+
                  +--------+                   |            |tabletA|
                  | Router |eth1  10.0.0.0/24  |.100        +-------+
                  | ISP    +---------------------+
                  |        |.1       |.10     |.11         ((-+
                  +--------+         |        |               |
                      |eth2      +------+ +-------            |.102
   10.10.2.0/24       |.1        |host A| |host B|         +-------+
  --------------------------     +------+ +------+         |tabletB|
           |.10                                            +-------+
           |
    +-------------+
    |server       |
    |www          |
    |ports 80,443 |
    +-------------+

```



           internet
        +---------------------------+
                      |DYN_IP_address
                      |inet
                  +--------+     +-)) wifinnetwork         ((-+
                  | Router |wlan |    192.168.1.0/24          |
                  | ISP    +-----+                            |.101
                  |        |.1                             +-------+
                  +--------+                               |tabletA|
                      |lan                                 +-------+
   10.55.0.0/16       |.0.1
  ---------------------------------------------
           |.1.10            |.2.1    |.2.2
           |                 |        |
    +-------------+      +------- +-------
    |server       |      |host A| |host B|
    |www          |      +------+ +------+
    |ports 80,443 |
    +-------------+


##########################
                                   ********
                                  *        *
                                 *          *
                                 * ROUTER A *
                                 *          *
                                  *        *
                                   ********
                   ********                          ********
                  *        *                        *        *
                 *          *                      *          *
+---------------+* ROUTER A *+--------------------+* ROUTER A *+--------------+
                 *          *                      *          *
                  *        *                        *        *
                   ***+****                          ****+***
                      |                                  |
                      +----+                             |
                      |    |                             |
                      |    |                             |
                      |    |                             |
                      |    |                             |
                      |    |                             |
                      |    |                             |
                      |    |                             |
                      |    |                             |
                      |    |                             |
                      +    +-+                           |
                                                         ++



## Referencias:

[documentación redhat 1](ftp://archive.download.redhat.com/pub/redhat/linux/7.3/de/doc/RH-DOCS/rhl-rg-en-7.3/ch-iptables.html)

[documentación de redhat 2](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Security_Guide/sect-Security_Guide-IPTables.html)

[20 ejemplos de órdenes de iptables para novatos](https://www.cyberciti.biz/tips/linux-iptables-examples.html)

[man de iptables](http://ipset.netfilter.org/iptables.man.html)

[man de iptables-extensions](http://ipset.netfilter.org/iptables-extensions.man.html)

[documentación archlinux sobre iptables](https://wiki.archlinux.org/index.php/Iptables)



[explicación iptables en DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-using-iptables-on-ubuntu-14-04)
