
# Routing avanzado con mikrotik

Si queremos balancear el tráfico de salida a internet o bien redirigir
cierto tráfico por un camino o por otro hay que tener un cuenta varios 
factores:

- Toda una conexión TCP o UDP ha de salir por la misma ruta. Si parte
 de la conexión sale por un router hacia un servidor de internet y parte 
 por otro router, el servidor de internet no es capaz de seguir la conexión
 y nuestro router tampoco. 
 
- En caso de conexiones web, es importante que el tráfico desde una ip origen
hacia un servidor con una ip destino y un puerto destino se mantenga por la 
misma ruta. Los servidores webs actuales realizan múltiples conexiones cuando
navegamos, incluso pueden usar distintos puertos destino. 
Estas conexiones deberían partir de una misma ip origen pública contra la misma 
ip destino, no es buena idea balancear o distribuir el tráfico web desde dos 
ips orígenes distintas si el servidor es web.

- Si existen varias rutas con una misma dirección destino en una misma
tabla de routing, la distancia de la ruta es determinante para decidir
cual es la ruta activa. Tiene preferencia para quedarse como activa 
la ruta con menor distancia

# 
