# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 31-03-2017
#VERSION:- 1.1

########################################################################
#entrada:- una cadena
#descripcion:- orderna un lista
########################################################################
import sys
import funcions_racionals
########################################################################

#lligim data
cadena = raw_input()
lista = cadena.split()

#cheak all num 
lista_new = []
for elem in lista :
	if funcions_racionals.es_racional_valid(elem) :
		lista_new.append(elem)

#main
print lista_new
maxi = funcions_racionals.buscar_maximo_racional(lista_new) 

print maxi
