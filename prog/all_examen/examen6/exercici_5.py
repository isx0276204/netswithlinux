# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 13-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- mirar si es number es capicou or no

#ESPECIFICACIONS D'ENTRADA:-

########################################################################
import math
import modul_examen6
import sys
########################################################################
#control de error
if len(sys.argv) != 2 :
	print 'error_1'
	exit (1)
	
if not(sys.argv[1].isdigit()) :
	print 'error_2'
	exit (1)

cadena_base_3 = sys.argv[1]


for c in cadena_base_3 :
	if c > '3' :
		print 'error_3'
		exit (3)

#main programa
cadena_final_1 = ''
cadena_final_2 = ''

if modul_examen6.es_palindrom(cadena_base_3):
	cadena_final_1 = cadena_base_3,' es capicua'
	
	cadena_decimal = modul_examen6.change_cual_base_3__in_decimal(cadena_base_3,3)
	
	cadena_base_2 = modul_examen6.de_decimal_a_base_b(cadena_decimal,2)
	
	if modul_examen6.es_palindrom(cadena_base_2) :
		cadena_final_2 = cadena_base_2,' es capicua'
	else :
		cadena_final_2 = cadena_base_2
		
else :
	cadena_final_1 =  cadena_base_3,'no es capicua'
	
print cadena_final_1
print cadena_final_2
