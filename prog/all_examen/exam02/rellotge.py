# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 28-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR SEGON DESPUES DE SUMAR IN UN SEGON.

#Especificacions d'entrada:TRES NUMBER INT POSITIVE 
#HORA ENTRES 0 -23 ,MINTO ENTRE 0-59,SEGON ENTRE 0-59

########################################################################

#REFINAMENT

#1. llegim hores, minuts i segons
hora = int(raw_input('hora= '))
minto = int(raw_input('minto= '))
segon = int(raw_input('segon= '))

hora_new = hora
minto_new = minto

#2. incrementem els segons en 1

segon_new = segon + 1 

#3. si ens hem passat de segons, rectifiquem

if segon_new > 59 :
	minto_new = minto + (segon_new / 60)
	segon_new = 0
	#3.1. si ens hem passat de minuts, rectifiquem
	if minto_new > 59 :
			hora_new = hora + (minto_new / 60)
			minto_new = 0
		
		#3.1.1. si ens hem passat d'hores, rectifiquem
			if hora_new > 23 :
				hora_new = 0
#4. mostrem en format 'hores : minuts : segons'
print hora_new,':',minto_new,':',segon_new
