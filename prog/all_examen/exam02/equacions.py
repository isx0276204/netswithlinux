# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 28-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- Programa per a resoldre equacions de segon grau
#Especificacions d'entrada: especificacions d’entrada: siguin a, b, c 
#nombres float qualssevol


########################################################################

#REFINAMENT

#llegim a, b i c    (Els coeficients de l’equació)
number_a = float(raw_input('number_a= '))
number_b = float(raw_input('number_b= '))
number_c = float(raw_input('number_c= '))

#si és un cas no degenerat: (a != 0)
if number_a != 0 :
	#CALCULAR EL DISCRIMINANT RESPECTO DE FORMULA
	discri = ((number_b)**2 - 4 * number_a * number_c)

	#SI EL DISCRIMINANT MES GRAN DE ZERO
	if discri > 0 :
		#CALCULAR LOS DOS SOLUTION
		solution_1 = (-(number_b) + (discri)**(1.0 / 2)) / (2 * number_a)
		solution_2 = (-(number_b) - (discri)**(1.0 / 2)) / (2 * number_a)
		print 'dues sol-lucions',solution_1,solution_2
	#SI EL DISCRIMINANT ES ZERO
	elif discri == 0 :
	#CALCULAR ONE SOLUTION
		solution_1 = -number_b / (2 * number_a)
		print 'dues sol-lucions',solution_1
	#SI NO HA SOLUTION
	else :
		print 'no solution'


#sinó:   (a == 0)
else :
	#mirar si c == 0
	if number_c == 0 :
		print 'infinites sol-lucions'
	else :
		#mirar si b == 0
		if number_b == 0 :
			print 'no sol-lucions'
		else :
			#calcular sol-lucions
			solcion = -(number_c) / number_b
			print 'equcio de primer grau:',solcion
		
