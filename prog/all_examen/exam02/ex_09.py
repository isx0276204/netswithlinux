# !/usr/bin/python
# -*-cofing: utf-8*-

########################################################################
#nom: Arnau Esteban Contreras
#isx: 47590131
#data: 18/10/16
#versio v.2
########################################################################
#Joc de proves:
# ______entrada__________sortida_________________________________________
# |  a	 |  b  |  c  |   solucio  				   | equacio1 | equacio2|    		
# |------|-----|-----|-----------------------------|		  |			|
# |	 2   | -4  |  4	 | L arrel no te solucions 	   |		  |  		|
# |4,1   |  8  |  5	 | L arrel no te solucions     |		  |			|
# |  1   |  3  |  2	 | L arrel te dos solucions    |	-1.0  |	 -2.0	|
# |  -2  |  4  |1,2	 | L arrel te dos solucions    |   -0.26  |	 2.26	|
# |1,5   | -6  |  3  | L arrel te dos solucions    |	3.41  |	 0.58	|
# |  1	 |  2  |  1  | L arrel te una solucio      |	-1.0  |	 		|
# |  5	 |  2  |  2  | L arrel no te solucions	   |		  |	 		|
# |  1   |  0  |  0  | L arrel te una solucio	   |	-0.0  |	 		|
# -----------------------------------------------------------------------
########################################################################

#descripcio: donades a, b i c com a floats et dira quantes solucions
#tindra larrel.
#especificacions dentrada: a, b , c float i a no pot ser 0

#llegim lequacio.
a = float(raw_input('Introdueix a: '))
b = float(raw_input('Introdueix b: '))
c = float(raw_input('Introdueix c: '))

#calculem el discriminant
discriminant = b**2.0 -4.0 * a * c

#si es positiu (larrel) -te dos solucions
if discriminant > 0:
	print 'L arrel te dos solucions.' 
	equacio = ((-b) + (discriminant)**(1/2.0))/(2.0*a)
	print 'x1 =', equacio
	equacio2 = ((-b) - (discriminant)**(1/2.0))/(2.0*a)
	print 'x2 =', equacio2
	
#si es 0 -te una solucion
elif discriminant == 0: 
	print 'L arrel te una solucio.' 
	equacio3 = ((-b)/(2.0*a))
	print 'x1 =',equacio3
	
#si no -no te solucio
else:
	print 'L arrel no te solucions.' 
		



		






