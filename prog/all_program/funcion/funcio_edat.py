# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 18-01-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA MIRAR SI ES MEJOR DE EDAD
# I CONTROL DE ERROR.

#ESPECIFICACIONS D'ENTRADA:- CUAL QUIRO COSA WITH CONTROL DE ERROR.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		0									FALSE
#		5									FALSE
#		18									TRUE
#		30									TRUE
#		
#		
#		
#		
#		
#		
#		        										
#											
#																		
#		
#		
#		
#	
#	
#	

########################################################################

#REFINAMENT

#IMPORT MODUL
import sys

########################################################################

#NOW FUNCTION ES_MAJOR_DE_EDAT
def es_major_de_edat(cheak_edat) :
	'''
	UNA FUNCIO QUE DICE SI ES MAJOR DE EDAT OR NO
	ENTRADA:- UNA INT POSITIVE NUMBER
	RETURN:- BOLLEAN
	'''
	return cheak_edat >= 18 
	#JOC DE PROVA
	#PRINT ES_MAJOR_DE_EDAT(5) --> FALSE
	#PRINT ES_MAJOR_DE_EDAT(18)--->TRUE
########################################################################

#CONTROL DE ERROR
if len(sys.argv) != 2 :
	print 'only need one argument for this programa'
	exit(1)
	
edat = sys.argv[1]


#BUCLE FOR SI ES UNA INT NUMBER
for c in edat :
	if c < '0' or c > '9' :
		print 'must need int number'
		exit(2)

########################################################################



#CHANGE IN INT FORM
edat_ok = int(edat)

#MOSTRA DATA
print es_major_de_edat(edat_ok)
