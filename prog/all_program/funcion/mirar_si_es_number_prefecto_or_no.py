# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 20-01-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA MIRAR SI ES NUMBER ES PREFECTO OR NO.

#ESPECIFICACIONS D'ENTRADA:- CUAL QUIRO COSA WITH CONTROL DE ERROR.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		0 5									no valida
#		0									no valida
#		5									FALSE
#		28									TRUE
#		496 								TRUE
#		6									true

########################################################################

#REFINAMENT



#IMPORT MODUL
import sys

########################################################################

#NOW FUNCTION FOR NUMBER IS PREFETO OR NO
def number_prefecto(number_ok) :
	'''
	UNA FUNCIO QUE MIRAR SI ES  PREFECTO OR NO
	ENTRADA:- UNA INT POSITIVE NUMBER GRAN DE ZERO
	RETURN:- BOLLEAN
	'''
	suma = 0
	for d in range(1,number_ok) :
	
		if number_ok %  d == 0 :
			suma += d

	return suma == number_ok
	
	#joc de prova
	#	print number_prefect(6) --> true
	#	print number_prefect(7) ---> false
########################################################################

#CONTROL DE ERROR
if len(sys.argv) != 2 :
	print 'only need one argument for this programa'
	exit(1)
	
number = sys.argv[1]


#BUCLE FOR SI ES UNA DIGITS
for c in number :
	if c < '0' or c > '9' :
		print 'must need int positive gran de zero number'
		exit(2)

########################################################################




#CHANGE IN INT FORM
number = int(number)

#MOSTRA DATA
print number_prefecto(number)
