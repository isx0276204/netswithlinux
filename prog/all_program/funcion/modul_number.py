# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 24-01-2017
#VERSION:- 1.1

########################################################################

#IMPORT MODUL
import sys
import math

########################################################################

def quadrat(number) :
	'''
	UNA FUNCIO CALCULAR QUADRAT DE UNA NUMBER
	ENTRADA:- UNA INT NUMBER 
	RETURN:- UNA INT NUMBER
	'''
	return number * number
	

def es_int(number) :
	'''
	UNA FUNCIO MIRAR SI ES UNA NUMBER ES INT OR NOT
	ENTRADA:- UNA STR
	RETURN:- BOOLEAN
	'''
	if number[0] == '+' or number[0] == '-' :
		number = number[1:]
	
	for c in number :
		if not (c >= '0' and c < '9') :  
			return False
	return True

	
def es_real(number) :
	'''
	UNA FUNCIO MIRAR SI ES UNA NUMBER ES REAL OR NOT
	ENTRADA: UNA STR
	RETURN:- BOOLEAN
	'''
	if es_int(number) :
		return True
		
	if '.' in number :
		if number[0] == '+' or number[0] == '-' :
			number = number[1:]
		for i in range(0,len(number)) :
			if number[i] == '.' :
				cadena_1 = number[:i]
				cadena_2 = number[i+1:]
				return (cadena_1.isdigit() and cadena_2.isdigit()) 
	return False				
	

def suma(a,b) :
	'''
	FUNCIO SUMA DOS VARIABLE
	ENTRADA:- DOS INT NUMBER
	RETURN:- UNA INT
	'''
	return a + b
	
def ip_binari_en_decimal(ip) :
	'''
	UNA FUNCIO CHANGE IP BINARI IN DECIMAL
	ENTRADA:- ALL NUMBER BINARI 0 I 1 IN STR FORM
	RETURN:- UNA CADENA
	'''
	count = 0
	limite = 128
	suma = 0
	cadena_new = ''
	for  c in ip :
		if c == '1' :
			suma += limite
		count +=1
		limite /= 2
		if count == 8 :
			cadena_new += str(suma) + '.'
			count = 0
			limite = 128
			suma = 0
	return cadena_new[:-1]
	
def es_major_de_edat(cheak_edat) :
	'''
	UNA FUNCIO QUE DICE SI ES MAJOR DE EDAT OR NO
	ENTRADA:- UNA INT POSITIVE NUMBER
	RETURN:- BOLLEAN
	'''
	return cheak_edat >= 18 

def es_primo_number_or_no(number) :
	'''
	FUNCIO ES CHEAK NUMBER IS PRIMO OR NOT
	ENTRADA:- UNA INT POSITIVE NUMBER
	RETURN:- BOOLEAN
	'''
	for i in range(2,number-1) :
		if number % i == 0 :
			return False
	return True

def segons_change_formato_correcta(segon) :
	'''
	FUNCION CHANGE SEGON EN FORMATO HH:MM:SS
	ENTRADA:- UNA INT POSITIVE NUMBER
	RETURN:- CADENA
	'''
	hora = segon / 3600
	segon = segon % 3600
	minto = segon / 60
	segon = segon % 60
	cadena = str(hora) + ':' + str(minto) + ':' + str(segon)
	return cadena
	
def find_lletra_de_nif(number) :
	'''
	FUNCIO PARA CALCULAR LA LLETRA DE NIF
	ENTRADA:- INT POSITIVE NUMBER
	RETURN:- CADENA
	'''
	cadena = 'TRWAGMYFPDXBNJZSQVHLCKE'
	return cadena[number % 23]
	
def validar_dni(dni) :
	'''
	FUNCIO PARA MIRAR SI UNA NUMBER DE DNI ES CORRECTO OR NO
	ENTRADA:- UNA CADENA
	RETURN:- BOOLEAN
	'''
	if len(dni) != 9 :
		return False
	cadena = 'TRWAGMYFPDXBNJZSQVHLCKE'
	if not(dni[:-1].isdigit()) or dni[-1] not in cadena :
		return False
	return find_lletra_de_nif(int(dni[:-1])) == dni[-1] 
	
	
###########################TEST DRIVER##################################

if __name__ == '__main__' :
	if False :
		sys.stdout.write(str(quadrat(4)) + '\n')
		sys.stdout.write(str(quadrat(-4)) + '\n')
		sys.stdout.write(str(quadrat(0)) + '\n\n')
	if False :
		sys.stdout.write(str(es_int('4')) + '\n')
		sys.stdout.write(str(es_int('4.4')) + '\n')
		sys.stdout.write(str(es_int('A')) + '\n\n')
	if False :
		sys.stdout.write(str(es_real('4')) + '\n')
		sys.stdout.write(str(es_real('4.4')) + '\n')
		sys.stdout.write(str(es_real('A')) + '\n')
		sys.stdout.write(str(es_real('+4.4')) + '\n')
		sys.stdout.write(str(es_real('.4')) + '\n\n')
	if False :
		sys.stdout.write(str(suma(4,5)) + '\n')
		sys.stdout.write(str(suma(-4,5)) + '\n\n')
	if False :
		sys.stdout.write(str(ip_binari_en_decimal('11111111111111111111111111111111')) + '\n')
		sys.stdout.write(str(ip_binari_en_decimal('11111111111111111111111111111110')) + '\n')
		sys.stdout.write(str(ip_binari_en_decimal('01111111111111111111111111111110')) + '\n')
		sys.stdout.write(str(ip_binari_en_decimal('00000000000000000000000000000000')) + '\n\n')
	if False :
		sys.stdout.write(str(es_major_de_edat(0)) + '\n')
		sys.stdout.write(str(es_major_de_edat(18)) + '\n')
		sys.stdout.write(str(es_major_de_edat(30)) + '\n\n')
	if False :
		sys.stdout.write(str(es_primo_number_or_no(3)) + '\n')
		sys.stdout.write(str(es_primo_number_or_no(10)) + '\n')
		sys.stdout.write(str(es_primo_number_or_no(5)) + '\n')
		sys.stdout.write(str(es_primo_number_or_no(31)) + '\n')
		sys.stdout.write(str(es_primo_number_or_no(17)) + '\n\n')
	if False :
		sys.stdout.write(segons_change_formato_correcta(3) + '\n')
		sys.stdout.write(segons_change_formato_correcta(60) + '\n')
		sys.stdout.write(segons_change_formato_correcta(3600) + '\n')
		sys.stdout.write(segons_change_formato_correcta(3666) + '\n')
		sys.stdout.write(segons_change_formato_correcta(0) + '\n\n')
	if False :
		sys.stdout.write(find_lletra_de_nif(48090390) + '\n')
		sys.stdout.write(find_lletra_de_nif(60123456) + '\n')
		sys.stdout.write(find_lletra_de_nif(360055555) + '\n')
		sys.stdout.write(find_lletra_de_nif(3666) + '\n')
		sys.stdout.write(find_lletra_de_nif(0) + '\n\n')
	if True :
		sys.stdout.write(str(validar_dni('48090390N')) + '\n')
		sys.stdout.write(str(validar_dni('60123456')) + '\n')
		sys.stdout.write(str(validar_dni('3600555552')) + '\n')
		sys.stdout.write(str(validar_dni('3666555WH')) + '\n')
		sys.stdout.write(str(validar_dni('48090390B')) + '\n\n')
