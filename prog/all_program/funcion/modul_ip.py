# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 27-01-2017
#VERSION:- 1.1

########################################################################

#IMPORT MODUL
import sys
import math

########################################################################

def es_cadena_binaria(cadena) :
	'''
	FUNCIO PARA MIRAR SI ES UNA CADENA BINARIA OR NO
	ENTRADA:- UNA CADENA
	RETURN:- BOOLEAN
	'''
	for c in cadena :
		if c not in '10' :
			return False
	return True
	
def ip_binario_valida(ip_binari) :
	'''
	FUNCIO PARA MIRAR UNA IP BINARIO VALIDA OR NO
	ENTRADA:- UNA CADENA
	RETURN:- BOLLEAN
	'''
	if len(ip_binari) != 32 :
		return False
	return es_cadena_binaria(ip_binari)
	
def binari_chage_in_decimal(binari) :
	'''
	FUNCIO CHANGE CADENA BINARIO IN DECIMAL
	ENTRADA:- UNA CADENA EN BINARI
	RETURN:- UNA INT POSITIVE ALSO ZERO POSIBLE
	'''
	start = 1
	suma = 0
	for i in range(len(binari)-1,-1,-1) :
		if binari[i] == '1' :
			suma += start
			
		start *= 2
	return suma

def ip_binari_en_decimal(ip_binari) :
	'''
	FUNCION PARA CAMBIAR IP BINARI TO DECIMAL FORMAT
	ENTRADA:- UNA CADENA IP VALIDA
	RETURN:- UNA CADENA IP DECIMAL
	'''
	inici = 0
	final = 8
	cadena_new = ''
	for  i in range(0,4) :
		cadena_new += str(binari_chage_in_decimal(ip_binari[inici:final])) + '.'
		inici += 8
		final += 8
	return cadena_new[:-1]
			
##########################TEST DRIVE####################################
if __name__ == '__main__' :
	if True :
		sys.stdout.write(str(es_cadena_binaria('010101')) + '\n')
		sys.stdout.write(str(es_cadena_binaria('-a101')) + '\n')
		sys.stdout.write(str(es_cadena_binaria('010156')) + '\n\n')
	if True :
		sys.stdout.write(str(ip_binario_valida('11111111111111111111111111111110')) + '\n')
		sys.stdout.write(str(ip_binario_valida('1111111111111111111111111111111')) + '\n')
		sys.stdout.write(str(ip_binario_valida('111111111111111111111111111111A')) + '\n\n')
	
	if True :
		sys.stdout.write(str(binari_chage_in_decimal('11111111')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('00000000')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('00000001')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('00000010')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('00000101')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('00000001')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('00001001')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('00010001')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('00100001')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('01000001')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('10000001')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('01111110')) + '\n')
		sys.stdout.write(str(binari_chage_in_decimal('')) + '\n\n')
	if True :
		sys.stdout.write(str(ip_binari_en_decimal('11111111111111111111111111111111')) + '\n')
		sys.stdout.write(str(ip_binari_en_decimal('00111111111111111111111111111110')) + '\n')
		sys.stdout.write(str(ip_binari_en_decimal('01111111111111111111111111111110')) + '\n')
		sys.stdout.write(str(ip_binari_en_decimal('01111111111111111111111111111111')) + '\n')
		sys.stdout.write(str(ip_binari_en_decimal('01111111111111111111111111111100')) + '\n\n')
