# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 19-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- MODUL

#ESPECIFICACIONS D'ENTRADA:- 

########################################################################
import math
import sys
import some_importe_funcion_diccionaris
########################################################################
text = sys.argv[1]

lista_text = text.split()

dic = some_importe_funcion_diccionaris.compta_repetions_parula(lista_text)

#print len(lista_text)
#print len(dic)

tupla_text = some_importe_funcion_diccionaris.make_tupla(dic)

tupla_text.sort

some_importe_funcion_diccionaris.histrograma_orderna_dicc_amb_tupla(dic,tupla_text)

tupla_text.sort(cmp=some_importe_funcion_diccionaris.comprara_segon_camp)

some_importe_funcion_diccionaris.histrograma_orderna_dicc_amb_tupla(dic,tupla_text)


#some_importe_funcion_diccionaris.histrograma(dic)

#some_importe_funcion_diccionaris.mostra_dicc(dic)
