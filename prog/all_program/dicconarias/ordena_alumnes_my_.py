# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 14-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- MODUL

#ESPECIFICACIONS D'ENTRADA:- 

########################################################################
import math
import sys
########################################################################
def avg(data) :
	'''
	funcion for calculate to avg of given  lista format
	entrada:- lista 
	return:- una lista de avg
	'''
	lista_avg = []
	for i in range(0,len(data)) :
		suma = 0
		count = 0
		for sub_elem in data[i][2] :
			suma += sub_elem
			count += 1
		avg = suma / count
		data[i][2] = avg
	return data
	
dadesAlumnes = [['12345678A','jordi perez',[5.2,8,9,4.3]],['52345678A',\
'manel garcia',[2,1.5,3,5]],['62345678A','joan-ramon vila',[7,7,8,10]],\
['72345678A','maria casas',[7,2.5,9,3]],['111111111D','josep-lluis marquez',\
[3,10,5]]]

print avg(dadesAlumnes)
