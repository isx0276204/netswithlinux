# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 14-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- MODUL

#ESPECIFICACIONS D'ENTRADA:- 

########################################################################
import math
import sys
########################################################################

def find_in_dicc(dicc,valor) :
	'''
	funcion tell you if clave in the dicc or not
	param:- dicc in correct form i una valor
	return:- boolean
	'''
	for k in dicc :
		if valor == dicc[k] :
			return True
	return False

def make_lista_for_dicc(dicc) :
	'''
	funcion make lista de keys
	param:- dicc correct from
	return:- lista de keys
	'''
	lista_keys = []
	for k in dicc :
		lista_keys.append(k)
	return lista_keys
	
def make_lista_of_valor_form_dicc(dicc) :
	'''
	funcion make lista de valor
	param:- dicc correct from
	return:- lista de valor
	'''
	lista_valor = []
	for k in dicc :
		if dicc[k] not in lista_valor :
			lista_valor.append(dicc[k])
	return lista_valor
	
def change_formato_data_num_to_lletra(date) :
	'''
	funcio change date numeric form to alfabatic
	param:- correct form of date in str
	return:- data in llatra
	'''
	dicc = {1:'jan',2:'feb',3:'mar',4:'apr',5:'may',6:'jun',7:'jul',8:'aug',9:'sep',10:'oct',11:'nov',12:'dec'}
	month = dicc[int(date[3:5])]
	day = date[:2]
	year = date[6:]
	date_latter = day + ' de ' + month +' de '+ year
	return date_latter
	
def change_formato_data_llatra_to_num(date) :
	'''
	funcio change date  alfabatic to number
	param:- correct form of date in str
	return:- data in num
	'''
	dicc = {1:'jan',2:'feb',3:'mar',4:'apr',5:'may',6:'jun',7:'jul',8:'aug',9:'sep',10:'oct',11:'nov',12:'dec'}
	month = date[6:9]
	day = date[:2]
	year = date[13:]
	for key in dicc :
		if dicc[key] == month :
			month = str(key)
	date_num = day + '/' + month +'/'+ year
	return date_num

def histrograma(dicc) :
	'''
	funcion printa una histrograma de programa
	entrada:- dicc tipo {'a':5,'b':5}
	return: res (print histrograma)
	print histrograma(['a','e','i','o','u'],'hola hola hola')
	a --> ***
	e --> 
	i --> 
	o --> ***
	u --> 
	'''
	CHAR = '*'
	for key in dicc :
		print '%s    : %s' %(key,dicc[key] * CHAR)
	return

def compta_repetions(cadena,elem) :
	'''
	funcion compta char in text
	entrada:- str and,str(caracter de buscar)
	return:- diccionarion
	'''
	dicc = {}
	for char in cadena :
		if char in elem :
			if char in dicc :
				dicc[char] += 1
			else :
				dicc[char] = 1
	return dicc
	
def compta_repetions_parula(lista) :
	'''
	funcion compta char in text
	entrada:- str and,str(caracter de buscar)
	return:- diccionarion
	'''
	dicc = {}
	for elem in lista :
			if elem in dicc :
				dicc[elem] += 1
			else :
				dicc[elem] = 1
	return dicc
	
def make_tupla(dicc) :
	'''
	funcion make tupla de dicc
	entrada:- dicc tipo {'a':5,'d':5}
	return:- tupla tipo [('a',5),('b',5)]
	'''
	tupla = []
	for k in dicc :
		tupla.append((k,dicc[k]))
	return tupla
	
def comprara_segon_camp(lista_a,lista_b) :
	####falta terminar####
	'''
	funcio compra lista for segun camp
	parametro:- dos lista
	return:- -1,0,1
	'''
	valor = 0
	if lista_a[1] > lista_b[1] :
		valor = 1
	elif lista_a[1] < lista_b[1] :
		valor = -1
	return valor

def mostra_dicc(dicc) :
	'''
	funcion fro print dicc keys and valor
	entrada:- dicc tip {'a':5,'b':10}
	reurn:- res(print de dicc)
	'''
	for k in dicc :
		print 'clau:',k,' valor:',dicc[k]
	return
	
def histrograma_orderna_dicc_amb_tupla(dicc,tupla_ordenada) :
	'''
	funcion printa dicc k and valor using tupla by order
	entrada:- dicc tipo {'a':5,'b':6} tupla tipo
	return:-res
	'''
	char = '*'
	for elem in tupla_ordenada :
		print '%10s:%s' % (elem[0],dicc[elem[0]] * char)
	return
	
if __name__ == '__main__' :
	if False  :
		print find_in_dicc({'a':123,'b':124,'c':786},123)
		print find_in_dicc({'a':123,'b':124,'c':786},'d')
	if False :
		print make_lista_for_dicc({'a':123,'b':124,'c':786})
		print make_lista_for_dicc({'a':123,'b':124,1:786})
	if False :
		print make_lista_of_valor_form_dicc({'a':123,'b':124,'c':786})
		print make_lista_of_valor_form_dicc({'a':123,'b':124,'c':'acd','ok':'b'})
	if False :
		print change_formato_data_num_to_lletra('01/01/2017')
		print change_formato_data_num_to_lletra('31/12/2017')
	if False :
		print change_formato_data_llatra_to_num('01 de jan de 2010')
		print change_formato_data_llatra_to_num('05 de dec de 2010')
	if False :
		print compta_repetions('hello boss','a,e,i,o,u')
	if False :
		print histrograma({'a':5,'b':10})
		print histrograma({'e': 1, 'o': 2})
	if True :
		print mostra_dicc({'a':5,'b':10})
	if True :
		print make_tupla({'a':5,'b':10})
