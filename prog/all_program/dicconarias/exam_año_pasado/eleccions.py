# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 19-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- MODUL

#ESPECIFICACIONS D'ENTRADA:- 

########################################################################
import math
import sys
#import some_importe_funcion_diccionaris
########################################################################

import modul_eleccions
		

partits = ['PEI:Partit dels Estudiants Informàtica:6500:Jose Perez:01/12/2000', 
'VET:Verds Escola del Treball          :4000:Maria Pi:27/02/2007', 
'PAG:Partit dels Aprovats Generals     :850:Manel Garcia:18/04/2016', 
'VCJ:Volem Classes al Juliol           :0:Pepe Sanchez:31/01/2000']


dicc_all = modul_eleccions.crea_dic_eleccions(partits)

#print modul_eleccions.repartir_escons_majoria(50, dicc_all)

print modul_eleccions.repartir_escons_EDT(10, dicc_all)

tupla = modul_eleccions.make_tupla_three(dicc_all,'escons','vots')

tupla.sort(cmp=modul_eleccions.comprara_segon_camp_y_mes)

modul_eleccions.mostra_dicc_amb_tubpla_ordenada(dicc_all,tupla)
