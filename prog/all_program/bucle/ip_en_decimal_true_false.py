# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 15-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR IP EN DECIMAL SI ES CORRECTA.

#ESPECIFICACIONS D'ENTRADA:- CUATRO NUMBER INT. 

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		192.155.200.0						TRUE
#		-5.15.5.0							FALSE
#		300.0.0.0							FALSE
#		0.-5.5.5							FALSE
#		0.0.260.0							FALSE
#		0.5.0-6								FALSE

########################################################################

#REFINAMENT

#CONSTANT START
ip_correct = True

#CONSTANT COUNT
count= 0

#LLIGIM DATA I BUCLE FOR IP 
while ip_correct and count <= 3 :
	ip = int(raw_input('escribar ip= '))
	if ip < 0 or ip > 255 :
		ip_correct = False
	count += 1
#MOSTRA
print 'esta ip es',ip_correct
