# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 18-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR TODO FACTORS PRIMOS DE UN 
#NUMBER.

#ESPECIFICACIONS D'ENTRADA:- UN NUMBER INT POSITIVE I MES  GRAN ZERO.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		12								2,3
#		5								5
#		9								3
#		17								17
#		100								2,5
########################################################################

#REFINAMENT

#COUNT CONSTANT
count = 2


#LLIGIM DATA
number = int(raw_input('escribar number= '))

#BUCLE FOR MIRAR I PRINT
while  count <= number :
	
	if number % count == 0 : #MIRAR CONDITON SI ES MULTIPUL
		
		start = 2				#CONSTANT FOR PRIMO START
		primo_correct = True   #VARIABLE FOR PRIMO ES TRUE
	
	
		
		while  start < count and primo_correct : #BUCLE FOR MIRAR SI ES PRIMO
			if count % start == 0 :
				primo_correct = False
			start += 1
		
		if primo_correct : #MOSTRA SI ESTA PRIMO 
			print count
	count += 1
	
