# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 13-12-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA MIRARSI ES NUMBER ES PRIMO.

#ESPECIFICACIONS D'ENTRADA:- DOS INT NUMBER .

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		2									TRUE
#		5									TRUE
#		9									FALSE
#		19									TRUE
#		
########################################################################

#REFINAMENT

#LLEGIR DATA
number = int(raw_input('number= '))

#VARIABLE LETS GUESSIS TRUE
es_primo = True

#BUCLE FOR MOSTRA DATA
for n in range(2,number) :
	if number % n == 0 :
		es_primo = False

#MOSTRA DATA
print es_primo
