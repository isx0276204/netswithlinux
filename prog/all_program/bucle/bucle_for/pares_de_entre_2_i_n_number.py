# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 07-12-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA ESCRIBAR PARES NUMBER ENTRE 2-N NUMBER.

#ESPECIFICACIONS D'ENTRADA:- UN INT NUMBER POSITIVO.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		10									2
#											.
#											.
#											10
#		
########################################################################

#REFINAMENT

#LLEGIR DATA
number = int(raw_input('number= '))

#BUCLE NUMBER LIMITE
number += 1
#BUCLE FOR MOSTRA DATA
for n in range(0,number,2) :
	print n
