# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 18-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR TODO MULTIPUL DE UN NUMBER
#IN ORDER ASCENDENT.

#ESPECIFICACIONS D'ENTRADA:- UN NUMBER INT POSITIVE I MES  GRAN ZERO.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		12								1,2,3,4,6,12
#		5								1,5
#		9								1,3,9
#		17								1,17
#	
########################################################################

#REFINAMENT

#START CONSTANT
count = 1


#LLIGIM DATA
number = int(raw_input('escribar number= '))

#BUCLE FOR MIRAR I PRINT
while  count <= number :
	if number % count == 0 :
		print count
	count += 1
