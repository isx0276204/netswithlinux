# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR MULTIPUL DE DOS NUMBER CON 
#SUMAR N TIMES.

#ESPECIFICACIONS D'ENTRADA:- DOS NUMBER INT POSITIVE TAMBIEN ZERO 
#POSIBLE.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#	A		B					
#	4		5								20
#	4		0								0
#	0		0								0
#	5		1								5
########################################################################

#REFINAMENT

#START VARIABLA
count = 0

#SUMA TOTAL
suma = 0

#LLIGIM DATA
number = int(raw_input('escriure number= '))
n_times = int(raw_input('escriure multipul= '))

#BUCLE FOR SUMAR
while  count < n_times :
	suma += number
	count += 1
	
	

#MOSTRA DATA
print suma
