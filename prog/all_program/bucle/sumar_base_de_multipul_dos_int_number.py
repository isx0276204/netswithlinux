# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-11-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA CALCULAR MULTIPUL DE DOS NUMBER CON 
#SUMAR N TIMES.

#ESPECIFICACIONS D'ENTRADA:- DOS NUMBER INT TAMBIEN ZERO 
#POSIBLE.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#	A		B					
#	4		-5								-20
#	-4		0								0
#	0		0								0
#	-5		-1								5
########################################################################

#REFINAMENT

#SIGNA VARIABLE
positive = True

#START CONSTANT
count = 0

#SUMA TOTAL
suma = 0

#LLEGIM DATA
number = int(raw_input('escribar number= '))
n_times = int(raw_input('escribar multipul= '))

if (number < 0 and n_times >= 0 ) or (number > 0 and n_times <= 0) :
	positive = False
	
#VALOR ABSLOTE
if number < 0 :
	number = -(number)
if n_times < 0 :
	n_times = -(n_times)



#BUCLE FOR SUMAR
while  count < n_times :
	suma += number
	count += 1
	


if not positive :
	suma = - (suma)

#MOSTRA DATA
print suma
