# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 16-12-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA ESCRIBAR UNA CADENA IN INVERTIDA 
#VERTICAL.

#ESPECIFICACIONS D'ENTRADA:- UN CADENA.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		abc									c
#											b
#											a	
#		abcd								d
#											c
#											b
#											a
#	
#	
#	
#	
#	
#	

########################################################################

#REFINAMENT

#LLEGIR DATA
cadena = raw_input('cadena= ')


#BUCLE FOR MOSTAR DATA
for i in range(len(cadena)-1,-1,-1):
	print cadena[i]




