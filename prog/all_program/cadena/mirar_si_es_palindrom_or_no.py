# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 18-01-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA MIRAR SI UNA CADENA ES DENTRO DE OTRA 
#CADENA.

#ESPECIFICACIONS D'ENTRADA:- UNA CADENA.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#						
#		TODO (odot)							false
#		mal muy (yumlam)					false
#		oko (oko)							true
#		lool (lool)							true
#		dabale arroz a la zorra el abad		true
#		        										
#											
#																		
#		
#		
#		
#	
#	
#	

########################################################################

#REFINAMENT

#IMPORT MODUL
import sys
import math



#VARIABLE BOOLEAN
es_palindrom = True

#LLEGIR DATA
cadena_1 = sys.argv[1]

cadena_new = ''
cadena_inver = ''



#BUCLE FOR CADENA GRAN to revome space
for c in cadena_1 :
	if c != ' ' :
		cadena_new += c
		cadena_inver = c + cadena_inver
		
		
if cadena_new != cadena_inver :
	es_palindrom = False

		
print es_palindrom
