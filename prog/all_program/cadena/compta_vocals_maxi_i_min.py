# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 10-01-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM PARA BUSCAR VOCALS DE UNA CADENA I COPMTI 
#VOCAL MAXI I MIN.

#ESPECIFICACIONS D'ENTRADA:- UNA CADENA.

########################################################################
#			joc de prova
#		ENTRADA								SORTIDA
#		HOLA								A ES 1 VEGADAS
#											E ES 0 VEGADAS
#											
#											
#		
#
#			
#		
#		
#	
#	
#	
#	

########################################################################

#REFINAMENT

#LLEGIR DATA
cadena = raw_input('cadena= ')

#VARIABLA FOR COUNT VOCALS
a_total = 0
e_total = 0
i_total = 0
o_total = 0
u_total = 0
 
#BUSCAR MOSTRA DATA
for c in cadena :
	if c == 'a' :
		a_total += 1
	if c == 'e' :
		e_total += 1
	if c == 'i' :
		i_total += 1
	if c == 'o' :
		o_total += 1
	if c == 'u' :
		u_total += 1
		
#BUSCAR MEJOR
vocal = 'a'
mejor = a_total
if e_total > mejor :
	mejor = e_total
	vocal = 'e'
if i_total > mejor :
	mejor = i_total
	vocal = 'i'
if o_total > mejor :
	mejor = o_total
	vocal = 'o'
if u_total > mejor :
	mejor = u_total
	vocal = 'u'
	
#BUSCAR Menor
vocal_min = 'a'
menor = a_total
if e_total < menor :
	menor = e_total
	vocal_min = 'e'
if i_total < menor :
	menor = i_total
	vocal_min = 'i'
if o_total < menor :
	menor = o_total
	vocal_min = 'o'
if u_total < menor :
	menor = u_total
	vocal_min = 'u'
	
#MOSTRA DATA
print 'La ',vocal,'és la vocal més abundant amb ',mejor,' coincidències\
\nLa ',vocal_min, 'és la vocal que és repeteix menys vegades amb \
',menor ,' coincidències'
