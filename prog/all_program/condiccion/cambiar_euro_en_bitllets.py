# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 04-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR EL BITLLETS I MONEDAS
#UNA EXPERSION CORRECTA.
#ESPECIFICACIONS D'ENTRADA:- VARIABLA NUMBER INT POSITIVE I TAMBIEN 
#POSIBLE ZERO. 

########################################################################

#JOC DE PROVA
#		ENTRADA							SORTIDA
#									20	10	5	2	1
#		100							5	0	0	0	0
#		77							3	1	1	1	0
#					

#REFINAMENT
#LLEGIR DATAS
total_euro = int(raw_input('euro= '))

#CALCULAR BITLLET DE 20 I RESTA DE EURO.
bitllet_20 = total_euro / 20
total_euro = total_euro % 20

#CALCULAR BITLLETS DE 10 I RESTA DE EURO.
bitllet_10 = total_euro / 10
total_euro = total_euro % 10

#CALCULAR BITLLETS DE 5 I RESTA DE EURO.
bitlle_5 = total_euro / 5
total_euro = total_euro % 5

#CALCULAR MONEDAS DE 2 I RESTA DE EURO.
monedas_2 = total_euro / 2
total_euro = total_euro % 2

#CALCULAR MONEDAS DE 1
monedas_1 = total_euro

#MOSTAR TODO HORA,MINTO I SEGUNDO
print "bitllets_20=%s\nbitllets_10=%s\nbitllets_5=%s\nmonedas_2=\
%s\nmonedas_1=%s" % (bitllet_20,bitllet_10,bitlle_5,monedas_2,monedas_1)
