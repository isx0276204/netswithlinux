# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 02-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- UN PROGRAMA QUE REALICE LA CONVERSION DE KILOGRAMOS A 
#LIBRAS.
#ESPECIFICACIONS D'ENTRADA:-ONE VARIABLA NUMBER FLOAT POSITIVE I TAMBIEN
#POSIBLE ZERO

########################################################################

#						JOC DE PROVA
#			ENTRADA							SORTIDA
#			1.0								2.2046
#			0.0								0.0
#			2.0								4.4092


#NUMBER COSTANT VALUE DE KILOGRAMS EN LIBRAS.
COSTAT_KILO = 2.2046
#REFINAMENT

#LLEGIR DATAS
kilogram = float(raw_input('escribar en kilograms = '))

#CALCULAR EN LIBRAS.
libras = kilogram * COSTAT_KILO

#MOSTAR EN LIBRAS
print libras
