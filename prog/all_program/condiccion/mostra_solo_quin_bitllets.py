# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 04-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR EL BITLLETS I MONEDAS
#UNA EXPERSION CORRECTA.
#ESPECIFICACIONS D'ENTRADA:- VARIABLA NUMBER INT POSITIVE I TAMBIEN 
#POSIBLE ZERO. 

########################################################################

#JOC DE PROVA
#		ENTRADA							SORTIDA
#									20	10	5	2	1
#		100							5	
#		77							3	1	1	1	
#					

#REFINAMENT
#LLEGIR DATAS
total_euro = int(raw_input('euro= '))

#CALCULAR BITLLET DE 20 I RESTA DE EURO.
bitllet_20 = total_euro / 20
total_euro = total_euro % 20


#CALCULAR BITLLETS DE 10 I RESTA DE EURO.
bitllet_10 = total_euro / 10
total_euro = total_euro % 10

#CALCULAR BITLLETS DE 5 I RESTA DE EURO.
bitllet_5 = total_euro / 5
total_euro = total_euro % 5

#CALCULAR MONEDAS DE 2 I RESTA DE EURO.
monedas_2 = total_euro / 2
total_euro = total_euro % 2

#CALCULAR MONEDAS DE 1
monedas_1 = total_euro

#MOSTRA BITLLETS ALL
if bitllet_20 > 0 :
	print 'bitllets_20 ',bitllet_20
if bitllet_10 > 0 :
	print 'bitllets_10 ',bitllet_10
if bitllet_5 > 0 :
	print 'bitllets_5 ',bitllet_5
if monedas_2 > 0 :
	print 'monedas_2 ',monedas_2
if monedas_1 > 1 :
	print 'monedas_1 ',monedas_1
