# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 29-09-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR EL SUELDO QUE EL EMPLEADO
#COBRA POR HORA I SE LE DESCUENTA EL 10% EN CONCEPTO DE IMPUESTO SOBRE 
#LA RENTA.
#ESPECIFICACIONS D'ENTRADA:-NOM ES UN STRING,HORA EN FLOAT POSITIVE
#MES GRAN DE ZERO I PREU PER HORA ES TAMBIEN FLOAT POSITIVE MES GRAN 
#ZERO

########################################################################

#JOC DE PROVA
#			ENTRADA						SORTIDA
#	NOM		HORA		PREU				
#	PERE	100.0		10.0			900.0
#	ANNA	0.0			0.0				0.0		

#REFINAMENT
#LLEGIR DATAS
nom = raw_input('escribar nom ')
hora = float(raw_input('escribar cuanto hora que trabajado '))
preu_hora = float(raw_input('escriber preu per hora '))

#CALCULAR TOTAL DE LO QUE TIENES QUE COBRAR
total_cobra = hora * preu_hora

#CALCULAR 10 PERCENTAGE DE RENTA
renta = total_cobra * 10 / 100

#CALCULAR DESPUES DE DESCCUNTO
total_cobra = total_cobra - renta

#MOSTAR CON NOM I LO QUE COBRA FINAL
print 'nom %s cobra %s'%(nom,total_cobra)
