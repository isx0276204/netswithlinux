# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 02-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- UN PROGRAMA QUE REALICE LA CONVERSION DE LIBRAS A 
#KILOGRAMOS.
#ESPECIFICACIONS D'ENTRADA:-ONE VARIABLA NUMBER FLOAT POSITIVE I TAMBIEN
#POSIBLE ZERO

########################################################################

#						JOC DE PROVA
#			ENTRADA							SORTIDA
#			2.2046							1.0
#			0.0								0.0
#			4.4092							2.0


#NUMBER COSTANT VALUE DE KILOGRAMS EN LIBRAS.
COSTAT_KILO = 2.2046
#REFINAMENT

#LLEGIR DATAS
libras = float(raw_input('escribar en libras = '))

#CALCULAR EN KILOGRAMS.
kilograms = libras / COSTAT_KILO

#MOSTAR EN KILOGRAMS
print kilograms
