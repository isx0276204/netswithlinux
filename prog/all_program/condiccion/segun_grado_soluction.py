# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 14-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR QUINA SOLUTION DA .
#ESPECIFICACIONS D'ENTRADA:- TRES VARIABLE FLOAT NUMBER I A NEVER ZERO.

########################################################################
#			joc de prova
#		ENTRADA						SORTIDA
#	A	B		C
#	1	-4		4						2	
#	1	4		4						-2
#	1	5		4						-1,-4
#-5.0	5.0		3.0						-0.421954445729,1.42195444573
#5.0	5.0		3.0						NO SOLUTION			
########################################################################

#REFINAMENT

#LLIGIR DATA
number_1 = float(raw_input('escribar number_1= '))
number_2 = float(raw_input('escriber number_2= '))
number_3 = float(raw_input('escribar number_3= '))


#CALCULAR EL DISCRIMINANT RESPECTO DE FORMULA
discri = ((number_2)**2 - 4 * number_1 * number_3)

#SI EL DISCRIMINANT MES GRAN DE ZERO
if discri > 0 :
#CALCULAR LOS DOS SOLUTION
	solution_1 = (-number_2 + (discri)**(1.0 / 2)) / (2 * number_1)
	solution_2 = (-number_2 - (discri)**(1.0 / 2)) / (2 * number_1)
	print solution_1,solution_2
#SI EL DISCRIMINANT ES ZERO
elif discri == 0 :
#CALCULAR ONE SOLUTION
	solution_1 = -number_2 / (2 * number_1)
	print solution_1
#SI NO HA SOLUTION
else :
	print 'no solution'
