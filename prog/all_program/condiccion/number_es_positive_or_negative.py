# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 11-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR SI NUMBER IS POSTIVE OR
#NEGATIVE.
#ESPECIFICACIONS D'ENTRADA:- VARIABLA NUMBER INT  I TAMBIEN 
#POSIBLE ZERO. 

########################################################################

#JOC DE PROVA
#		ENTRADA							SORTIDA
#		10.0							POSITIVE			
#		0.0								POSITIVE
#		-1.0							NEGATIVE

#REFINAMENT
#LLEGIR DATAS
number = float(raw_input('number= '))

#CALCULAR NUMBER IS POSITIVE OR NEGATIVE
if number >= 0 :
	print 'number es positive'
else :
	print 'number es negative'
