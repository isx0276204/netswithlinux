# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 13-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR IP BINARI EN IP 
#VALOR ENDECIMAL.
#ESPECIFICACIONS D'ENTRADA:- VARIABLAS NUMBER INT POSITIVE,ZERO OR UNO. 

########################################################################

#JOC DE PROVA
#		ENTRADA							SORTIDA
#		11111111						256
#		00000000						0
#		00000001						1

#REFINAMENT
#LLEGIR DATAS
B_1 = int(raw_input('b_1= '))
B_2 = int(raw_input('b_2= '))
B_3 = int(raw_input('b_3= '))
B_4 = int(raw_input('b_4= '))
B_5 = int(raw_input('b_5= '))
B_6 = int(raw_input('b_6= '))
B_7 = int(raw_input('b_7= '))
B_8 = int(raw_input('b_8= '))

#CALCULAR AREA DE TRIANGLE BY USE FORMULA
decimal = (B_8 * 2**7) + (B_7 * 2**6) + (B_6 * 2**5) + (B_5 * 2**4) + \
(B_4 * 2**3) + (B_3 * 2**2) + (B_2 * 2**1) + (B_1 * 2**0)

#MOSTRA TODOS DATA
print decimal
