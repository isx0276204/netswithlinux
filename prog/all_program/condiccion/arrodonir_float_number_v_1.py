# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 14-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR ARRODONOR NUMBER EN UNA NUMBER DE 
#DEPUES DE DECIMAL .
#ESPECIFICACIONS D'ENTRADA:- UNA VARIABLE FLOAT NUMBER POSITIVE 
#I OTRA VARIABLE INT POSITIVE.

########################################################################
#			joc de prova
#		ENTRADA						SORTIDA
#	A			B
#	3.12565		3					3.126
#	3.12500		2					3.13
#	3.12565		2					3.13
#	3.0			2					3.0
#	2			0					2.
#	3.2554		3					3.255
#	3.2554		2					3.26
########################################################################

#REFINAMENT

#LLIGIR DATA
number_1 = float(raw_input('escribar number_1= '))
number_2 = int(raw_input('escribar number_2= '))

#CALCULAR VALOR IN DECIMAL
number_1 = number_1 * 10 ** number_2  


#CALCULAR QUE VALOR ES ARRODONIR
number_int = int(number_1)

#CALCULAR DECIMAL
decimal = number_1 - number_int

#CHECK PARA SUMA UNA MAS
if decimal >= 0.5 :
	number_int = number_int + 1
	
#CAMBIAR FORMATO
number_int = number_int / (10.0) ** number_2

#MOSTRA NUMBER
print number_int
