# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 07-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR QUIN DIA DE SEMANA I QUIAN SEMANA 
#DE ANYO ES .
#INTRODUCES DATAS:- UNA VARIABLE INT POSITIVE,MES GRAN DE ZERO I SMALL 
#OR EQUAL DE 366.

########################################################################
#			joc de prova
#	entrada					
#	Dia				Dia de la setmana 		Setmana de l'any
#1 (1 de gener)		1 (dimarts)				1
#3 (3 de gener)		3						1
#7 					0						2
#8 					1						2
#19					5						3


########################################################################

#REFINAMENT

#LLIGIR DATA
dia_anyo = int(raw_input('escribar el dia de anyo= '))

#CALCULAR DIA DE SEMANA
dia_semana = dia_anyo % 7

#NOW CALCULATE SEMANA DE ANYO BY ADD ONE
semana_anyo = (dia_anyo / 7) + 1 

#NOW LETS SHOW THE CALCULATE VALUE
print 'dia = ',dia_semana,'\nsemana = ',semana_anyo
