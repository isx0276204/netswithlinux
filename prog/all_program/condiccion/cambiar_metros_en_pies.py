# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 02-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- UN PROGRAMA QUE REALICE LA CONVERSION DE METROS A PIES .
#ESPECIFICACIONS D'ENTRADA:-ONE VARIABLA NUMBER FLOAT POSITIVE I TAMBIEN
#POSIBLE ZERO

########################################################################

#JOC DE PROVA
#			ENTRADA							SORTIDA
#			1.0								3.2808
#			0.0								0.0
#			2.0								6.5616


#NUMBER COSTANT VALUE DE METROS EM PIES.
COSTAT_PIES = 3.2808
#REFINAMENT

#LLEGIR DATAS
metros = float(raw_input('escribar en metros = '))

#CALCULAR EN PIES.
pies = metros * COSTAT_PIES

#MOSTAR EN PIES
print pies
