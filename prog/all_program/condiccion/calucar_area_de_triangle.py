# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 13-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR AREA DE TRIANGLAR.
#ESPECIFICACIONS D'ENTRADA:- VARIABLAS NUMBER FLOAT POSITIVE I TAMBIEN 
#POSIBLE MES GRAN ZERO. 

########################################################################

#JOC DE PROVA
#		ENTRADA							SORTIDA
#		base	altura		
#		1.0		1.0						0.5
#		2.0		2.0						2.0							

#REFINAMENT
#LLEGIR DATAS
base = float(raw_input('base= '))
altura = float(raw_input('altura= '))

#CALCULAR AREA DE TRIANGLE BY USE FORMULA
area = (base * altura) / 2

#MOSTRA TODOS DATA
print area
