# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 01-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR EL PULGADS EN CM..
#ESPECIFICACIONS D'ENTRADA:-ONE VARIABLA NUMBER FLOAT POSITIVE I TAMBIEN
#POSIBLE ZERO

########################################################################

#JOC DE PROVA
#			ENTRADA							SORTIDA
#			0.39737							1.0
#			0.0								0.0
#			0.79474							2.0

#NUMBER COSTANT VALUE DE CM. EN PULGADAS
COSTAT_PULGADAS = 0.39737

#REFINAMENT

#LLEGIR DATAS
pulgadas = float(raw_input('escribar en pulgadas = '))

#CALCULAR EN CM.
cm = pulgadas / COSTAT_PULGADAS

#MOSTAR EN CM
print cm
