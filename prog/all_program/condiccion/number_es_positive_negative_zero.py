# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 11-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR SI NUMBER IS POSTIVE ,
#NEGATIVE or ZERO.
#ESPECIFICACIONS D'ENTRADA:- VARIABLA NUMBER INT  I TAMBIEN 
#POSIBLE ZERO. 

########################################################################

#JOC DE PROVA
#		ENTRADA							SORTIDA
#		10.0							POSITIVE			
#		0.0								ZERO
#		-1.0							NEGATIVE

#REFINAMENT
#LLEGIR DATAS
number = float(raw_input('number= '))

#CALCULAR NUMBER IS POSITIVE,NEGATIVE OR ZERO.
if number > 0 :
	print 'number es positive'
elif number == 0 :
	print 'number is zero'
else :
	print 'number es negative'
