z# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 13-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR BASE DE GIVEN NUMBER
#USING TRES NUMBER DE BASE DE 10.

#ESPECIFICACIONS D'ENTRADA:- VARIABLAS TRES NUMBERS 
#INT POSITIVE I TAMBIEN POSIBLE ZERO I BASE ALWAYS TRES NUMBER SMALLER 
#THAN BASE. 

########################################################################

#					JOC DE PROVA
#			ENTRADA							SORTIDA
#		BASE	A	B	C
#		3		0	1	2					5
#		2		0	1	1					3
#		10		0	1	2					12

#REFINAMENT
#LLEGIR DATAS
base = int(raw_input('base= '))
d_1= int(raw_input('d_1= '))
d_2 = int(raw_input('d_2= '))
d_3 = int(raw_input('d_3= '))

#CALCULAR AREA DE TRIANGLE BY USE FORMULA
decimal = (d_1 * base**2) + (d_2 * base**1) + (d_3 * base**0) 

#MOSTRA TODOS DATA
print decimal
