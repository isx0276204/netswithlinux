# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 14-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTA PROGRAM CALCULAR EDAD.
#ESPECIFICACIONS D'ENTRADA:- TRES VARIABLE INT POSITIVE NUMBER NO 
#POSIBLE MES GRAN QUE FECHA ACTUAL.

########################################################################
#			joc de prova
#		ENTRADA						SORTIDA
#	DIA		MES		ANYO				
#	1		1		2016			0
#	18		10		1990			26
#	18		10		2018			NO
#	17		10		1990			26
#	19		10		1990			25		
########################################################################

#REFINAMENT

#CONSTAT
anyo_actul = 2016
mes_actul = 10
dia_actul = 18

#LLIGIR DATA
anyo = int(raw_input('escribar anyo= '))
mes = int(raw_input('escriber mes= '))
dia = int(raw_input('escribar dia= '))

#CALCULAR EDAD CON ANYO

edad = anyo_actul - anyo


# COMPAREM EL MES ACTUAL PASADO I DIA ACTUAL PASADO
if mes > mes_actul :
	edad = edad - 1
	
elif mes == mes_actul and dia > dia_actul :
	edad = edad - 1

#MOSTRA EDAD
print edad
