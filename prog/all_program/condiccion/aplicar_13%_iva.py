#NAME:- PARVEEN
#isx0276204
#DATE:- 04-10-2016
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- ESTE PROGRAMA SIRVE PARA CALCULAR EL PREU ARTICULA 
#DESPRES APLICAR DE 13% IVA.
#ESPECIFICACIONS D'ENTRADA:- VARIABLA NUMBER FLOAT POSITIVE I TAMBIEN 
#POSIBLE ZERO. 

########################################################################

#					JOC DE PROVA
#		ENTRADA							SORTIDA
#		1.0								1.13
#		0.0								0.0
#		10.0							11.3
#					

#REFINAMENT
#LLEGIR DATAS
preu = float(raw_input('euro= '))

#CALCULAR PRESENTAGE 13%.
aplicar = preu * 13 / 100

#CALCULAR TOTAL PREU.
preu = preu + aplicar

#MOSTAR TODO HORA,MINTO I SEGUNDO
print 'preu es =%s' % (preu)
