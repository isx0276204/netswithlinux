# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################

#NAME:- PARVEEN
#isx0276204
#DATE:- 14-1-2017
#VERSION:- 1.1

########################################################################

#DESCRIPCION:- BUSCAR PARAULA EN DOS LISTA COMMON 

#ESPECIFICACIONS D'ENTRADA:- 

########################################################################
import math
import sys
import modul_lista
########################################################################
lista_1 = ['hola','hello','ok','bye']
lista_2 = ['parveen','david','hello','ok']

lista_common = modul_lista.common_lista(lista_1,lista_2)
lista_1_si_not_2 = modul_lista.left_join_lista(lista_1,lista_2)
lista_2_si_not_1 = modul_lista.right_join_lista(lista_1,lista_2)
lista_union = modul_lista.union_all(lista_1,lista_2)

print lista_common,'\n',lista_1_si_not_2,'\n',lista_2_si_not_1,'\n',lista_union
		
