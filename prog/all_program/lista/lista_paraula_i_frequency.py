# !/usr/bin/python
# -*-coding: utf-8-*-

########################################################################
#AUTOR:	 	parveen
#ISX: 	 	isx0276204
#DATA: 	 	10/03/2017 
#VERSIÓ: 	1.0
########################################################################


########################################################################
#                    MÒDUL QUE TREBALLA AMB DATES                      #
########################################################################
import sys

lista = sys.argv[1].split()

def lista_paraula_i_frequencias(llista) :
	'''
	'''
	lista_final = []
	lista_frequency = []
	for elem in llista :
		if elem not in lista_final :
			lista_final.append(elem)
			lista_frequency.append(1)
		else :
			for i in range(0,len(lista_final)) :
				if elem == lista_final[i] :
					lista_frequency[i] += 1
					
	return lista_frequency
	
print lista_paraula_i_frequencias(lista)
