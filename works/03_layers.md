#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"
	
	[root@j01 ~]# ip a f dev enp2s0
	
	[root@j01 ~]# ip a f all
	
	Device "all" does not exist.

	[root@j01 ~]# ip a f a

	Device "a" does not exist.

	[root@j01 ~]# ip r f all
	[root@j01 ~]# ip a

	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		valid_lft forever preferred_lft forever
    
		inet6 ::1/128 scope host 
			valid_lft forever preferred_lft forever

	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff

	[root@j01 ~]# ip r

	[root@j01 ~]# 

	
Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
		inet 2.2.2.2/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 3.3.3.3/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 4.4.4.4/25 scope global enp2s0
		   valid_lft forever preferred_lft forever

Consulta la tabla de rutas de tu equipo

	[root@j01 ~]# ip r
	2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4 


Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132

	2.2.2.2 ==> OK PING
	2.2.2.254 ==>   [root@j01 ~]# ping 2.2.2.54
					PING 2.2.2.54 (2.2.2.54) 56(84) bytes of data.
					From 2.2.2.2 icmp_seq=1 Destination Host Unreachable
					From 2.2.2.2 icmp_seq=2 Destination Host Unreachable
					From 2.2.2.2 icmp_seq=3 Destination Host Unreachable

	2.2.5.2 ==> 	[root@j01 ~]# ping 2.2.5.2
					connect: Network is unreachable
	
	3.3.3.35 ==>  		[root@j01 ~]# ping 3.3.3.35
						PING 3.3.3.35 (3.3.3.35) 56(84) bytes of data.
						^C
						--- 3.3.3.35 ping statistics ---
						3 packets transmitted, 0 received, 100% packet loss, time 1999ms

	3.3.200.45 ==> 	waiting may be posible any of host cannect

	4.4.4.8		==> wating inside mascara but host connect with this ip
	
	4.4.4.132 ==> network is unreachable due to mascara is not primitted 
	
#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	[root@j01 ~]# ip a f dev enp2s0
	[root@j01 ~]# ip r f all
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	[root@j01 ~]# 


Conecta una segunda interfaz de red por el puerto usb

==>ok

Cambiale el nombre a usb0

==> ok

Modifica la dirección MAC

==>

Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.

	[root@j01 ~]# ip a a 5.5.5.5/24 dev usb0
	[root@j01 ~]# ip a a 7.7.7.7/24 dev enp2s0
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
		inet 192.168.3.1/16 brd 192.168.255.255 scope global dynamic enp2s0
		   valid_lft 20171sec preferred_lft 20171sec
		inet 7.7.7.7/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet6 fe80::16da:e9ff:fe99:a0e4/64 scope link 
		   valid_lft forever preferred_lft forever
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
		inet 5.5.5.5/24 scope global usb0
		   valid_lft forever preferred_lft forever
	[root@j01 ~]# 


Observa la tabla de rutas

	[root@j01 ~]# ip r s
	default via 192.168.0.5 dev enp2s0  proto static  metric 100 
	5.5.5.0/24 dev usb0  proto kernel  scope link  src 5.5.5.5 linkdown 
	7.7.7.0/24 dev enp2s0  proto kernel  scope link  src 7.7.7.7 
	192.168.0.0/16 dev enp2s0  proto kernel  scope link  src 192.168.3.1  metric 100 


#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	[root@j01 ~]# ip a f dev enp2s0
	
	[root@j01 ~]# ip a f all
	
	Device "all" does not exist.

	[root@j01 ~]# ip a f a

	Device "a" does not exist.

	[root@j01 ~]# ip r f all
	[root@j01 ~]# ip a

	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		valid_lft forever preferred_lft forever
    
		inet6 ::1/128 scope host 
			valid_lft forever preferred_lft forever

	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff

	[root@j01 ~]# ip r

	[root@j01 ~]# 

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

	[root@j01 ~]# ip a f dev enp2s0
	[root@j01 ~]# ip a a 172.16.99.01/24 dev enp2s0
	[root@j01 ~]# ip a 
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
		inet 172.16.99.1/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
		inet 5.5.5.5/24 scope global usb0
		   valid_lft forever preferred_lft forever
	[root@j01 ~]# 


Lanzar iperf en modo servidor en cada ordenador

	[root@j01 ~]# iperf -s
	------------------------------------------------------------
	Server listening on TCP port 5001
	TCP window size: 85.3 KByte (default)
	------------------------------------------------------------
	[  4] local 172.16.99.1 port 5001 connected with 172.16.99.1 port 54482
	[ ID] Interval       Transfer     Bandwidth
	[  4]  0.0-10.0 sec  10.0 GBytes  8.61 Gbits/sec

	[isx0276204@j01 netswithlinux]$ iperf -c 172.16.99.1
	------------------------------------------------------------
	Client connecting to 172.16.99.1, TCP port 5001
	TCP window size: 2.50 MByte (default)
	------------------------------------------------------------
	[  3] local 172.16.99.1 port 54482 connected with 172.16.99.1 port 5001
	[ ID] Interval       Transfer     Bandwidth
	[  3]  0.0-10.0 sec  10.0 GBytes  8.61 Gbits/sec
	[3]+  Done                    geany guide/es/00_cheatsheet_commands_for_networks.md


Comprueba con netstat en qué puerto escucha

	[root@j01 ~]# netstat -l
	Active Internet connections (only servers)
	Proto Recv-Q Send-Q Local Address           Foreign Address         State      
	tcp        0      0 0.0.0.0:commplex-link   0.0.0.0:*               LISTEN     
	tcp        0      0 0.0.0.0:sunrpc          0.0.0.0:*               LISTEN     
	tcp        0      0 0.0.0.0:mountd          0.0.0.0:*               LISTEN     
	tcp        0      0 localhost:ipp           0.0.0.0:*               LISTEN     
	tcp        0      0 localhost:postgres      0.0.0.0:*               LISTEN     
	tcp        0      0 0.0.0.0:40889           0.0.0.0:*               LISTEN     
	tcp        0      0 0.0.0.0:40957           0.0.0.0:*               LISTEN     
	tcp        0      0 0.0.0.0:nfs             0.0.0.0:*               LISTEN     
	tcp6       0      0 [::]:44941              [::]:*                  LISTEN     
	tcp6       0      0 [::]:sunrpc             [::]:*                  LISTEN     
	tcp6       0      0 [::]:mountd             [::]:*                  LISTEN     
	tcp6       0      0 [::]:59537              [::]:*                  LISTEN     
	tcp6       0      0 localhost:ipp           [::]:*                  LISTEN     
	tcp6       0      0 localhost:postgres      [::]:*                  LISTEN     
	tcp6       0      0 [::]:nfs                [::]:*                  LISTEN     
	udp        0      0 0.0.0.0:mdns            0.0.0.0:*                          
	udp        0      0 0.0.0.0:17867           0.0.0.0:*                          
	udp        0      0 0.0.0.0:34285           0.0.0.0:*                          
	udp        0      0 0.0.0.0:nfs             0.0.0.0:*                          
	udp        0      0 0.0.0.0:40317           0.0.0.0:*                          
	udp        0      0 0.0.0.0:mountd          0.0.0.0:*                          
	udp        0      0 0.0.0.0:bootpc          0.0.0.0:*                          
	udp        0      0 0.0.0.0:sunrpc          0.0.0.0:*                          
	udp        0      0 0.0.0.0:36992           0.0.0.0:*                          
	udp        0      0 localhost:323           0.0.0.0:*                          
	udp        0      0 localhost:807           0.0.0.0:*                          
	udp        0      0 0.0.0.0:809             0.0.0.0:*                          
	udp6       0      0 [::]:46305              [::]:*                             
	udp6       0      0 [::]:mdns               [::]:*                             
	udp6       0      0 [::]:59336              [::]:*                             
	udp6       0      0 [::]:nfs                [::]:*                             
	udp6       0      0 [::]:39056              [::]:*                             
	udp6       0      0 [::]:59727              [::]:*                             
	udp6       0      0 [::]:mountd             [::]:*                             
	udp6       0      0 [::]:sunrpc             [::]:*                             
	udp6       0      0 localhost:323           [::]:*                             
	udp6       0      0 [::]:809                [::]:*                             
	raw6       0      0 [::]:ipv6-icmp          [::]:*                  7          
	Active UNIX domain sockets (only servers)
	Proto RefCnt Flags       Type       State         I-Node   Path
	unix  2      [ ACC ]     STREAM     LISTENING     22529    /run/user/42/systemd/private
	unix  2      [ ACC ]     STREAM     LISTENING     21506    /var/run/NetworkManager/private-dhcp
	unix  2      [ ACC ]     STREAM     LISTENING     27907    /tmp/.esd-202172/socket
	unix  2      [ ACC ]     STREAM     LISTENING     27910    /run/user/202172/pulse/native
	unix  2      [ ACC ]     STREAM     LISTENING     17624    /var/lib/gssproxy/default.sock
	unix  2      [ ACC ]     STREAM     LISTENING     22535    /run/user/42/bus
	unix  2      [ ACC ]     STREAM     LISTENING     27706    @/tmp/.ICE-unix/1127
	unix  2      [ ACC ]     STREAM     LISTENING     26401    /tmp/.X11-unix/X0
	unix  2      [ ACC ]     STREAM     LISTENING     26400    @/tmp/.X11-unix/X0
	unix  2      [ ACC ]     STREAM     LISTENING     12841    /run/systemd/private
	unix  2      [ ACC ]     STREAM     LISTENING     27670    @/tmp/dbus-A5cFDJ9LV6
	unix  2      [ ACC ]     STREAM     LISTENING     11316    /run/systemd/journal/stdout
	unix  2      [ ACC ]     STREAM     LISTENING     19362    /var/lib/sss/pipes/autofs
	unix  2      [ ACC ]     STREAM     LISTENING     20102    /var/lib/sss/pipes/pam
	unix  2      [ ACC ]     STREAM     LISTENING     20103    /var/lib/sss/pipes/private/pam
	unix  2      [ ACC ]     STREAM     LISTENING     19349    /var/lib/sss/pipes/nss
	unix  2      [ ACC ]     STREAM     LISTENING     27707    /tmp/.ICE-unix/1127
	unix  2      [ ACC ]     SEQPACKET  LISTENING     12861    /run/systemd/coredump
	unix  2      [ ACC ]     STREAM     LISTENING     22662    @/tmp/.ICE-unix/859
	unix  2      [ ACC ]     SEQPACKET  LISTENING     12865    /run/udev/control
	unix  2      [ ACC ]     STREAM     LISTENING     20899    @/tmp/dbus-eI6VeVmv
	unix  2      [ ACC ]     STREAM     LISTENING     12872    /run/lvm/lvmpolld.socket
	unix  2      [ ACC ]     STREAM     LISTENING     16114    @ISCSID_UIP_ABSTRACT_NAMESPACE
	unix  2      [ ACC ]     STREAM     LISTENING     12876    /run/lvm/lvmetad.socket
	unix  2      [ ACC ]     STREAM     LISTENING     28793    @/tmp/dbus-aNxXlqDn
	unix  2      [ ACC ]     STREAM     LISTENING     26977    /run/user/202172/systemd/private
	unix  2      [ ACC ]     STREAM     LISTENING     26983    /run/user/202172/bus
	unix  2      [ ACC ]     STREAM     LISTENING     23962    @/tmp/dbus-nYqVaGhtVp
	unix  2      [ ACC ]     STREAM     LISTENING     27041    @/tmp/dbus-PXrpMNjp
	unix  2      [ ACC ]     STREAM     LISTENING     23915    /run/user/42/wayland-0
	unix  2      [ ACC ]     STREAM     LISTENING     23148    /run/user/42/pulse/native
	unix  2      [ ACC ]     STREAM     LISTENING     17532    /dev/gpmctl
	unix  2      [ ACC ]     STREAM     LISTENING     24093    @/tmp/dbus-JwHPCH40
	unix  2      [ ACC ]     STREAM     LISTENING     22663    /tmp/.ICE-unix/859
	unix  2      [ ACC ]     STREAM     LISTENING     27036    /run/user/202172/keyring/control
	unix  2      [ ACC ]     STREAM     LISTENING     27798    /run/user/202172/keyring/pkcs11
	unix  2      [ ACC ]     STREAM     LISTENING     27040    @/tmp/dbus-4bSmJbXL
	unix  2      [ ACC ]     STREAM     LISTENING     20902    @/tmp/dbus-BrH86hje
	unix  2      [ ACC ]     STREAM     LISTENING     27817    /run/user/202172/keyring/ssh
	unix  2      [ ACC ]     STREAM     LISTENING     20900    @/tmp/dbus-PRxwME6F
	unix  2      [ ACC ]     STREAM     LISTENING     20911    /var/run/libvirt/libvirt-sock
	unix  2      [ ACC ]     STREAM     LISTENING     20913    /var/run/libvirt/libvirt-sock-ro
	unix  2      [ ACC ]     STREAM     LISTENING     22718    /tmp/.X11-unix/X1024
	unix  2      [ ACC ]     STREAM     LISTENING     16110    @ISCSIADM_ABSTRACT_NAMESPACE
	unix  2      [ ACC ]     STREAM     LISTENING     22717    @/tmp/.X11-unix/X1024
	unix  2      [ ACC ]     STREAM     LISTENING     17875    /var/run/abrt/abrt.socket
	unix  2      [ ACC ]     STREAM     LISTENING     17625    /run/gssproxy.sock
	unix  2      [ ACC ]     STREAM     LISTENING     18588    /var/lib/sss/pipes/private/sbus-monitor
	unix  2      [ ACC ]     STREAM     LISTENING     19803    /var/lib/sss/pipes/private/sbus-dp_default.621
	unix  2      [ ACC ]     STREAM     LISTENING     20903    @/tmp/dbus-SBKLay9H
	unix  2      [ ACC ]     STREAM     LISTENING     47846    /tmp/geany_socket.9b6c8ab8
	unix  2      [ ACC ]     STREAM     LISTENING     16107    /var/run/libvirt/virtlockd-sock
	unix  2      [ ACC ]     STREAM     LISTENING     20207    /var/run/postgresql/.s.PGSQL.5432
	unix  2      [ ACC ]     STREAM     LISTENING     16111    /run/dbus/system_bus_socket
	unix  2      [ ACC ]     STREAM     LISTENING     20210    /tmp/.s.PGSQL.5432
	unix  2      [ ACC ]     STREAM     LISTENING     16115    /var/run/cups/cups.sock
	unix  2      [ ACC ]     STREAM     LISTENING     16117    /var/run/libvirt/virtlogd-sock
	unix  2      [ ACC ]     STREAM     LISTENING     16119    /var/run/avahi-daemon/socket
	unix  2      [ ACC ]     STREAM     LISTENING     16122    /var/run/rpcbind.sock
	Active Bluetooth connections (only servers)
	Proto  Destination       Source            State         PSM DCID   SCID      IMTU    OMTU Security
	Proto  Destination       Source            State     Channel
	[root@j01 ~]# 


Conectarse desde otro pc como cliente



Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

Encontrar los 3 paquetes del handshake de tcp

Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas

