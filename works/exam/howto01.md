#----1

	[root@j01 ~]# systemctl stop NetworkManager
	[root@j01 ~]# ip a f dev enp2s0
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	3: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
	[root@j01 ~]# ip a a 10.8.8.101/16 dev enp2s0
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	3: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
		inet 10.8.8.101/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
	[root@j01 ~]# ip a a 10.7.01.100/24 dev enp2s0
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	3: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
		inet 10.8.8.101/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 10.7.1.100/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
	[root@j01 ~]# 


	[root@j01 ~]# ping 10.8.1.1
	PING 10.8.1.1 (10.8.1.1) 56(84) bytes of data.
	64 bytes from 10.8.1.1: icmp_seq=1 ttl=64 time=0.333 ms
	64 bytes from 10.8.1.1: icmp_seq=2 ttl=64 time=0.224 ms
	^C
	--- 10.8.1.1 ping statistics ---
	2 packets transmitted, 2 received, 0% packet loss, time 1000ms
	rtt min/avg/max/mdev = 0.224/0.278/0.333/0.057 ms
	[root@j01 ~]# 

	[root@j01 ~]# ping 10.7.1.1
	PING 10.7.1.1 (10.7.1.1) 56(84) bytes of data.
	64 bytes from 10.7.1.1: icmp_seq=1 ttl=64 time=0.364 ms
	64 bytes from 10.7.1.1: icmp_seq=2 ttl=64 time=0.179 ms
	^C
	--- 10.7.1.1 ping statistics ---
	2 packets transmitted, 2 received, 0% packet loss, time 999ms
	rtt min/avg/max/mdev = 0.179/0.271/0.364/0.093 ms
	[root@j01 ~]# 


	[root@j01 ~]# ping 10.7.10.1
	connect: Network is unreachable
	[root@j01 ~]# 
#----2
	[root@j01 ~]# ip a a 10.9.9.201/24 dev enp2s0
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	3: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
		inet 10.8.8.101/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 10.7.1.100/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 10.9.9.201/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
	[root@j01 ~]# 
	[root@j01 ~]# ip r a 10.6.6.0/24 via 10.9.9.1

	[root@j01 ~]# ping 10.6.6.2
	PING 10.6.6.2 (10.6.6.2) 56(84) bytes of data.
	64 bytes from 10.6.6.2: icmp_seq=1 ttl=63 time=0.598 ms
	64 bytes from 10.6.6.2: icmp_seq=2 ttl=63 time=0.617 ms
	64 bytes from 10.6.6.2: icmp_seq=3 ttl=63 time=0.581 ms
	64 bytes from 10.6.6.2: icmp_seq=4 ttl=63 time=0.565 ms
	^C
	--- 10.6.6.2 ping statistics ---
	4 packets transmitted, 4 received, 0% packet loss, time 3008ms
	rtt min/avg/max/mdev

#----3

	[root@j01 ~]# tshark -i enp2s0  -w /tmp/ping01.pcap
	Running as user "root" and group "root". This could be dangerous.
	Capturing on 'enp2s0'
	90 ^C

	[root@j01 ~]# tshark -i enp2s0 -c 2 -w /tmp/ping.pcap icmp
	Running as user "root" and group "root". This could be dangerous.
	Capturing on 'enp2s0'
	2 


	[root@j01 ~]# tshark -r /tmp/ping.pcap
	Running as user "root" and group "root". This could be dangerous.
	  1 0.000000000     10.7.1.1 → 10.7.1.100   ICMP 98 Echo (ping) request  id=0x1273, seq=0/0, ttl=64
	  2 0.000032220   10.7.1.100 → 10.7.1.1     ICMP 98 Echo (ping) reply    id=0x1273, seq=0/0, ttl=64 (request in 1)
	[root@j01 ~]# 

#---4
	[root@j01 ~]# ip link set usb0 down
	[root@j01 ~]# ip link set usb0 name usb01
	[root@j01 ~]# ip link set usb0 a
	address       alias         allmulticast  arp           
	[root@j01 ~]# ip link set usb0 address 44:44:44:00:00:01
	Cannot find device "usb0"
	[root@j01 ~]# ip link set usb01 address 44:44:44:00:00:01
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: usb01: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 44:44:44:00:00:01 brd ff:ff:ff:ff:ff:ff
	3: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
		inet 10.8.8.101/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 10.7.1.100/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 10.9.9.201/24 scope global enp2s0
		   valid_lft forever preferred_lft forever


	[root@j01 ~]# ip a a 10.5.5.101/24 dev usb01
	[root@j01 ~]# ip link set usb01 up
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: usb01: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 44:44:44:00:00:01 brd ff:ff:ff:ff:ff:ff
		inet 10.5.5.101/24 scope global usb01
		   valid_lft forever preferred_lft forever
		inet6 fe80::4644:44ff:fe00:1/64 scope link 
		   valid_lft forever preferred_lft forever
	3: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
		inet 10.8.8.101/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 10.7.1.100/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 10.9.9.201/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
	[root@j01 ~]# 


	[root@j01 ~]# ip a a 10.5.5.101/24 dev usb01
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	3: enp2s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
		inet 10.8.8.101/16 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 10.7.1.100/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
		inet 10.9.9.201/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
	5: usb01: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 44:44:44:00:00:01 brd ff:ff:ff:ff:ff:ff
		inet 10.5.5.101/24 scope global usb01
		   valid_lft forever preferred_lft forever
		inet6 fe80::4644:44ff:fe00:1/64 scope link 
		   valid_lft forever preferred_lft forever
	[root@j01 ~]# ip r s
	10.5.5.0/24 dev usb01  proto kernel  scope link  src 10.5.5.101 
	10.6.6.0/24 via 10.9.9.1 dev enp2s0 linkdown 
	10.7.1.0/24 dev enp2s0  proto kernel  scope link  src 10.7.1.100 linkdown 
	10.8.0.0/16 dev enp2s0  proto kernel  scope link  src 10.8.8.101 linkdown 
	10.9.9.0/24 dev enp2s0  proto kernel  scope link  src 10.9.9.201 linkdown 
	[root@j01 ~]# ping 10.5.5.1
	PING 10.5.5.1 (10.5.5.1) 56(84) bytes of data.
	64 bytes from 10.5.5.1: icmp_seq=1 ttl=64 time=0.810 ms
	64 bytes from 10.5.5.1: icmp_seq=2 ttl=64 time=1.55 ms
	64 bytes from 10.5.5.1: icmp_seq=3 ttl=64 time=1.53 ms
	^C
	--- 10.5.5.1 ping statistics ---
	3 packets transmitted, 3 received, 0% packet loss, time 2001ms
	rtt min/avg/max/mdev = 0.810/1.299/1.551/0.345 ms
	[root@j01 ~]# 


#---5
	[root@j01 ~]# ip a f dev enp2s0
	[root@j01 ~]# ip a f dev usb01
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	3: enp2s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
	5: usb01: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 44:44:44:00:00:01 brd ff:ff:ff:ff:ff:ff
	[root@j01 ~]# 


	[root@j01 ~]# ip a a 172.17.0.1/16 dev usb01
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	3: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
	5: usb01: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 44:44:44:00:00:01 brd ff:ff:ff:ff:ff:ff
		inet 172.17.0.1/16 scope global usb01
		   valid_lft forever preferred_lft forever
	[root@j01 ~]# dhclient
	[root@j01 ~]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	3: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
		inet 192.168.3.1/16 brd 192.168.255.255 scope global dynamic enp2s0
		   valid_lft 21657sec preferred_lft 21657sec
	5: usb01: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 44:44:44:00:00:01 brd ff:ff:ff:ff:ff:ff
		inet 172.17.0.1/16 scope global usb01
		   valid_lft forever preferred_lft forever
	[root@j01 ~]# 


	[root@j01 ~]# echo 1 > /proc/sys/net/ipv4/ip_forward
	[root@j01 ~]# iptables -t nat -A POSTROUTING -o usb01 -j MASQUERADE
	[root@j01 ~]# ip r s
	default via 192.168.0.5 dev enp2s0 
	172.17.0.0/16 dev usb01  proto kernel  scope link  src 172.17.0.1 linkdown 
	192.168.0.0/16 dev enp2s0  proto kernel  scope link  src 192.168.3.1 
	[root@j01 ~]# iptables -t nat -L
	Chain PREROUTING (policy ACCEPT)
	target     prot opt source               destination         

	Chain INPUT (policy ACCEPT)
	target     prot opt source               destination         

	Chain OUTPUT (policy ACCEPT)
	target     prot opt source               destination         

	Chain POSTROUTING (policy ACCEPT)
	target     prot opt source               destination         
	MASQUERADE  all  --  anywhere             anywhere            
	[root@j01 ~]# 

#----6

	[root@j01 ~]# traceroute papua.go.id > /tmp/papua01.txt
	[root@j01 ~]# 

#----7
	[root@j01 ~]# netstat -utlnpa | grep "cups.service"
	[root@j01 ~]# netstat -utlnpa | grep "cups"
	tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1374/cupsd          
	tcp6       0      0 ::1:631                 :::*                    LISTEN      1374/cupsd  

	[root@j01 ~]# netstat -utlnpa | grep "firefox"
	tcp        0      0 192.168.3.1:45350       54.231.134.44:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:51110       52.58.208.10:443        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:41964       216.58.211.202:80       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:47978       185.86.139.29:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:42846       136.243.51.131:80       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:39694       82.194.77.43:80         ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:45332       54.231.134.44:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:54226       152.163.64.2:443        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:42200       104.244.43.39:443       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:41236       216.58.211.234:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:49162       198.47.127.27:443       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:33692       185.86.139.19:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:39292       23.38.28.103:443        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:55450       54.192.78.131:443       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:55698       37.157.6.252:443        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:41950       216.58.211.202:80       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:37932       54.200.125.198:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:36154       83.247.129.60:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:38572       217.12.1.35:443         ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:47872       216.58.211.194:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:45330       54.231.134.44:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:42270       46.137.124.35:443       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:37962       93.184.220.29:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:33690       185.86.139.19:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:48738       216.58.210.230:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:36156       83.247.129.60:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:40112       52.32.150.180:443       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:50990       217.12.1.155:443        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:40426       77.238.185.35:443       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:41952       216.58.211.202:80       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:56924       199.96.57.6:443         ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:44114       216.58.211.238:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:60706       2.20.44.25:80           ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:57796       185.59.220.14:443       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:45178       216.58.211.238:80       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:33604       216.58.211.195:80       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:45338       54.231.134.44:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:55490       178.250.0.76:443        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:36776       216.58.211.206:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:40206       185.43.182.80:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:45348       54.231.134.44:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:51956       216.58.211.193:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:38182       54.230.79.195:443       ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:32976       104.244.42.200:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:37940       54.200.125.198:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:51154       216.58.210.162:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:45328       54.231.134.44:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:45326       54.231.134.44:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:45354       54.231.134.44:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:51148       216.58.210.162:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:59066       217.12.1.36:443         ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:45352       54.231.134.44:80        ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:54356       216.58.211.202:443      ESTABLISHED 3031/firefox        
	tcp        0      0 192.168.3.1:45340       54.231.134.44:80        ESTABLISHED 3031/firefox  



#---8

