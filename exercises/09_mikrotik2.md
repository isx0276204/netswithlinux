EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.1XX/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# Eliminamos el bridge

	/interface bridge port remove 1
	/interface bridge port remove 0
	/interface bridge remove 0  

	# Eliminamos que un puerto sea master de otro

	/interface ethernet set [ find default-name=ether3 ] master-port=none
	/interface ethernet set [ find default-name=ether4 ] master-port=none

	# Cambiamos nombres a los puertos

	/interface ethernet set [ find default-name=ether1 ] name=eth1
	/interface ethernet set [ find default-name=ether2 ] name=eth2
	/interface ethernet set [ find default-name=ether3 ] name=eth3
	/interface ethernet set [ find default-name=ether4 ] name=eth4

	# Deshabilitamos la wifi

	/interface wireless set [ find default-name=wlan1 ] disabled=yes

	# Eliminamos servidor y cliente dhcp

	/ip pool remove 0
	/ip dhcp-server network remove 0
	/ip dhcp-server remove 0
	/ip dhcp-client remove 0
	/ip dns static remove 0
	/ip dns set allow-remote-requests=no

	# Eliminamos regla de nat masquerade
	/ip firewall nat remove 0

```

Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt00
/system backup save name="20170317_zeroconf"
```

###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router

''''

########################################################################
[admin@mkt00] > s
snmp  special-login  system  setup
[admin@mkt00] > setup 
Setup addresses on interfaces and default route.

[admin@mkt00] > :       
caps-man     console  file       ip    log   port  queue   routing  special-login  tool  beep   export  password  quit  setup
certificate  driver   interface  ipv6  mpls  ppp   radius  snmp     system         user  blink  import  ping      redo  undo 
[admin@mkt00] > :
environment  delay  error    find  foreach  if   local    parse  put      return  time     tobool  toip   tonum  totime  while
terminal     do     execute  for   global   len  nothing  pick   resolve  set     toarray  toid    toip6  tostr  typeof  
[admin@mkt00] > file 
[admin@mkt00] /file> ls
bad command name ls (line 1 column 1)
[admin@mkt00] /file> /  
[admin@mkt00] > /
[admin@mkt00] > /system reset-configuration 
Dangerous! Reset anyway? [y/N]: 
y
system configuration will be reset
Connection closed by foreign host.
[root@j01 m07]# telnet 192.168.88.1
Trying 192.168.88.1...
telnet: connect to address 192.168.88.1: No route to host
[root@j01 m07]# ip r
default via 192.168.0.1 dev enp2s0  proto static  metric 100 
192.168.0.0/16 dev enp2s0  proto kernel  scope link  src 192.168.3.1  metric 100 
192.168.88.0/24 dev usb0  proto kernel  scope link  src 192.168.88.100 
[root@j01 m07]# ping 192.168.88.1
PING 192.168.88.1 (192.168.88.1) 56(84) bytes of data.
64 bytes from 192.168.88.1: icmp_seq=1 ttl=64 time=2.91 ms
64 bytes from 192.168.88.1: icmp_seq=2 ttl=64 time=0.837 ms
64 bytes from 192.168.88.1: icmp_seq=3 ttl=64 time=1.17 ms
^C
--- 192.168.88.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2001ms
rtt min/avg/max/mdev = 0.837/1.642/2.912/0.909 ms
[root@j01 m07]# telnet 192.168.88.1
Trying 192.168.88.1...
Connected to 192.168.88.1.
Escape character is '^]'.

MikroTik v6.33.5 (stable)
Login: admin
Password: 








  MMM      MMM       KKK                          TTTTTTTTTTT      KKK
  MMMM    MMMM       KKK                          TTTTTTTTTTT      KKK
  MMM MMMM MMM  III  KKK  KKK  RRRRRR     OOOOOO      TTT     III  KKK  KKK
  MMM  MM  MMM  III  KKKKK     RRR  RRR  OOO  OOO     TTT     III  KKKKK
  MMM      MMM  III  KKK KKK   RRRRRR    OOO  OOO     TTT     III  KKK KKK
  MMM      MMM  III  KKK  KKK  RRR  RRR   OOOOOO      TTT     III  KKK  KKK

  MikroTik RouterOS 6.33.5 (c) 1999-2015       http://www.mikrotik.com/

[?]             Gives the list of available commands
command [?]     Gives help on the command and list of arguments

[Tab]           Completes the command/word. If the input is ambiguous,
                a second [Tab] gives possible options
                                                                                                                                                                              Move up to base level



-------------------------------------------------------------------------------
RouterMode:
 * WAN port is protected by firewall and enabled DHCP client
 * Wireless interfaces are part of LAN bridge
 * WAN port is protected by firewall and enabled DHCP client
 * IP address 192.168.88.1/24 is set on LAN port
wlan1 Configuration:
    mode:          ap-bridge;
    band:          2ghz-b/g/n;
    ht-chains:     0,1;
    ht-extension:  20/40mhz-Ce;
LAN Configuration:
    switch group: ether2 (master), ether3, ether4
    DHCP Server: enabled;
    DNS: enabled;
WAN (gateway) Configuration:
    gateway:  ether1 ;
    firewall:  enabled;
    NAT:   enabled;

-------------------------------------------------------------------------------
You can type "v" to see the exact commands that are used to add and remove
this default configuration, or you can view them later with
'/system default-configuration print' command.
To remove this default configuration type "r" or hit any other key to continue.
If you are connected using the above IP and you remove it, you will be disconnected.

Confirming configuration
  
[admin@MikroTik] > /file print
 # NAME                                                          TYPE                                                               SIZE CREATION-TIME       
 0 skins                                                         directory                                                               jan/01/1970 00:00:01
 1 20170317_zeroconf.backup                                      backup                                                          25.4KiB jan/02/1970 00:44:28
 2 auto-before-reset.backup                                      backup                                                          25.4KiB jan/02/1970 01:14:18
[admin@MikroTik] > /system backup 

.. -- go up to system
load -- Loads a previously saved backup
save -- Makes a backup of the current configuration


[admin@MikroTik] > /system backup load name=

Name ::= 20170317_zeroconf.backup | auto-before-reset.backup


[admin@MikroTik] > /system backup load name=20170317_zeroconf.backup   

name -- Name of the backup file
password -- 


[admin@MikroTik] > /system backup load name=20170317_zeroconf.backup  
password: 
Restore and reboot? [y/N]: 
y
Restoring system configuration
System configuration restored, rebooting now
Connection closed by foreign host.
[root@j01 m07]# telnet 192.168.88.1
Trying 192.168.88.1...
Connected to 192.168.88.1.
Escape character is '^]'.

MikroTik v6.33.5 (stable)
Login: 
########################################################################

'''

### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*1XX: red pública
*2XX: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis


```
/interface wireless set 0 ssid="free1XX"
/interface wireless add ssid="private2XX" master-interface=wlan1
```



###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estar interfaces. Cambiales
el nombre a wFree wPrivate y dejalas deshabilitadas

'''

########################################################################
[admin@MikroTik] > /file print
 # NAME                                                          TYPE                                                               SIZE CREATION-TIME       
 0 skins                                                         directory                                                               jan/01/1970 00:00:01
 1 20170317_zeroconf.backup                                      backup                                                          25.4KiB jan/02/1970 00:44:28
 2 auto-before-reset.backup                                      backup                                                          25.4KiB jan/02/1970 01:14:18
[admin@MikroTik] > /system backup 

.. -- go up to system
load -- Loads a previously saved backup
save -- Makes a backup of the current configuration


[admin@MikroTik] > /system backup load name=

Name ::= 20170317_zeroconf.backup | auto-before-reset.backup


[admin@MikroTik] > /system backup load name=20170317_zeroconf.backup   

name -- Name of the backup file
password -- 


[admin@MikroTik] > /system backup load name=20170317_zeroconf.backup  
password: 
Restore and reboot? [y/N]: 
y
Restoring system configuration
System configuration restored, rebooting now
Connection closed by foreign host.
[root@j01 m07]# telnet 192.168.88.1
Trying 192.168.88.1...
Connected to 192.168.88.1.
Escape character is '^]'.

MikroTik v6.33.5 (stable)
   in:                                                                                                                                                         assword: 



Password: 








  MMM      MMM       KKK                          TTTTTTTTTTT      KKK
  MMMM    MMMM       KKK                          TTTTTTTTTTT      KKK
  MMM MMMM MMM  III  KKK  KKK  RRRRRR     OOOOOO      TTT     III  KKK  KKK
  MMM  MM  MMM  III  KKKKK     RRR  RRR  OOO  OOO     TTT     III  KKKKK
  MMM      MMM  III  KKK KKK   RRRRRR    OOO  OOO     TTT     III  KKK KKK
  MMM      MMM  III  KKK  KKK  RRR  RRR   OOOOOO      TTT     III  KKK  KKK

  MikroTik RouterOS 6.33.5 (c) 1999-2015       http://www.mikrotik.com/

[?]             Gives the list of available commands
command [?]     Gives help on the command and list of arguments

[Tab]           Completes the command/word. If the input is ambiguous,
                a second [Tab] gives possible options

/               Move up to base level
..              Move up one level
/command        Use command at the base level
jan/02/1970 00:03:07 system,error,critical login failure for user  from 192.168.88.100 via telnet
  
[admin@mkt00] > /interface wireless set 0 ssid="free101"  
[admin@mkt00] > /interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BF:11:3A
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BF:11:3B
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BF:11:3C
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BF:11:3D
 4  X  wlan1                               wlan             1500  1600       2290 6C:3B:6B:BF:11:3E
[admin@mkt00] > /interface wireless add ssid="private201" master-interface=wlan1  
[admin@mkt00] > /interface print                                                
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BF:11:3A
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BF:11:3B
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BF:11:3C
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BF:11:3D
 4  X  wlan1                               wlan             1500  1600       2290 6C:3B:6B:BF:11:3E
 5  X  wlan2                               wlan                   1600       2290 6E:3B:6B:BF:11:3E
[admin@mkt00] > /setup    
Setup addresses on interfaces and default route.

[admin@mkt00] > /interface wireless se
security-profiles  set
[admin@mkt00] > /interface wireless set [find default-name=wlan1] name=free101
[admin@mkt00] > /interface print                                              
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BF:11:3A
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BF:11:3B
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BF:11:3C
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BF:11:3D
 4  X  free101                             wlan             1500  1600       2290 6C:3B:6B:BF:11:3E
 5  X  wlan2                               wlan                   1600       2290 6E:3B:6B:BF:11:3E
[admin@mkt00] > /interface wireless set [find name=wlan2] name=wprivate         
[admin@mkt00] > /interface print                                               
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BF:11:3A
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BF:11:3B
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BF:11:3C
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BF:11:3D
 4  X  free101                             wlan             1500  1600       2290 6C:3B:6B:BF:11:3E
 5  X  wprivate                               wlan                   1600       2290 6E:3B:6B:BF:11:3E
[admin@mkt00] > /interface wireless set [find default-name=wlan1] name=wfree     
[admin@mkt00] > /interface print                                            
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BF:11:3A
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BF:11:3B
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BF:11:3C
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BF:11:3D
 4  X  wfree                               wlan             1500  1600       2290 6C:3B:6B:BF:11:3E
 5  X  wpravate                               wlan                   1600       2290 6E:3B:6B:BF:11:3E
[admin@mkt00] > /system backup save name="20170317_parveen_zeroconf"
Saving system configuration
Configuration backup saved
[admin@mkt00] > quit
interrupted
           Connection closed by foreign host.
#############################################################################################

'''


## Crear interfaces virtuales y hacer bridges

```
/interface vlan add name eth4-vlan1XX vlan-id=1XX interface=eth4
/interface vlan add name eth4-vlan2XX vlan-id=2XX interface=eth4   

/interface bridge add name=br-vlan1XX
/interface bridge add name=br-vlan2XX


/interface bridge port add interface=eth4-vlan1XX bridge=br-vlan1XX
/interface bridge port add interface=eth3 bridge=br-vlan1XX
/interface bridge port add interface=wFree  bridge=br-vlan1XX      


/interface bridge port add interface=eth4-vlan2XX bridge=br-vlan2XX
/interface bridge port add interface=wPrivate   bridge=br-vlan2XX

/interface print


''' 

### [ejercicio3] Comenta cada una de estas líneas de la configuración
anterior para que quede bien documentado

'''
<<<<<<< HEAD

=======
########################################################################
>>>>>>> 2caaaf1f7b1a5a440670eeab5a6edbc7c7415061
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     ether1                              ether            1500  1598       2028 6C:3B:6B:C2:26:C1
 1  RS ether2-master                       ether            1500  1598       2028 6C:3B:6B:C2:26:C2
 2   S ether3                              ether            1500  1598       2028 6C:3B:6B:C2:26:C3
 3   S ether4                              ether            1500  1598       2028 6C:3B:6B:C2:26:C4
 4   S wfree                               wlan             1500  1600       2290 6C:3B:6B:C2:26:C5
 5     wprivate                            wlan             1500  1600       2290 6E:3B:6B:C2:26:C5
<<<<<<< HEAD
 6  R  ;;; defconf
       bridge                              bridge           1500  1598            6C:3B:6B:C2:26:C2
 7     ether4-vlan101                      vlan             1500  1594            6C:3B:6B:C2:26:C4
 8     ether4-vlan201                      vlan             1500  1594            6C:3B:6B:C2:26:C4
[admin@MikroTik] > 

[admin@MikroTik] > interface bridge port add interface=wfree  bridge=br-vlan101 
failure: device already added as bridge port
[admin@MikroTik] > interface bridge port add interface=ether4-vlan201 bridge=br-vlan201  
[admin@MikroTik] > interface bridge port add interface=wprivate   bridge=br-vlan201 
[admin@MikroTik] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     ether1                              ether            1500  1598       2028 6C:3B:6B:C2:26:C1
 1  RS ether2-master                       ether            1500  1598       2028 6C:3B:6B:C2:26:C2
 2   S ether3                              ether            1500  1598       2028 6C:3B:6B:C2:26:C3
 3   S ether4                              ether            1500  1598       2028 6C:3B:6B:C2:26:C4
 4   S wfree                               wlan             1500  1600       2290 6C:3B:6B:C2:26:C5
 5   S wprivate                            wlan             1500  1600       2290 6E:3B:6B:C2:26:C5
 6  R  br-vlan101                          bridge           1500  1594            6C:3B:6B:C2:26:C4
 7  R  br-vlan201                          bridge           1500  1594            6C:3B:6B:C2:26:C4
 8  R  ;;; defconf
       bridge                              bridge           1500  1598            6C:3B:6B:C2:26:C2
 9   S ether4-vlan101                      vlan             1500  1594            6C:3B:6B:C2:26:C4
10   S ether4-vlan201                      vlan             1500  1594            6C:3B:6B:C2:26:C4
[admin@MikroTik] > 


'''
=======
 6  R  br-vlan1XX                          bridge           1500 65535            00:00:00:00:00:00
 7  R  br-vlan2XX                          bridge           1500 65535            00:00:00:00:00:00
 8  R  ;;; defconf
       bridge                              bridge           1500  1598            6C:3B:6B:C2:26:C2
[admin@MikroTik] > 
#########################################################################################################
'''''

>>>>>>> 2caaaf1f7b1a5a440670eeab5a6edbc7c7415061

### [ejercicio4] Pon una ip 172.17.1XX.1/24 a br-vlan1XX 
y 172.17.2XX.1/24 a br-vlan2XX y verifica desde la interface de la placa 
base de tu ordenador que hay conectividad (puedes hacer ping) configurando
unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración

,,,

[root@j01 ~]# ip link add enp2s0 name vlanlinux
Not enough information: "type" argument is required
[root@j01 ~]# ip link add link enp2s0 name vlanlinux
Not enough information: "type" argument is required
[root@j01 ~]# ip link add link enp2s0 name vlanlinux type vlan id 101
[root@j01 ~]# ip a a 172.17.
Error: ??? prefix is expected rather than "172.17.".
[root@j01 ~]# ip a a 172.17.101.10/24 dev vlanlinux
[root@j01 ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
    inet 172.17.101.10/32 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 172.17.101.10/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet6 fe80::16da:e9ff:fe99:a0e4/64 scope link 
       valid_lft forever preferred_lft forever
3: usb0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
    inet 192.168.88.101/24 scope global usb0
       valid_lft forever preferred_lft forever
    inet6 fe80::d37f:6a19:bbf4:7e02/64 scope link 
       valid_lft forever preferred_lft forever
4: vlanlinux@enp2s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 14:da:e9:99:a0:e4 brd ff:ff:ff:ff:ff:ff
    inet 172.17.101.10/24 scope global vlanlinux
       valid_lft forever preferred_lft forever
[root@j01 ~]# ping 172.17.101.1
PING 172.17.101.1 (172.17.101.1) 56(84) bytes of data.
64 bytes from 172.17.101.1: icmp_seq=1 ttl=64 time=0.480 ms
64 bytes from 172.17.101.1: icmp_seq=2 ttl=64 time=0.248 ms
64 bytes from 172.17.101.1: icmp_seq=3 ttl=64 time=0.239 ms
64 bytes from 172.17.101.1: icmp_seq=4 ttl=64 time=0.249 ms
^C
--- 172.17.101.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3000ms
rtt min/avg/max/mdev = 0.239/0.304/0.480/0.101 ms
[root@j01 ~]# 

[admin@MikroTik] > ip address add interface=br-vlan101 address=172.17.101.1 netmask=255.255.255.0
[admin@MikroTik] > interface bridge print 
Flags: X - disabled, R - running 
 0  R name="br-vlan101" mtu=auto actual-mtu=1500 l2mtu=1594 arp=enabled mac-address=6C:3B:6B:C2:26:C4 protocol-mode=rstp priority=0x8000 auto-mac=yes 
      admin-mac=00:00:00:00:00:00 max-message-age=20s forward-delay=15s transmit-hold-count=6 ageing-time=5m 

 1  R name="br-vlan201" mtu=auto actual-mtu=1500 l2mtu=1594 arp=enabled mac-address=6C:3B:6B:C2:26:C4 protocol-mode=rstp priority=0x8000 auto-mac=yes 
      admin-mac=00:00:00:00:00:00 max-message-age=20s forward-delay=15s transmit-hold-count=6 ageing-time=5m 

 2  R ;;; defconf
      name="bridge" mtu=auto actual-mtu=1500 l2mtu=1598 arp=enabled mac-address=6C:3B:6B:C2:26:C2 protocol-mode=rstp priority=0x8000 auto-mac=no 
      admin-mac=6C:3B:6B:C2:26:C2 max-message-age=20s forward-delay=15s transmit-hold-count=6 ageing-time=5m 
[admin@MikroTik] > ip address print 
Flags: X - disabled, I - invalid, D - dynamic 
 #   ADDRESS            NETWORK         INTERFACE                                                                                                            
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    bridge                                                                                                               
 1   172.17.101.1/24    172.17.101.0    br-vlan101                                                                                                           
[admin@MikroTik] > ip address add interface=br-vlan201 address=172.17.201.1 netmask=255.255.255.0   
[admin@MikroTik] > ip address print                                                              
Flags: X - disabled, I - invalid, D - dynamic 
 #   ADDRESS            NETWORK         INTERFACE                                                                                                            
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    bridge                                                                                                               
 1   172.17.101.1/24    172.17.101.0    br-vlan101                                                                                                           
 2   172.17.201.1/24    172.17.201.0    br-vlan201                                                                                                           
[admin@MikroTik] > 

[admin@mkt00] > ip address add interface=eth1 address=192.168.3.1 netmask=255.255.0.0                   
[admin@mkt00] > /ip address export                                                   
# jan/02/1970 00:14:29 by RouterOS 6.33.5
# software id = LTDT-3V15
#
/ip address
add address=192.168.88.1/24 comment=defconf interface=eth2 network=192.168.88.0
add address=172.17.101.1/24 interface=br-vlan101 network=172.17.101.0
add address=172.17.201.1/24 interface=br-vlan201 network=172.17.201.0
add address=192.168.3.1/16 interface=eth1 network=192.168.0.0
[admin@mkt00] > ping 192.168.0.1
  SEQ HOST                                     SIZE TTL TIME  STATUS                                                                                         
    0 192.168.0.1                                56  64 1ms  
    1 192.168.0.1                                56  64 0ms  
    2 192.168.0.1                                56  64 0ms  
    3 192.168.0.1                                56  64 0ms  
    4 192.168.0.1                                56  64 0ms  
    sent=5 received=5 packet-loss=0% min-rtt=0ms avg-rtt=0ms max-rtt=1ms 

[admin@mkt00] > 


,,,

### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
.101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
Crear reglas de nat para salir a internet. 

'''
[admin@mkt00] > ip pool    
[admin@mkt00] /ip pool> add name=range_public ranges=172.17.100.101-172.17.100.250
[admin@mkt00] /ip pool> add name=range_private ranges=172.17.200.101-172.17.200.250        
[admin@mkt00] /ip pool> /    
[admin@mkt00] > ip dhcp-server print
Flags: X - disabled, I - invalid 
 #   NAME                               INTERFACE                               RELAY           ADDRESS-POOL                               LEASE-TIME ADD-ARP
[admin@mkt00] > ip dhcp-server add interface=br-vlan101 address-pool=range_p
range_private  range_public
[admin@mkt00] > ip dhcp-server add interface=br-vlan101 address-pool=range_public 
[admin@mkt00] > ip dhcp-server add interface=br-vlan201 address-pool=range_private  
[admin@mkt00] > ip dhcp-server print                                                
Flags: X - disabled, I - invalid 
 #   NAME                               INTERFACE                               RELAY           ADDRESS-POOL                               LEASE-TIME ADD-ARP
 0 X dhcp1                              br-vlan101                                              range_public                               10m       
 1 X dhcp2                              br-vlan201                                              range_private                              10m       
[admin@mkt00] > 


'''




### [ejercicio6] Activar redes wifi y dar seguridad wpa2

'''
[admin@mkt00] > /ip dhcp-server print   
Flags: X - disabled, I - invalid 
 #   NAME                               INTERFACE                               RELAY           ADDRESS-POOL                               LEASE-TIME ADD-ARP
 0 X dhcp1                              br-vlan101                                              range_public                               10m       
 1 X dhcp2                              br-vlan201                                              range_private                              10m       
[admin@mkt00] > /ip dhcp-server set 0 disabled=no
[admin@mkt00] > /ip dhcp-server set 1 disabled=no 
[admin@mkt00] > /ip dhcp-server print            
Flags: X - disabled, I - invalid 
 #   NAME                               INTERFACE                               RELAY           ADDRESS-POOL                               LEASE-TIME ADD-ARP
 0   dhcp1                              br-vlan101                                              range_public                               10m       
 1   dhcp2                              br-vlan201                                              range_private                              10m       
[admin@mkt00] > /ip dhcp-server set 1 disabled=no


'''

### [ejercicio6b] Opcional (monar portal cautivo)

### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar
puertos en Free.

### [ejercicio8] Conexión a switch cisco con vlans

### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante

### [ejercicio9] QoS

### [ejercicio9] Balanceo de salida a internet

### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos

### [ejercicio11] Firewall avanzado

