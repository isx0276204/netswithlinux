******************************************************************************
  M01-ISO Sistemes Operatius
  UF1: Linux Usuari: Instal·lació, configuració i explotació d'un sistema
                     informàtic
******************************************************************************
  A01-04-Redireccionaments,pipes, filtres, variables i shell bàsic
  Exercici 12: filtres
	- cut
	- sort
	- file
	- split, cat
	- diff, cmp, sdiff, 
	- gzip, gunzip, bzip, zcat
	- pste, join
	- exercicis globals
******************************************************************************

==============================================================================
Ordre: CUT
==============================================================================
00) situar-se al directori actiu /tmp/m01 i realizat TOTES les ordres des d'aquest directori (usuari hisx).
	$ cd /tmp/m01


01) Retallar del caracter 2 al 10 del d'un llistat llarg

	$ ls -la | cut -c 2-10

02) Retallar el nom d'usuari i el del fitxer. 
    Primer cal normalitzar (usem b = espai en blanc per marcar la solució)
	
	$ ls -la | sed 's/bb*/b/g' | cut -f5,8 -d'b'
	         | tr -s 'b' 'b'   |
	         | tr -s [:blank:] |
 	             normalitzar
   
03) Retallar permisos, nom d'usuari i nom de fitxer.
    (usem b = espai en blanc per marcar la solució)
	
	ls -la | sed 's/bb*/b/g' | cut -f1,5,8 -d'b' | cut -c2-
	          normalitzar                           només
	                                               permisos
	
	o be x | x | cut -c2-20 -f5,8

	
04) Retallar del fitxer llistat.txt (un llistat llarg normalitzat)
    del primer al camp n, tot variant n de 1 al final.
   
       cut -f1-1 -d' ' llistat.txt
       cut -f1-2 -d' ' llistat.txt       
       cut -f1-3 -d' ' llistat.txt   
       cut -f1-4 -d' ' llistat.txt   
   
   Veure que si l'origen no esta normalitzat, els camps no es 
   corresponen entre linies a causa de l'espai com a separador
   de camp.

==============================================================================
Ordre: SORT
==============================================================================
05) Tipus+Permis Link User Group Mida Data Hora  Nom
   -----------> permisos.txt
               <-------------- nopermisos.txt -----
   ---------------------------> inicial.txt
                              <------ final.txt ---

   Genrar un fitxer amb un llistat llarg de nom llistat.txt. 
   Editar aquests fitxer posant valors en les columnes d'enllaços
   i de mides de de diferents xifres (1,2 i 3 dígits).

    permisos.txt:
    $ cut -c1-10 llistat.txt > permisos.txt
    
    nopermisos.txt
    $ cut -c11- llistat.txt > nopermisos.txt
    
    inicial.txt
    $ cut -c1-31 llistat.txt > inicial.txt
    
    final.txt
    $ cut -c32-  llistat.txt > final.txt


06) Ordenar per línia en ordre invers.
    $ sort -r llistat.txt

07) Ordenar per data.
    $ sort +5 -6  +6n -7  llistat.txt
          mes    dia(num)

08) Ordenar per data i hora.
    $ sort +5 -6  +6n -7  +7 -8  llistat.txt
          mes    dia     hora

09) Ordenar número d'enllaços lexicogràficament.
    $ sort +1  llistat.txt
          links

10) Ordenar número d'enllaços per magnitud.
    $ sort +1n  llistat.txt
          links

11) Ordenar per nom de fitxer.      
    $ sort +8  llistat.txt
          noms

12) Ordenar per mida de fitxer.
    $ sort +4n llistat.txt
          mida

13) Ordenar per nom de fitxer, usuari, grup i mida.
    $ sort +8   +2      +3     +4n  llistat.txt
          nom  usuari  grup   mida

14) Ordenar final.txt lexicogràficament.
    $ sort final.txt

15) Ordenar final.txt ignorant espais inicials.
    $ sort -b final.txt    

16) Ordenar final.txt pel valor numèric del camp mida.    $ sort -n  final.txt
          mida


==============================================================================
Ordre: file
==============================================================================
17) Llistar tots els noms dels fitxers del directori actiu que son del tipus ascii
    $ file `ls` | grep ASCII
    $ ls | xargs file | grep ASCII 
               
18) LListar el nom dels fitxers ASCII del directori x.


19) LListar el tipus de fitxers dels directoris x, y, z.


20) Indicar el numero total de fitxers del tipus a del directori x.


21) Indicar el numero total de fitxers del tipus a o del tipus b del directori x.

   
22) Indicar el numero de fitxers del tipus (llistat_de_tipus) del directori x.


==============================================================================
Ordre: split
==============================================================================
23) Comptar el numero de linies del fitxer llistat.txt.
    $ cat llistat.txt | wc -l  --> 13 linies
       
24) Fer-ne tres parts de 5 linies cada part com a màxim.
    $ split -l 5 llistatc.txt 
    genera els fitxers: xaa xab xac
       
25) Vaure el contingut de xaa de xab i de xac
    $ cat xaa xab xac
       
26) Generar un fitxer de nom xtot.txt amb el resultat d'ajuntar els tres fitxers anteriors
    $ cat xaa xab xac > xtot.txt


==============================================================================
Ordres: diff, cmp, sdiff
==============================================================================
27) Comprobar que xtot.txt i llistat.txt son identics.
    $ diff llistatc.txt xtot.txt
      cmp
      sdiff
       
27) Generar tres parts del fitxer llistat.txt de noms NOMaa NOMab NOMac i tornar a ajuntarlos a l'inversa en un altre fitxer. Comparar l'original i reconstrucció errònia
    $ split -l 5 llistat.txt NOM
    $ cat NOMac NOMab NOMaa > erroni.txt
    $ diff llistat.txt erroni.txt


==============================================================================
Ordres: gzip, gunzip, bzip, zcat
==============================================================================
28) Comprimir el fitxer xtot.txt.
    $ gzip xtot.txt    --> genera el fitxer xtot.txt.gz (123 bytes)
                       no existeix el xtot inicial
       
29) De quin tipus es aquest fitxer?
    $ file xtot.txt.gz
              
30) Fer-ne tres parts de 125 bytes cada part
    $ split -b 125 xtot.txt.gz
       
31) Tornar a ajuntar les parts en un fitxer de nom: nou.gz
    $ cat xaa xab xac > nou.gz
       
32) Comprobar que nou.gz i xtot.txt.gz son identics
    $ diff xtot.txt.gz nou.gz
       
33) Descomprimir nou.gz i xtot.txt.gz
    $ gunzip nou.gz    --> nou
    $ gunzip xtot.gz   --> xtot
    --> desaparix el .gz i es genera el fitxer original
	
34) Comprobar que nou i xtot.txt son el mateix fitxer
    $ diff nou xtot.txt


==============================================================================
Ordres: paste, join
==============================================================================
35) (a) T+P L U NOM TE.txt    
    (b) G M D H NOM TD.txt
    Generar aquests dos fitxers a partir de llistat.txt
    $ cut -f1-3,9 -d' ' llistat.txt > TE.txt
    $ cut -f4-    -d' ' llistat.txt > TD.txt
       
36) Ajuntar els dos fitxers mostrant-ho per pantalla tal i com era originalment.
    $ paste TE.txt TD.txt  	--> Es repeteix el camp nom.
    $ cut -f1-3 -d' ' TE.txt | paste  - TD.txt  
    # eliminar el camp nom i es mostra tal i com era originalment

37) Mostrar ajuntant inicial.txt i final.txt
    $ paste inicial.txt final.txt
    
38) Mostrar ajuntant permis.txt  i nopermis.txt
    $ paste permis.txt nopermis.txt
       
39) Mostrar una taula amb el seguent format: Nom T+P L U
    $ paste `cut -f4 -d' ' TE.txt` `cut -f1-3 -d' ' TE.txt` 
    aixo no va perque paste utilitza noms de fitxers o un dels arguments
    pot ser la entrada estandard.
       
    $cut -f4 -d' ' TE.txt | paste - TE.txt |  cut -f1-4 -d' ' 
          Nom               T+P L U Nom       eliminar Nom final
	     
40) Unir les dues taules TE.txt i TD.txt en aquelles linies en que els camps clau coincideixen.
   
    $ join  -1 4 -2 6 TE.txt TD.txt
            -1 4 --> La clau del primer fitxer es el 4t camp
	    -2 6 --> La clau del segon fitxer es el 6e camp

41) Idem mostrant els dos primers camps de cada taula i la clau al final.
    $ join -1 4 -2 6 -o 1.1,1.2,2.1,2.2,0 TE.txt TD.txt
                        llistat de sortida      
       
42) Generar un fitxer TDR.txt amb el contingut de TD.txt ordenat en ordre invers.
    $ sort -r TD.txt > TDR.txt
       
43) Fer un join entre les taules TE.txt i TRD.txt. Quants registres (línies)
   mostra, perque?
    $ join -1 4 -2 6 TE.txt TDR.txt
       
    mostra 4 linies perque son les uniques en que coincideix el camp nom
    en la mateixa línia de les dues taules.




