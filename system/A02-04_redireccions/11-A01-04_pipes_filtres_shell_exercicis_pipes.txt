******************************************************************************
  M01-ISO Sistemes Operatius
  UF1: Linux Usuari: Instal·lació, configuració i explotació d'un sistema
                     informàtic
******************************************************************************
  A01-04-Redireccionaments,pipes, filtres, variables i shell bàsic
  Exercici 11: pipes, redireccionaments i variables
	- pipes
	- redireccionamnets
	- variables
******************************************************************************

==============================================================================
Ordre: pipes i redireccion
==============================================================================
00) situar-se al directori actiu /tmp/m01 i realizat TOTES les ordres des d'aquest directori (usuari hisx).

##
[isx0276204@j01 ~]$ mkdir /tmp/m01
[isx0276204@j01 ~]$ cd /tmp/m01/
[isx0276204@j01 m01]$


01) llistar el número major i el número menor dels dispositius corresponents a la entrada estàndard, sortida estàndard i d'error. Seguir el lnk fins identificar el device real on esta lligat

##
/dev/stderr: symbolic link to /proc/self/fd/2
[isx0276204@j01 m01]$ file /dev/stdout
/dev/stdout: symbolic link to /proc/self/fd/1
[isx0276204@j01 m01]$ file /dev/stdin
/dev/stdin: symbolic link to /proc/self/fd/0

[isx0276204@j01 m01]$ file /proc/self/fd/2
/proc/self/fd/2: symbolic link to /dev/tty3
[isx0276204@j01 m01]$ file /proc/self/fd/1
/proc/self/fd/1: symbolic link to /dev/tty3
[isx0276204@j01 m01]$ file /proc/self/fd/0
/proc/self/fd/0: symbolic link to /dev/tty3
[isx0276204@j01 m01]$

[isx0276204@j01 m01]$ ls -la /dev/std*
lrwxrwxrwx. 1 root root 15 Dec  1 08:42 /dev/stderr -> /proc/self/fd/2
lrwxrwxrwx. 1 root root 15 Dec  1 08:42 /dev/stdin -> /proc/self/fd/0
lrwxrwxrwx. 1 root root 15 Dec  1 08:42 /dev/stdout -> /proc/self/fd/1
[isx0276204@j01 m01]$


02) Desar en un fitxer de nom http.txt tots els serveis que continguin la cadena http.

##
[isx0276204@j01 m01]$ grep "http" /etc/services > http.txt
[isx0276204@j01 m01]$



03) Desar en un fitxer de nom http.txt tots els serveis que continguin la cadena http però que al mateix temps es mostri per pantalla.

##
[isx0276204@j01 m01]$ grep "http" /etc/services | tee http.txt



04) Desar en un fitxer de nom ftp.txt el llistat de tots els serveis que contenen la cadena ftp ordenats lexicogràficament. La sortida s'ha de mostrar simultàniament per pantalla

##
[isx0276204@j01 m01]$ cat /etc/services | grep "ftp" | sort | tee ftp.txt


	
05) Idem exercici anterior però mostrant per pantalla únicament quants serveis hi ha.

##

[isx0276204@j01 m01]$ cat /etc/services | grep "ftp" | sort | tee ftp.txt | wc -l
55



06) Idem anterior però comptant únicament quants contenen la descrició TLS/SSL

##
[isx0276204@j01 m01]$ cat /etc/services | grep "ftp" | grep "TLS/SSL" | sort | tee ftp.txt | wc -l
6


07) Llista l'ocupació d'espai del directori tmp fent que els missatges d'error s'ignorin.

##
[isx0276204@j01 m01]$ du /tmp/ 2> /dev/null
12      /tmp/m01
0       /tmp/.org.chromium.Chromium.rYhyrX
0       /tmp/tracker-extract-files.202172
0       /tmp/.esd-202172
0       /tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-colord.service-YNAZWu
0       /tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-rtkit-daemon.service-Ls4QcR
0       /tmp/.Test-unix
0       /tmp/.font-unix
0       /tmp/.XIM-unix
0       /tmp/.ICE-unix
0       /tmp/.X11-unix
68      /tmp/


08) Idem anterior desant el resultat a disc.txt i ignorant els errors.

##
[isx0276204@j01 m01]$ du /tmp/ 2> /dev/null > disk.txt
[isx0276204@j01 m01]$ cat disk.txt
12      /tmp/m01
0       /tmp/.org.chromium.Chromium.rYhyrX
0       /tmp/tracker-extract-files.202172
0       /tmp/.esd-202172
0       /tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-colord.service-YNAZWu
0       /tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-rtkit-daemon.service-Ls4QcR
0       /tmp/.Test-unix
0       /tmp/.font-unix
0       /tmp/.XIM-unix
0       /tmp/.ICE-unix
0       /tmp/.X11-unix
68      /tmp/

09) Idem enviant tota la sortida (errors i dades) al fitxer disc.txt

##
[isx0276204@j01 m01]$ du /tmp/ &> disk.txt
[isx0276204@j01 m01]$ cat disk.txt
12      /tmp/m01
du: cannot read directory '/tmp/.org.chromium.Chromium.rYhyrX': Permission denied
0       /tmp/.org.chromium.Chromium.rYhyrX
0       /tmp/tracker-extract-files.202172
0       /tmp/.esd-202172
du: cannot read directory '/tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-colord.service-YNAZWu': Permission denied
0       /tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-colord.service-YNAZWu
du: cannot read directory '/tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-rtkit-daemon.service-Ls4QcR': Permission denied
0       /tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-rtkit-daemon.service-Ls4QcR
0       /tmp/.Test-unix
0       /tmp/.font-unix
0       /tmp/.XIM-unix
0       /tmp/.ICE-unix
0       /tmp/.X11-unix
68      /tmp/


10) Afegir al fitxer disc.txt el sumari de l'ocupació de disc dels directoris /boot i /mnt. Els errors cal ignorar-los

##
[isx0276204@j01 m01]$ du /boot/ /mnt/ 2> /dev/null >> disk.txt
[isx0276204@j01 m01]$ cat disk.txt
12      /tmp/m01
du: cannot read directory '/tmp/.org.chromium.Chromium.rYhyrX': Permission denied
0       /tmp/.org.chromium.Chromium.rYhyrX
0       /tmp/tracker-extract-files.202172
0       /tmp/.esd-202172
du: cannot read directory '/tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-colord.service-YNAZWu': Permission denied
0       /tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-colord.service-YNAZWu
du: cannot read directory '/tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-rtkit-daemon.service-Ls4QcR': Permission denied
0       /tmp/systemd-private-2856dfd43ae14481ac4f9549e4d36a0d-rtkit-daemon.service-Ls4QcR
0       /tmp/.Test-unix
0       /tmp/.font-unix
0       /tmp/.XIM-unix
0       /tmp/.ICE-unix
0       /tmp/.X11-unix
68      /tmp/
2396    /boot/grub2/i386-pc
4160    /boot/grub2/locale
1320    /boot/grub2/fonts
7084    /boot/grub2/themes/system
7088    /boot/grub2/themes
14992   /boot/grub2
4       /boot/efi
1092    /boot/extlinux
123580  /boot/
4       /mnt/

11) Anomana per a què serveixen les variables:
	HOME, PWD, UID, EUID, HISTFILE; HISTFILESIZE, DISPLAY, SHELL, 
	HOSTNAME, HOSTYPE,LANG, PATH, PPID, PS1, PS2, TERM, USERS

##

12) Assigna a la variable NOM el teu nom complert (nom i cognoms) i assegura't que s'exporta als subprocessos.

##
[isx0276204@j01 m01]$ nom='parveen parveen'
[isx0276204@j01 m01]$ echo $nom
parveen parveen
[isx0276204@j01 m01]$ bash
[isx0276204@j01 m01]$ echo $nom

[isx0276204@j01 m01]$


13)Assigna el prompt un format que mostri la hora, la ruta absoluta del directori actiu i el nom d'usuari.

##

[parveen@localhost ~]$ PS1="\t \w \u "
20:03:24 ~ parveen cd /tmp/
20:03:35 /tmp parveen mkdir prova
20:03:45 /tmp parveen cd prova/
20:03:49 /tmp/prova parveen 

14) Assigna al prompt el format on mostra el nom d'usuari, de host, el número d'ordre i el número en l'històric d'ordres.

##
[parveen@localhost ~]$ PS1="\t \w \u "
20:03:24 ~ parveen cd /tmp/
20:03:35 /tmp parveen mkdir prova
20:03:45 /tmp parveen cd prova/
20:03:49 /tmp/prova parveen PS1="\u \h \# \! "
parveen localhost 6 815 ls
parveen localhost 7 816 ok
bash: ok: no se encontró la orden...
parveen localhost 8 817 



