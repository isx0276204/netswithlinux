===============================
Exercicis A01-04 Shell Bàsic I
===============================

10 - Exercicis shell (manual odt vell)
11 - Exercicis de redireccionamants i pipes (OK)
12 - Exercicis globals de filtres:
               cut, sort, file, 
               split, diff, cmp, sdiff
               gzip, gunzip, bzip, zcat, bzip2
               paste, join
13 - exercicis globals de filtres avançats
14 - exercicis de comandes (comandes, alias, expansion...)

Recomanats:  (11) (14)
Opcionals: 10, 12, 13

===============================
Apunts:                                    
===============================

a01-04-pipes_filtres_shell_apunts_exercicis.pdf
    apunts de tot el tema excepte filtres

a01-04-filtres-resum.pdf
    apunts de filtres i resum d'ordres

man-bash.pdf
    manual del bash en format pdf (man bash)

GNUbash.pdf
    manual de gnu del bash (no la pàgina del manual)

man-bash.html
    pàgina del man bash navegable per html
