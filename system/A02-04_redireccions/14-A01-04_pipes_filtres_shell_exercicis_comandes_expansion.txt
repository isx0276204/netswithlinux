******************************************************************************
  M01-ISO Sistemes Operatius
  UF1: Linux Usuari: Instal·lació, configuració i explotació d'un sistema
                     informàtic
******************************************************************************
  A01-04-Redireccionaments,pipes, filtres, variables i shell bàsic
  Exercici 14: exercicis de comandes:
                     -- path
                     -- alias
                     -- valors de retorn
                     -- expansions
                     -- command substitution
******************************************************************************
------------------------------------------------------------------------
alias
------------------------------------------------------------------------
01) Mostrar els alies actualment definits

##
x0276204@j01 ~]$ alias
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias l.='ls -d .* --color=auto'
alias ll='ls -l --color=auto'
alias ls='ls --color=auto'
alias mc='. /usr/libexec/mc/mc-wrapper.sh'
alias vi='vim'
alias which='(alias; declare -f) | /usr/bin/which --tty-only --read-alias --read-functions --show-tilde --show-dot'
alias xzegrep='xzegrep --color=auto'
alias xzfgrep='xzfgrep --color=auto'
alias xzgrep='xzgrep --color=auto'
alias zegrep='zegrep --color=auto'
alias zfgrep='zfgrep --color=auto'
alias zgrep='zgrep --color=auto'
[isx0276204@j01 ~]$



02) Fer un alias anomenat meu-data que mostri la data i el calendari.

##
[isx0276204@j01 ~]$ alias meu-data="date;cal"
[isx0276204@j01 ~]$ meu-data
Wed Dec  7 09:14:41 CET 2016
    December 2016
Su Mo Tu We Th Fr Sa
             1  2  3
 4  5  6  7  8  9 10
11 12 13 14 15 16 17
18 19 20 21 22 23 24
25 26 27 28 29 30 31

[isx0276204@j01 ~]$



03) Fer una alias de la ordre uname de manera que sempre mostri tota la 
    informació possible.
    Observar amb which quina és l'ordre associada a uname ara.

##
[isx0276204@j01 tmp]$ alias uname='uname -a'
[isx0276204@j01 tmp]$ uname
Linux j01.informatica.escoladeltreball.org 4.7.3-200.fc24.x86_64 #1 SMP Wed Sep 7 17:31:21 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux
[isx0276204@j01 tmp]$

[isx0276204@j01 tmp]$ which uname
alias uname='uname -a'
        /usr/bin/uname
[isx0276204@j01 tmp]$


04) Fer un alias de tree que llisti únicament els directoris.
    observar amb which l'ordre associada a tree.
##
[isx0276204@j01 tmp]$ alias tree='tree -d'
[isx0276204@j01 tmp]$ tree
.
├—— dirprova
├—— systemd-private-ea6549cfb78849ae90775858f613b5fd-colord.service-J0m1Y1 [error opening dir]
├—— systemd-private-ea6549cfb78849ae90775858f613b5fd-rtkit-daemon.service-o4Ivu4 [error opening dir]
└—— tracker-extract-files.202172

4 directories
[isx0276204@j01 tmp]$ which tree
alias tree='tree -d'
        /usr/bin/tree
[isx0276204@j01 tmp]$


05) Definir un alias permanentment a l'usuari que s'anomeni quisoc i 
    mostri per pantalla el uid, gid i el n'hom d'usuari.
##
[isx0276204@j01 ~]$ alias quisoc='id -u; id -g; echo $USER'
[isx0276204@j01 ~]$ quisoc
202172
200005
isx0276204
[isx0276204@j01 ~]$

06) Definir un alias permanentment per a tots els usuaris del sistema 
    anomenat sistema que mostra la data, la informació complerta de 
    uname i tota la informació descriptiva de la cpu.
##
alias processos='ps'
alias quihiha='finger;w'
alias sistema='date; uname -a; lscpu'
"/etc/bashrc" 95L, 2921C written
[root@j01 ~]# vim /etc/bashrc
[pere@j01 ~]$ sistema
Fri Dec  9 09:36:21 CET 2016
Linux j01.informatica.escoladeltreball.org 4.7.3-200.fc24.x86_64 #1 SMP Wed Sep 7 17:31:21 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                2
On-line CPU(s) list:   0,1
Thread(s) per core:    1
Core(s) per socket:    2
Socket(s):             1
NUMA node(s):          1
Vendor ID:             AuthenticAMD
CPU family:            16
Model:                 6
Model name:            AMD Athlon(tm) II X2 250 Processor
Stepping:              3
CPU MHz:               800.000
CPU max MHz:           3000.0000
CPU min MHz:           800.0000
BogoMIPS:              6027.06
Virtualization:        AMD-V
L1d cache:             64K
L1i cache:             64K
L2 cache:              1024K
NUMA node0 CPU(s):     0,1
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx mmxext fxsr_opt pdpe1gb rdtscp lm 3dnowext 3dnow constant_tsc rep_good nopl nonstop_tsc extd_apicid eagerfpu pni monitor cx16 popcnt lahf_lm cmp_legacy svm extapic cr8_legacy abm sse4a
misalignsse 3dnowprefetch osvw ibs skinit wdt nodeid_msr hw_pstate vmmcall npt lbrv svm_lock nrip_save
[pere@j01 ~]$

------------------------------------------------------------------------
PATH: ordres externes i internes
------------------------------------------------------------------------
07) Mostrar el PATH actualment definit
##
[isx0276204@j01 ~]$ echo $PATH
/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/users/inf/hisx1/isx0276204/.local/bin:/home/users/inf/hisx1/isx0276204/bin
[isx0276204@j01 ~]$


08) Eliminar el path (abans fer-ne una còpia a la variable OLDPATH) i 
    observar què passa en executar una ordre interna i una externa)
##
[isx0276204@j01 ~]$ echo $PATH
/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/users/inf/hisx1/isx0276204/.local/bin:/home/users/inf/hisx1/isx0276204/bin
[isx0276204@j01 ~]$ oldpath=$PATH
[isx0276204@j01 ~]$ echo $oldpath
/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/users/inf/hisx1/isx0276204/.local/bin:/home/users/inf/hisx1/isx0276204/bin
[isx0276204@j01 ~]$ unset PATH
[isx0276204@j01 ~]$ ls
-bash: ls: No such file or directory
[isx0276204@j01 ~]$ ls -la
-bash: ls: No such file or directory
[isx0276204@j01 ~]$ cd
[isx0276204@j01 ~]$ c
caller                    cd                        command_not_found_handle  complete                  continue
case                      command                   compgen                   compopt                   coproc
[isx0276204@j01 ~]$ mv
.adobe/            .cache/            Downloads/         .ICEauthority      .local/            netswithlinux/     Public/            .vim/
.bash_history      .config/           .esd_auth          image              .macromedia/       netswithlinux_old/ .shutter/          .viminfo
.bash_logout       Desktop/           .gitconfig         isx0276204/        markdown-plus/     out.jpg            .ssh/
.bash_profile      disc.img           .gnome2/           .lesshst           .mozilla/          Pictures/          Templates/
.bashrc            Documents/         .gnupg/            llista.txt         Music/             .pki/              Videos/
[isx0276204@j01 ~]$ mv disc.img disc.img
-bash: mv: No such file or directory
[isx0276204@j01 ~]$


09) Llistar l'estat de les ordres internes per obsevar si estan 
    activades o desactivades.
##
[isx0276204@j01 ~]$ enable -a
enable .
enable :
enable [
enable alias
enable bg
enable bind
enable break
enable builtin
enable caller
enable cd
enable command
enable compgen
enable complete
enable compopt
enable continue
enable declare
enable dirs
enable disown
enable echo
enable enable
enable eval
enable exec
enable exit
enable export
enable false
enable fc
enable fg
enable getopts
enable hash
enable help
enable history
enable jobs
enable kill
enable let
enable local
enable logout
enable mapfile
enable popd
enable printf
enable pushd
enable pwd
enable read
enable readarray
enable readonly
enable return
enable set
enable shift
enable shopt
enable source
enable suspend
enable test
enable times
enable trap
enable true
enable type
enable typeset
enable ulimit
enable umask
enable unalias
enable unset
enable wait
[isx0276204@j01 ~]$


10) Mostrar la pàgina de manual de les ordres internes

##
[isx0276204@j01 ~]$ builtin help
GNU bash, version 4.3.42(1)-release (x86_64-redhat-linux-gnu)
These shell commands are defined internally.  Type `help' to see this list.
Type `help name' to find out more about the function `name'.
Use `info bash' to find out more about the shell in general.
Use `man -k' or `info' to find out more about commands not in this list.

A star (*) next to a name means that the command is disabled.

 job_spec [&]                                                                    history [-c] [-d offset] [n] or history -anrw [filename] or history -ps arg >
 (( expression ))                                                                if COMMANDS; then COMMANDS; [ elif COMMANDS; then COMMANDS; ]... [ else COMM>
 . filename [arguments]                                                          jobs [-lnprs] [jobspec ...] or jobs -x command [args]
 :                                                                               kill [-s sigspec | -n signum | -sigspec] pid | jobspec ... or kill -l [sigsp>
 [ arg... ]                                                                      let arg [arg ...]
 [[ expression ]]                                                                local [option] name[=value] ...
 alias [-p] [name[=value] ... ]                                                  logout [n]
 bg [job_spec ...]                                                               mapfile [-n count] [-O origin] [-s count] [-t] [-u fd] [-C callback] [-c qua>
 bind [-lpsvPSVX] [-m keymap] [-f filename] [-q name] [-u name] [-r keyseq] [->  popd [-n] [+N | -N]
 break [n]                                                                       printf [-v var] format [arguments]
 builtin [shell-builtin [arg ...]]                                               pushd [-n] [+N | -N | dir]
 caller [expr]                                                                   pwd [-LP]
 case WORD in [PATTERN [| PATTERN]...) COMMANDS ;;]... esac                      read [-ers] [-a array] [-d delim] [-i text] [-n nchars] [-N nchars] [-p prom>
 cd [-L|[-P [-e]] [-@]] [dir]                                                    readarray [-n count] [-O origin] [-s count] [-t] [-u fd] [-C callback] [-c q>
 command [-pVv] command [arg ...]                                                readonly [-aAf] [name[=value] ...] or readonly -p
 compgen [-abcdefgjksuv] [-o option]  [-A action] [-G globpat] [-W wordlist]  >  return [n]
 complete [-abcdefgjksuv] [-pr] [-DE] [-o option] [-A action] [-G globpat] [-W>  select NAME [in WORDS ... ;] do COMMANDS; done
 compopt [-o|+o option] [-DE] [name ...]                                         set [-abefhkmnptuvxBCHP] [-o option-name] [--] [arg ...]
 continue [n]                                                                    shift [n]
 coproc [NAME] command [redirections]                                            shopt [-pqsu] [-o] [optname ...]
 declare [-aAfFgilnrtux] [-p] [name[=value] ...]                                 source filename [arguments]
 dirs [-clpv] [+N] [-N]                                                          suspend [-f]
 disown [-h] [-ar] [jobspec ...]                                                 test [expr]
 echo [-neE] [arg ...]                                                           time [-p] pipeline
 enable [-a] [-dnps] [-f filename] [name ...]                                    times
 eval [arg ...]                                                                  trap [-lp] [[arg] signal_spec ...]
 exec [-cl] [-a name] [command [arguments ...]] [redirection ...]                true
 exit [n]                                                                        type [-afptP] name [name ...]
 export [-fn] [name[=value] ...] or export -p                                    typeset [-aAfFgilrtux] [-p] name[=value] ...
 false                                                                           ulimit [-SHabcdefilmnpqrstuvxT] [limit]
 fc [-e ename] [-lnr] [first] [last] or fc -s [pat=rep] [command]                umask [-p] [-S] [mode]
 fg [job_spec]                                                                   unalias [-a] name [name ...]
 for NAME [in WORDS ... ] ; do COMMANDS; done                                    unset [-f] [-v] [-n] [name ...]
 for (( exp1; exp2; exp3 )); do COMMANDS; done                                   until COMMANDS; do COMMANDS; done
 function name { COMMANDS ; } or name () { COMMANDS ; }                          variables - Names and meanings of some shell variables
 getopts optstring name [arg]                                                    wait [-n] [id ...]
 hash [-lr] [-p pathname] [-dt] [name ...]                                       while COMMANDS; do COMMANDS; done
 help [-dms] [pattern ...]                                                       { COMMANDS ; }
[isx0276204@j01 ~]$


11) Mostrar l'ajuda de la ordre interna fg.

##
[isx0276204@j01 ~]$ help fg
fg: fg [job_spec]
    Move job to the foreground.

    Place the job identified by JOB_SPEC in the foreground, making it the
    current job.  If JOB_SPEC is not present, the shell's notion of the
    current job is used.

    Exit Status:
    Status of command placed in foreground, or failure if an error occurs.
[isx0276204@j01 ~]$

12) Crear una versió pròpia de l'ordre cal (per exemple que faci un ls), 
    i assignar-li precedència respecte a l'ordre del sistema. És a dir,
    fer que la versió de l'ordre de l'usuari s'executi en lloc de la del
    sistema.

##
[isx0276204@j01 prova]$ cp /usr/bin/ls cal
[isx0276204@j01 prova]$ ls
cal  calender  llista
[isx0276204@j01 prova]$ cal
    December 2016
Su Mo Tu We Th Fr Sa
             1  2  3
 4  5  6  7  8  9 10
11 12 13 14 15 16 17
18 19 20 21 22 23 24
25 26 27 28 29 30 31

[isx0276204@j01 prova]$
----------------------------in other term-------------------

[isx0276204@j01 prova]$ cal
cal  calender  llista
[isx0276204@j01 prova]$


13) Crear una ordre pròpia local de l'usuari anomenada meu-data (en 
    realitat és la ròpia ordre date).
##
[isx0276204@j01 prova]$ cp /usr/bin/date meu-data
[isx0276204@j01 prova]$
-------------------------in orter term------------------------
[isx0276204@j01 prova]$ meu-data
Wed Dec  7 10:36:39 CET 2016
[isx0276204@j01 prova]$


14) Elimina el directori local de l'usuari del PATH i executa l'ordre 
    meu-data. Què cal fer?
##

[isx0276204@j01 prova]$ meu-data
Wed Dec  7 10:36:39 CET 2016
[isx0276204@j01 prova]$


------------------------------------------------------------------------
Valors de retorn i compound commands
------------------------------------------------------------------------
15) Executa l'ordre free i comprova el valor de retorn d'execució.
##
[isx0276204@j01 prova]$ free
              total        used        free      shared  buff/cache   available
Mem:        7915612      490652     6703044       14088      721916     7117464
Swap:       5239804           0     5239804
[isx0276204@j01 prova]$ echo $?
0
[isx0276204@j01 prova]$


16) Executa l'ordre du -s i comproba el valor retornat.
##
[isx0276204@j01 prova]$ du -s
392     .
[isx0276204@j01 prova]$ echo $?
0
[isx0276204@j01 prova]$

    
17) Executa l'ordre date --noval i observa el codi de retorn.
##
[isx0276204@j01 prova]$ date --noval
date: unrecognized option '--noval'
Try 'date --help' for more information.
[isx0276204@j01 prova]$ echo $?
1
[isx0276204@j01 prova]$


18) Executa l'ordre nipum-nipam i observa el valor de retorn.
##
[isx0276204@j01 prova]$ nipim-nipum
bash: nipim-nipum: command not found...
[isx0276204@j01 prova]$ echo $?
127
[isx0276204@j01 prova]$


19) Crear dins de tmp un directori de nom dirprova. Únicament si l'ordre
    funciona mostrar per pantalla un missatge dient "ok directori creat"

##
[isx0276204@j01 tmp]$ mkdir dirprova && echo "ok director creat"
ok director creat
[isx0276204@j01 tmp]$ mkdir dirprova && echo "ok director creat"
mkdir: cannot create directory ‘dirprovả: File exists
[isx0276204@j01 tmp]$


20) Crear dins de tmp un directori de nom dirprova. Únicament si l'ordre
    falla mostrar per pantalla el codi d'error de l'ordre i un missatge
    dient "ERR: el directori no s'ha pogut crear".
##
[isx0276204@j01 tmp]$ mkdir dirprova &> /dev/null || echo "ERR: el directori no s'ha pogut crear"
ERR: el directori no s'ha pogut crear


    
------------------------------------------------------------------------
Expansions
------------------------------------------------------------------------
21) Mostra el resultat de multiplicar 10 per 15 i restar-n'hi 5
##
[pere@j01 bin]$ echo $((15*10-5))
145

    
22) Assigna a la variable num el valor de dividir 40 entre 4?
##
[pere@j01 bin]$ num=$((40/4))
[pere@j01 bin]$ echo $num
10
[pere@j01 bin]$


23) Assigna a la variable num el valor equivalent al doble del uid de 
    l'usuari

##
[pere@j01 bin]$ valor=$(($UID*3))
[pere@j01 bin]$ valor=$((UID*3))
[pere@j01 bin]$ echo $valor
3006
[pere@j01 bin]$

24) Assigna a la variable CASA el home de l'usuari usant tilde expansion.

##
[parveen@localhost ~]$ casa=~
[parveen@localhost ~]$ echo $casa
/home/parveen


25) Mostra el home de l'usuari root i el de l'usuari pere
##
[parveen@localhost ~]$ echo ~parveen
/home/parveen
[parveen@localhost ~]$ echo ~root
/root
[parveen@localhost ~]$ 


26) Assigna a la variable FITS el nom de tots els fitxers del directori 
    actual
##
[parveen@localhost ~]$ fits=$(ls)
[parveen@localhost ~]$ echo $fits
borrar clientes1 Descargas Documentos Escritorio from_server1 Imágenes markdown-plus Música netswithlinux Vídeos wifi


27) Assigna a la variable fits el nom de tots els fitxers acabats en txt
    del directori actual.

##


28) Mostra per pantalla els noms noma, nomb, nomc, nomd

##
[parveen@localhost ~]$ echo nom{a..d}
noma nomb nomc nomd
[parveen@localhost ~]$ 


29) Crear els directoris /tmp/dirprova/dir1 fins a dir10 de cop.

##
[parveen@localhost prova]$ mkdir -p dirprova/dir{1..10}
[parveen@localhost prova]$ ls dirprova/
dir1  dir10  dir2  dir3  dir4  dir5  dir6  dir7  dir8  dir9
[parveen@localhost prova]$ 



30) Crear dins de /tmp/dirprova els següents fitxers buits: carta-1.txt,
    carta-a.txt...fins a carta-e.txt, carta-10.txt... fins a carta-15.txt.

##
[parveen@localhost prova]$ touch carta-{1,{a..e},{10..15}}.txt
[parveen@localhost prova]$ ls -la
total 0
drwxrwxr-x.  3 parveen parveen 300 dic 13 20:59 .
drwxrwxrwt. 12 root    root    300 dic 13 20:54 ..
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-10.txt
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-11.txt
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-12.txt
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-13.txt
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-14.txt
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-15.txt
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-1.txt
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-a.txt
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-b.txt
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-c.txt
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-d.txt
-rw-rw-r--.  1 parveen parveen   0 dic 13 20:59 carta-e.txt
drwxrwxr-x. 12 parveen parveen 240 dic 13 20:54 dirprova
[parveen@localhost prova]$ 


30) Mostrar per pantalla els següents noms: informe1.txt, informe5.txt, 
    carta-a.txt...fins a carta-e.txt, treball.inf, dossier10... fins 
    dossier22.

##
[parveen@localhost prova]$ echo informe{1,5}.txt carta-{a..e}.txt treball.inf dossier{10..22}
informe1.txt informe5.txt carta-a.txt carta-b.txt carta-c.txt carta-d.txt carta-e.txt treball.inf dossier10 dossier11 dossier12 dossier13 dossier14 dossier15 dossier16 dossier17 dossier18 dossier19 dossier20 dossier21 dossier22
[parveen@localhost prova]$ 


------------------------------------------------------------------------
Command substitution
------------------------------------------------------------------------
31) Assigna a la variable DATANOW la hora actual.

##
[parveen@localhost prova]$ datanow=$(date)
[parveen@localhost prova]$ echo $datanow
mar dic 13 21:04:56 CET 2016
[parveen@localhost prova]$ 

    
32) Assigna a la variable la data en el moment en que es visualitza.

##


33) llista el nom del paquet al que pertany l'ordre executable fsck.

##
[pere@j01 bin]$ which fdisk
/usr/sbin/fdisk
[pere@j01 bin]$ rpm -qf $(which fdisk)
util-linux-2.28.2-1.fc24.x86_64
[pere@j01 bin]$

34) Executa un finger de l'uauri actual usant whoami.
##
[pere@j01 bin]$ finger $(whoami)
finger: uid=1002(pere): no such user.
finger: gid=1003(pere): no such user.
finger: groups=1003(pere),100(users),1004(basket): no such user.
finger: context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023: no such user.
Login: pere                             Name:
Directory: /home/pere                   Shell: /bin/bash
On since Fri Dec  9 09:38 (CET) on tty4   4 seconds idle
No mail.
No Plan.
[pere@j01 bin]$


35) Executa l'ordre que indica el tipus de fitxer que són cada un dels 
    executables del paquet al que pertany l'ordre fsck

##
[root@localhost ~]# rpm -ql $(rpm -qf $(which file))
/etc/magic
/usr/bin/file
/usr/share/doc/file
/usr/share/doc/file/ChangeLog
/usr/share/doc/file/README
/usr/share/licenses/file
/usr/share/licenses/file/COPYING
/usr/share/man/man1/file.1.gz
[root@localhost ~]# 


36) Assihna a la variable FORMATS el nom dels fitxers executables que
    permeten formatar sistemes de fitxers.

##
[root@localhost ~]# formato=$(locate $order | grep bin)
locate: no pattern to search for specified
[root@localhost ~]# order=mkfs
[root@localhost ~]# formato=$(locate $order | grep bin)
[root@localhost ~]# echo $formato
/usr/sbin/mkfs /usr/sbin/mkfs.btrfs /usr/sbin/mkfs.cramfs /usr/sbin/mkfs.ext2 /usr/sbin/mkfs.ext3 /usr/sbin/mkfs.ext4 /usr/sbin/mkfs.ext4dev /usr/sbin/mkfs.fat /usr/sbin/mkfs.hfsplus /usr/sbin/mkfs.minix /usr/sbin/mkfs.msdos /usr/sbin/mkfs.ntfs /usr/sbin/mkfs.vfat /usr/sbin/mkfs.xfs
[root@localhost ~]# order=file
[root@localhost ~]# formato=$(locate $order | grep bin)
[root@localhost ~]# echo $formato
/etc/selinux/targeted/contexts/files/file_contexts.bin /etc/selinux/targeted/contexts/files/file_contexts.homedirs.bin /etc/selinux/targeted/contexts/files/file_contexts.local.bin /usr/bin/abrt-action-trim-files /usr/bin/cd-create-profile /usr/bin/cd-fix-profile /usr/bin/check-binary-files /usr/bin/clean-binary-files /usr/bin/desktop-file-edit /usr/bin/desktop-file-install /usr/bin/desktop-file-validate /usr/bin/ephy-profile-migrator /usr/bin/file /usr/bin/file-roller /usr/bin/foomatic-datafile /usr/bin/foomatic-ppdfile /usr/bin/grub2-file /usr/bin/gupnp-dlna-ls-profiles-2.0 /usr/bin/gvfs-monitor-file /usr/bin/ntfsdump_logfile /usr/bin/profiles /usr/bin/sndfile-resample /usr/bin/systemd-tmpfiles /usr/bin/winefile /usr/sbin/filefrag /usr/sbin/fixfiles /usr/sbin/makedumpfile /usr/sbin/readprofile /usr/sbin/setfiles /usr/sbin/xfs_mkfile


    

