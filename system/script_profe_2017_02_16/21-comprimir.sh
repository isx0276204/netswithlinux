#! /bin/bash
# @edt M01-ISO
# febrer 2017
# Descripció: crear directoris
# Synopsis: prog.sh arg[...]
# ---------------------------------------
ERR=1
ERR_FILE=2
numfile=0
status=0
#validar arg
if [ $# -lt 1 ]  
then 
  echo "error: n args incorrecte"
  echo "usage: ---"
  exit $ERR 
fi
# itera args
llista=$*
for arg in $llista
do 
  gzip $arg &> /dev/null
  if [ $? -ne 0 ] 
  then
    echo "error: $arg no" >> /dev/stderr
    status="$ERR_FILE"
  else
    echo "se ha comprimit $arg.gz"
    numfile=$((numfile+1))
  fi
done
echo $numfile
exit $status

