#! /bin/bash
# @edt M01
# gener 2017
# Descripcio: exemple buble for
# synopsis: $ prog.sh [arg]...
# ---------------------------------
numlin=1
for arg in $*
do
  echo "$numlin: $arg"
  numlin=$((numlin+1))
done

exit 0
