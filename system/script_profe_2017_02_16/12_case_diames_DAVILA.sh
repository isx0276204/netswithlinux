#! /bin/bash
# @edt Adri isx48102233
# febrer 2017
# Descripcio: programa que et diu quants dies te un mes introduint un mes
# Synopsis: prog.sh -h
# """"""""" prog.sh mes
# ---------------------------------
OK=0
ERR=1
USAGEHELP="USAGE: prog.sh -h" 
USAGESY="USAGE: prog.sh mes"
diames=0
#1) Validar que rep 1 argv
if [ $# -ne 1 ]
then
  echo "ERROR: Quantitat d'arguments no valid"
  echo $USAGEHELP
  echo $USAGESY
  exit $ERR
fi
#2) Valida si l'arg es help
if [ $1 = "-h" ]
then
  echo "Aquest programa et diu la quantitat de dies que té un mes, introduint on numero de mes [1-12]"
  echo $USAGEHELP
  echo $USAGESY
  exit $OK
fi
#3) Valida que el valor d l'argument es valid
if [ $1 -lt 1 -o $1 -gt 12 ]
then
  echo "ERROR: Valor del mes no valid [1-12]"
  echo $USAGEHELP
  echo $USAGESY
  exit $ERR
fi
mes=$1
case $mes in
  "1"|"3"|"5"|"7"|"8".|"10"|"12")
  diames=31;;
  "4"|"6"|"9"|"11")
  diames=30;;
  *)
  diames=28;;
esac
echo "Els dies del mes $1 son: $diames"
exit $OK
