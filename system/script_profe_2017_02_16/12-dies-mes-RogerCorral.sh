#! /bin/bash
#Roger Corral Calero
#isx48125075
#2017/02/08
#Descripcio: donat un mes, el programa determina quants dies te el mes
#Synopsis: prog.sh -h
#	   prog.sh arg
#------------------------------------------------------

if [ $# -ne 1 ]
then
	echo "Error: numero d'arguments incorrecte"
	echo "Usage: prog -h"
	echo "       prog arg"
	exit 1
fi
if [ $1 = "-h" ]
then
	echo "mostro l'ajuda que calgui"
	exit 0
fi
if ! [ $1 -ge 1 -a $1 -le 12 ]
then
	echo "Error: mes fora del rang valid"
	echo "Range: mes valid entre 1-12"
	exit 2
fi
mes=$1
case $mes in
"4"|"6"|"9"|"11")
	echo "el mes $mes té 30 dies";;
"2")
	echo "el mes $mes té 28 dies";;
*)
	echo "el mes $mes té 31 dies";;
esac
exit 0
