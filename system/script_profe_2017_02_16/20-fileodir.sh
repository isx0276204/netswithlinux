#! /bin/bash
# @edt M01-ISO
# febrer 2017
# Descripció: crear directoris
# Synopsis: prog.sh -f|-d arg1 arg2 arg3
# ---------------------------------------
ERR_ARGS=1
ERR_OPC=2
status=0
if [ $# -ne 4 ]
then
  echo "error numero arguments incorrecte"
  echo "usage: prog -f|-d arg1 arg2 arg3"
  exit $ERR_ARGS
fi
if [ "$1" != "-f" -a "$1" != "-d" ]
then
  echo "error: opcio incorrecte"
  echo "usage: prog -f|-d arg1 arg2 arg3"
  exit $ERR_OPC
fi
opc="$1"
shift
llista=$*
for arg in $llista
do
  if ! [ "$opc"  "$arg" ]
  then
    echo "error: $arg no tipus ($opc)" >> /dev/stderr
    status=3
  fi
done
exit $status


