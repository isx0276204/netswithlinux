#! /bin/bash
# @edt M01
# gener 2017
# Descripcio: exemple bucles while
# ---------------------------------
# exemple 4)
num=1
while [ $# -gt 0 ]
do
  echo "$num: $1"
  num=$((num+1))
  shift
done
exit 0

# exemple 3) mostrar args amb while
num=1
MAX=$#
while [ $num -le $MAX ]
do
  echo "$num: $1"
  num=$((num+1))
  shift
done
exit 0

# exemple 2) amb while mostrar els arguments
num=1
MAX=$#
while [ $num -le $MAX ]
do
  echo $1, $#, $*
  num=$((num+1))
  shift
done
exit 0

# exemple 1) Mostrar els números del 0 al 10
MAX=10
num=0
while [ $num -le $MAX ]
do
  echo $num
  num=$((num+1))
done
exit 0
