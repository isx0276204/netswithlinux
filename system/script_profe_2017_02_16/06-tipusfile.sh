#! /bin/bash
# @edt M01
# gener 2017
# Descripcio: indica el tipus d'element del filesystem
# synopsis: $ prog.sh file
# ---------------------------------
# 0) declaracions
OK=0
ERR_NARGS=1
ERR_ARGNOTEXIST=2
#1) Validar numero arguments
if [ $# -ne 1 ]
then
  echo "Error, numero d'arguments no valid"
  echo "Usage: prog.sh file"
  exit $ERR_NARGS
fi
#2) Validar que l'argument existeix en el filesystem
if ! [ -e "$1" ]
then
  echo "Error, element inexisten en el filesystem"
  echo "Usage: prog.sh file"
  exit $ERR_ARGNOTEXIST
fi
#3) Mostrar el tipus d'element
if [ -h "$1" ]
then
  msg="Link"
elif [ -d "$1" ]
then
  msg="Directori"
elif [ -f "$1" ]
then
  msg="Regular file"
else
  msg="Altre tipus d'element (p,s,b,c)"
fi
echo "L'element $1 es de tipus: $msg"
exit $OK
