ASIX MP1_ISO
Curs 2010-211
A01-03 Permisos B�sics

===============================
     Permisos B�sics
===============================


Exercici_7
==========
-------------------------------------------------------------------------------
part1:
-------------------------------------------------------------------------------
00) Crear l'estructura de directoris i fitxers:
  a) Com usuari alumne crear un directori de nom mp1/calaix dins de tmp i
     assignar-li els seg�ents permisos: (rwx)(rwx)(---).
##
[isx0276204@j01 tmp]$ chmod 770 mp1/
[isx0276204@j01 tmp]$ chmod 770 mp1/calaix/
[isx0276204@j01 tmp]$ ls -la mp1/
total 0
drwxrwx---.  3 isx0276204 hisx1  60 Oct 24 12:23 .
drwxrwxrwt. 12 root       root  340 Oct 24 12:24 ..
drwxrwx---.  2 isx0276204 hisx1  40 Oct 24 12:23 calaix


  b) Crear dos fitxers dins de calaix  de nom fit1 i fit2. Els fitxers
     han de contenir dades. 
##
[isx0276204@j01 tmp]$ cat > mp1/calaix/fit01
hello
boos
good
and yyou
what is going
[isx0276204@j01 tmp]$ cat > mp1/calaix/fit02
hello
kaka mama papa chacha
abracadabra es contrase�a de de mi guest usari.

  c) Assignar perm��s de lectura dels fitxers al propietari, al grup i a 
     altres.
##
[isx0276204@j01 tmp]$ chmod 444 mp1/calaix/*
[isx0276204@j01 tmp]$ ls -la mp1/calaix/
total 8
drwxrwx---. 2 isx0276204 hisx1 80 Oct 24 12:26 .
drwxrwx---. 3 isx0276204 hisx1 60 Oct 24 12:23 ..
-r--r--r--. 1 isx0276204 hisx1 39 Oct 24 12:26 fit01
-r--r--r--. 1 isx0276204 hisx1 78 Oct 24 12:27 fit02
   
01) Crear un usuari user1 (si no existeix) amb password user1
	# useradd -d /tmp/user1 -m user1
	# passwd user1 #(posar user1 de password)
##
[root@j01 ~]# useradd -d /tmp/user1 -m user1
[root@j01 ~]# passwd user1
Changing password for user user1.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.

02) Situar-se al directori actiu mp1 i fer des d'aquest directori totes les ordres.
##
cd mp1


03) com a usuari user1 fer les ordres seg�ents (des del directori actiu mp1):
##
[user1@j01 tmp]$ cd mp1/
[user1@j01 mp1]$

  a) ls calaix
##
[user1@j01 mp1]$ ls calaix/
ls: cannot open directory 'calaix/': Permission denied
  
b) ls -la calaix
##
[user1@j01 mp1]$ ls -la calaix/
ls: cannot open directory 'calaix/': Permission denied
  
c) cat calaix/fit1
##
[user1@j01 mp1]$ cat calaix/fit01
cat: calaix/fit01: Permission denied
  

d) cd calaix
##
[user1@j01 mp1]$ cd calaix/
-bash: cd: calaix/: Permission denied
  

e) Es pot fer alguna d'aquestes ordres? NO.
##
no
04) Assignar a calaix els permisos (rwx)(rwx)(r--) i provar:
##
[isx0276204@j01 tmp]$ chmod 774 mp1/calaix/
[isx0276204@j01 tmp]$ ls -la mp1/
total 0
drwxr-xr-x.  3 isx0276204 hisx1  60 Oct 24 12:23 .
drwxrwxrwt. 13 root       root  360 Oct 24 12:44 ..
drwxrwxr--.  2 isx0276204 hisx1  80 Oct 24 12:26 calaix
  
a) ls calaix         --> mostra noms (fit1 i fit2)  + permiso denegado.
##
[user1@j01 mp1]$ ls calaix/
ls: cannot access 'calaix/fit02': Permission denied
ls: cannot access 'calaix/fit01': Permission denied
fit01  fit02
  
b) ls -la calaix     --> mostra noms (incloent . .. fit1 i fit2) + permiso denegado.
##
[user1@j01 mp1]$ ls -la calaix/
ls: cannot access 'calaix/.': Permission denied
ls: cannot access 'calaix/..': Permission denied
ls: cannot access 'calaix/fit02': Permission denied
ls: cannot access 'calaix/fit01': Permission denied
total 0
d????????? ? ? ? ?            ? .
d????????? ? ? ? ?            ? ..
-????????? ? ? ? ?            ? fit01
-????????? ? ? ? ?            ? fit02
  
c) cat calaix/fit1   --> qu� creus que passa?
##
[user1@j01 mp1]$ cat calaix/fit01
cat: calaix/fit01: Permission denied
  
d) cd calaix         --> No es poden fer.
##
[user1@j01 mp1]$ cd calaix/
-bash: cd: calaix/: Permission denied

05) Assignar a calaix els permisos (rwx)(rwx)(--x) i provar:
##
[isx0276204@j01 tmp]$ chmod 771 mp1/calaix/
[isx0276204@j01 tmp]$ ls -la mp1/
total 0
drwxr-xr-x.  3 isx0276204 hisx1  60 Oct 24 12:23 .
drwxrwxrwt. 13 root       root  360 Oct 24 12:46 ..
drwxrwx--x.  2 isx0276204 hisx1  80 Oct 24 12:26 calaix
  
a) ls calaix         --> qu� passa?
##
[user1@j01 mp1]$ ls calaix/
ls: cannot open directory 'calaix/': Permission denied
  
b) ls -la calaix     --> No es poden fer.
##
[user1@j01 mp1]$ ls -la calaix/
ls: cannot open directory 'calaix/': Permission denied
  
c) cat calaix/fit1   --> Mostra el contingut del fitxer.
##
[user1@j01 mp1]$ cat calaix/fit01
hello
boos
good
and yyou
what is going
  
d) cd calaix         --> Ok.
##
[user1@j01 mp1]$ cd calaix/
[user1@j01 calaix]$ echo $?
0
  
e) vi calaix/fit1    --> No es pot fer perqu� no es disposa de w en el fitxer.
##
not possible change any think  
f) mv calaix/fit1 calaix/fit1.txt --> No es pot fer perqu� no es disposa del perm�s w en el directori 
##
[user1@j01 calaix]$ mv fit01 fit01.txt
mv: cannot move 'fit01' to 'fit01.txt': Permission denied


-------------------------------------------------------------------------------
part2:
-------------------------------------------------------------------------------
06) Assignar a calaix els permisos (rwx)(rwx)(r-x) i provar:
##
[isx0276204@j01 tmp]$ chmod 775 mp1/calaix/
[isx0276204@j01 tmp]$ ls -la mp1/
total 0
drwxr-xr-x.  3 isx0276204 hisx1  60 Oct 24 12:23 .
drwxrwxrwt. 13 root       root  360 Oct 24 12:53 ..
drwxrwxr-x.  2 isx0276204 hisx1  80 Oct 24 12:26 calaix
  
a) ls calaix         --> 
##
[user1@j01 calaix]$ cd ..
[user1@j01 mp1]$ ls calaix/
fit01  fit02
  
b) ls -la calaix     --> 
##
[user1@j01 mp1]$ ls -la calaix/
total 8
drwxrwxr-x. 2 isx0276204 hisx1 80 Oct 24 12:26 .
drwxr-xr-x. 3 isx0276204 hisx1 60 Oct 24 12:23 ..
-r--r--r--. 1 isx0276204 hisx1 39 Oct 24 12:26 fit01
-r--r--r--. 1 isx0276204 hisx1 78 Oct 24 12:27 fit02
  
c) cat calaix/fit1   --> 
##
[user1@j01 mp1]$ cat calaix/fit01
hello
boos
good
and yyou
what is going
  
d) cd calaix         --> Tots els anteriors ok.
##
[user1@j01 mp1]$ cd calaix/
[user1@j01 calaix]$ echo $?
0
  
e) vi calaix/fit1    --> No es pot fer perqu� no es disposa de w en el fitxer.
##
not posible write in this file  
f) mv calaix/fit1 calaix/fit1.txt --> No es pot fer perqu� no es disposa del perm�s w en el directori.
##
not posible  
g) touch calaix/fit3
##
[user1@j01 calaix]$ touch fit03
touch: cannot touch 'fit03': Permission denied
  
h) mkdir calaix/noudir --> no es poden fer els dos �ltims en no disposar del perm�s w en el directori.
##
[user1@j01 calaix]$ cd ..
[user1@j01 mp1]$ mkdir calaix/noudir
mkdir: cannot create directory �calaix/noudir�: Permission denied

07) Assignar a calaix els permisos (rwx)(rwx)(-w-) i provar:
##
[isx0276204@j01 tmp]$ chmod 772 mp1/calaix/
[isx0276204@j01 tmp]$ ls -la mp1/
total 0
drwxr-xr-x.  3 isx0276204 hisx1  60 Oct 24 12:23 .
drwxrwxrwt. 13 root       root  360 Oct 24 13:03 ..
drwxrwx-w-.  2 isx0276204 hisx1  80 Oct 24 12:26 calaix
  
a) ls calaix         --> 
##
[user1@j01 mp1]$ ls calaix/
ls: cannot open directory 'calaix/': Permission denied
  
b) ls -la calaix     --> 
##
[user1@j01 mp1]$ ls -la calaix/
ls: cannot open directory 'calaix/': Permission denied
  
c) cat calaix/fit1   --> 
##
[user1@j01 mp1]$ cat calaix/fit01
cat: calaix/fit01: Permission denied
  
d) cd calaix         --> No es pot fer cap d'aquests perqu� falta r i x.
##
[user1@j01 mp1]$ cd calaix/
-bash: cd: calaix/: Permission denied
  
e) touch calaix/fit3 --> No es pot fer. Tot i disposar de w ens falta x per poder actuar en el directori.
##
[user1@j01 mp1]$ touch calaix/fit03
touch: cannot touch 'calaix/fit03': Permission denied
  
f) mv calaix/fit1 calaix/fit4.txt --> No es pot fer perqu� no es disposa del perm�s x en el directori.
##
[user1@j01 mp1]$ mv calaix/fit01 calaix/fit01.txt
mv: failed to access 'calaix/fit01.txt': Permission denied

08) Assignar a calaix els permisos (rwx)(rwx)(rw-) i provar:
##
[isx0276204@j01 tmp]$ chmod 776 mp1/calaix/
[isx0276204@j01 tmp]$ ls -la mp1/
total 0
drwxr-xr-x.  3 isx0276204 hisx1  60 Oct 24 12:23 .
drwxrwxrwt. 13 root       root  360 Oct 24 13:06 ..
drwxrwxrw-.  2 isx0276204 hisx1  80 Oct 24 12:26 calaix
  
a) ls calaix         --> Llista noms + permiso denegado (com amb r)
##
[user1@j01 mp1]$ ls calaix/
ls: cannot access 'calaix/fit02': Permission denied
ls: cannot access 'calaix/fit01': Permission denied
fit01  fit02
  
b) ls -la calaix     --> Idem r.
##
[user1@j01 mp1]$ ls -la calaix/
ls: cannot access 'calaix/.': Permission denied
ls: cannot access 'calaix/..': Permission denied
ls: cannot access 'calaix/fit02': Permission denied
ls: cannot access 'calaix/fit01': Permission denied
total 0
d????????? ? ? ? ?            ? .
d????????? ? ? ? ?            ? ..
-????????? ? ? ? ?            ? fit01
-????????? ? ? ? ?            ? fit02
  
c) cat calaix/fit1   --> No es permet, falta x.
##
[user1@j01 mp1]$ cat calaix/fit01
cat: calaix/fit01: Permission denied
  
d) cd calaix         --> No es permet, falta x.
##
[user1@j01 mp1]$ cd calaix/
-bash: cd: calaix/: Permission denied
  
e) touch calaix/fit3 --> No es pot fer. Tot i disposar de w ens falta x per poder actuar en el directori.
##
[user1@j01 mp1]$ touch calaix/fit03
touch: cannot touch 'calaix/fit03': Permission denied
  
f) mv calaix/fit1 calaix/fit4.txt --> No es pot fer perqu� no es disposa del perm�s x en el directori. La w sense la x no aporta res a la r.
##
not posible

09) Assignar a calaix els permisos (rwx)(rwx)(-wx) i provar:
  a) ls calaix         --> No perm�s, falta r.
  b) ls -la calaix     --> No perm�s falta r.
  c) cat calaix/fit1   --> Ok perqu� tenim x.
  d) cd calaix         --> Ok perqu� tenim x.
  e) touch calaix/fit3 --> Ok perqu� tenim w i x.
  f) mv calaix/fit1 calaix/fit4.txt --> Ok perqu� tenim x.
  g) ls fit1           
  h) ls -l fit1        --> Ok Perqu� la x permet accedit a la info dels fitxers tot i no tenir r.
     			   En realitat la falta de r simplement impedeix fer el llistat del directori. Per� si sabem el nom del que hi ha dins, amb 
  			   la x podem accedir al element de dins (cat, ls...).
  i) cp calaix/fit1 calaix/fit5 --> Ok tenim w en el directori i x, drets administratius en el directori.
  j) rm fit2            --> Ok perqu� es disposa de w i x en el directori.

10) Assignar a calaix els permisos (rwx)(rwx)(rwx) i provar:
  a) Es pot fer tot el que es vol si els permisos dels fitxers ho permeten.



-------------------------------------------------------------------------------
part3:
-------------------------------------------------------------------------------
11) Assignar a calaix els permisos (rwx)(rwx)(-wt)
  a) ls calaix         --> No.
  b) ls -la calaix     --> No.
  c) cat calaix/fit1   --> Ok perqu� tenim x.
  d) cd calaix         --> Ok perqu� tenim x.
  e) ls fit1           --> Ok per la r.
  f) ls -l fit1        --> Ok per la x.
  g) touch calaix/fit3 --> Ok perqu� tenim w i x.  
  h) cat > calaix/fit4 --> Ok perqu� tenim w i x.  
  i) cp calaix/fit1 calaix/fit5     --> Ok tenim w i x. 
  j) rm fit2           --> No per l'sticky bit.
  k) mv calaix/fit1 calaix/fit4.txt --> No per l'sticky bit. Si crea
                           fit4 pero no pot eliminar fit1.
	     La t (conjuntament amb la w) permet la x (entrar, veure descripcions...) pero treu els drets administratius de esborrar i 
	     canviar de nom un element. Es a dir, nomes es pot usar la w si l'element es seu, si es d'un altre propietari no.
  l) vi fit3 + editar + desar --> Ok perque es de user1.
  m) vi fit1 + editar + desar --> No perque es de alumne.
  
12) Assignar a calaix els permisos (rwx)(rwx)(--t)
  a) Permet fer el mateix que amb el permis X pero te activat el t,
     que no es pot aplicar perque no tenim w per esborrar coses.
  b) La t es aplicacble als altres permisos de propietari i de grup.
  

13) Assignar a calaix els permisos (rwx)(rwx)(--T)
  a) T no conte x.
  b) L'sticky bit s'aplica a propietari i grup si tenen la w 
     activada pero no es aplicable a altres perque no tenen x.

14) Assignar a calaix els permisos (rwx)(rws)(rwx)     
   a) Tot el que es crea en el directori pertany al grup al que
      pertany el directori.
      
15) Assignar a calaix els permisos (rwx)(rwS)(rwx)
   a) No x per a grup a pesar que esta actiu el setguid.
   b) No te sentit que el grup tingui setguid pero al meteix temps
      es restringueixi la x al grup.

16) Assignar a calaix els permisos (rws)(rwx)(rwx)
   a) No aplicable
   
 



