#! /bin/bash
# substituir strings amb sed
# substituir1.ss
#
#
clear
echo "sed per substituir: "
echo "del seguent fitxer: "
echo
cat text4.txt
echo
echo "sed 's/Mr. Smith/Ms. Wilson/g' text4.txt"
sed 's/Mr. Smith/Ms. Wilson/g' text4.txt
echo
read

clear
echo "sed per substituir: "
echo "del seguent fitxer: "
echo
cat text5.txt
echo
echo "s/sed 'EDT[^A-Za-z]*edt/g' text5.txt"
sed 's/EDT[^A-Za-z]*edt/EDT-XX-edt/g' text5.txt
echo
read

clear
echo "sed per substituir UN SOL cop: "
echo "del seguent fitxer: "
echo
cat text4.txt
echo
echo "sed 's/Mr. Smith/Ms. Wilson/' text4.txt"
sed 's/Mr. Smith/Ms. Wilson/' text4.txt
echo
read

clear
echo "sed per eliminar un string: "
echo "del seguent fitxer: "
echo
cat text4.txt
echo
echo "sed 's/Mr. Smith//g' text4.txt"
sed 's/Mr. Smith//g' text4.txt
echo
read

clear
echo "sed substituint de la linea 2 a la 4"
echo "del seguent fitxer: "
echo
cat text4.txt
echo
echo "sed '2,4 s/Mr. Smith/Ms. Wilson/g' text4.txt"
sed '2,4 s/Mr. Smith/Ms. Wilson/g' text4.txt
echo
read

clear
echo "sed substituint les lineas entre dues fites"
echo "les fites son cap1 i cap2"
echo "del seguent fitxer: "
echo
cat text6.txt
echo
echo "sed '/^ *cap1/,/^ *cap2/ s/Mr. Smith/Ms. Wilson/g' text6.txt"
sed '/^ *cap1/,/^ *cap2/  s/Mr. Smith/Ms. Wilson/g' text6.txt
echo
read

clear
echo "sed substituint les lineas que contenen el patro A"
echo "del seguent fitxer: "
echo
cat text6.txt
echo
echo "sed '/A/ s/Mr. Smith/Ms. Wilson/g' text6.txt"
sed '/A/  s/Mr. Smith/Ms. Wilson/g' text6.txt
echo
read

clear
echo "idem amb patro B, delimitador :"
echo "del seguent fitxer: "
echo
cat text6.txt
echo
echo "sed '\:B: s/Mr. Smith/Ms. Wilson/g' text6.txt"
sed '\:B:  s/Mr. Smith/Ms. Wilson/g' text6.txt
echo
read

clear
ID="hola xx que xxx tal x"
echo "Substituir: tots els grups de x per un sol -"
echo "del seguent text: hola xx que xxx tal x"
echo "echo $ID | sed 's/[x*]/?/g'"
echo
echo $ID | sed 's/x*/?/g' 
echo
echo "falla perque cal indicar un o mes x "
echo "echo $ID | sed 's/[xx*]/?/g'"
echo
echo $ID | sed 's/xx*/?/g'
echo
read

clear
echo "Recordar el que coincideix en el patro"
echo "busca les x i les torna a pintar entra altre text"
echo "del seguent fitxer: "
echo
cat text8.txt
echo
echo
echo "'s/\([x*]\)/-\1- /g'"
sed 's/\([x*]\)/-\1- /g' text8.txt
echo
read

clear
echo "Eliminar-ho tot menys el que coincideix amb el patro"
echo "Nomes deixa Mr."
echo "del seguent fitxer: "
echo
cat text4.txt
echo
echo
echo "sed 's/.*\(Mr.\).*/\1 /g' text4.txt"
sed 's/.*\(Mr.\).*/\1 /g' text4.txt
echo
read

clear
echo "Posar separacio de milers a xifres"
echo "del seguent fitxer: "
echo
cat text9.txt
echo
echo
echo "sed 's/\([0-9]\{1,3\}\)\([0-9]\{3\}\)/\1,\2/g' text9.txt"
sed 's/\([0-9]\{1,3\}\)\([0-9]\{3\}\)/\1,\2/g' text9.txt
echo
read
