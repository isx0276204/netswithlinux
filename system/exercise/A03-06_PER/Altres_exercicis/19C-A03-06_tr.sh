#! /bin/bash
# Traduir caracters
# traduir-ss
#
#
clear
echo "Convertir el fitxer text1.txt"
echo "cat text1.txt | tr '[a-c]' '[1-3]'"
echo
cat text1.txt
echo
echo
echo "Traduccio: "
cat text1.txt | tr '[a-c]' '[1-3]'
echo
read

clear
echo "Eliminar caracters del fitxer"
echo " tr -d 'ax1'"
echo
cat text1.txt
echo
echo
echo "Traduccio: "
cat text1.txt | tr -d '[ax1]'
echo
read


clear
echo "Convertir el fitxer text1.txt desactivant el vuite bit"
echo " tr '[\001-\176]\177' '[\201-\376]\377'"
echo
cat text1.txt
echo
echo
echo "Traduccio: "
cat text1.txt | tr  '[\001-\176]\177' '[\201-\376]\377'
echo
read


clear
echo "Convertir el fitxer text2.txt a TEXT desactivant el vuite bit"
echo " tr '[\201-\376]\377' '[\001-\176]\177'"
echo
cat text2.txt
echo
echo
echo "Traduccio: "
cat text2.txt | tr '[\201-\376]\377' '[\001-\176]\177'
echo
read

clear
echo "Eliminar els caracters de control"
echo " tr -d '[\001-\010][\013-\037]\177'"
echo
cat text2.txt
echo
echo
echo "Traduccio: "
echo
cat text2.txt |  tr -d '[\001-\010][\013-\037]\177'
echo
read


clear
echo "Traduir els caracters de control a un de error"
echo " tr '[\001-\010][\013-\037]\177' '[^*]'"
echo
cat text2.txt
echo
echo
echo "Traduccio: "
echo
cat text2.txt |  tr '[\001-\010][\013-\037]\177' '[^*]'
echo
read

clear
echo "Traduir els espais a :"
echo " tr ' ' '[:]'"
echo
cat text1.txt
echo
echo
echo "Traduccio: "
echo
cat text1.txt |  tr '[ ]' '[:]'
echo
read
