#/bin/bash
#
# Departament d'informatica
# Credit 1: Sistemes Operatius
# 2001
#
###########################################################

clear
cat dades.txt
echo
echo "substituir un nom per un altre: word per -noutext-"
echo 'sed "s/Word/-noutext-/g" dades.txt'
echo
sed 's/Word/-noutext-/g' dades.txt
echo
read opc

clear
cat dades.txt
echo
echo "substuir totes les aparicions de: 3 per -noutext-"
echo 'sed "s/3/-noutext-/g" dades.txt'
echo
sed 's/3/-noutext-/g' dades.txt
echo
read opc

clear
cat dades.txt
echo
echo "substuir UNA aparicio de: 3 per -noutext-"
echo 'sed "s/3/-noutext-/" dades.txt'
echo
sed 's/3/-noutext-/' dades.txt
echo
read opc

clear
cat dades.txt
echo
echo "substuir 3 per -noutext- NOMES en la linia de Excel"
echo 'sed "/Excel/ s/3/-noutext-/" dades.txt'
echo
sed '/Excel/ s/3/-noutext-/' dades.txt
echo
read opc

clear
cat dades.txt
echo
echo "substuir tota la linia de excel(literal) per -noutext-"
echo 'sed "s/2:Excel:Curs de fc:350:30:0/-noutext-/" dades.txt'
echo
sed 's/2:Excel:Curs de fc:350:30:0/-noutext-/' dades.txt
echo
read opc


clear
cat dades.txt
echo
echo "substuir tota la linia de excel per -noutext- RECORDANT la linia"
echo 'sed "s/\(2:Excel:Curs de fc:350:30:0\)/\1-noutext-/" dades.txt'
echo
sed 's/\(2:Excel:Curs de fc:350:30:0\)/\1-noutext-/' dades.txt
echo
read opc


clear
cat dades.txt
echo
echo "AFEGIR -noutext- entre la descripcis i les hores"
echo 'sed "s/\(2:Excel:Curs de fc:\)\(350:30:0\)/\1 -noutext- \2/" dades.txt'
echo
sed 's/\(2:Excel:Curs de fc:\)\(350:30:0\)/\1 -noutext- \2/' dades.txt
echo
read opc

clear
cat dades.txt
echo
echo "SUBSTITUIR el numero de hores per -numhores-"
echo 'sed "s/\(2:Excel:Curs de fc:\)350\(:30:0\)/\1 -nunhores- \2/" dades.txt'
echo
sed 's/\(2:Excel:Curs de fc:\)350\(:30:0\)/\1 -nunhores- \2/' dades.txt
echo
read opc


clear
cat dades.txt
echo
echo "ELIMINAR el numero de hores"
echo 'sed "s/\(2:Excel:Curs de fc:\)350\(:30:0\)/\1\2/" dades.txt'
echo
sed 's/\(2:Excel:Curs de fc:\)350\(:30:0\)/\1\2/' dades.txt
echo
read opc


clear
cat dades.txt
echo
echo "ELIMINAR el numero de hores usant comodins per les hores (les elimina)"
echo 'sed "s/\(2:Excel:Curs de fc:\).*\(:30:0\)/\1\2/" dades.txt'
echo
sed "s/\(2:Excel:Curs de fc:\).*\(:30:0\)/\1\2/" dades.txt
echo
read opc

clear
cat dades.txt
echo
echo "Usar variables pel nom del curs"
echo "nom='Excel'"
echo 'sed "s/\(2:$nom:Curs de fc:\)350\(:30:0\)/\1\2/" dades.txt'
echo
nom='Excel'
sed "s/\(2:$nom:Curs de fc:\).*\(:30:0\)/\1-noutext-\2/" dades.txt
echo
read opc

clear
cat dades.txt
echo
echo "Usar variables: codi:curs:descripcio: i una per: places:inscrits"
echo "inicial='2:Excel:Curs de fc:'"
echo "final=':30:0'"
echo 'sed "s/\($inicial\).*\($final\)/\1\2/" dades.txt'
echo
inicial='2:Excel:Curs de fc:'
final=':30:0'
sed "s/\($inicial\).*\($final\)/\1-noutext-\2/" dades.txt
echo
read opc





