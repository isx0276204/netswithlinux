#! /bin/bash
# eliminar lineas duplicades
# uniques.ss
#
#
clear
echo "llistar les lineas uniques"
echo "del seguent fitxer: "
echo
cat text3.txt
echo
echo
echo "uniq text3.txt: "
uniq text3.txt
echo
read


clear
echo "llistar les lineas uniques previament ordenades"
echo "del seguent fitxer: "
echo
cat text3.txt
echo
echo
echo "sort text3.txt | uniq "
sort text3.txt | uniq
echo
read


clear
echo "llistar les lineas uniques previament ordenades   UNIQUES	"
echo "del seguent fitxer: "
echo
cat text3.txt
echo
echo
echo "sort text3.txt | uniq  -u"
sort text3.txt | uniq -u
echo
read

clear
echo "llistar les lineas uniques previament ordenades  DUPLICADES"
echo "del seguent fitxer: "
echo
cat text3.txt
echo
echo
echo "sort text3.txt | uniq -d"
sort text3.txt | uniq -d
echo
read

clear
echo "llistar les lineas uniques previament ordenades  COMPTANT"
echo "del seguent fitxer: "
echo
cat text3.txt
echo
echo
echo "sort text3.txt | uniq -c"
sort text3.txt | uniq -c
echo
read


