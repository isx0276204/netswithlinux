******************************************************************************
  M01-ISO Sistemes Operatius
  UF2: Gestió de la informació i recursos de xarxa
******************************************************************************
  A03-06-PER i processament de text
  Exercici 20: exercicis de sed i grep i pER
******************************************************************************
------------------------------------------------------------------------
PER i filtres
------------------------------------------------------------------------
01) Del resultat de fer un head de les 15 primeres línies del fitxer
	/etc/passwd mostrar les línies que contenen un 2 en algun lloc.
	$ head -15 /etc/passwd | grep 2
	daemon:x:2:2:daemon:/sbin:/sbin/nologin
	mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
	games:x:12:100:games:/usr/games:/sbin/nologin

02) Del resultat de fer un head de les 15 primeres línies del fitxer
	/etc/passwd mostrar només les línies que tenen uid 2.
	$ head -15 /etc/passwd | grep "^[^:]*:[^:]*:2:"
	daemon:x:2:2:daemon:/sbin:/sbin/nologin

03) Usant grep valida si un dni té el format apropiat.
	$ echo "12345678A" | grep -E "^[0-9]{8}[A-Za-z]$"
	12345678A

04) Usant grep valida si una data té el format dd-mm-aaaa.
	$ echo "31-01-2001" | grep -E "^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
	31-01-2001

05) Usant grep valida si una data té el format dd/mm/aaaa.
	$ echo "31/01/2001" | grep -E "^[0-9]{2}/[0-9]{2}/[0-9]{4}$"
	31-01-2001

	$ echo "31/01/2001" | grep "^[0-9]\{2\}/[0-9]\{2\}/[0-9]\{4\}$"
	31/01/2001

06) Usant grep validar si una data té un format vàlid. Els formats poden
	ser: dd-mm-aaaa o dd/mm/aa.
	$ echo "31/01/2001" | grep -E "^[0-9]{2}[/-][0-9]{2}[/-][0-9]{4}$"
	31/01/2001
	$ echo "31-01-2001" | grep -E "^[0-9]{2}[/-][0-9]{2}[/-][0-9]{4}$"
	31-01-2001

07) Buscar totes les línies del fitxer noms1.txt que tenen la cadena
	Anna o la cadena Jordi
	$ grep -E "Anna|Jordi" noms1.txt 
	Jordi	Puig	Barcelona	1
	Anna	Puig	Girona		2
	Anna	Mas	Barcelona	4
	Anna	Puig	Barcelona	5

08) Substituir del fitxer noms1.txt tots els noms Anna i Jordi per -nou-.
	$ sed -r 's/Anna|Jordi/-nou-/g' noms1.txt 
	nom	cognom	ciutat		codi
	-nou-	Puig	Barcelona	1
	-nou-	Puig	Girona		2
	Zeus	Mas	Hospitalet	3
	-nou-	Mas	Barcelona	4
	-nou-	Puig	Barcelona	5

09) Del resultat de fer un head (10 línies) del fitxer /etc/passwd
	substituir '/bin/nologin' per '-noshell'.
	*nota: atenció a com escapeu l'slash '/'
	$ head /etc/passwd | sed 's/\/sbin\/nologin/-noshell-/' 
	root:x:0:0:root:/root:/bin/bash
	bin:x:1:1:bin:/bin:-noshell-
	daemon:x:2:2:daemon:/sbin:-noshell-
	adm:x:3:4:adm:/var/adm:-noshell-
	lp:x:4:7:lp:/var/spool/lpd:-noshell-
	sync:x:5:0:sync:/sbin:/bin/sync
	shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
	halt:x:7:0:halt:/sbin:/sbin/halt
	mail:x:8:12:mail:/var/spool/mail:-noshell-
	uucp:x:10:14:uucp:/var/spool/uucp:-noshell-

10) Ídem que l'exercici anteior però fent la substitució només
	de les línies 4 a la 8.
	$ head /etc/passwd | sed '4,8 s/\/sbin\/nologin/-noshell-/' 
	root:x:0:0:root:/root:/bin/bash
	bin:x:1:1:bin:/bin:/sbin/nologin
	daemon:x:2:2:daemon:/sbin:/sbin/nologin
	adm:x:3:4:adm:/var/adm:-noshell-
	lp:x:4:7:lp:/var/spool/lpd:-noshell-
	sync:x:5:0:sync:/sbin:/bin/sync
	shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
	halt:x:7:0:halt:/sbin:/sbin/halt
	mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
	uucp:x:10:14:uucp:/var/spool/uucp:/sbin/nologin

11) Ídem exercici anterior però fent les substitucions des de la línia
	que conté admin fins la línia que conté halt.
	$ head /etc/passwd | sed '/adm/,/halt/ s/\/sbin\/nologin/-noshell-/' 
	root:x:0:0:root:/root:/bin/bash
	bin:x:1:1:bin:/bin:/sbin/nologin
	daemon:x:2:2:daemon:/sbin:/sbin/nologin
	adm:x:3:4:adm:/var/adm:-noshell-
	lp:x:4:7:lp:/var/spool/lpd:-noshell-
	sync:x:5:0:sync:/sbin:/bin/sync
	shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
	halt:x:7:0:halt:/sbin:/sbin/halt
	mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
	uucp:x:10:14:uucp:/var/spool/uucp:/sbin/nologin

