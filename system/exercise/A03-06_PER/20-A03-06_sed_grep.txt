******************************************************************************
  M01-ISO Sistemes Operatius
  UF2: Gestió de la informació i recursos de xarxa
******************************************************************************
  A03-06-PER i processament de text
  Exercici 20: exercicis de sed i grep i pER
******************************************************************************
------------------------------------------------------------------------
PER i filtres
------------------------------------------------------------------------
01) Del resultat de fer un head de les 15 primeres línies del fitxer
	/etc/passwd mostrar les línies que contenen un 2 en algun lloc.
##
[isx0276204@j01 m01]$ head -n 15 passwd | grep "2"
daemon:x:2:2:daemon:/sbin:/sbin/nologin
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin


02) Del resultat de fer un head de les 15 primeres línies del fitxer
	/etc/passwd mostrar només les línies que tenen uid 2.
##
[isx0276204@j01 m01]$ head -n 15 passwd | grep "^[^:]*:[^:]*:2:"
daemon:x:2:2:daemon:/sbin:/sbin/nologin


03) Usant grep valida si un dni té el format apropiat.
##
[isx0276204@j01 m01]$ echo "12345678A" | egrep "^[0-9]{8}[A-Z]$"
12345678A
[isx0276204@j01 m01]$ echo "12345678A" | egrep "^[0-9]{8}[A-Z]$"
12345678A
[isx0276204@j01 m01]$ echo "12345678b" | egrep "^[0-9]{8}[A-Z]$"
[isx0276204@j01 m01]$ echo "12345678B" | egrep "^[0-9]{8}[A-Z]$"
12345678B
[isx0276204@j01 m01]$ echo "1234567B" | egrep "^[0-9]{8}[A-Z]$"
[isx0276204@j01 m01]$ echo "12345673B" | egrep "^[0-9]{8}[A-Z]$"
12345673B
[isx0276204@j01 m01]$ echo "123456733B" | egrep "^[0-9]{8}[A-Z]$"
[isx0276204@j01 m01]$ echo "12345673B" | egrep "^[0-9]{8}[A-Z]$"
12345673B
[isx0276204@j01 m01]$


04) Usant grep valida si una data té el format dd-mm-aaaa.
##
x0276204@j01 m01]$ echo "12-12-2016" | egrep "^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
12-12-2016
[isx0276204@j01 m01]$ echo "12-12-2016s" | egrep "^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
[isx0276204@j01 m01]$ echo "12-12-20165" | egrep "^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
[isx0276204@j01 m01]$ echo "12-12--20165" | egrep "^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
[isx0276204@j01 m01]$ echo "12-312-2016" | egrep "^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
[isx0276204@j01 m01]$ echo "12-12-2016" | egrep "^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
12-12-2016
[isx0276204@j01 m01]$


05) Usant grep valida si una data té el format dd/mm/aaaa.
##
[isx0276204@j01 m01]$ echo "12-12-2016" | egrep "^[0-9]{2}/[0-9]{2}/[0-9]{4}$"
[isx0276204@j01 m01]$ echo "12/12/2016" | egrep "^[0-9]{2}/[0-9]{2}/[0-9]{4}$"
12/12/2016
[isx0276204@j01 m01]$



06) Usant grep validar si una data té un format vàlid. Els formats poden
	ser: dd-mm-aaaa o dd/mm/aa.
##
[isx0276204@j01 m01]$ echo "12/12/2016" | egrep "^[0-9]{2}/[0-9]{2}/[0-9]{4}$|^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
12/12/2016
[isx0276204@j01 m01]$ echo "12/12-2016" | egrep "^[0-9]{2}/[0-9]{2}/[0-9]{4}$|^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
[isx0276204@j01 m01]$ echo "12-12-2016" | egrep "^[0-9]{2}/[0-9]{2}/[0-9]{4}$|^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
12-12-2016
[isx0276204@j01 m01]$



07) Buscar totes les línies del fitxer noms1.txt que tenen la cadena
	Anna o la cadena Jordi
##
[isx0276204@j01 m01]$ cat noms.txt | grep "anna|jordi"
[isx0276204@j01 m01]$ cat noms.txt | egrep "anna|jordi"
jordi puig barcelona 1
anna puig girona 2
anna mas girona 4
anna puig barcelona 5
jordi puigpelat puigros 6
[isx0276204@j01 m01]$


08) Substituir del fitxer noms1.txt tots els noms Anna i Jordi per -nou-.
	$ sed -r 's/Anna|Jordi/-nou-/g' noms1.txt 
##
[isx0276204@j01 m01]$ sed -r 's/jordi|anna/-nou-/g' noms.txt
-nou- puig barcelona 1
-nou- puig girona 2
zoe mas hospitalet 3
-nou- mas girona 4
-nou- puig barcelona 5
-nou- puigpelat puigros 6
marc puig tarragona
pau mas tarragona
ramon mas puigros
pere puig tarragona
maria puig girona


09) Del resultat de fer un head (10 línies) del fitxer /etc/passwd
	substituir '/bin/nologin' per '-noshell'.
	*nota: atenció a com escapeu l'slash '/'
##
[isx0276204@j01 m01]$ head -n 10 passwd | sed -r 's/\/bin\/nologin/-noshell-/g'
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
[isx0276204@j01 m01]$

[isx0276204@j01 m01]$ head -n 10 passwd | sed -r 's/\/sbin\/nologin/-noshell-/g'
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:-noshell-
daemon:x:2:2:daemon:/sbin:-noshell-
adm:x:3:4:adm:/var/adm:-noshell-
lp:x:4:7:lp:/var/spool/lpd:-noshell-
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:-noshell-
operator:x:11:0:operator:/root:-noshell-

10) Ídem que l'exercici anteior però fent la substitució només
	de les línies 4 a la 8.
##
[isx0276204@j01 m01]$ sed -r '4,8 s/jordi|anna/-nou-/g' noms.txt
jordi puig barcelona 1
anna puig girona 2
zoe mas hospitalet 3
-nou- mas girona 4
-nou- puig barcelona 5
-nou- puigpelat puigros 6
marc puig tarragona
pau mas tarragona
ramon mas puigros
pere puig tarragona
maria puig girona


[isx0276204@j01 m01]$ head -n 10 passwd | sed '4,8 s/\/sbin\/nologin/-nou-/g'
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:-nou-
lp:x:4:7:lp:/var/spool/lpd:-nou-
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin


11) Ídem exercici anterior però fent les substitucions des de la línia
	que conté admin fins la línia que conté halt.
 ##
[isx0276204@j01 m01]$ head -n 10 passwd | sed '/adm/,/halt/ s/\/sbin\/nologin/-nou-/g'
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:-nou-
lp:x:4:7:lp:/var/spool/lpd:-nou-
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
[isx0276204@j01 m01]$

