========================================================================
ASIX M01_ISO
Curs 2014-2015
A01-02 Gestió de fitxers i directoris
========================================================================
Exercici2
========================================================================
cp, mv, cat, rm, mkdir, rmdir, whreis, file

part1:
------------------------------------------------------------------------
00) situar-se al directori actiu /var/tmp i realizat TOTES les ordres 
    des d'aquest directori.
01) Crear dins de /var/tmp un directori anomenat m01.
02) crear un directori de nom dades dins de m01.
	(es podia fer tot de cop?).
03) Crear dins de m01 un directori anomenat fonts.
04) Posar dins de fonts els següents fitxers:
	- informe.txt	carta01.txt carta02.txt 	carta3.txt
	- ls (és el fitxer binari de la ordre ls)
	- pwd (és el fitxer binari de la ordre pwd)
	- manls (és el fitxer tar.gz que conté la primera pàgina de manual 
	de l'ordre ls. Atenció, cal assignar-li aquest nom.
	- manpwd (és el fitxer tar.gz que conté la primera pàgina de manual
	de l'ordre pwd. Atenció, cal assignar-li aquest nom)


part2:
------------------------------------------------------------------------
05) dir de quin tipus és cada un dels fitxers de fonts (una sola ordre)
06) dir de quin tipus és cada fitxer de nom carta*
07) idem dels fitxers de nom carta0?.txt
08) idem dels fitxers de nom carta?.txt
09) crear un directori de nom binaris dins de m01
10) crear un directori de nom manuals dins de m01

part3:
------------------------------------------------------------------------
11) copiar els fitxers binaris de les ordres ls i pwd dins de binaris
12) copiar els tar.gz dels manuals de les ordres ls i pwd al directori
    manuals.
13) moure tot el directori fonts i posar-lo dins de dades
14) esborrar els fitxers binaris ls i pwd que hi ha a fonts
15) esborrar el directori fonts i tot el que conté (de cop)
16) canviar de nom el directori manuals per el nom manpages




