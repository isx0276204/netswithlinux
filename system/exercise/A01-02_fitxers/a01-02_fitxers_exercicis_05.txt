ASIX MP1_ISO
Curs 2014-2015
A01-02 Gestió de fitxers i directoris
========================================================================
Exercici_5
========================================================================
part1:
------------------------------------------------------------------------
00) situar-se al directori actiu /var/tmp i realizat TOTES les ordres 
    des d'aquest directori.
01) Crear dins de /var/tmp un directori anomenat m01/files.
02) crear un directori de nom dades dins de m01.
03) Posar dins de files els següents fitxers:
	informe.txt carta01.txt carta02.txt carta3.txt carta4.txt
	carta05.txt	carta06.txt
	.oculta		.ocultba	.ocultbc	.ocultbe
	- manls (és el fitxer tar.gz que conté la primera pàgina de manual 
	de l'ordre ls. Atenció, cal assignar-li aquest nom.
	- manpwd (és el fitxer tar.gz que conté la primera pàgina de manual
	de l'ordre pwd. Atenció, cal assignar-li aquest nom)


part2:
------------------------------------------------------------------------
0x) situar-se al directori dades i fer TOTES les ordres des d'aquest directori.
05) crear al directori dades un ellaç al fitxer informe.txt.
06) crear al directori dades un enllaç al fitxer informe.txt anomenat informe-bis.txt.
07) crear al directori dades un ellaç per a cada un dels fitxers de nom carta (en una sola ordre).
08) crear al directori dades un enllaç el directori fonts.
09) modificar el nom de tots els fitxers que comencin per carta per tal de que comencin per treball.
10) crear al directori dades un enllaç simbòlic al fitxer /etc/fstab.
11) crear al directori dades un enllaç simbòlic al directori /boot.

part3:
------------------------------------------------------------------------
12) comparar el fitxer informe.txt i informe-bis.txt del directori dades.
13) comparar el fitxer carta01.txt i carta02.txt del directori fonts.
14) copiar carta3.txt a dades, modificar una sola paraula en una sola línia i posar una línia en blanc de més. Comparar els dos fitxers.
15) comparar el fitxer ls del directori fonts amb l'executable de l'ordre ls.
16) copiar el fitxer ls al directori dades i dividir-lo en tres torços.
17) ajuntar els tres troços en un nou fitxer anomemenat ajuntat.bin
18) comparar el fitxer ls i el ajuntat.bin.
19) dividir el fitxer ls en troços de 50k. Ajuntar-lo de nou amb el nom trocets.bin
20) fer l'ordre stat del fitxer ls i del fitxer trocets.bin.

part4:
------------------------------------------------------------------------
21) realitza l'ordre updatedb
22) localitza tots els fitxers de nom vmlinuz
23) localitza el fitxer de nom carta03
24) localitza tots els fitxers que comencin per carta
25) comptar les línies del fitxer de passwords.
26) llistar la quota de l'usuari.
27) llistar l'ocupació de disc (du) del directori mp1 calculant-ne el total.
28) llistar l'espai ocupat de disc del directori mp1 mostrant els subtotals de cada subdirectori (només un nivell de profunditat).
29) fer el mateix del directori de l'usuri i de l'arrel del sistema.
39) llistar l'ocupació de disc del directori de l'usuari agrupant fins a dos nivells de profunditat.


Ordres:
------------------------------------------------------------------------
enllaç dur i enllaç simbòlic
comparar: diff, cmp, diff3
fitxers: head, tail, sort, less, more, grep, wc, od, hexdump, zcat
parts: split, cat
disc: quota, df, du
buscar: grep, locate, updatedb, find (nom, propietari, mida, permisos)
rename

