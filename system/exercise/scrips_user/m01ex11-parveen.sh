# !/bin/bash
#nom parveen
#date 2017-04-03
#group hisix1
#descripcion funcions
#-----------------------------------------------------------------
function usersize() {
	#control de error
	if [ $# -ne 1 ]
	then 
		echo "errror: wrong number of argument"
		return 1
	fi
	file_user=/etc/passwd
	user=$1
	linia=$(grep "^$user:" $file_user) 	
	if [ -z $linia ]
	then
		echo "error: login no exist"
		return 2
	fi
	home_dir=$(echo $linia | cut -d':' -f6)
	du -sh $home_dir &> /dev/null
	if [ $? -ne 0 ]
	then 
		echo "error:for order du" >> /dev/stderr
	else
		size=$(du -sh /home/pere/ | tr -s '[:blank:]' ' ' | cut -d' ' -f1)
		if [ $? -ne 0 ]
		then
			size="none"
		fi
		echo "$user $size"
	
	fi
}

function usersexceed() {
	#control de error
	if [ $# -ne 2 ]
	then 
		echo "errror: wrong number of argument"
                return 1
	fi
	file_user=/etc/passwd
	shell=$1
        llista_user=$(grep ":$shell$" $file_user | cut -d':' -f1)
	if [ -z $llista_user ]
	then
		echo "error: shell no exist"
                return 2
	fi
	size_given=$2
	for user in $llista_user
	do
		size_user=$(usersize $user | cut -d' ' -f2)
		info=$(grep "$user:" $file_user | cut -d':' -f1,3)
		echo $info
		if [ $size_user -gt $size_given ]

		then
			info=$(grep "$user:" $file_user | cut -d':' -f1,3)
			echo "$info $size_user"
		fi
	done

}

function listabusers() {
        file_user=/etc/passwd
        llista_shell=$(cat $file_user | sort -t':' -k7,7 | cut -d':' -f7)
        for shell in $llista_shell
	do

		llista_user=$(grep ":$shell$" $file_user | cut -d':' -f1)
		echo $llista_user
        	for user in $llista_user
        	do
                	echo "$shell"
			size_user=$(usersize $user | cut -d' ' -f2)
                	info=$(grep "$user:" $file_user | cut -d':' -f1,3)
                	echo $info
                	if [ $size_user -gt $size_given ]

                	then
                        	info=$(grep "$user:" $file_user | cut -d':' -f1,3)
                        	echo "$info $size_user"
                	fi
        	done
	done
}
