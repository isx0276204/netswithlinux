# ! /bin/bash
# nom paarveen
# hisix isx0276204
# date 03-02-2017
# descripcion :- one prog who help and also grep no. of file
# syspois:- prog --help  / prog --si|--no patro file file[....]
#-----------------------------------------------------------------------------------------
#VARIABLE
OK=0
ERROR_1=1
ERROR_2=2
#control de error
if [ $# -lt 1 ]
then
	echo "error: no. of argument worng"
	echo "usage: prog prog --help  / prog --si|--no patro file file[....]"
	exit $ERROR_1
fi

if [ $1 == "--help" -a $# -eq 1 ]
then
	echo "help:one prog who help and also grep no. of file"
	echo "prog --help  / prog --si|--no patro file file[....]"
	exit $OK
fi
if [ $# -lt 3 ]
then 
		echo "error: no. of argument worng"
		echo "usage: prog prog --help  / prog --si|--no patro file file[....]"
		exit $ERROR_1

fi
if ! [ $1 == "--si" -o $1 == "--no" ]
then
	echo "error: worng argument "
	echo "usage: prog prog --help  / prog --si|--no patro file file[....]"
	exit $ERROR_2
fi
#main prog
opion=""
total=0
okeys=0
errors=0
skipped=0
patro=$2
llista=$(echo $* | cut -d' ' -f3-)
if [ $1 == "--no" ]
then
	opion="-v"
fi
for arg in $llista
do
	if ! [ -f "$arg" ]
	then
		skipped=$((skipped+1))
	else
		grep $opion "$patro" $arg &> /dev/null
		if [ $? -eq 0 ]
		then
			okeys=$((okeys+1))
		else
			errors=$((errors+1))	
		fi
	fi	
total=$((total+1))
done
echo -e "total: $total\nokeys: $okeys\nerrors: $errors\nskipped: $skipped"
exit $OK
