#! /bin/bash
# name parveen
# group hisix1
# date 2017-03-27
# descripcion:- create group for 30 people
#------------------------------------------------------------
OK=0
ERROR_1=1
ERROR_2=2
ERROR_3=3
#controll de error
if [ $# -ne 1 ]
then 
	echo "error: wrong number of argument"
	echo "usage: must need rone argument"
	exit $ERROR_1
fi
groupadd $1 &> /dev/null
if [ $? -ne 0 ]
then
        echo "error: problem with make group or already exist"
        echo "usage: plese try again"
	exit $ERROR_2
fi
mkdir /home/$1 &> /dev/null
if [ $? -ne 0 ]
then
	echo "error: problem whit director or already exist"
        echo "usage: plese try again"
	exit $ERROR_3
fi
gname=$1
dir_home=/home/$gname
llista_user=$(echo "$gname"{1..5})
for user in $llista_user
do
	useradd -g $gname -b $dir_home $user &> /dev/null
	echo -e "$user\n$user" | passwd $user &> /dev/null
done
exit $OK


