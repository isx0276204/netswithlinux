ASIX MP1_ISO
Curs 2014-2015
A01-02 Gestió de fitxers i directoris
========================================================================
Exercici_3
========================================================================
ls, ls -la, ll, ls -d
<filename> * ? [...] [^...] [!...] 

part1:
------------------------------------------------------------------------
00) situar-se al directori actiu /var/tmp i realizat TOTES les ordres 
    des d'aquest directori.
01) Crear dins de /var/tmp un directori anomenat m01/files.
02) Crear un directori de nom dades dins de m01.
03) Posar dins de files els següents fitxers:
	informe.txt carta01.txt carta02.txt carta3.txt carta4.txt
	carta05.txt	carta06.txt
	.oculta		.ocultba	.ocultbc	.ocultbe
	- manls (és el fitxer tar.gz que conté la primera pàgina de manual 
	de l'ordre ls. Atenció, cal assignar-li aquest nom.
	- manpwd (és el fitxer tar.gz que conté la primera pàgina de manual
	de l'ordre pwd. Atenció, cal assignar-li aquest nom)

part2:
------------------------------------------------------------------------
05) llistar totes les entrades de directori (totes) del home de 
    l'usuari.
06) llistar totes les entrades de directori (totes) de l'arrel del 
    sistema.
07) llistar amb ll el directori files. Fer-ho també amb ls -la.
09) llistar únicament els fitxers ocults del directori files.
10) llistar únicament els directoris del directori files.
11) quants enllaços durs té l'element . del directori files?
12) quants enllaços durs té el directori .. del directori ..?
13) són els mateixos que del directori m01?

part3:
------------------------------------------------------------------------
llistar del directori files tots els fitxers que:
14) són d'extensió txt
15) que comencen per lalletra c
16) que contene el caràcter 0 en el nom.
17) que contenen 'carta0' en el nom
18) de com 'carta0' més un digit del 1 al 5
19) de nom 'carta0' i un caràcter que no sigui el 5 ni el 6
20) de nom '.ocultb' i un caràcter de la a a la d
21) de nom '.ocult' i una lletra més
22) de nom 'carta' i una sola lletra més i l'extensió txt



