#! /bin/bash
# nom parveen
# curs hisix
# gener 2017
# descripcion:- exampla order case
# synopsis:- $ prog.sh [arg...]
#-------------------------------------------------------------
llista="pere marta anna jordi pere anna ramon mireia ailass"

count_nen=0
count_nena=0
other=0
for nom in $llista
do
	case $nom in
	"pere"|"jordi"|"pau")
		echo "$nom es un nen"
		count_nen=$((count_nen+1));;
	"marta"|"anna"|"julia")
		echo "$nom es una nena"
		count_nena=$((count_nena+1));;
	*)
		echo "$nom unknown"
		other=$((other+1));;
	esac
done
echo "nen:$count_nen nena:$count_nena other:$other"
exit 0
