#! /bin/bash
# nom parveen
# curs hisix
# feb 2017
# descripcion:- copia todos files in directori
# synopsis:- $ prog.sh arg [arg]... desti
#################################################################
ERROR_1=1
ERROR_2=2
OK=0
# validar no de arg
if [ $# -lt 2 ]
then
	echo "error:- wrong no. of argument"
	echo "usage:- prog.sh file [file]... dir"
	exit $ERROR_1
fi
dir=$(echo $* | sed 's/^.* //')
llista_file=$(echo $* | sed 's/ [^ ]*$//')
if ! [ -d $dir ]
then
	echo "error:- last argument not a dir"
        echo "usage:- prog.sh file [file]... dir"
        exit $ERROR_2
fi
for file in $llista_file
do 
	if [ -f $file ]
	then 
		cp $file $dir
	else
		echo "error:- $file not exists"
	fi
done
exit $OK
