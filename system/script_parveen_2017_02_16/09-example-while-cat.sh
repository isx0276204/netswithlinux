#! /bin/bash
# nom parveen
# curs hisix
# gener 2017
# descripcion:- exampla bucle while
# synopsis:- $ prog.sh [arg...]
#-------------------------------------------------------------

# pragrama passa a majuscules
while read -r line
do
	echo $line | tr '[:lower:]' '[:upper:]'
done
exit 0

# numpera per stdout l'entrda stdin
count=1
while read -r line
do
	echo "$count:$line"
	count=$((count+1))
done
exit 0
