#! /bin/bash
# nom parveen
# curs hisix
# feb 2017
# descripcion:- buscar cuanto dias tienes mes con case
# synopsis:- $ prog.sh arg
##################################################################
# variable must be need
OK=0
ERROR_1=1
ERROR_2=2
# control de error
# no. de argument
if [ $# -ne 1 ]
then
	echo "error:wrong number of arguments"
	echo "usage:prog arg"
	exit $ERROR_1
fi
# mirar si es help or not
if [ $1 == "-h" ]
then
	echo "help to find number in day en mes"
        echo "usage:prog arg"
        exit $OK
fi
# valor de argument es correta or no
if [ $1 -lt 1 -o $1 -gt 12 ]
then
	echo "error:wrong value of argument"
        echo "usage:prog [1-12]"
        exit $ERROR_2
fi
#################################################################
# main program
mes=$1
case $mes in
"2")
	echo "mes $mes -->> 28 dia";;
"4"|"6"|"9"|"11")
	echo "mes $mes -->> 30 dia";;
*)
	echo "mes $mes -->> 31 dia";;
esac
exit $OK
